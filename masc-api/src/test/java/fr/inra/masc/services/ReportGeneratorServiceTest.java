/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import com.google.common.collect.Lists;
import fr.inra.masc.charts.ChartModel;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.reports.ReportModel;
import fr.inra.masc.synoptic.CriteriaGraphBuilder;
import fr.inra.masc.synoptic.SynopticModel;
import fr.inra.masc.utils.MascUtil;
import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.JFreeChart;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.svg.SVGDocument;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class ReportGeneratorServiceTest extends AbstractDexiFilesTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ReportGeneratorServiceTest.class);

    protected ReportGeneratorService reportGeneratorService;

    protected ImageGeneratorService imageGeneratorService;

    protected MascModelService mascModelService;

    public ReportGeneratorServiceTest(DEXI_TEST_FILE testFile) {
        super(testFile);
    }

    @Before
    public void setUp() throws Exception {
        reportGeneratorService = getService(ReportGeneratorService.class);
        imageGeneratorService = getService(ImageGeneratorService.class);
        mascModelService = getService(MascModelService.class);
    }

    @Test
    public void testGenerateReport() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testGenerateReport] Test with file : " + dexiTestFile.getName());
        }

        File reportFile = File.createTempFile("report", ".pdf");

        reportGeneratorService.generateReport(testFile.getFileName(), getReportModel(dexiTestFile, reportFile));
//        Desktop.getDesktop().open(reportFile);
    }

    @Test
    @Ignore
    public void testGenerateReportAsHtml() throws Exception {
        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testGenerateReportAsHtml] Test with file : " + dexiTestFile.getName());
        }

        File reportFile = File.createTempFile("report", ".html");
        String html = reportGeneratorService.generateReportHTML(testFile.getFileName(), getReportModel(dexiTestFile, reportFile));
        FileUtils.writeStringToFile(reportFile, html, "utf8");

        Desktop.getDesktop().open(reportFile);
    }

    protected ReportModel getReportModel(File dexiTestFile, File reportFile) throws Exception {
        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        ReportModel reportModel = new ReportModel();
        reportModel.setReportFile(reportFile);
        reportModel.setMascModel(mascModel);

        List<String> criteriaSelected = Lists.newArrayList();
        criteriaSelected.addAll(MascUtil.extractCriteriaInMapUUIDKey(mascModel.getCriteria(0)).keySet());

        SynopticModel model = new SynopticModel();
        model.setYGap(10);
        model.setXGap(10);
        model.setEdgeWidth(10);
        model.setBoxXSize(10);
        model.setBoxYSize(10);
        model.setWeightFontSize(10);
        model.setCriteriaNameFontSize(10);
        model.setDuplicateLinkedCriterias(false);
        model.setResolveReference(false);
        model.setCriteriaList(criteriaSelected);
        if (!mascModel.isOptionEmpty()) {

            model.setOption(mascModel.getOption().get(0));
        }

        CriteriaGraphBuilder synopticBuilder = new CriteriaGraphBuilder(mascModel.getCriteria(0), model);

        SVGDocument treeImage = imageGeneratorService.convertModelToTreeImage(synopticBuilder, model);

        reportModel.setSynoptic(treeImage);

        ChartModel chartModel = new ChartModel();
        chartModel.setSelectedCriterias(mascModel.getCriteria());
        chartModel.setSelectedOptions(mascModel.getOption());

        JFreeChart chart = imageGeneratorService.getChart(chartModel);
        BufferedImage chartImage = imageGeneratorService.getChartImage(chart);

        reportModel.setGraphic(chartImage);

        return reportModel;
    }
}
