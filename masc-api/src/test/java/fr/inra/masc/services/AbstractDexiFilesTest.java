package fr.inra.masc.services;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.custommonkey.xmlunit.Diff;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Tests over dexi files.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.8
 */
@RunWith(Parameterized.class)
public abstract class AbstractDexiFilesTest extends AbstractServiceTest {

    public enum DEXI_TEST_FILE {

        CAR("Car.dxi"),
        DEXI_MODEL_2_0("dexi-model2.0.dxi"),
        DEXI_MODEL_4_0("dexi-model4.0.dxi"),
        DEXI_2_0("dexi2.0.dxi"),
        EMPLOY("Employ.dxi"),
        ENTREPRISE("Enterprise.dxi"),
        MASC2_0("MASC2.0.dxi"),
        NURSERY("Nursery.dxi"),
        DEXIPM_INTERFACE_UNIV("DEXiPM_interface_univ.dxi"),
//        MASC_RMT_SDCI("MASC RMT SdCi.dxi"),
//        ARBRE_SATELLITES("arbres satellites.dxi"),
//        MAITRISE_DE_LETAT_STRUCTURAL_DU_SOL("Maitrise de l etat structural du sol CR.dxi"),
//        CAPTAGE_PERCEPTION_AGRICOL_2("captage perception agriculteur2.dxi"),
        DR2_22C_DEXiPM_8_11_2010("DR2.22c_DEXiPM_8-11-2010.dxi"),
//        ARBRE_MASC_ARBRES_SAT("arbre_MASC_arbres_satellites_2.1_15_mai_V4.dxi"),
        SHUTTLE("Shuttle.dxi");

        private String fileName;

        DEXI_TEST_FILE(String fileName) {
            this.fileName = fileName;
        }

        public String getFileName() {
            return fileName;
        }
    }

    @Parameterized.Parameters
    public static List<Object[]> data() {
        List<Object[]> result = Lists.newLinkedList();
        for (DEXI_TEST_FILE testFile : DEXI_TEST_FILE.values()) {
            result.add(new Object[]{testFile});
        }
        return result;
    }

    protected final DEXI_TEST_FILE testFile;

    protected AbstractDexiFilesTest(DEXI_TEST_FILE testFile) {
        this.testFile = testFile;
    }

    protected File getTestFile(DEXI_TEST_FILE testFile) {
        return new File(testResourcesDir, testFile.getFileName());
    }

    protected void assertFilesEquals(File in, File out) throws IOException, SAXException {
        String input = FileUtils.readFileToString(in);
        String output = FileUtils.readFileToString(out);

        // Hack to don't take care of end of line char
        input = input.replaceAll("\r\n", "\n");

        assertEquals(input.trim(), output.trim());

        Diff myDiff = new Diff(input, output);
        assertTrue("XML similar " + myDiff.toString(),
                   myDiff.similar());
        assertTrue("XML identical " + myDiff.toString(),
                   myDiff.identical());
    }

    protected void assertFilesExist(File... files) {
        if (files != null) {
            for (File file : files) {
                boolean exists = file.exists();
                assertTrue("File '" + file.getPath() + "'  don't exist", exists);
            }
        }
    }


    protected void assertContentEquals(File expected, File actual) throws IOException {
        String expectedContent = FileUtils.readFileToString(expected);
        String actualContent = FileUtils.readFileToString(actual);
        assertEquals(expectedContent, actualContent);
    }

}
