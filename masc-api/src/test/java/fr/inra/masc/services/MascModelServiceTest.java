/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import fr.inra.masc.utils.MascUtil;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Option;
import fr.reseaumexico.model.ExperimentDesign;
import fr.reseaumexico.model.InputDesign;
import fr.reseaumexico.model.Scenario;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Collection;
import java.util.List;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class MascModelServiceTest extends AbstractDexiFilesTest {

    /** Logger. */
    private static Log log = LogFactory.getLog(MascModelServiceTest.class);

    protected MascModelService mascModelService;

    public MascModelServiceTest(DEXI_TEST_FILE testFile) {
        super(testFile);
    }

    @Before
    public void setUp() throws Exception {
        mascModelService = getService(MascModelService.class);
    }

    @Test
    public void testLoadModelWithDexiTestModelFile() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testLoadModelWithDexiTestModelFile] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        assertNotNull(mascModel);
    }

    @Test
    public void testLoadModelWithDexiTestFile() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testLoadModelWithDexiTestFile] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        assertNotNull(mascModel);
    }

    @Test
    public void testSaveModelWithDexiTestModelFile() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testSaveModelWithDexiTestModelFile] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);

        File savedFile = new File(testWorkDir, "dexiTestModelFileResult.dxi");
        mascModelService.exportToDEXiModel(mascModel, savedFile, false);

        // compare input and output files
        assertFilesEquals(dexiTestFile, savedFile);
    }

    @Test
    public void testSaveWithDexiTestFile() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testSaveWithDexiTestFile] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);

        File savedFile = new File(testWorkDir, "dexiTestFileResult.dxi");
        mascModelService.exportToDEXiModel(mascModel, savedFile, false);

        // compare input and output files
        assertFilesEquals(dexiTestFile, savedFile);
    }


    @Test
    public void testConvertMascModelToMexicoExperimentDesignModel() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testConvertMascModelToMexicoExperimentDesignModel] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        ExperimentDesign experimentDesign = mascModelService.convertMascModelToMexicoExperimentDesignModel(mascModel);

        // count masc model criteria
        List<EditableCriteria> editableCriterias = MascUtil.extractAllEditableCriterias(mascModel);
        assertEquals(editableCriterias.size(), experimentDesign.getFactor().size());
    }

    @Test
    public void testConvertMascModelToMexicoInputDesignModel() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testConvertMascModelToMexicoInputDesignModel] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        ExperimentDesign experimentDesign = mascModelService.convertMascModelToMexicoExperimentDesignModel(mascModel);
        InputDesign inputDesign = mascModelService.convertMascModelToMexicoInputDesignModel(mascModel, experimentDesign);

        Collection<Option> options = mascModel.getOption();
        Collection<Scenario> scenario = inputDesign.getScenario();
        int size = scenario == null ? 0 : scenario.size();
        assertEquals(options.size(), size);
    }

    @Test
    public void testLoadExperimentDesignModel() throws Exception {

        File dexiTestFile = getTestFile(testFile);

        if (log.isDebugEnabled()) {
            log.debug("[testLoadExperimentDesignModel] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        ExperimentDesign experimentDesign = mascModelService.convertMascModelToMexicoExperimentDesignModel(mascModel);

        File savedFile = new File(testWorkDir, "experimentDesignTestFileResult.xml");
        mascModelService.saveExperimentDesign(experimentDesign, savedFile);

        try {
            ExperimentDesign loadedExperimentDesign = mascModelService.loadExperimentDesignModel(savedFile);
            File loadedSavedFile = new File(testWorkDir, "loadedExperimentDesignTestFileResult.xml");
            mascModelService.saveExperimentDesign(loadedExperimentDesign, loadedSavedFile);

            assertContentEquals(savedFile, loadedSavedFile);
        } catch (Exception eee) {
            log.info("Failed to parse file :");
            log.info(FileUtils.readFileToString(savedFile));
            throw eee;
        }
    }

    @Test
    public void testLoadInputDesignDesignModel() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testLoadInputDesignDesignModel] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        ExperimentDesign experimentDesign = mascModelService.convertMascModelToMexicoExperimentDesignModel(mascModel);
        InputDesign inputDesign = mascModelService.convertMascModelToMexicoInputDesignModel(mascModel, experimentDesign);

        File savedFile = new File(testWorkDir, "inputDesignTestFileResult.xml");
        mascModelService.saveInputDesign(inputDesign, savedFile);

        InputDesign loadedInputDesign = mascModelService.loadInputDesignDesignModel(savedFile);

        File loadedInputDesignFile = new File(testWorkDir, "loadedInputDesignTestFileResult.xml");
        mascModelService.saveInputDesign(loadedInputDesign, loadedInputDesignFile);

        assertContentEquals(savedFile, loadedInputDesignFile);

        Collection<Scenario> scenario = inputDesign.getScenario();
        Collection<Scenario> scenario2 = loadedInputDesign.getScenario();

        int size = scenario == null ? 0 : scenario.size();
        int size2 = scenario2 == null ? 0 : scenario2.size();
        assertEquals(size, size2);
    }

    @Test
    public void testSaveExperimentDesign() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testConvertMascModelToMexicoInputDesignModel] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        ExperimentDesign experimentDesign = mascModelService.convertMascModelToMexicoExperimentDesignModel(mascModel);

        File savedFile = new File(testWorkDir, "experimentDesignTestFileResult.xml");
        mascModelService.saveExperimentDesign(experimentDesign, savedFile);
    }

    @Test
    public void testSaveInputDesign() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testConvertMascModelToMexicoInputDesignModel] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        ExperimentDesign experimentDesign = mascModelService.convertMascModelToMexicoExperimentDesignModel(mascModel);
        InputDesign inputDesign = mascModelService.convertMascModelToMexicoInputDesignModel(mascModel, experimentDesign);

        File savedFile = new File(testWorkDir, "inputDesignTestFileResult.xml");
        mascModelService.saveInputDesign(inputDesign, savedFile);
    }
}
