/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import fr.inra.masc.model.MascModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class DexiEvalInvokerServiceTest extends AbstractDexiFilesTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(DexiEvalInvokerServiceTest.class);

    protected MascModelService mascModelService;

    protected DexiEvalInvokerService dexiEvalInvokerService;

    public DexiEvalInvokerServiceTest(DEXI_TEST_FILE testFile) {
        super(testFile);
    }

    @BeforeClass
    public static void beforeClass() throws IOException {
        AbstractServiceTest.beforeClass();
        assumeDesktopOpenIsSupported();
        assumeDexiEvalExists();
    }

    @Before
    public void setUp() throws Exception {
        mascModelService = getService(MascModelService.class);
        dexiEvalInvokerService = getService(DexiEvalInvokerService.class);
    }

    @Test
    public void testInvokeDexiEval() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testInvokeDexiEval] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);

        dexiEvalInvokerService.evalMascModel(mascModel, dexiTestFile);
    }
}
