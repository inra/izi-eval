/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import fr.inra.masc.MascUIModel;
import fr.inra.masc.model.MascModel;
import java.io.File;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.3
 */
public class MascUIModelServiceTest extends AbstractDexiFilesTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(MascUIModelServiceTest.class);

    protected MascModelService mascModelService;

    protected MascUIModelService mascUIModelService;

    public MascUIModelServiceTest(DEXI_TEST_FILE testFile) {
        super(testFile);
    }

    @Before
    public void setUp() throws Exception {
        mascModelService = getService(MascModelService.class);
        mascUIModelService = getService(MascUIModelService.class);
    }

    @Test
    public void serializeDeserializeTest() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[serializeDeserializeTest] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        MascUIModel mascUIModel = new MascUIModel();
        mascUIModel.setMascModel(mascModel);

        File savedFile = new File(testWorkDir, "mascModel.masc");
        mascUIModelService.saveModel(mascUIModel, savedFile);

        MascUIModel result = mascUIModelService.loadModel(savedFile);

        File dexiSaveFile = new File(testWorkDir, "decoderMascModel.dxi");
        mascModelService.exportToDEXiModel(result.getMascModel(), dexiSaveFile, false);

        // compare input and output files
        assertFilesEquals(dexiTestFile, dexiSaveFile);
    }
}
