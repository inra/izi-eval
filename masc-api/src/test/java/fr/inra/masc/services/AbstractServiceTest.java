/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import fr.inra.masc.MascConfig;
import fr.inra.masc.utils.MascUtil;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.nuiton.util.FileUtil;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public abstract class AbstractServiceTest extends Assert {

    protected static final long BUILD_TIMESTAMP = System.nanoTime();

    protected static File globalTestWorkDir;

    protected static File testResourcesDir;

    private static MascConfig globalConfig;

    protected File testWorkDir;

    protected MascConfig config;

    protected MascServiceFactory serviceFactory;

    protected MascServiceContext context;

    @Rule
    public final TestName testName = new TestName();

    @BeforeClass
    public static void beforeClass() throws IOException {

        // get maven env basedir
        String basedir = System.getenv("basedir");
        if (basedir == null) {

            // says basedir is where we start tests.
            basedir = new File("").getAbsolutePath();
        }
        File basedirFile = new File(basedir);

        // says test global directory is tmp directory (maven will change this for us)
        globalTestWorkDir = new File(System.getProperty("java.io.tmpdir"));

        // mkdirs on it
        FileUtils.forceMkdir(globalTestWorkDir);

        // where to find resources (this is bad:(, should look out in
        // class-path instead of this hardcoded stuff...)
        testResourcesDir = FileUtil.getFileFromFQN(basedirFile,
                                                   "src.test.resources.dxi");

        globalConfig = new MascConfig("masc-test.properties", false);
    }

    @Before
    public void before() throws IOException {
        testWorkDir = FileUtil.getFileFromPaths(
                globalTestWorkDir,
                getClass().getName(), testName.getMethodName() + "." + BUILD_TIMESTAMP);

        FileUtils.forceMkdir(testWorkDir);

        synchronized (this) {

            System.setProperty("test.directory", testWorkDir.getAbsolutePath());
            config = new MascConfig("masc-test.properties", true);
        }

        serviceFactory = new MascServiceFactory();

        context = new MascServiceContext(config, serviceFactory);
    }

    protected MascConfig getConfig() {
        return config;
    }

    protected <E extends MascService> E getService(Class<E> clazz) throws Exception {
        return serviceFactory.newService(clazz, context);
    }

    protected static void assumeDesktopOpenIsSupported() {
        Assume.assumeTrue(
                Desktop.isDesktopSupported() &&
                Desktop.getDesktop().isSupported(Desktop.Action.OPEN));
    }

    protected static void assumeDexiEvalExists() {
        File file = globalConfig.getDexiEvalExecutableFile();
        Assume.assumeTrue(file != null && file.exists());
    }

    protected static void assumeDexiExists() {
        File file = globalConfig.getDexiExecutableFile();
        Assume.assumeTrue(file != null && file.exists());
    }
    protected static void assumeRExists() {

        File rExecutableFile = globalConfig.getRExecutableFile();
        boolean rExists = MascUtil.isExecutableFile(rExecutableFile);
        Assume.assumeTrue(rExists);
    }
}
