/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class DexiInvokerServiceTest extends AbstractDexiFilesTest {

    protected DexiInvokerService dexiInvokerService;

    public DexiInvokerServiceTest(DEXI_TEST_FILE testFile) {
        super(testFile);
    }

    @BeforeClass
    public static void beforeClass() throws IOException {
        AbstractServiceTest.beforeClass();
        assumeDesktopOpenIsSupported();
        assumeDexiExists();
    }

    @Before
    public void setUp() throws Exception {

        dexiInvokerService = getService(DexiInvokerService.class);
    }

    @Test
    @Ignore
    public void testInvokeDexi() throws Exception {

        File dexiFile = getTestFile(testFile);

        dexiInvokerService.invokeDexi(dexiFile);
    }
}
