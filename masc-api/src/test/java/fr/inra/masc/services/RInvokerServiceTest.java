/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import fr.inra.masc.ExecutorException;
import fr.inra.masc.R_SCRIPT;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.RScriptModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.5
 */
public class RInvokerServiceTest extends AbstractDexiFilesTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(RInvokerServiceTest.class);

    // TODO sletellier : dexi2.0.dxi, dexi-model2.0.dxi and Car.dxi doesnt works
    // (https://mulcyber.toulouse.inra.fr/tracker/index.php?func=detail&aid=1625&group_id=160&atid=799)
    private static List<DEXI_TEST_FILE> toTest = Arrays.asList(
//            DEXI_TEST_FILE.EMPLOY,
//            DEXI_TEST_FILE.ENTREPRISE,
//            DEXI_TEST_FILE.MASC2_0,
//            DEXI_TEST_FILE.NURSERY
                                                       DEXI_TEST_FILE.SHUTTLE,
                                                       DEXI_TEST_FILE.DR2_22C_DEXiPM_8_11_2010,
                                                       DEXI_TEST_FILE.DEXIPM_INTERFACE_UNIV
    );

    protected MascModelService mascModelService;

    protected RInvokerService rInvokerService;

    public RInvokerServiceTest(DEXI_TEST_FILE testFile) {
        super(testFile);
    }

    @BeforeClass
    public static void beforeClass() throws IOException {

        AbstractDexiFilesTest.beforeClass();

        assumeDesktopOpenIsSupported();
        assumeRExists();
    }

    @Before
    public void setUp() throws Exception {

        Assume.assumeTrue(toTest.contains(testFile));

        mascModelService = getService(MascModelService.class);
        rInvokerService = getService(RInvokerService.class);
    }

    @Test
    public void testExecuteRScript() throws Exception {

        File dexiTestFile = getTestFile(testFile);
        if (log.isDebugEnabled()) {
            log.debug("[testExecuteRScript] Test with file : " + dexiTestFile.getName());
        }

        MascModel mascModel = mascModelService.loadModelFromDexiFile(dexiTestFile);
        File toExport = new File(getConfig().getTmpDirectory(), testFile.getFileName().replaceAll(".dxi", "") + System.nanoTime() + ".dxi");
        mascModelService.exportToDEXiModel(mascModel, toExport, true);

        File workDir = getConfig().getTmpDirectory();
        for (R_SCRIPT rScript : R_SCRIPT.values()) {

            RScriptModel model = new RScriptModel();
            model.setScript(rScript);
            model.setOption(mascModel.getOption(0));
            try {
                rInvokerService.executeRScript(toExport, model);
            } catch (ExecutorException eee) {
                // log R log
                log.error(model.getLogs(workDir));
                log.error(eee.getLogs());
                fail();
            }

            assertFilesExist(model.getResultTextFiles(workDir, toExport.getName().replaceAll(".dxi", ""), mascModel));
            assertFilesExist(model.getResultImageFiles(workDir, toExport.getName().replaceAll(".dxi", ""), mascModel));
        }
    }
}
