package fr.inra.masc.synoptic;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.CriteriaImpl;
import fr.inra.masc.utils.MascUtil;
import java.io.File;
import java.util.List;
import java.util.Map;
import javax.swing.JFrame;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.FileUtil;

/**
 * To test the {@link CriteriaGraphModel}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.8
 */
public class CriteriaGraphModelTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CriteriaGraphModelTest.class);

    @Test
    public void testGraphBuilder() throws Exception {

        TestCriteria a = new TestCriteria("a");
        TestCriteria b = a.add("b");
        TestCriteria c = a.add("c");
        TestCriteria d = b.add("d");
        TestCriteria e = b.add("e");
        TestCriteria f = e.add("f");
        TestCriteria g = e.add("g");
        TestCriteria h = e.add("h");
        TestCriteria i = f.add("i");
        TestCriteria j = f.add("j");
        TestCriteria k = h.add("k");
        TestCriteria l = h.add("l");
        TestCriteria m = i.add("m");
        TestCriteria n = l.add("n");

        Map<Criteria, Integer> expectedCriteriaHeight = Maps.newHashMap();
        expectedCriteriaHeight.put(a, 11);
        expectedCriteriaHeight.put(b, 9);
        expectedCriteriaHeight.put(c, 1);
        expectedCriteriaHeight.put(d, 1);
        expectedCriteriaHeight.put(e, 7);
        expectedCriteriaHeight.put(f, 3);
        expectedCriteriaHeight.put(g, 1);
        expectedCriteriaHeight.put(h, 3);
        expectedCriteriaHeight.put(i, 1);
        expectedCriteriaHeight.put(j, 1);
        expectedCriteriaHeight.put(k, 1);
        expectedCriteriaHeight.put(l, 1);
        expectedCriteriaHeight.put(m, 1);
        expectedCriteriaHeight.put(n, 1);

        // get all criterias
        List<String> criteriaSelected = Lists.newArrayList();
        criteriaSelected.addAll(MascUtil.extractCriteriaInMapUUIDKey(a).keySet());

        SynopticMatrixResolver matrix = new SynopticMatrixResolver(criteriaSelected, false);

        // compute matrix
        Criteria[][] criterias = matrix.buildMatrix(a, 1);

        Map<Criteria, Integer> criteriaHeight = matrix.criteriaHeight;

        for (Map.Entry<Criteria, Integer> entry : criteriaHeight.entrySet()) {
            Criteria criteria = entry.getKey();
            Assert.assertEquals("Criteria \"" + criteria.getName() + "\"", expectedCriteriaHeight.get(criteria), entry.getValue());
        }
        Assert.assertEquals(expectedCriteriaHeight, criteriaHeight);

        // init expected matrix
        Criteria[][] expectedCriteriaMatrix = new Criteria[11][];
        for (int x=0;x<expectedCriteriaMatrix.length;x++) {
            expectedCriteriaMatrix[x] = new Criteria[6];
        }

        if (log.isDebugEnabled()) {
            for (int x=0;x<criterias.length;x++) {
                Criteria[] criteria = criterias[x];
                for (int y=0;y<criteria.length;y++) {
                    Criteria value = criterias[x][y];
                    System.out.print(value == null ? " |" : value + "|");
                }
                System.out.println();
            }
        }

// Expected

//    0 1 2 3 4 5
// 0   | |d| | | |
// 1   | | | | | |
// 2   | | | |i|m|
// 3   | | |f| | |
// 4   |b| | |j| |
// 5  a| |e|g| | |
// 6   | | | |k| |
// 7   | | |h| | |
// 8   | | | |l|n|
// 9   | | | | | |
// 10  |c| | | | |

        // fill
        expectedCriteriaMatrix[0][2] = d;
        expectedCriteriaMatrix[2][4] = i;
        expectedCriteriaMatrix[2][5] = m;
        expectedCriteriaMatrix[3][3] = f;
        expectedCriteriaMatrix[4][1] = b;
        expectedCriteriaMatrix[4][4] = j;
        expectedCriteriaMatrix[5][0] = a;
        expectedCriteriaMatrix[5][2] = e;
        expectedCriteriaMatrix[5][3] = g;
        expectedCriteriaMatrix[6][4] = k;
        expectedCriteriaMatrix[7][3] = h;
        expectedCriteriaMatrix[8][4] = l;
        expectedCriteriaMatrix[8][5] = n;
        expectedCriteriaMatrix[10][1] = c;

        for (int x=0;x<criterias.length;x++) {
            Criteria[] criteria = criterias[x];
            Assert.assertArrayEquals(expectedCriteriaMatrix[x], criteria);
        }
    }

    @Test
    public void testGraphBuilderWithRef() throws Exception {

        TestCriteria a = new TestCriteria("a");
        TestCriteria b = a.add("b");
        TestCriteria c = a.add("c");
        TestCriteria d = a.add("d");
        TestCriteria e = d.add("e");
        TestCriteria f = d.add("f");

        List<Criteria> bChildren = Lists.newArrayList((Criteria)d);
        b.setChild(bChildren);

        List<Criteria> cChildren = Lists.newArrayList((Criteria)d);
        c.setChild(cChildren);

        DuplicatedLinkedCloneVisitor cloneVisitor = new DuplicatedLinkedCloneVisitor(a, true);

        // clone all duplicate linked criterias
        a.accept(cloneVisitor, null);

        Criteria cloned = cloneVisitor.getRoot();

        // get all criterias
        List<String> criteriaSelected = Lists.newArrayList();
        criteriaSelected.addAll(MascUtil.extractCriteriaInMapUUIDKey(cloned).keySet());

        SynopticMatrixResolver matrix = new SynopticMatrixResolver(criteriaSelected, false);

        // compute matrix
        Criteria[][] criterias = matrix.buildMatrix(cloned, 1);

        if (log.isDebugEnabled()) {
            for (int x=0;x<criterias.length;x++) {
                Criteria[] criteria = criterias[x];
                for (int y=0;y<criteria.length;y++) {
                    Criteria value = criterias[x][y];
                    System.out.print(value == null ? " |" : value + "|");
                }
                System.out.println();
            }
        }

        // init expected matrix
        Criteria[][] expectedCriteriaMatrix = new Criteria[11][];
        for (int x=0;x<expectedCriteriaMatrix.length;x++) {
            expectedCriteriaMatrix[x] = new Criteria[6];
        }

// Expected

//    0 1 2 3
// 0  | | |e|
// 1  |b|d| |
// 2  | | |f|
// 3  | | |e|
// 4 a|c|d| |
// 5  | | |f|
// 6  | |e| |
// 7  |d| | |
// 8  | |f| |
    }

    public static void main(String... args) {

        // get maven env basedir
        String basedir = System.getenv("basedir");
        if (basedir == null) {

            // says basedir is where we start tests.
            basedir = new File("").getAbsolutePath();
        }
        File basedirFile = new File(basedir + "/masc-api/");

        File dxiFile = new File(FileUtil.getFileFromFQN(basedirFile,
                "src.test.resources.dxi"), "DR2.22c_DEXiPM_8-11-2010.dxi");

        TestCriteria a = new TestCriteria("a");
        TestCriteria b = a.add("b");
        TestCriteria c = a.add("c");
        TestCriteria d = a.add("d");
        TestCriteria e = d.add("e");
        TestCriteria f = d.add("f");

        List<Criteria> bChildren = Lists.newArrayList((Criteria)d);
        b.setChild(bChildren);

        List<Criteria> cChildren = Lists.newArrayList((Criteria)d);
        c.setChild(cChildren);

//        MascModel mascModel = new MascModelService().loadModelFromDexiFile(dxiFile);

        // get all criterias
        List<String> criteriaSelected = Lists.newArrayList();
        criteriaSelected.addAll(MascUtil.extractCriteriaInMapUUIDKey(a).keySet());

        SynopticModel model = new SynopticModel();
        model.setCriteriaList(criteriaSelected);
        model.setMirror(true);
        model.setResolveReference(true);
        model.setDuplicateLinkedCriterias(true);
        model.setYGap(80);
        model.setEdgeWidth(5);
//        model.setOption(mascModel.getOption(0));

        CriteriaGraphBuilder criteriaGraphBuilder = new CriteriaGraphBuilder(a, model);

        mxGraph graph = criteriaGraphBuilder.build(model);
        mxGraphComponent container = new mxGraphComponent(graph);
        JFrame frame = new JFrame();
        frame.add(container);
        frame.setSize(frame.getToolkit().getScreenSize());
        frame.setVisible(true);
    }

    protected static class TestCriteria extends CriteriaImpl {

        public TestCriteria() {
            super();
        }

        public TestCriteria(String name) {
            this();
            setName(name);
        }

        public TestCriteria add(String childName) {
            TestCriteria child = new TestCriteria(childName);
            if (getChild() == null) {
                List<Criteria> children = Lists.newArrayList();
                setChild(children);
            }
            addChild(child);
            return child;
        }

        @Override
        public String toString() {
            return getName();
        }
    }
}
