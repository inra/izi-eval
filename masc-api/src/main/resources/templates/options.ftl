<!--
  #%L
  Masc :: API
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 Inra, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<div <#if !landscape>class="landscapePage"</#if> style="page-break-inside: avoid;">
    <h2 class="pageBreak">${I18n._("masc.report.options")}</h2>

    <b>${I18n._("masc.report.criteria")}</b>
    <#list mascModel.option as option>
        <#if option_index &lt; 5>
            <span class="firstSpan" style="left:${350 + (option_index * 140)}px;"><b>${StringUtils.abbreviate(option.name, 20)}</b></span>
        </#if>
    </#list>
    <hr/>
    <#list mascModel.criteria as criteria>
        <#if criteria_index == 1>
            <div class="pageBreak">
        </#if>
        <@displayCriteria criteria=criteria/>
        <#if criteria.child??>
            <ul class="tree">
                <#list criteria.child as child>
                    <@displayAllCriteria criteria=child last=!child_has_next/>
                </#list>
            </ul>
        </#if>
        <hr/>
        <#if !criteria_has_next && criteria_index &gt; 0>
            </div>
        </#if>

    </#list>
</div>

<#macro displayAllCriteria criteria last>
    <#if last>
        <li class="last">
            <#else>
                <li>
    </#if>
    <@displayCriteria criteria=criteria/>
    <#if criteria.child??>
        <#if !criteria.reference>

            <#list criteria.child as child>
                <ul>
                    <@displayAllCriteria criteria=child last=!child_has_next/>
                </ul>
            </#list>
        </#if>
    </#if>
    </li>
</#macro>

<#macro displayCriteria criteria>
    ${StringUtils.abbreviate(criteria.name, 40)}
    <#list mascModel.option as option>
        <#if option_index &lt; 5>
            <span class="firstSpan" style="left:${350 + (option_index * 140)}px;color:${MascUtil.getColorHEXOfWorstResolvedOptionValuesGroupForCriteria(option, criteria)}">${StringUtils.abbreviate(MascUtil.getResolvedOptionValuesForCriteriaAsString(option, criteria), 20)}</span>
        </#if>
    </#list>
</#macro>
