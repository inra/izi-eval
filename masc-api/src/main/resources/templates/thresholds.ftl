<!--
  #%L
  Masc :: API
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 Inra, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<h2 class="pageBreak">${I18n._("masc.report.threshold")}</h2>

<table>
    <thead>
    <tr>
        <th>${I18n._("masc.report.criteria")}</th>
        <th>${I18n._("masc.report.threshold")}</th>
        <th>${I18n._("masc.report.unit")}</th>
    </tr>
    </thead>
    <tbody>

    <#list MascUtil.extractAllThresholdCriterias(mascModel) as criteria>
        <@displayCriteria criteria=criteria/>
    </#list>
    </tbody>
</table>

<#macro displayCriteria criteria>
    <tr>
        <td>${criteria.name}</td>
        <td>${MascUtil.getThresholdAsString(criteria)}</td>
        <td>
          <#if criteria.unit??>
          ${criteria.unit}
          </#if>
        </td>
    </tr>
</#macro>

