<#--
        #%L
        Masc :: API

        $Id$
        $HeadURL$
        %%
        Copyright (C) 2011 - 2012 Inra, Codelutin
        %%
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public
        License along with this program.  If not, see
<http://www.gnu.org/licenses/gpl-3.0.html>.
#L%
-->
<!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/>

    <style type="text/css">
        @page land {
            size:landscape;
        }

        .landscapePage {
            page:land; width: 29.7cm;
        }

        * {
            font-size: small;
        }

        h1 {
            text-align: center;
            font-size: medium;
            font: bold;
            text-decoration:underline;
        }

        h2 {
            font-size: medium;
            font: bold;
            text-decoration:underline;
        }

        h3 {
            text-align: center;
            font-style: normal;
        }

        table {
            margin: 1em; border-collapse: collapse;
        }

        td, th {
            padding: .3em; border: 1px #ccc solid;
            text-align: left;
        }

        .pageBreak {
            page-break-before:always;
            page-break-after:avoid;
        }

        span {
            position: absolute;
        }

        .firstSpan {
            left: 350px;
            position: absolute;
            width: 20cm;
        }

        ul.tree, ul.tree ul {
            list-style-type: none;
            background: url(./vline.png) repeat-y;
            margin: 0;
            padding: 0;
        }

        ul.tree ul {
            margin-left: 10px;
        }

        ul.tree li {
            margin: 0;
            padding: 0 8px;
            line-height: 14px;
            background: url(./node.png) no-repeat;
        }

        ul.tree li.last {
            background: #fff url(./lastnode.png) no-repeat;
        }
    </style>

    <#if mascModel.name??>
        <title>${mascModel.name}</title>
    </#if>
</head>

<body <#if landscape>class="landscapePage"</#if>>
${content}
</body>
</html>