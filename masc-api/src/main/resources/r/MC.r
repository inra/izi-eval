#JEB
#Program to perform a MC Sensitivity analysis on a DEXi model
#Two arguments requested: the name of the dexi file and the number of runs (default=10000)
#Load requested libraries
library(XML) #To manage XML
library(AlgDesign)  #To create factorial plan
#================This part is to manage the tree ===============================
#Class definitions--------------------------------------------------------------
setClass("Tree",
         representation(nbAttributes="numeric",             #number of attributes
                        nbLeaves="numeric",                 #number of leaves
                        Depth="numeric",                    #Maximum depth of the tree
                        Attributes="character",             #List of names of attributes
                        Leaves="character",                 #List of names of leaves
                        Aggregated="character",             #List of names of aggregated nodes
                        isMultiple="logical",                #a simple tag to know if multiple leaves
                        Multiple="matrix",                  #List of the multiple leaves and number of occurence
                        Paths="list",                       #Path from leaf to root
                        Nodes="list",                       #List of nodes
                        rootName="character"))              #name of the root node

setClass("Node",
         representation(name="character",                   #name of the node
                        isLeaf="logical",                   #is it a leaf?
                        children="character",               #list of the names of the node's children
                        sisters="character",                #list of the names of the node's sisters
                        mother="character",                 #name of the node's mother
                        aggregation="matrix",               #if aggregated node, table of aggregation
                        Proba="numeric",                    #if Leaf equals
                        Depth="numeric",                    #Depth of the node
                        place="numeric",                     #To deal with multiple leaves. If greater than 1, multiple leaf
                        CondiProbaList="list",
                        rangeScale="numeric",               #range scale
                        scaleLabel="character"))            #Labels of the different scales

createTree<-function(MT,treeName,aTree=0)
{
  if(missing(treeName))
    {rootName<-sapply(getNodeSet(MT,"/DEXi/ATTRIBUTE/NAME"),xmlValue)}else
    {rootName<-treeName}
#list of the attributes names
  l.Attrib<-c(rootName,sapply(getNodeSet(MT,paste("//ATTRIBUTE[NAME='",rootName,"']//ATTRIBUTE/NAME",sep="")),xmlValue))
  nbAttrib<-length(l.Attrib)
#list of leaves: ie attributes without children  ATTRIBUTE
#to find them, check that there is no FUNCTION child in the branch
  l.Leaves<-character(nbAttrib)
  nbLeaves<-0
  for(i in 1:nbAttrib)
  {
    chaine<-paste("//ATTRIBUTE[NAME='",l.Attrib[i],"']/FUNCTION",sep="")
    if(!length(sapply(getNodeSet(MT,chaine),xmlSize)))
    {
      nbLeaves<-nbLeaves+1
      l.Leaves[nbLeaves]<-l.Attrib[i]
    }
  }
  l.Leaves<-l.Leaves[1:nbLeaves]
#Check for duplicated leaves
  #nrow defined considering the number of leaves repeted
  Multiple<-matrix(nrow=length(unique(l.Leaves[duplicated(l.Leaves)])),ncol=2)
  isMultiple<-FALSE
  colnames(Multiple)<-c("Leaf","Occ")
  j<-1
  if(length(l.Leaves)>length(unique(l.Leaves)))
  {
    isMultiple<-TRUE
    for(i in unique(l.Leaves))
    {
      if(sum(l.Leaves==i)>1)
      {
        Multiple[j,1]<-i
        Multiple[j,2]<-sum(l.Leaves==i)
        j<-j+1
      }
    }
  }
#Aggregated (Attributes-Leaves)
  l.Aggregated<-setdiff(l.Attrib,l.Leaves)
#Deepest level: recursive search of /ATTRIBUTE
  chaine<-paste("//ATTRIBUTE")
  nbLevels<-0
  while(length(sapply(getNodeSet(MT,chaine),xmlValue)))
  {
    nbLevels<-nbLevels+1
    chaine<-paste(chaine,"/ATTRIBUTE[NAME='",rootName,"']",sep="")
  }
#Path from leaves up to the root - A bit complex due to sometimes multiple occurence of a leaf
#To deal with the multiple leaves
  indic<-cbind(Multiple,c(rep(1,dim(Multiple)[1])))
  path<- vector("list",nbLeaves)
  chaine2<-"/.."
  chaine3<-"/NAME"
  for (i in 1:nbLeaves)
  {
    path[[i]][1]<-l.Leaves[i]
    if((!isMultiple)||(!length(Multiple[Multiple[,1]==l.Leaves[i],])))
    {
      if(missing(treeName))
        {
          chaine1<-paste("//ATTRIBUTE[NAME='",l.Leaves[i],"']",sep="")
        } else
        {
          chaine1<-paste("//ATTRIBUTE[NAME='",treeName,"']//ATTRIBUTE[NAME='",l.Leaves[i],"']",sep="")
        }
      j<-1
      isEnd<-FALSE
      while(!isEnd)
      {
        chaine<-paste(chaine1,paste(rep(chaine2,j),collapse=""),chaine3,sep="")
        path[[i]][j+1]<-sapply(getNodeSet(MT,chaine),xmlValue)
        if(path[[i]][j+1]==rootName)
          isEnd<-TRUE
        j<-j+1
      }
    }else
    {
#we will look for parent of the first parent which differ for the repeated leaves
      chaine1<-paste("//ATTRIBUTE[NAME='",l.Leaves[i],"']/../NAME",sep="")
      ind<-as.numeric(indic[indic[,1]==l.Leaves[i],3])
      parent<-sapply(getNodeSet(MT,chaine1),xmlValue)[ind]
      path[[i]][2]<-parent
      indic[indic[,1]==l.Leaves[i],3]<-ind+1
      chaine1<-paste("//ATTRIBUTE[NAME='",parent,"']",sep="")
      j<-2
      isEnd<-FALSE
      while(!isEnd)
      {
        chaine<-paste(chaine1,paste(rep(chaine2,j-1),collapse=""),chaine3,sep="")
        path[[i]][j+1]<-sapply(getNodeSet(MT,chaine),xmlValue)
        if(path[[i]][j+1]==rootName)
          isEnd<-TRUE
        j<-j+1
      }
    }
  }
#Create all the nodes
  TreeNodes<-vector(mode="list",nbAttrib)
  indic<-cbind(Multiple,c(rep(1,dim(Multiple)[1])))
  for(i in 1:nbAttrib)
  {
    if((isMultiple)&&length(Multiple[Multiple[,1]==l.Attrib[i],]))
    {
      place<-as.numeric(indic[indic[,1]==l.Attrib[i],3])
      indic[indic[,1]==l.Attrib[i],3]<-place+1
    }else
      place<-0
    if(missing(aTree))
      TreeNodes[[i]]<-createNode(l.Attrib[i],MT,Multiple,place)
    else
      TreeNodes[[i]]<-aTree@Nodes[[l.Attrib[i]]]
  }
  names(TreeNodes)<-l.Attrib
  out <- new("Tree", rootName=rootName, nbAttributes=nbAttrib, nbLeaves=nbLeaves,Depth=nbLevels,
             Nodes=TreeNodes,Multiple=Multiple,isMultiple=isMultiple,
             Attributes=l.Attrib, Leaves=l.Leaves, Aggregated=l.Aggregated,Paths=path)
}

createNode <- function(nameNode,MT,Multiple,place,myTable)
{
#to deal with multiple leaves
  if(place)
  {
      chaine1<-paste("//ATTRIBUTE[NAME='",nameNode,"']/../NAME",sep="")
      mother<-sapply(getNodeSet(MT,chaine1),xmlValue)[place]
      chaine<-paste("//ATTRIBUTE[NAME='",mother,"']/ATTRIBUTE[NAME='",nameNode,"']",sep="")
  }else
    chaine<-paste("//ATTRIBUTE[NAME='",nameNode,"']",sep="")
#Children
    chaine1<-paste(chaine,"/ATTRIBUTE/NAME",sep="")
    l.Attrib<-sapply(getNodeSet(MT,chaine1),xmlValue)
    if(length(l.Attrib)==0){
      l.Attrib<-vector(mode="character",length=0)
  }
#Mother if any
  chaine1<-paste(chaine,"/../NAME",sep="")
  mother<-as.character(sapply(getNodeSet(MT,chaine1),xmlValue))
#Sisters: they do have the same mother
  if(length(mother))
  {
    chaine1<-paste(chaine,"/../ATTRIBUTE/NAME",sep="")
    sisters<-sapply(getNodeSet(MT,chaine1),xmlValue)
    sisters<-sisters[sisters[]!=nameNode]
  }else
    sisters<-vector(mode="character",length=0)
#                        coNodes="list",
#Scales for node i
  chaine1<-paste(chaine,"/SCALE/SCALEVALUE",sep="")
  scaleNode<-length(sapply(getNodeSet(MT,chaine1),xmlSize))
  chaine1<-paste(chaine,"/SCALE/SCALEVALUE/NAME",sep="")
  scaleLabel<-sapply(getNodeSet(MT,chaine1),xmlValue)
#Function
  chaine1<-paste(chaine,"/FUNCTION/LOW",sep="")
  c.Function<-sapply(getNodeSet(MT,chaine1),xmlValue)
  if(!length(c.Function))
    {isLeaf<-TRUE}else
      {isLeaf<-FALSE}
  if(!isLeaf)
  {
#Transform as a vector
    nbChar<-nchar(c.Function)
    v.Function<-numeric(nbChar)
    for(i in 1:nbChar)
    {
#Modify attribute 0...n to 1...n+1
      v.Function[i]<-as.numeric(substr(c.Function,i,i))+1
    }
#Scales from nodes n-1
#HM: j'ai remplac� nbSisters par nbChildren
    nbChildren <- length(l.Attrib)
    scaleChildren <- numeric(nbChildren)
    for(i in 1:nbChildren)
    {
#To deal with multiple leaves
      if(length(intersect(Multiple[,1],l.Attrib[i])))
        {chaine4<-paste("//ATTRIBUTE[NAME='",nameNode,"']/ATTRIBUTE[NAME='",l.Attrib[i],"']/SCALE/SCALEVALUE",sep="")}else
        {chaine4<-paste("//ATTRIBUTE[NAME='",l.Attrib[i],"']/SCALE/SCALEVALUE",sep="")}
      scaleChildren[i]<-length(sapply(getNodeSet(MT,chaine4),xmlSize))
    }
#Create the factorial plan
    factorialPlan<-rev(gen.factorial(rev(as.numeric(scaleChildren)),center=FALSE))
    nbFactorialPlan<-dim(factorialPlan)[1]
    aggregation <- as.matrix(factorialPlan[,seq(ncol(factorialPlan))])
    aggregation <- cbind(aggregation,v.Function)
    colnames(aggregation) <- c(l.Attrib, nameNode)
  }else
    aggregation<-as.matrix(0)
#Create the weights (equal weights if not defined)##############################
  WeightList<-numeric(scaleNode)
  if(isLeaf)
#  if(!row.names(myTable)[i])
      WeightList <- rep(1/scaleNode,scaleNode)
#Define the depth of the given node
#Path from leaves up to the root - added from version 2
  Depth<-1
  rootName<-sapply(getNodeSet(MT,"/DEXi/ATTRIBUTE/NAME"),xmlValue)
  chaine2<-"/.."
  chaine3<-"/NAME"
  isEnd<-FALSE
  if(rootName!=nameNode)
  {
    while(!isEnd)
    {
      chaine1<-paste(chaine,paste(rep(chaine2,Depth),collapse=""),chaine3,sep="")
      path<-sapply(getNodeSet(MT,chaine1),xmlValue)
      if(path[[1]]==rootName)
        isEnd<-TRUE
      Depth<-Depth+1
    }
  }
#Merge with the function
  out <- new("Node",name=nameNode, Depth=Depth,isLeaf=isLeaf,
             mother=mother, sisters=sisters, children=l.Attrib,place=place,
             aggregation=aggregation, rangeScale=scaleNode, scaleLabel=scaleLabel, Proba=WeightList)
}

#Get the list of the leaves of a given aggregated node
getLeaves<-function(MT,nodeName)
{
  subTree<-createTree(MT,nodeName)
  return(subTree@Leaves)
}

EvaluateScenario<-function(aTree,option)
{
  results<-numeric(aTree@nbAttributes)
  names(results)<-aTree@Attributes
  results[]<--1
  if(!is.null(names(option)))
    results[names(option)]<-option
  else
    results[rownames(option)]<-option
#Thanks to the hierchical structure of the tree... use rev()
  for(i in rev(aTree@Aggregated))
  {
  #Compute the value thanks to the attribution table
    if(results[i]<0)
    {
      nbc<-length(aTree@Nodes[[i]]@children)

      value<-aTree@Nodes[[i]]@aggregation
      for(j in 1:nbc)
      {
        value<-value[value[,j]==results[aTree@Nodes[[i]]@children[j]],]
      }
      results[i]<-value[nbc+1]
    }
  }
  return(results)
}
#===============================================================================
#
#================This part is to manage the MC =================================
MonteCarlo<-function(aTree,nbRuns,isFile)
{
  MC<-matrix(ncol=nbRuns,nrow=aTree@nbAttributes,dimnames = list(c(aTree@Attributes),c(seq(1:nbRuns))))
  option<-matrix(nrow=aTree@nbLeaves,ncol=nbRuns)
  rownames(option)<-aTree@Leaves
  for(k in aTree@Leaves)
  {
    option[k,]<-sample(aTree@Nodes[[k]]@rangeScale,nbRuns,replace=TRUE,prob=aTree@Nodes[[k]]@Proba)
  }
  if(aTree@isMultiple)
  {
    for(j in 1:dim(aTree@Multiple)[1])
    {
      val<-option[aTree@Multiple[j,1],]
        for(z in 1:dim(option)[1])
        {
          if(dimnames(option)[[1]][z]==aTree@Multiple[j,1])
          option[z,]<-val
        }
    }
  }
  for(i in 1:nbRuns)
  {
    MC[,i]<-EvaluateScenario(aTree,option[,i])
  }
  if(isFile)
#Write a file that contain all the random options selected for the MC analysis
    write.table(option,file="MC options.csv",sep=",",row.names=T,col.names=NA)
  return(MC)
}
#To show the MC
ShowMC<-function(Node,MC)
{
#  Node<-theTree@Nodes[[nodeName]]
  typ<-"A"
  if(Node@isLeaf)
    typ<-"L"
  bar<-tapply(MC[Node@name,],MC[Node@name,],sum)
  if(length(bar)<Node@rangeScale)
  {
    newbar<-array(data=0,dim=Node@rangeScale,dimnames=list(c(seq(1:Node@rangeScale))))
    for(i in 1:dim(bar))
    {
      newbar[names(bar)[i]]<-bar[i]
    }
    bar<-newbar
  }
  bar[]<-(bar[]/c(1:Node@rangeScale))/nbRuns
  mc<-barplot(bar,main=paste(Node@name," [",typ,"]",sep=""),xlab="Modalities",
  ylab="Frequencies",ylim=c(0,(max(bar)+0.1)),las=1,cex=1,cex.main=1,cex.lab=1,cex.axis=0.9) #,names.arg=Node@scaleLabel
  text(mc,bar,format(round(bar,digits=2)),xpd=T,cex=0.8,pos=3)
#  ref_points<-as.matrix(read.table(file="MC bar lengths_ref.csv",sep=",",row.names=1))
#  points(mc,as.vector(na.omit((ref_points))),pch=18,col=2)
  legend("topright",legend=paste(names(bar),abbreviate(Node@scaleLabel)),box.lty=0,cex=0.8)
  text(length(bar))
  #write.table(bar,file="MC bar lengths.csv",sep=",",row.names=T,col.names=NA)
  cat(row.names=Node@name,bar,file="MC bar lengths.csv",sep=",",fill=T,append=T)
  return(bar)
   #("Modalities",Node@name))
}
#To write the list
list2ascii <- function(x,file=paste(deparse(substitute(x)),".txt",sep=""))
{
   # MHP July 7, 2004
   # R or S function to write an R list to an ASCII file.
   # This can be used to create files for those who want to use
   # a spreadsheet or other program on the data.
   #
   tmp.wid = getOption("width")  # save current width
   options(width=10000)          # increase output width
   sink(file)                    # redirect output to file
   print(x)                      # print the object
   sink()                        # cancel redirection
   options(width=tmp.wid)        # restore linewidth
   return(invisible(NULL))       # return (nothing) from function
}

#===============================================================================

#================This part is to perfom MC =====================================
#Run on a batch mode
#rm(list=ls())
args=(commandArgs(TRUE))
if(length(args)==0)
{
  print("No arguments supplied.")
##supply default values
  dexi<-"MASC2.0.dxi"
  nbRuns<-10000
##node will be assign later, when tree will be created
}else
{
  for(i in 1:length(args))
  {
    eval(parse(text=args[[i]]))
  }
}
#read the structure of the model
MT<-xmlTreeParse(dexi,useInternalNodes=T)
DEXi<-createTree(MT)
#Give a generic name
theTree<-DEXi
#Run MC
MC<-MonteCarlo(theTree,nbRuns,1)
if(!exists("node"))
  node<-1
#Compute frequencies
for(i in theTree@Attributes)
{
  myData<-matrix(data=NA,nrow=1,ncol=theTree@Nodes[[i]]@rangeScale,dimnames=list(1,c(theTree@Nodes[[i]]@scaleLabel)))
  myTable<-table(MC[i,])/nbRuns
  for(j in 1:theTree@Nodes[[i]]@rangeScale)
  {
    myData[j]<-myTable[paste('',j,'',sep="")]
  }
  if(exists("resMC"))
    resMC<-c(resMC,list(myData))
  else
    resMC<-list(myData)
}
names(resMC)<-c(theTree@Attributes)
#Create output file
list2ascii(resMC,"MC.txt")
jpeg(file=paste("MC_",theTree@Attributes[[node]],".jpeg",sep=""))
ShowMC(theTree@Nodes[[node]],MC)
dev.off()