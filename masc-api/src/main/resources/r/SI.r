#JEB
#Program to perform a MC Sensitivity analysis on a DEXi model
#Two arguments requested: the name of the dexi file and the number of runs (default=10000)
#Load requested libraries
library(XML) #To manage XML
library(AlgDesign)  #To create factorial plan
#================This part is to manage the tree ===============================
#Class definitions--------------------------------------------------------------
setClass("Tree",
         representation(nbAttributes="numeric",             #number of attributes
                        nbLeaves="numeric",                 #number of leaves
                        Depth="numeric",                    #Maximum depth of the tree
                        Attributes="character",             #List of names of attributes
                        Leaves="character",                 #List of names of leaves
                        Aggregated="character",             #List of names of aggregated nodes
                        isMultiple="logical",                #a simple tag to know if multiple leaves
                        Multiple="matrix",                  #List of the multiple leaves and number of occurence
                        Paths="list",                       #Path from leaf to root
                        Nodes="list",                       #List of nodes
                        rootName="character"))              #name of the root node

setClass("Node",
         representation(name="character",                   #name of the node
                        isLeaf="logical",                   #is it a leaf?
                        children="character",               #list of the names of the node's children
                        sisters="character",                #list of the names of the node's sisters
                        mother="character",                 #name of the node's mother
                        aggregation="matrix",               #if aggregated node, table of aggregation
                        Proba="numeric",                    #if Leaf equals
                        Depth="numeric",                    #Depth of the node
                        place="numeric",                     #To deal with multiple leaves. If greater than 1, multiple leaf
                        CondiProbaList="list",
                        rangeScale="numeric",               #range scale
                        scaleLabel="character"))            #Labels of the different scales

createTree<-function(MT,treeName,aTree=0)
{
  if(missing(treeName))
    {rootName<-sapply(getNodeSet(MT,"/DEXi/ATTRIBUTE/NAME"),xmlValue)}else
    {rootName<-treeName}
#list of the attributes names
  l.Attrib<-c(rootName,sapply(getNodeSet(MT,paste("//ATTRIBUTE[NAME='",rootName,"']//ATTRIBUTE/NAME",sep="")),xmlValue))
  nbAttrib<-length(l.Attrib)
#list of leaves: ie attributes without children  ATTRIBUTE
#to find them, check that there is no FUNCTION child in the branch
  l.Leaves<-character(nbAttrib)
  nbLeaves<-0
  for(i in 1:nbAttrib)
  {
    chaine<-paste("//ATTRIBUTE[NAME='",l.Attrib[i],"']/FUNCTION",sep="")
    if(!length(sapply(getNodeSet(MT,chaine),xmlSize)))
    {
      nbLeaves<-nbLeaves+1
      l.Leaves[nbLeaves]<-l.Attrib[i]
    }
  }
  l.Leaves<-l.Leaves[1:nbLeaves]
#Check for duplicated leaves
  #nrow defined considering the number of leaves repeted
  Multiple<-matrix(nrow=length(unique(l.Leaves[duplicated(l.Leaves)])),ncol=2)
  isMultiple<-FALSE
  colnames(Multiple)<-c("Leaf","Occ")
  j<-1
  if(length(l.Leaves)>length(unique(l.Leaves)))
  {
    isMultiple<-TRUE
    for(i in unique(l.Leaves))
    {
      if(sum(l.Leaves==i)>1)
      {
        Multiple[j,1]<-i
        Multiple[j,2]<-sum(l.Leaves==i)
        j<-j+1
      }
    }
  }
#Aggregated (Attributes-Leaves)
  l.Aggregated<-setdiff(l.Attrib,l.Leaves)
#Deepest level: recursive search of /ATTRIBUTE
  chaine<-paste("//ATTRIBUTE")
  nbLevels<-0
  while(length(sapply(getNodeSet(MT,chaine),xmlValue)))
  {
    nbLevels<-nbLevels+1
    chaine<-paste(chaine,"/ATTRIBUTE[NAME='",rootName,"']",sep="")
  }
#Path from leaves up to the root - A bit complex due to sometimes multiple occurence of a leaf
#To deal with the multiple leaves
  indic<-cbind(Multiple,c(rep(1,dim(Multiple)[1])))
  path<- vector("list",nbLeaves)
  chaine2<-"/.."
  chaine3<-"/NAME"
  for (i in 1:nbLeaves)
  {
    path[[i]][1]<-l.Leaves[i]
    if((!isMultiple)||(!length(Multiple[Multiple[,1]==l.Leaves[i],])))
    {
      if(missing(treeName))
        {
          chaine1<-paste("//ATTRIBUTE[NAME='",l.Leaves[i],"']",sep="")
        } else
        {
          chaine1<-paste("//ATTRIBUTE[NAME='",treeName,"']//ATTRIBUTE[NAME='",l.Leaves[i],"']",sep="")
        }
      j<-1
      isEnd<-FALSE
      while(!isEnd)
      {
        chaine<-paste(chaine1,paste(rep(chaine2,j),collapse=""),chaine3,sep="")
        path[[i]][j+1]<-sapply(getNodeSet(MT,chaine),xmlValue)
        if(path[[i]][j+1]==rootName)
          isEnd<-TRUE
        j<-j+1
      }
    }else
    {
#we will look for parent of the first parent which differ for the repeated leaves
      chaine1<-paste("//ATTRIBUTE[NAME='",l.Leaves[i],"']/../NAME",sep="")
      ind<-as.numeric(indic[indic[,1]==l.Leaves[i],3])
      parent<-sapply(getNodeSet(MT,chaine1),xmlValue)[ind]
      path[[i]][2]<-parent
      indic[indic[,1]==l.Leaves[i],3]<-ind+1
      chaine1<-paste("//ATTRIBUTE[NAME='",parent,"']",sep="")
      j<-2
      isEnd<-FALSE
      while(!isEnd)
      {
        chaine<-paste(chaine1,paste(rep(chaine2,j-1),collapse=""),chaine3,sep="")
        path[[i]][j+1]<-sapply(getNodeSet(MT,chaine),xmlValue)
        if(path[[i]][j+1]==rootName)
          isEnd<-TRUE
        j<-j+1
      }
    }
  }
#Create all the nodes
  TreeNodes<-vector(mode="list",nbAttrib)
  indic<-cbind(Multiple,c(rep(1,dim(Multiple)[1])))
  for(i in 1:nbAttrib)
  {
    if((isMultiple)&&length(Multiple[Multiple[,1]==l.Attrib[i],]))
    {
      place<-as.numeric(indic[indic[,1]==l.Attrib[i],3])
      indic[indic[,1]==l.Attrib[i],3]<-place+1
    }else
      place<-0
    if(missing(aTree))
      TreeNodes[[i]]<-createNode(l.Attrib[i],MT,Multiple,place)
    else
      TreeNodes[[i]]<-aTree@Nodes[[l.Attrib[i]]]
  }
  names(TreeNodes)<-l.Attrib
  out <- new("Tree", rootName=rootName, nbAttributes=nbAttrib, nbLeaves=nbLeaves,Depth=nbLevels,
             Nodes=TreeNodes,Multiple=Multiple,isMultiple=isMultiple,
             Attributes=l.Attrib, Leaves=l.Leaves, Aggregated=l.Aggregated,Paths=path)
}

createNode <- function(nameNode,MT,Multiple,place,myTable)
{
#to deal with multiple leaves
  if(place)
  {
      chaine1<-paste("//ATTRIBUTE[NAME='",nameNode,"']/../NAME",sep="")
      mother<-sapply(getNodeSet(MT,chaine1),xmlValue)[place]
      chaine<-paste("//ATTRIBUTE[NAME='",mother,"']/ATTRIBUTE[NAME='",nameNode,"']",sep="")
  }else
    chaine<-paste("//ATTRIBUTE[NAME='",nameNode,"']",sep="")
#Children
    chaine1<-paste(chaine,"/ATTRIBUTE/NAME",sep="")
    l.Attrib<-sapply(getNodeSet(MT,chaine1),xmlValue)
    if(length(l.Attrib)==0){
      l.Attrib<-vector(mode="character",length=0)
  }
#Mother if any
  chaine1<-paste(chaine,"/../NAME",sep="")
  mother<-as.character(sapply(getNodeSet(MT,chaine1),xmlValue))
#Sisters: they do have the same mother
  if(length(mother))
  {
    chaine1<-paste(chaine,"/../ATTRIBUTE/NAME",sep="")
    sisters<-sapply(getNodeSet(MT,chaine1),xmlValue)
    sisters<-sisters[sisters[]!=nameNode]
  }else
    sisters<-vector(mode="character",length=0)
#                        coNodes="list",
#Scales for node i
  chaine1<-paste(chaine,"/SCALE/SCALEVALUE",sep="")
  scaleNode<-length(sapply(getNodeSet(MT,chaine1),xmlSize))
  chaine1<-paste(chaine,"/SCALE/SCALEVALUE/NAME",sep="")
  scaleLabel<-sapply(getNodeSet(MT,chaine1),xmlValue)
#Function
  chaine1<-paste(chaine,"/FUNCTION/LOW",sep="")
  c.Function<-sapply(getNodeSet(MT,chaine1),xmlValue)
  if(!length(c.Function))
    {isLeaf<-TRUE}else
      {isLeaf<-FALSE}
  if(!isLeaf)
  {
#Transform as a vector
    nbChar<-nchar(c.Function)
    v.Function<-numeric(nbChar)
    for(i in 1:nbChar)
    {
#Modify attribute 0...n to 1...n+1
      v.Function[i]<-as.numeric(substr(c.Function,i,i))+1
    }
#Scales from nodes n-1
#HM: j'ai remplac� nbSisters par nbChildren
    nbChildren <- length(l.Attrib)
    scaleChildren <- numeric(nbChildren)
    for(i in 1:nbChildren)
    {
#To deal with multiple leaves
      if(length(intersect(Multiple[,1],l.Attrib[i])))
        {chaine4<-paste("//ATTRIBUTE[NAME='",nameNode,"']/ATTRIBUTE[NAME='",l.Attrib[i],"']/SCALE/SCALEVALUE",sep="")}else
        {chaine4<-paste("//ATTRIBUTE[NAME='",l.Attrib[i],"']/SCALE/SCALEVALUE",sep="")}
      scaleChildren[i]<-length(sapply(getNodeSet(MT,chaine4),xmlSize))
    }
#Create the factorial plan
    factorialPlan<-rev(gen.factorial(rev(as.numeric(scaleChildren)),center=FALSE))
    nbFactorialPlan<-dim(factorialPlan)[1]
    aggregation <- as.matrix(factorialPlan[,seq(ncol(factorialPlan))])
    aggregation <- cbind(aggregation,v.Function)
    colnames(aggregation) <- c(l.Attrib, nameNode)
  }else
    aggregation<-as.matrix(0)
#Create the weights (equal weights if not defined)##############################
  WeightList<-numeric(scaleNode)
  if(isLeaf)
#  if(!row.names(myTable)[i])
      WeightList <- rep(1/scaleNode,scaleNode)
#Define the depth of the given node
#Path from leaves up to the root - added from version 2
  Depth<-1
  rootName<-sapply(getNodeSet(MT,"/DEXi/ATTRIBUTE/NAME"),xmlValue)
  chaine2<-"/.."
  chaine3<-"/NAME"
  isEnd<-FALSE
  if(rootName!=nameNode)
  {
    while(!isEnd)
    {
      chaine1<-paste(chaine,paste(rep(chaine2,Depth),collapse=""),chaine3,sep="")
      path<-sapply(getNodeSet(MT,chaine1),xmlValue)
      if(path[[1]]==rootName)
        isEnd<-TRUE
      Depth<-Depth+1
    }
  }
#Merge with the function
  out <- new("Node",name=nameNode, Depth=Depth,isLeaf=isLeaf,
             mother=mother, sisters=sisters, children=l.Attrib,place=place,
             aggregation=aggregation, rangeScale=scaleNode, scaleLabel=scaleLabel, Proba=WeightList)
}
#Get the list of the leaves of a given aggregated node
getLeaves<-function(MT,nodeName)
{
  subTree<-createTree(MT,nodeName)
  return(subTree@Leaves)
}
EvaluateScenario<-function(aTree,option)
{
  results<-numeric(aTree@nbAttributes)
  names(results)<-aTree@Attributes
  results[]<--1
  if(!is.null(names(option)))
    results[names(option)]<-option
  else
    results[rownames(option)]<-option
#Thanks to the hierchical structure of the tree... use rev()
  for(i in rev(aTree@Aggregated))
  {
  #Compute the value thanks to the attribution table
    if(results[i]<0)
    {
      nbc<-length(aTree@Nodes[[i]]@children)

      value<-aTree@Nodes[[i]]@aggregation
      for(j in 1:nbc)
      {
        value<-value[value[,j]==results[aTree@Nodes[[i]]@children[j]],]
      }
      results[i]<-value[nbc+1]
    }
  }
  return(results)
}
# Calculation of the Sensitivity Index (SI)
SI_DEXi<-function(Tree)
{
  nodeName<-Tree@rootName
  WeightList <- vector(mode="list", length=Tree@nbAttributes)
  names(WeightList) <- Tree@Attributes
  for(i in 1:Tree@nbAttributes)
  {
    WeightList[[i]]<-Tree@Nodes[[i]]@Proba
  }
  CondiProbaList <- vector(mode="list", length=Tree@nbAttributes)
  names(CondiProbaList) <-Tree@Attributes
# Loop on the Aggregate attributes in reverse order
  for(node.name in rev(Tree@Aggregated))
  {
    Node<-Tree@Nodes[[node.name]]
    NodeChildren <- Node@children
    # info on weights required for direct descendant calculations
    ChildrenWeights <- vector(mode="list",length=length(NodeChildren))
    for(i in seq(NodeChildren))
    {
      child.name <- NodeChildren[[i]]
      ChildrenWeights[[i]] <- WeightList[[child.name]]
    }
# conditional proba calculations (direct descendants)
    Table <- Node@aggregation
    Probas <- condprob.direct(Node@aggregation, ChildrenWeights,Node@rangeScale)
#In case of rangeScale <> of modalities in Y aggregation table, need to add some information in Probas
    if(Node@rangeScale!=(length(Probas[[length(NodeChildren)+1]])))
    {
      nbC<-length(NodeChildren)
      nbVal<-length(Probas[[nbC+1]])
      diff<-Node@rangeScale-nbVal
      Probas[[nbC+1]][c((nbVal+1):Node@rangeScale)]<-0
      names(Probas[[nbC+1]])<-c(names(Probas[[nbC+1]][1:nbVal]),setdiff(as.character(seq(1:Node@rangeScale)),names(Probas[[nbC+1]])))
    }
    names(Probas) <- c(NodeChildren, node.name)
    NodeWeights <- Probas[[length(Probas)]]
    DirectCondiProbaList <- Probas[-length(Probas)]
# conditional proba calculations (indirect descendants)
    IndirectCondiProbaList <- vector(mode="list",length=0)
    for(i in seq(NodeChildren))
    {
      child.name <- NodeChildren[[i]]
      child.node <- Tree@Nodes[[child.name]]
      if(!child.node@isLeaf)
      {
        CPL.DA <- CondiProbaList[[child.name]]
        CPL.DY <- vector(mode="list", length=length(CPL.DA))
        for(j in seq(CPL.DA))
        {
          CPL.DY[[j]] <- CPL.DA[[j]] %*% Probas[[child.name]]
        }
        names(CPL.DY) <- names(CPL.DA)
        IndirectCondiProbaList <- c(IndirectCondiProbaList, CPL.DY)
      }
    }
    WeightList[[node.name]] <- NodeWeights
    CondiProbaList[[node.name]] <- c(DirectCondiProbaList, IndirectCondiProbaList)
  }
  CPL <- c( CondiProbaList[[nodeName]], WeightList[nodeName])
  Wgt <- WeightList[names(CondiProbaList[[nodeName]])]
  SI <- sensitivity.condprob(condproblist=CPL,weightlist=Wgt)
# would be useful to print also the name of the analysed variable
  print(SI)
  return(SI)
}

#---------------------------------------------------------------------------
condprob.direct <- function(table,weightlist,sy,Ylevels){
  # Calculates the probabilities of Y=y and the probabilities of Y conditional
  # to its direct descendants A_i, when given the complete table of the
  # Y modalities with respect to the A_i factors, and when given the A_i
  # probabilities
  # ARGUMENTS
  #  table : a matrix giving all the level combinations of the A_i
  #          factors and, in the last column, the associated Y values
  #  weightlist : a list whose elements are the weight vectors of each A_i
  #               variable
  #               if missing, A_i levels are assumed to have equal weights
  #  Ylevels : optional argument giving the Y levels. If missing, the Ylevels
  #            are extracted from the table
  # OUTPUT
  #  a list containing the matrices of Y probabilities conditional to each A_i
  #  factor and, in the last position, the vector of marginal Y probabilities
  # DETAILS
  #  The Ylevels argument is useful to cope with the cases when not all
  #  Ylevels are in the table
  # EXAMPLE
  #  toto <- condprob.direct(ImpMilTable, weightlist)

  # Preliminary calculations
  A <- table[,-ncol(table),drop=F]
  Y <- table[, ncol(table)]
  # - number of factors A (n)
  #   and numbers of levels of the factors A (s) and of Y (sY)
  n <-ncol(A)
  s <- apply(A, 2, function(x){length(unique(x))})
  if(missing(Ylevels))
  {
    Ylevels <- sort(unique(Y))
  }
  # equal weights if missing
  if(missing(weightlist)){
    weightlist <- lapply(s, function(n){rep(1,n)/n})
  }
  # Weights of the table rows for each A variable and for Y
  Aweights <- matrix(NA, nrow(A), n)
  for (i in 1:n)
  {
    Aweights[,i] <- weightlist[[i]][A[,i]]
  }
  Yweights <- apply(Aweights, 1, prod)
  # Calculation of the Y probabilities
  Yproba <- c( tapply(Yweights, Y, sum) )

  # Calculation of the Y probabilities conditional to the A_i
  YAproba <- vector("list",length=n)
  for(i in 1:n)
  {
    probas.i <- Yweights/Aweights[,i]
    condproba <-  tapply(probas.i, list(A[,i],Y), sum)
    condproba[is.na(condproba)] <- 0
    YAproba[[i]] <- condproba
  }
  # Results storage
  out <- c(YAproba, list(Yproba))
  names(out) <- colnames(table)
#Modification in case sy>unique(Y)
  if(sy>length(unique(Y)))
  {
    newlist<-vector(mode="list",length=n+1)
    for(j in 1:n)
    {
      newlist[[j]]<-matrix(0,nrow=dim(out[[j]])[1],ncol=sy)
      dimnames(newlist[[j]])[1]<-c(dimnames(out[[j]])[1])
      dimnames(newlist[[j]])[2]<-list(c(1:sy))
      for(k in as.numeric(dimnames(out[[j]])[[2]]))
        newlist[[j]][,k]<-out[[j]][,as.character(k)]
    }
    newlist[[n+1]]<-rep(0,sy)
    names(newlist[[n+1]])<-c(1:sy)
    for(k in as.numeric(names(Yproba)))
      newlist[[n+1]][k]<-Yproba[as.character(k)]
    names(newlist)<-colnames(table)
    return(newlist)
  }
  return(out)
}
#===========================================================================
sensitivity.condprob <- function(condproblist,weightlist)
{
  # Calculates the first order sensitivity indices of Y with respect
  # to A_i descendants, when given the conditional probabilities and A_i weights
  # ARGUMENTS
  #  condproblist : list of matrices of conditional probabilities
  #             (Y conditional to each A_i) + the vector of Y probabilities
  #  weightlist : the list of weights of the A_i factor levels
  # OUTPUT
  #  a vector of SI sensitivity indices
  # EXAMPLE
  #
  #

  # preliminaries
  n <- length(condproblist)-1
  Yproba <- condproblist[[n+1]]
  Ylevels <- as.numeric( names(Yproba) )
  # Y expectation and variance
  Yexp <- sum( Yproba*Ylevels )
  Yvar <- sum( Yproba*(Ylevels-Yexp)^2 )
  # sensitivity indices
  SI <- vector(length=n)
  for(i in seq(n)){
    Ycondexp.Ai <- condproblist[[i]] %*% Ylevels
    Yvar.Ai <- sum( weightlist[[i]]*(Ycondexp.Ai-Yexp)^2 )
    SI[i] <- Yvar.Ai/Yvar
  }
  # Results storage
  names(SI) <- names(condproblist[seq(n)])
  return(SI)
}




#===============================================================================

#================This part is to perfom MC =====================================
#Run on a batch mode
#rm(list=ls())
args=(commandArgs(TRUE))
if(length(args)==0)
{
  print("No arguments supplied.")
##supply default values
  dexi<-"MASC2.0.dxi"
##node will be assign later, when tree will be created
}else
{
  for(i in 1:length(args))
  {
    eval(parse(text=args[[i]]))
  }
}
#read the structure of the model
MT<-xmlTreeParse(dexi,useInternalNodes=T)
DEXi<-createTree(MT)
#Give a generic name
theTree<-DEXi
#Run SI
SI<-SI_DEXi(theTree)
jpeg(file=paste("SI_",dexi,".jpeg",sep=""))
par(mgp=c(7,1,0),oma=c(0,20,0,0))
mc<-barplot(as.vector(rev(SI[theTree@Leaves])),horiz=T,xlim=c(0,max(SI[theTree@Leaves])),ylab="Indicators")
axis(side=2,at=mc,labels=rev(theTree@Leaves),las=2)
abline(v=0.02, untf = FALSE,lty=3)
mtext("Sensitivity Index",1,line=3)
mtext("Basic attributes",2,outer=T, line=15)
dev.off()
