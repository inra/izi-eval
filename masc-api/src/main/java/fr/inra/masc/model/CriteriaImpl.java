/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.model;

import com.google.common.collect.Lists;
import fr.inra.masc.utils.CriteriaVisitor;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public abstract class CriteriaImpl extends Criteria {

    private static final long serialVersionUID = 1L;

    public CriteriaImpl() {
        this.uuid = UUID.randomUUID().toString();
    }

    @Override
    public Collection<Criteria> getChild(boolean withRef) {
        Collection<Criteria> children = super.getChild();
        if (children == null || (!withRef && isReference())) {
            return Lists.newArrayList();
        }
        List<Criteria> result = Lists.newArrayList(children);
        if (withRef && isReference()) {
            result = Lists.newArrayList(result.get(0).getChild(withRef));
        }
        return result;
    }

    @Override
    public void accept(CriteriaVisitor visitor, Criteria parentCriteria) {

        if (visitor.visit(parentCriteria, this)) {

            Collection<Criteria> children;
            children = this.getChild(visitor.resolvRefs());
            if (children != null) {
                for (Criteria child : children) {
                    child.accept(visitor, this);
                }
            }
        }
    }
}
