package fr.inra.masc.model;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class ThresholdValueImpl extends ThresholdValue {

    private static final long serialVersionUID = 1L;

    public ThresholdValueImpl() {
    }

    public ThresholdValueImpl(ThresholdValue value) {
        this(value.getValue(), value.getSign(), value.getCanBeEqual());
    }

    public ThresholdValueImpl(Double value,
                              ScaleOrder sign,
                              boolean canBeEqual) {
        setValue(value);
        setSign(sign);
        setCanBeEqual(canBeEqual);
    }

}
