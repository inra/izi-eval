package fr.inra.masc.model;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

/**
 * Criteria utilisies
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.8
 */
public class Criterias {

    public static final Function<Criteria,String> CRITERIA_BY_NAME = new Function<Criteria, String>() {
        @Override
        public String apply(Criteria input) {
            return input.getName();
        }
    };

    private Criterias() {
        // utility class
    }

    public static <T extends Criteria> Multimap<String, T> criteriaByName(Iterable<T> criterias) {
        Multimap<String, T> result = Multimaps.index(criterias, CRITERIA_BY_NAME);
        return result;
    }
}
