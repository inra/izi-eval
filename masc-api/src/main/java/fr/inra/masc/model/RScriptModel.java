/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.model;

import com.google.common.collect.Lists;
import fr.inra.masc.MascConfig;
import fr.inra.masc.R_SCRIPT;
import org.apache.commons.io.FileUtils;
import org.jdesktop.beans.AbstractSerializableBean;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Represent all properties to launch a R script.
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.5
 */
public class RScriptModel extends AbstractSerializableBean {

    public static final String R_CMD = "CMD BATCH --slave --no-restore";

    public static final String R_ARGS = "--args";

    public static final String WORKDIR_PREFIX = "masc-";

    public static final String CONTEXT_DEXI_MODEL_NAME = "dexiModelName";

    public static final String CONTEXT_CRITERIA_NAME = "criteriaName";

    public static final String CONTEXT_OPTION_NAME = "optionName";

    public static final String PROPERTY_NODE = "node";

    public static final String PROPERTY_OPTION = "option";

    public static final String PROPERTY_NB_RUNS = "nbRuns";

    public static final String PROPERTY_SCRIPT = "script";

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_SELECTED_RESULT = "selectedResult";

    public static final String PROPERTY_ARGS = "args";

    public static final String R_ROUT = ".Rout";

    public static final String DEXI_MODEL_NAME = "dexi='${dexiModelName}'";

    private static final long serialVersionUID = 1L;

    protected R_SCRIPT script;

    protected List<String> selectedResult;

    protected long id;

    protected Option option;

    protected int node = 0;

    protected int nbRuns = 5000;

    public RScriptModel() {
        selectedResult = Lists.newArrayList();
    }

    public static String getRFileName(String mascFileName) {
        return mascFileName.replace(" ", "") + ".dxi";
    }

    public R_SCRIPT getScript() {
        return script;
    }

    public void setScript(R_SCRIPT script) {
        R_SCRIPT oldValue = getScript();
        this.script = script;
        firePropertyChange(PROPERTY_SCRIPT, oldValue, script);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        long oldValue = getId();
        this.id = id;
        firePropertyChange(PROPERTY_ID, oldValue, id);
    }

    public List<String> getSelectedResult() {
        return selectedResult;
    }

    public void setSelectedResult(List<String> selectedResult) {
        List<String> oldValue = getSelectedResult();
        this.selectedResult = selectedResult;
        firePropertyChange(PROPERTY_SELECTED_RESULT, oldValue, selectedResult);
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        Option oldValue = getOption();
        this.option = option;
        firePropertyChange(PROPERTY_OPTION, oldValue, option);
    }

    public int getNode() {
        return node;
    }

    public void setNode(int node) {
        int oldValue = getNode();
        this.node = node;
        firePropertyChange(PROPERTY_NODE, oldValue, node);
    }

    public int getNbRuns() {
        return nbRuns;
    }

    public void setNbRuns(int nbRuns) {
        int oldValue = getNbRuns();
        this.nbRuns = nbRuns;
        firePropertyChange(PROPERTY_NB_RUNS, oldValue, nbRuns);
    }

    protected String getArgsAsString() {
        StringBuilder result = new StringBuilder();
        switch (getScript()) {
            case MC:
                append(result, PROPERTY_NB_RUNS, String.valueOf(nbRuns));
                append(result, PROPERTY_NODE, String.valueOf(node + 1));
                break;
            case OAT:
                append(result, PROPERTY_OPTION, "'" + option.getName() + ".csv'");
                break;
        }
        return result.toString();
    }

    protected void append(StringBuilder builder, String property, String value) {
        builder.append(property).append("=").append(value).append(" ");
    }

    //R CMD BATCH --slave --no-restore "--args dexi='MASC2.0.dxi' option='BW1.csv'" OAT.r
    public String getCmd(String mascModelName) {
        String dexiNameArg = process(new String[]{CONTEXT_DEXI_MODEL_NAME, mascModelName}, DEXI_MODEL_NAME);
        return R_CMD + " \"" + R_ARGS + " " + dexiNameArg + " " + getArgsAsString() + "\" " + getScriptFileName();
    }

    public String[] getResultTextFileNames() {
        return script.getResultTextFileNames();
    }

    public File[] getResultTextFiles(File tmpDirectory, String mascFileName, MascModel mascModel) {
        return getFiles(tmpDirectory, mascFileName, mascModel, getResultTextFileNames());
    }

    public String[] getResultImageFileNames() {
        return script.getResultImageFileNames();
    }

    public File[] getResultImageFiles(File tmpDirectory, String mascFileName, MascModel mascModel) {
        return getFiles(tmpDirectory, mascFileName, mascModel, getResultImageFileNames());
    }

    public String getScriptFileName() {
        return script.getScriptFileName();
    }

    public InputStream getStream() {
        String path = "/r/" + getScriptFileName();
        return MascConfig.class.getResourceAsStream(path);
    }

    public File getWorkDir(File workDir) {
        File result;
        if (getId() == 0l) {
            result = null;
        } else {
            result = new File(workDir, WORKDIR_PREFIX + getId());
        }
        return result;
    }

    protected File getLogFile(File workDir) {
        File logFile = new File(getWorkDir(workDir), getScriptFileName() + R_ROUT);
        return logFile;
    }

    public boolean logFileExist(File workDir) {
        return getLogFile(workDir).exists();
    }

    public String getLogs(File workDir) throws IOException {

        // move resources to tmp dir
        String result = FileUtils.readFileToString(getLogFile(workDir));
        return result;
    }

    protected String[] getDefaultContext(MascModel mascModel) {

        Criteria criteria = mascModel.getCriteria(node);

        String criteriaName = criteria == null ? "" : criteria.getName();
        String optionName = option == null ? "" : option.getName();

        return new String[]{CONTEXT_CRITERIA_NAME, criteriaName,
                            CONTEXT_OPTION_NAME, optionName};
    }

    protected File[] getFiles(File tmpDirectory,
                              String mascFileName,
                              MascModel mascModel,
                              String[] resultFileNames) {
        if (resultFileNames == null) {
            return null;
        }
        List<File> result = Lists.newArrayList();

        mascFileName = getRFileName(mascFileName);
        for (String resultFileName : resultFileNames) {
            if (!script.equals(R_SCRIPT.MC) || selectedResult.contains(resultFileName)) {
                resultFileName = process(new String[]{CONTEXT_DEXI_MODEL_NAME, mascFileName}, resultFileName);
                resultFileName = process(getDefaultContext(mascModel), resultFileName);
                result.add(new File(getWorkDir(tmpDirectory), resultFileName));
            }
        }
        return result.toArray(new File[result.size()]);
    }


    protected String process(String[] context, String toProcess) {

        try {
            for (int i = 0; i < context.length; ) {
                String propertyName = context[i++];
                String value = context[i++];

                toProcess = toProcess.replaceAll("\\$\\{" + propertyName + "\\}", value);
            }
        } catch (ArrayIndexOutOfBoundsException eee) {
            throw new IllegalArgumentException("Wrong number of argument "
                                               + context.length
                                               + ", you must have even number.");
        }
        return toProcess;
    }

    public boolean isAlreadyLaunched(File workDirectory) {
        File directory = getWorkDir(workDirectory);
        return directory != null && directory.exists();
    }

}