package fr.inra.masc.model;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Operator on a {@link ThresholdValue}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public enum ThresholdOperator {

    EQ("=", ScaleOrder.NONE, true),
    LT("<", ScaleOrder.ASC, false),
    LE("<=", ScaleOrder.ASC, true),
    GT(">", ScaleOrder.DESC, false),
    GE(">=", ScaleOrder.DESC, true);

    private final String text;

    private final ScaleOrder sign;

    private final boolean canBeEqual;

    ThresholdOperator(String text, ScaleOrder sign, boolean canBeEqual) {
        this.text = text;
        this.sign = sign;
        this.canBeEqual = canBeEqual;
    }

    public String getText() {
        return text;
    }

    public ScaleOrder getSign() {
        return sign;
    }

    public boolean isCanBeEqual() {
        return canBeEqual;
    }

    public static ThresholdOperator fromThresholdValue(ThresholdValue crit) {
        ThresholdOperator result = null;
        switch (crit.getSign()) {

            case DESC:
                if (crit.getCanBeEqual()) {
                    result = GE;
                } else {
                    result = GT;
                }
                break;
            case ASC:
                if (crit.getCanBeEqual()) {
                    result = LE;
                } else {
                    result = LT;
                }
                break;
            case NONE:
                result = EQ;
        }
        return result;
    }

    public static ThresholdOperator fromString(String value) {
        ThresholdOperator result = null;
        for (ThresholdOperator operator : values()) {
            if (operator.getText().equals(value)) {
                result = operator;
                break;
            }
        }
        return result;
    }

}
