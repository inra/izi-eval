/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.model;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.StringUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Object used to render {@link ScaleValue}
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class ScaleImpl extends Scale {

    private static final long serialVersionUID = 1L;

    @Override
    public Collection<ScaleValue> getScaleValue() {
        List<ScaleValue> result = (List<ScaleValue>) super.getScaleValue();
        if (result == null) {
            return result;
        }
        if (ScaleOrder.DESC.equals(getOrder())) {
            Collections.reverse(result);
        }
        return result;
    }

    @Override
    public ScaleValue getScaleValue(int index) {
        List<ScaleValue> result = (List<ScaleValue>) getScaleValue();
        if (result == null || index < 0) {
            return null;
        }
        return result.get(index);
    }

    @Override
    public String toString() {
        return toString(true);
    }

    public String toString(final boolean displayGroup) {

        Collection<ScaleValue> scaleValues = getScaleValue();
        if (scaleValues == null || scaleValues.isEmpty()) {
            return StringUtils.EMPTY;
        }

        StringUtil.ToString<ScaleValue> scaleValueToString = new StringUtil.ToString<ScaleValue>() {

            @Override
            public String toString(ScaleValue scaleValue) {
                String toString = scaleValue.getName();
                if (displayGroup) {
                    toString += getGroupForScaleValueDecoration(scaleValue.getGroup());
                }

                return toString;
            }
        };
        String result = StringUtil.join(scaleValues, scaleValueToString, ";", true);

        return result;
    }

    public static String getGroupForScaleValueDecoration(ScaleGroup scaleGroup) {
        String result;

        if (scaleGroup == null) {
            result = "";
        } else {
            switch (scaleGroup) {
                case BAD:
                    result = "(-)";
                    break;
                case GOOD:
                    result = "(+)";
                    break;
                default:
                    result = "";
            }
        }
        return result;
    }
}
