package fr.inra.masc.services;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jfree.chart.plot.DrawingSupplier;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.PlotState;
import org.jfree.chart.plot.SpiderWebPlot;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.ui.RectangleInsets;
import org.jfree.util.TableOrder;

/*
 * #%L
 * Masc :: API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2014 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.Scale;
import fr.inra.masc.model.ScaleValue;
import fr.inra.masc.utils.MascUtil;

public class MascSpiderWebPlot extends SpiderWebPlot {

    private static final long serialVersionUID = 1L;

    protected List<Option> options;

    protected List<Criteria> criterias;

    protected boolean showClasses;

    protected Font bigLabelFont;
    
    protected DrawingSupplier drawingSupplier;

    public MascSpiderWebPlot(CategoryDataset dataset,
                           List<Option> options,
                           List<Criteria> criterias,
                           boolean showClasses) {
        super(dataset);
        this.options = options;
        this.criterias = criterias;
        this.showClasses = showClasses;
        this.bigLabelFont = new Font("SansSerif", Font.PLAIN, 14);
        
        drawingSupplier = new MascDrawingSupplier(false);
    }

    @Override
    public DrawingSupplier getDrawingSupplier() {
        return drawingSupplier;
    }

    /**
     * Return label font bigger than default one.
     *
     * @return The font (never <code>null</code>).
     */
    public Font getLabelFont() {
        return this.bigLabelFont;
    }

    /**
     * Draws a radar plot polygon.
     *
     * @param g2       the graphics device.
     * @param plotArea the area we are plotting in (already adjusted).
     * @param centre   the centre point of the radar axes
     * @param info     chart rendering info.
     * @param series   the series within the dataset we are plotting
     * @param catCount the number of categories per radar plot
     * @param headH    the data point height
     * @param headW    the data point width
     */
    @Override
    protected void drawRadarPoly(Graphics2D g2,
                                 Rectangle2D plotArea,
                                 Point2D centre,
                                 PlotRenderingInfo info,
                                 int series, int catCount,
                                 double headH, double headW) {

        this.setLabelFont(ImageGeneratorService.FONT);
        g2.setFont(ImageGeneratorService.FONT);

        Map<String, Pair<String, Point2D>> lblToAdd = Maps.newHashMap();

        Polygon polygon = new Polygon();

        // plot the data...
        for (int cat = 0; cat < catCount; cat++) {
            Number dataValue = this.getPlotValue(series, cat);

            if (dataValue != null) {
                double value = dataValue.doubleValue();

                if (value > 0) { // draw the polygon series...

                    // Finds our starting angle from the centre for this axis

                    double angle = this.getStartAngle()
                            + (this.getDirection().getFactor() * cat * 360 / catCount);

                    Criteria criteria = criterias.get(cat);
                    Scale scale = criteria.getScale();
                    Collection<ScaleValue> scaleValues = scale.getScaleValue();
                    int size = scaleValues.size() - 1;

                    Point2D point = this.getWebPoint(plotArea, angle,
                            value / size);

                    polygon.addPoint((int) point.getX(), (int) point.getY());

                    // put an elipse at the point being plotted..

                    // TODO add tooltip/URL capability to this elipse

                    Paint paint = this.getSeriesPaint(series);
                    Paint outlinePaint = this.getSeriesOutlinePaint(series);
                    Stroke outlineStroke = this.getSeriesOutlineStroke(series);

                    Ellipse2D head = new Ellipse2D.Double(point.getX()
                            - headW / 2, point.getY() - headH / 2, headW,
                            headH);
                    g2.setPaint(paint);
                    g2.fill(head);
                    g2.setStroke(outlineStroke);
                    g2.setPaint(outlinePaint);

                    // display classes on point
                    if (showClasses) {
                        Option option = options.get(series);
                        String label = MascUtil.getClasses(value, option, criteria);

                        String id = criteria.getUuid() + label;

                        // map is used to display lbl only one time
                        lblToAdd.put(id, new ImmutablePair<String, Point2D>(label, point));
                    }

                    g2.draw(head);

                    // then draw the axis and category label, but only on the
                    // first time through.....

                    if (series == 0) {
                        Point2D endPoint = this.getWebPoint(plotArea, angle, 1);
                        // 1 = end of axis
                        Line2D line = new Line2D.Double(centre, endPoint);
                        g2.draw(line);
                    }
                }
            }
        }

        // map is used to display lbl only one time
        for (String id : lblToAdd.keySet()) {

            Pair<String, Point2D> value = lblToAdd.get(id);
            String label = value.getLeft();
            Point2D point = value.getRight();
            float x = (float) point.getX();
            float y = (float) point.getY();

            g2.drawString(label, x, y);
        }

        // Plot the polygon
        Stroke lineStroke = this.getSeriesOutlineStroke(series);
        g2.setStroke(lineStroke);

        Paint paint = this.getSeriesPaint(series);
        g2.setPaint(paint);
        g2.draw(polygon);
    }

    /**
     * izi-eval : increase default width to 2
     */
    @Override
    public Stroke getSeriesOutlineStroke(int series) {
        int width = 2;
        Stroke lineStroke = new BasicStroke(width);
        return lineStroke;
    }

    /**
     * Draws the label for one axis.
     *
     * @param g2         the graphics device.
     * @param plotArea   the plot area
     * @param value      the value of the label.
     * @param cat        the category (zero-based index).
     * @param startAngle the starting angle.
     * @param extent     the extent of the arc.
     */
    @Override
    protected void drawLabel(Graphics2D g2, Rectangle2D plotArea, double value,
                             int cat, double startAngle, double extent) {
        FontRenderContext frc = g2.getFontRenderContext();

        String label;
        if (this.getDataExtractOrder() == TableOrder.BY_ROW) {
            // if series are in rows, then the categories are the column keys
            label = getLabelGenerator().generateColumnLabel(this.getDataset(), cat);
        } else {
            // if series are in columns, then the categories are the row keys
            label = getLabelGenerator().generateRowLabel(this.getDataset(), cat);
        }

        Rectangle2D labelBounds = getLabelFont().getStringBounds(label, frc);
        LineMetrics lm = getLabelFont().getLineMetrics(label, frc);
        double ascent = lm.getAscent();

        Arc2D arc1 = new Arc2D.Double(plotArea, startAngle, 0, Arc2D.OPEN);
        Point2D point1 = arc1.getEndPoint();

        double deltaX = -(point1.getX() - plotArea.getCenterX())
                * this.getAxisLabelGap();
        double deltaY = -(point1.getY() - plotArea.getCenterY())
                * this.getAxisLabelGap();

        double labelX = point1.getX() - deltaX;
        double labelY = point1.getY() - deltaY;

        if (labelX < plotArea.getCenterX()) {
            labelX -= labelBounds.getWidth();
        }

        if (labelX == plotArea.getCenterX()) {
            labelX -= labelBounds.getWidth() / 2;
        }

        if (labelY > plotArea.getCenterY()) {
            labelY += ascent;
        }

        double chartWidth = g2.getClipBounds().getWidth();
        List<String> labels = this.justify(label, chartWidth, frc);
        if (labelX < 0) {
            labelX = deltaX;

            double textWidth = point1.getX();
            labels = this.justify(label, textWidth, frc);
        }

        if (labelX + labelBounds.getWidth() > chartWidth) {
            double textWidth = chartWidth - point1.getX();
            labels = this.justify(label, textWidth, frc);

            labelX = chartWidth - textWidth;
        }

        Composite saveComposite = g2.getComposite();

        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                1.0f));
        g2.setPaint(getLabelPaint());
        g2.setFont(getLabelFont());

        for (String lbl : labels) {
            g2.drawString(lbl, (float) labelX,
                    (float) labelY);

            labelY += labelBounds.getHeight() + 5;
        }
        g2.setComposite(saveComposite);
    }

    protected List<String> justify(String label, double textWidth, FontRenderContext frc) {
        String[] words = label.split(" ");
        List<String> result = Lists.newArrayList();
        String tmp = "";
        String tmp2 = "";
        for (String word : words) {
            tmp += word + " ";
            Rectangle2D labelBound = this.getLabelFont().getStringBounds(tmp, frc);
            if (labelBound.getWidth() > textWidth) {

                result.add(tmp2);
                tmp = word + " ";
                tmp2 = "";
            }
            tmp2 += word + " ";
        }

        // add last
        result.add(tmp);

        return result;
    }

}