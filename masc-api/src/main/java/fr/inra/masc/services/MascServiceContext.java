/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import fr.inra.masc.MascConfig;

/**
 * This contract represents objects you must provide when asking for a service.
 * Objects provided may be injected in services returned by
 * {@link MascServiceFactory#newService(Class, MascServiceContext)}
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class MascServiceContext {

    protected MascConfig config;

    protected MascServiceFactory serviceFactory;

    public MascServiceContext(MascConfig config, MascServiceFactory serviceFactory) {
        this.config = config;
        this.serviceFactory = serviceFactory;
    }

    public MascConfig getConfig() {
        return config;
    }

    public <E extends MascService> E newService(Class<E> clazz) {
        return serviceFactory.newService(clazz, this);
    }

}
