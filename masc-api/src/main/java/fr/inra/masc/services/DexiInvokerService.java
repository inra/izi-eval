/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import com.google.common.base.Preconditions;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

/**
 * Service used to invoke DEXi
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class DexiInvokerService extends MascService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DexiInvokerService.class);

    /** Invoke DEXi installation application */
    public void invokeDexiInstall() {
        File dexiInstallationFile = getConfig().getDexiInstallationFile();

        Preconditions.checkNotNull(dexiInstallationFile);

        final CommandLine commandLine = CommandLineUtils.newCommand(dexiInstallationFile);

        // launch DEXi installation in other thread
        Thread tread = new Thread(new Runnable() {

            @Override
            public void run() {
                // launch DEXi installation in other thread
                CommandLineUtils.invokeCommandeLine(log, commandLine, "Failed to launch DEXi installation", getTmpDirectory());
            }
        });
        tread.start();
    }

    /**
     * Invoke DEXi application
     *
     * @param importFile file to open in DEXi
     */
    public void invokeDexi(File importFile) {

        File dexiApp = getConfig().getDexiExecutableFile();

        Preconditions.checkNotNull(dexiApp);

        final CommandLine commandLine = CommandLineUtils.newCommand(dexiApp);

        if (importFile != null) {

            // open open dexi file
            String dexiInputFile = importFile.getPath();
            commandLine.addArgument(dexiInputFile);

            if (log.isDebugEnabled()) {
                log.debug("Starting DEXi with input file '" + dexiInputFile + "'");
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Starting DEXi without input file");
            }
        }

        // launch DEXi in other thread
        Thread tread = new Thread(new Runnable() {

            @Override
            public void run() {
                // launch DEXi in other thread
                CommandLineUtils.invokeCommandeLine(log, commandLine, "Failed to launch DEXi", getTmpDirectory());
            }
        });
        tread.start();
    }
}
