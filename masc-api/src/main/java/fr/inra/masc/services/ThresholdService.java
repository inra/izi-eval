package fr.inra.masc.services;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import fr.inra.masc.MascTechnicalException;
import fr.inra.masc.io.parser.CsvThresholdsParser;
import fr.inra.masc.io.parser.ThresholdsParser;
import fr.inra.masc.io.writer.CsvThresholdsWriter;
import fr.inra.masc.io.writer.ThresholdsWriter;
import fr.inra.masc.model.Criterias;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.ThresholdValue;
import fr.inra.masc.model.ThresholdValueImpl;
import fr.inra.masc.utils.MascUtil;

/**
 * Service to import/export thresholds.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.8
 */
public class ThresholdService extends MascService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ThresholdService.class);

    public Set<String> loadThresholds(File file, MascModel model) {

        if (log.isDebugEnabled()) {
            log.debug("Load threshold from file: " + file);
        }
        Set<String> unknownCriterias = Sets.newHashSet();

        List<EditableCriteria> criterias;

        try {
            if (file.getName().toLowerCase().endsWith(".csv")) {
                CsvThresholdsParser parser = new CsvThresholdsParser();
                criterias = parser.getModel(file);
            } else {
                ThresholdsParser parser = new ThresholdsParser();
                criterias = parser.getModel(file);
            }

        } catch (IOException e) {
            throw new MascTechnicalException(
                    "Error reading threshold file '" +
                    file.getName() + "'", e);
        }

        // get editable criterias from model
        List<EditableCriteria> editableCriterias =
                MascUtil.extractAllEditableCriterias(model);

        // reset all values
        for (EditableCriteria editableCriteria : editableCriterias) {
            editableCriteria.setUnit(null);
            editableCriteria.setValues(null);
        }

        // index them by name

        Multimap<String, EditableCriteria> editableCriteriaByName =
                Criterias.criteriaByName(editableCriterias);

        for (EditableCriteria criteria : criterias) {

            Collection<EditableCriteria> criteriaToSet =
                    editableCriteriaByName.get(criteria.getName());

            if (CollectionUtils.isEmpty(criteriaToSet)) {

                unknownCriterias.add(criteria.getName());
            } else {

                replaceCriteriathresholdValues(criteria, criteriaToSet);
            }
        }

        return unknownCriterias;

    }

    private void replaceCriteriathresholdValues(EditableCriteria loadedCriteria,
                                                Collection<EditableCriteria> criteriaToSet) {

        List<ThresholdValue> values = loadedCriteria.getValues();
        String unit = loadedCriteria.getUnit();

        Iterator<EditableCriteria> iterator = criteriaToSet.iterator();

        {

            // for first criteria copy straight values

            EditableCriteria criteria = iterator.next();
            criteria.setUnit(unit);
            criteria.setValues(values);
        }

        while (iterator.hasNext()) {

            // for others must create a fresh copy of values

            EditableCriteria criteria = iterator.next();
            criteria.setUnit(unit);

            List<ThresholdValue> newValues = Lists.newArrayList();
            for (ThresholdValue thresholdValue : values) {
                newValues.add(new ThresholdValueImpl(thresholdValue));
            }
            criteria.setValues(newValues);
        }
    }

    public void saveThresholds(List<EditableCriteria> criterias, File file) {
        if (log.isDebugEnabled()) {
            log.debug("Will export threshold to file: " + file);
        }
        try {
            if (file.getName().toLowerCase().endsWith(".csv")) {
                CsvThresholdsWriter writer = new CsvThresholdsWriter();
                writer.write(criterias, file);
            } else {
                ThresholdsWriter writer = new ThresholdsWriter();
                writer.write(criterias, file);
            }
        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Failed to write thresholds file", eee);
        }
    }
}
