/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.i18n.I18n;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.google.common.collect.Lists;
import com.lowagie.text.DocumentException;

import fr.inra.masc.MascConfig;
import fr.inra.masc.MascTechnicalException;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.RScriptModel;
import fr.inra.masc.reports.ReportModel;
import fr.inra.masc.utils.MascUtil;
import freemarker.core.Environment;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateModelException;

/**
 * Service to generate PDF reports
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class ReportGeneratorService extends MascService {

    protected enum Images {
        LAST_NODE("lastnode"),
        NODE("node"),
        VLINE("vline");

        protected String fileName;

        Images(String fileName) {
            this.fileName = fileName;
        }

        public InputStream getStream() {
            return MascConfig.class.getResourceAsStream("/images/" + fileName + PNG_EXT);
        }

        public String getFileName() {
            return fileName + PNG_EXT;
        }
    }

    public enum Reports {
        LAYOUT,
        DESCRIPTION,
        TREE,
        TREE_DESCRIPTION,
        THRESHOLDS,
        OPTIONS,
        GRAPHIC,
        SYNOPTIC,
        AS
    }

    public static final String WIDTH_PREFIX = "Width";

    public static final String HEIGHT_PREFIX = "Height";

    public static final String PNG_EXT = ".png";

    public static final String PNG_FORMAT_NAME = "png";

    public static enum ImageType {
        GRAPHIC("graphicImage", 650, 1000),
        SYNOPTIC("synopticImage", 1000, 650);

        private String name;

        private int maxWidth;

        private int maxHeight;

        private ImageType(String name, int maxWidth, int maxHeight) {
            this.name = name;
            this.maxWidth = maxWidth;
            this.maxHeight = maxHeight;
        }

        public String getName() {
            return name;
        }

        public int getMaxWidth() {
            return maxWidth;
        }

        public int getMaxHeight() {
            return maxHeight;
        }
    }

    public void generateReport(String mascFileName, ReportModel reportModel) {
        generateReport(mascFileName, reportModel, false, false);
    }

    public void generateReport(String mascFileName, ReportModel reportModel, boolean removePageBreak, boolean landscape) {
        String processed = generateReportHTML(mascFileName, reportModel, landscape);

        if (removePageBreak) {
            processed = processed.replaceAll(" class=\"pageBreak\"", "");
        }

        try {
            // write html file
            File reportFile = File.createTempFile("report", ".html");
            FileUtils.write(reportFile, processed, "utf8");

            // generate pdf
            generatePdf(reportModel, processed);
        } catch (Exception eee) {
            throw new MascTechnicalException("Failed to generate pdf", eee);
        }
    }

    public String generateReportHTML(String mascFileName, ReportModel reportModel) {
        return generateReportHTML(mascFileName, reportModel, false);
    }

    public String generateReportHTML(String mascFileName, ReportModel reportModel, boolean landscape) {
        List<Reports> reportsToGenerate = Lists.newArrayList();

        if (reportModel.isShowDescription()) {
            reportsToGenerate.add(Reports.DESCRIPTION);
        }
        if (reportModel.isShowTree()) {
            reportsToGenerate.add(Reports.TREE);
            reportsToGenerate.add(Reports.TREE_DESCRIPTION);
        }
        if (reportModel.isShowThresholds()) {
            reportsToGenerate.add(Reports.THRESHOLDS);
        }
        if (reportModel.isShowOptions()) {
            reportsToGenerate.add(Reports.OPTIONS);
        }
        boolean showGraphic = reportModel.isShowGraphic() && reportModel.getGraphic() != null;
        if (showGraphic) {
            reportsToGenerate.add(Reports.GRAPHIC);
        }
        boolean showSynaptic = reportModel.isShowSynoptic() && reportModel.getSynoptic() != null;
        if (showSynaptic) {
            reportsToGenerate.add(Reports.SYNOPTIC);
        }
        RScriptModel scriptModel = reportModel.getScriptModel();
        boolean showAs = reportModel.isShowAs() && scriptModel != null;
        if (showAs) {
            reportsToGenerate.add(Reports.AS);
        }

        // creating template context
        Map<String, Object> context = new HashMap<String, Object>();
        MascModel mascModel = reportModel.getMascModel();
        context.put("mascModel", mascModel);
        context.put("landscape", landscape);

        // static import
        // add masc utils
        addStaticImportToContext(context, MascUtil.class);

        // add i18n
        addStaticImportToContext(context, I18n.class);

        // add stringUtils
        addStaticImportToContext(context, StringUtils.class);

        // add StringEscapeUtils
        addStaticImportToContext(context, StringEscapeUtils.class);

        // move resources to tmp dir
        String workDir = getConfig().getTmpDirectory().getAbsolutePath();
        context.put("workDir", workDir);

        // copy images to work dir
        try {
            for (Images image : Images.values()) {
                File file = new File(workDir + File.separator + image.getFileName());
                IOUtils.copy(image.getStream(), new FileOutputStream(file));
            }
        } catch (Exception eee) {
            throw new MascTechnicalException(
                    "Failed to copy images files to work dir : " + workDir, eee);
        }

        // write images in tmp file
        if (showGraphic) {
            addImageToContext(context, reportModel.getGraphic(), ImageType.GRAPHIC);
        }
        if (showSynaptic) {
            addDocumentToContext(context, reportModel.getSynoptic(), ImageType.SYNOPTIC);
        }
        File tmpDirectory = getConfig().getTmpDirectory();
        if (showAs) {
            File[] resultImageFiles = scriptModel.getResultImageFiles(tmpDirectory, mascFileName, mascModel);
            List<String> resultImagePath = Lists.newArrayList();
            if (resultImagePath != null) {
                for (File image : resultImageFiles) {
                    resultImagePath.add(image.getPath());
                }
            }
            context.put("asImages", resultImagePath);

            File[] resultTextFiles = scriptModel.getResultTextFiles(tmpDirectory, mascFileName, mascModel);
            List<String> texts = Lists.newArrayList();
            if (resultTextFiles != null) {
                for (File textFile : resultTextFiles) {
                    try {
                        texts.add(FileUtils.readFileToString(textFile));
                    } catch (IOException eee) {
                        throw new MascTechnicalException(
                                "Failed to read file" + textFile, eee);
                    }
                }
            }
            context.put("asTexts", texts);
            context.put("scriptName", scriptModel.getScript().getName());
        }

        // render template
        StringWriter contentWriter = new StringWriter();
        for (Reports report : reportsToGenerate) {

            processTemplate(report, context, contentWriter);
        }

        String content = contentWriter.toString();
        context.put("content", content);

        StringWriter processedWriter = new StringWriter();
        processTemplate(Reports.LAYOUT, context, processedWriter);

        return processedWriter.toString();
    }

    protected void addStaticImportToContext(Map<String, Object> context, Class toImport) {
        BeansWrapper wrapper = BeansWrapper.getDefaultInstance();
        TemplateHashModel staticModels = wrapper.getStaticModels();
        TemplateHashModel mascUtilStatic;
        try {
            mascUtilStatic = (TemplateHashModel) staticModels.get(toImport.getName());
        } catch (TemplateModelException eee) {
            throw new MascTechnicalException("Failed to add static import '"
                                             + toImport.getName() + "' in freemaker context", eee);
        }
        context.put(toImport.getSimpleName(), mascUtilStatic);
    }

    protected void addImageToContext(Map<String, Object> context, BufferedImage image, ImageType imageType) {
        try {
            File tmpFile = File.createTempFile(imageType.getName(), PNG_EXT);
            int width = image.getWidth();
            int height = image.getHeight();

//            rotate if image is too large
//            if (width > MAX_WIDTH && rotateIfNeeded) {
//                image = MascUtil.rotateImage90(image);
//            }
//
//            width = image.getWidth();

            // resolve images width
            int maxWidth = imageType.getMaxWidth();
            if (width > maxWidth) {
                height = (maxWidth * height) / width;

                width = maxWidth;
            }

            // resolve images height
            int maxHeight = imageType.getMaxHeight();
            if (height > maxHeight) {
                width = (maxHeight * width) / height;

                height = maxHeight;
            }

            ImageIO.write(image, PNG_FORMAT_NAME, tmpFile);
            context.put(imageType.getName(), tmpFile.getPath());
            context.put(imageType.getName() + WIDTH_PREFIX, width);
            context.put(imageType.getName() + HEIGHT_PREFIX, height);

        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Failed to write graphic images", eee);
        }
    }

    protected void addDocumentToContext(Map<String, Object> context, Document doc, ImageType imageType) {
        try {

            File tmpFile = File.createTempFile(imageType.getName(), PNG_EXT);
            PNGTranscoder pngTranscoder = new PNGTranscoder();
            pngTranscoder.transcode(new TranscoderInput(doc), new TranscoderOutput(new FileOutputStream(tmpFile)));
            context.put(imageType.getName(), tmpFile.getPath());

            BufferedImage image = ImageIO.read(tmpFile);

            int width = image.getWidth();
            int height = image.getHeight();

//            rotate if image is too large
//            if (width > MAX_WIDTH && rotateIfNeeded) {
//                image = MascUtil.rotateImage90(image);
//            }
//
//            width = image.getWidth();

            // resolve images width
            int maxWidth = imageType.getMaxWidth();
            if (width > maxWidth) {
                height = (maxWidth * height) / width;

                width = maxWidth;
            }

            // resolve images height
            int maxHeight = imageType.getMaxHeight();
            if (height > maxHeight) {
                width = (maxHeight * width) / height;

                height = maxHeight;
            }

            context.put(imageType.getName() + WIDTH_PREFIX, width);
            context.put(imageType.getName() + HEIGHT_PREFIX, height);

        } catch (Throwable eee) {
            throw new MascTechnicalException(
                    "Failed to write graphic images", eee);
        }
    }

    protected void processTemplate(Reports report, Map<String, Object> context, StringWriter content) {

        try {
            // get template
            Template template = getTemplate(report);

            Environment env = template.createProcessingEnvironment(context, content);

            env.setOutputEncoding("utf8");
            env.process();
        } catch (TemplateException eee) {
            throw new MascTechnicalException(
                    "Failed to generating template file '" + report.name() + "'", eee);
        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Failed to read template file '" + report.name() + "'", eee);
        } catch (URISyntaxException eee) {
            throw new MascTechnicalException(
                    "Failed to read template file '" + report.name() + "'", eee);
        }
    }

    protected void generatePdf(ReportModel reportModel, String htmlContent) throws IOException, DocumentException {
        OutputStream os = new FileOutputStream(reportModel.getReportFile());

        try {
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(htmlContent);
            renderer.layout();
            renderer.createPDF(os);
            os.close();
        } finally {
            IOUtils.closeQuietly(os);
        }

    }

    protected Template getTemplate(Reports report) throws URISyntaxException {
        String reportName = report.name().toLowerCase() + ".ftl";
        InputStream stream = MascConfig.class.getResourceAsStream(getConfig().getTemplateDir() + reportName);

        if (stream == null) {
            throw new MascTechnicalException(
                    "failed to find template for '" + reportName + "'");
        }

        // Configuration
        Configuration config = new Configuration();

        // Encoding par default
        config.setDefaultEncoding("utf8");

        try {
            return new Template(report.name(), new InputStreamReader(stream), config);
        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Failed to read template file '" + report.name() + "'", eee);
        }
    }
}
