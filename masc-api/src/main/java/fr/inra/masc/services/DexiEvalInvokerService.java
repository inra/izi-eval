/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import fr.inra.masc.MascConfig;
import fr.inra.masc.MascTechnicalException;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionImpl;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.model.OptionValueImpl;
import fr.inra.masc.utils.MascUtil;

/**
 * Service used to invoke DEXi Eval
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class DexiEvalInvokerService extends MascService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(DexiEvalInvokerService.class);

    public static final String VALUES_SEPARATOR = ";";

    public void evalMascModel(MascModel mascModel, File mascFile) {

        // evaluate by DEXiEval
        File resultFile = createEvaluatedOptionFile(mascFile);

        // apply to incoming model
        importOptionsValues(mascModel, resultFile, false);
    }

    public MascModel importOptionsValues(MascModel mascModel,
                                         File optionModelFile,
                                         boolean threshold) {

        // load criterias values from result file
        List<Pair<String, String[]>> criterias = Lists.newLinkedList();
        String[] header = readEvaluatedOptionFile(optionModelFile, criterias);
        applyToModel(mascModel, criterias, threshold, header);

        return mascModel;
    }

    /**
     * Invoke DExIEval to produce evaluated option file
     *
     * @param mascFile file to open in DEXiEval
     * @return evaluated file, null if DEXiEval is not configured
     */
    protected File createEvaluatedOptionFile(File mascFile) {

        File csvOptionFile = newTempFile("option", ".csv");

        if (log.isDebugEnabled()) {
            log.debug("CsvOptionFile: " + csvOptionFile);
        }

        // invoke DEXiEval
        if (!invokeDexiEval("-save", "-in", mascFile, csvOptionFile)) {
            csvOptionFile = null;
        }

        File resultFile = newTempFile("option-result", ".csv");

        if (log.isDebugEnabled()) {
            log.debug("EvaluatedOptionFile: " + resultFile);
        }

        // invoke DEXiEval
        if (!invokeDexiEval("-in", mascFile, csvOptionFile, resultFile)) {
            resultFile = null;
        }
        return resultFile;
    }

    /**
     * Invoke DExIEval application with arguments
     *
     * @param args arguments
     * @return false if DEXiEval is not configured
     */
    protected boolean invokeDexiEval(Object... args) {

        File dexyEvalApp = getConfig().getDexiEvalExecutableFile();

        boolean result = false;

        //FIXME-tc 2012-11-12 should throw an error if dexi eval does not exists...
        if (dexyEvalApp != null) {

            CommandLine commandLine = CommandLineUtils.newCommand(dexyEvalApp, args);

            CommandLineUtils.invokeCommandeLine(
                    log, commandLine, "Failed to launch DEXiEval",
                    getTmpDirectory());
            result = true;
        }

        return result;
    }

    protected String[] readEvaluatedOptionFile(File dexiEvalResult,
                                               List<Pair<String, String[]>> evaluatedCriterias) {
        try {
            BufferedReader reader = FileUtil.getReader(dexiEvalResult);

            try {
                // read line
                String line = reader.readLine();

                // first is header
                String[] header = line.split(MascConfig.CSV_SEPARATOR);

                // read line
                line = reader.readLine();

                // parse all file
                while (StringUtils.isNotEmpty(line)) {

                    String[] splitedLine =
                            line.split("\"" + MascConfig.CSV_SEPARATOR);

                    // first is the key
                    String key = splitedLine[0].replaceAll("\"", "");

                    // parse values
                    String[] values = new String[header.length];
                    for (int i = 1; i < splitedLine.length; i++) {
                        String value = splitedLine[i];

                        if (StringUtils.isNotBlank(value)) {
                            value = value.replaceAll("\"", "");
                            if (!value.equals("*")) {
                                values[i] = value;
                            } else {
                                values[i] = null;
                            }
                        }
                    }

                    Pair<String, String[]> entry = Pair.of(key, values);

                    evaluatedCriterias.add(entry);

                    // read line
                    line = reader.readLine();
                }
                return header;

            } finally {
                reader.close();
            }
        } catch (IOException eee) {
            throw new MascTechnicalException("Failed to read result file", eee);
        }
    }

    protected void applyToModel(MascModel mascModel,
                                List<Pair<String, String[]>> evaluatedCriterias,
                                boolean threshold,
                                String... header) {

        // impact masc model
        // get options
        Map<String, Option> options = MascUtil.extractAllOptions(mascModel);

        List<Criteria> criterias = MascUtil.extractAllCriterias(mascModel);

        for (Pair<String, String[]> entry : evaluatedCriterias) {

            String name = entry.getKey();

            for (Criteria criteria : criterias) {
                if (name.equals(criteria.getName())) {

                    String[] values = entry.getValue();

                    if (values != null) {

                        // skip first header
                        for (int i = 1; i < values.length; i++) {

                            String value = values[i];
                            String optionName = header[i];

                            // remove "
                            optionName = optionName.replace("\"", "");

                            Option option = options.get(optionName);

                            // if not found create new one
                            if (option == null) {
                                option = new OptionImpl();
                                option.setName(optionName);
                                mascModel.addOption(option);
                                options.put(optionName, option);
                            }
                            Multimap<String, OptionValue> criteriaOptionValueMultimap = MascUtil.extractAllOptionValues(option);

                            // initialize option values
                            if (option.getOptionValue() == null) {
                                List<OptionValue> optionValues = Lists.newArrayList();
                                option.setOptionValue(optionValues);
                            }

                            applyToCriteria(criteriaOptionValueMultimap, option, criteria, value, threshold);

                            while (criteria.isReference()) {

                                criteria = criteria.getChild(0);

                                applyToCriteria(criteriaOptionValueMultimap, option, criteria, value, threshold);
                            }
                        }
                    }
                }
            }
        }
    }

    protected void applyToCriteria(Multimap<String, OptionValue> criteriaOptionValueMultimap,
                                   Option option,
                                   Criteria criteria,
                                   String value,
                                   boolean threshold) {

        Collection<OptionValue> optionValues = Lists.newArrayList(criteriaOptionValueMultimap.get(criteria.getUuid()));

        int optionValueCount = 0;
        if (value != null) {
            // some values are separated by ;
            String[] splitedValues = value.split(VALUES_SEPARATOR);
            List<String> splitedValuesList = Lists.newArrayList(splitedValues);

            for (String splitedValue : splitedValuesList) {

                OptionValue optionValue;
                if (optionValues.size() >= ++optionValueCount) {
                    optionValue = Lists.newArrayList(optionValues).get(optionValueCount - 1);
                } else {
                    optionValue = new OptionValueImpl();
                    optionValue.setCriteria(criteria);

                    option.addOptionValue(optionValue);
                }

                if (threshold) {
                    double inputValue = Double.parseDouble(splitedValue);
                    optionValue.setInputValue(inputValue);
                } else {
                    int valueInt = Integer.parseInt(splitedValue);

                    // Dexi eval return values + 1
                    valueInt -= 1;
                    if (criteria.getScale().sizeScaleValue() > valueInt) {
                        optionValue.setValue(valueInt);
                    }
                }
            }

        } else {
            if (!optionValues.isEmpty()) {
                OptionValue optionValue = Lists.newArrayList(optionValues).get(optionValueCount);
                optionValue.setValue(null);
                optionValueCount++;
            }
        }

        // il reste dans la memoire plus d'Option#optionValues que de resultats
        // renvoyé par dexieval on supprime les optionsValues en trop pour eviter
        // les duplications d'affichage
        for (int i = optionValueCount; i < optionValues.size(); i++) {
            OptionValue ov = Lists.newArrayList(optionValues).get(i); // WTF !!!
            option.getOptionValue().remove(ov);
        }
    }

    protected File newTempFile(String prefix, String suffix) {
        File tmpDirectory = getTmpDirectory();
        File result = new File(tmpDirectory, prefix + "_" + System.nanoTime() + suffix);
        return result;
    }
}
