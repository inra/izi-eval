/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011, 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.StringReader;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DrawingSupplier;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.w3c.dom.Document;
import org.w3c.dom.svg.SVGDocument;

import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.view.mxGraph;

import fr.inra.masc.charts.ChartModel;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.model.ScaleValue;
import fr.inra.masc.synoptic.CriteriaGraphBuilder;
import fr.inra.masc.synoptic.SynopticModel;
import fr.inra.masc.utils.MascUtil;

/**
 * Service used to generate {@link JFreeChart}
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class ImageGeneratorService extends MascService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ImageGeneratorService.class);

    public static final int IMAGE_GRAPH_WIDTH = 800;

    public static final int IMAGE_GRAPH_HEIGHT = 620;

    public static final double ZERO = 0.00000000000001;

    public static final Font FONT = new Font("SansSerif", Font.PLAIN, 14);

    /** Font des valeurs de la legende. */
    public static final Font LEGEND_FONT = FONT.deriveFont(20f);
    /** Font du titre de la legende. */
    public static final Font LEGEND_FONT_TITLE = LEGEND_FONT.deriveFont(Font.BOLD);

    public JFreeChart getChart(ChartModel chartModel) {

        List<Option> selectedOptions = chartModel.getSelectedOptions();
        if (selectedOptions == null) {
            selectedOptions = Lists.newArrayList();
        }
        List<Criteria> selectedCriterias = chartModel.getSelectedCriterias();
        JFreeChart result;
        switch (selectedCriterias.size()) {
            case 0:
                result = null;
                break;
            case 1:
                result = getBarChart(selectedOptions, selectedCriterias.get(0));
                break;
            case 2:
                result = getLineChart(selectedOptions, selectedCriterias);
                break;
            default:
                result = getSpiderChart(selectedOptions, selectedCriterias, chartModel.isShowClasses());
        }
        return result;
    }

    protected SymbolAxis getScaleValueAxis(Criteria criteria) {

        Collection<ScaleValue> scaleValues = criteria.getScale().getScaleValue();
        List<String> scaleValuesString = Lists.newArrayList();
        scaleValuesString.add(MascUtil.OPTION_VALUE_DEFAULT);
        for (ScaleValue scaleValue : scaleValues) {
            scaleValuesString.add(scaleValue.getName());
        }
        SymbolAxis symbolaxis = new SymbolAxis(criteria.getName(), scaleValuesString.toArray(new String[scaleValuesString.size()]));
//        symbolaxis.setTickLabelFont(JFreeChart.DEFAULT_TITLE_FONT);
        symbolaxis.setGridBandsVisible(false);
        symbolaxis.setAutoRangeIncludesZero(true);
        symbolaxis.setAutoRange(true);
        symbolaxis.setAutoRangeStickyZero(true);
        symbolaxis.setAutoTickUnitSelection(true);
        symbolaxis.setTickMarksVisible(true);

        symbolaxis.setLabelFont(LEGEND_FONT_TITLE);
        symbolaxis.setTickLabelFont(LEGEND_FONT);

        return symbolaxis;
    }

    protected JFreeChart getBarChart(List<Option> options, Criteria criteria) {

        JFreeChart jfreechart = ChartFactory.createBarChart3D(null,
                                                              "Options",
                                                              criteria.getName(),
                                                              getSimpleDataSet(options, Lists.newArrayList(criteria)),
                                                              PlotOrientation.HORIZONTAL,
                                                              false, true, false);

        CategoryPlot categoryplot = (CategoryPlot) jfreechart.getPlot();
        categoryplot.setDomainGridlinesVisible(true);
        categoryplot.setRangeAxis(getScaleValueAxis(criteria));

        categoryplot.getDomainAxis().setTickLabelFont(LEGEND_FONT);
        categoryplot.getDomainAxis().setLabelFont(LEGEND_FONT_TITLE);

        BarRenderer3D renderer = (BarRenderer3D) categoryplot.getRenderer(0);

        // get ratio with option size
        double maxBarWidth = 0.5 / options.size();
        renderer.setMaximumBarWidth(maxBarWidth);

        renderer.setBaseItemLabelGenerator(new MascLabelGenerator(options, criteria));
        renderer.setBaseItemLabelsVisible(true);

        return jfreechart;
    }

    protected static class MascLabelGenerator implements CategoryItemLabelGenerator {

        protected List<Option> options;
        protected Criteria criteria;

        public MascLabelGenerator(List<Option> options, Criteria criteria) {
            this.options = options;
            this.criteria = criteria;
        }

        @Override
        public String generateRowLabel(CategoryDataset dataset, int row) {
            return null;
        }

        @Override
        public String generateColumnLabel(CategoryDataset dataset, int column) {
            return null;
        }

        @Override
        public String generateLabel(CategoryDataset dataset, int row, int column) {

            List<String> inputValues = MascUtil.getInputValues(options.get(column), criteria, true);

            return StringUtils.join(inputValues, ", ");
        }
    }

    protected JFreeChart getLineChart(List<Option> options, List<Criteria> criterias) {

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        Criteria criteria1 = criterias.get(0);
        Criteria criteria2 = criterias.get(1);
        for (Option option : options) {

            List<OptionValue> optionValuesForCriteria1 = Lists.newArrayList(MascUtil.getOptionValuesForCriteria(option, criteria1));
            Collection<OptionValue> optionValuesForCriteria2 = MascUtil.getOptionValuesForCriteria(option, criteria2);

            Map<String, Collection<Integer>> linkedHashMap = Maps.newLinkedHashMap();
            Multimap<String, Integer> resolvedValues = Multimaps.newMultimap(linkedHashMap, new Supplier<Collection<Integer>>() {

                @Override
                public Collection<Integer> get() {
                    List<Integer> instance = Lists.newLinkedList();
                    return instance;
                }
            });

            // init map with null values
            for (ScaleValue scaleValue : criteria1.getScale().getScaleValue()) {
                resolvedValues.put(scaleValue.getName(), null);
            }

            int i = 0;
            for (OptionValue optionValue2 : optionValuesForCriteria2) {
                OptionValue optionValue1 = null;
                if (optionValuesForCriteria1.size() > i) {
                    optionValue1 = optionValuesForCriteria1.get(i);
                }
                Integer value = optionValue2.getValue();
                String scaleName = MascUtil.OPTION_VALUE_DEFAULT;
                if (value != null && optionValue1 != null) {
                    ScaleValue scaleValue = criteria1.getScale().getScaleValue(optionValue1.getValue());

                    if (scaleValue != null) {
                        scaleName = scaleValue.getName();
                    }
                }
                resolvedValues.put(scaleName, value);
            }

            for (String scaleName : resolvedValues.keySet()) {
                Collection<Integer> values = resolvedValues.get(scaleName);
                for (Integer value : values) {
                    dataset.addValue(value, option.getName(), scaleName);
                }
            }
        }

        // definition des parametre jfreechart
        CategoryAxis categoryAxis = new CategoryAxis(criteria1.getName());
        //categoryAxis.setLabelFont(LEGEND_FONT);
        ValueAxis valueAxis = new NumberAxis(criteria2.getName());
        //valueAxis.setLabelFont(LEGEND_FONT);

        // definition d'un drawingSupplier personalisé pour pouvoir augmenter la taille des marqueurs
        LineAndShapeRenderer lineandshaperenderer = new LineAndShapeRenderer() {
            protected DrawingSupplier drawingSupplier = new MascDrawingSupplier(true);
            @Override
            public DrawingSupplier getDrawingSupplier() {
                return drawingSupplier;
            }  
        };
        for (int i = 0; i < options.size(); i++) {
            lineandshaperenderer.setSeriesShapesVisible(i, true);
            lineandshaperenderer.setSeriesLinesVisible(i, false);
        }
        lineandshaperenderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());

        // configuration du rendu de la partie 'chart' (hors légende)
        CategoryPlot categoryplot = new CategoryPlot(dataset, categoryAxis, valueAxis, lineandshaperenderer);
        categoryplot.setRangeAxis(getScaleValueAxis(criteria2));
        categoryplot.getDomainAxis().setTickLabelFont(LEGEND_FONT);
        categoryplot.getDomainAxis().setLabelFont(LEGEND_FONT_TITLE);
        categoryplot.getRangeAxis().setTickLabelFont(LEGEND_FONT);
        categoryplot.getRangeAxis().setLabelFont(LEGEND_FONT_TITLE);
        categoryplot.setDomainGridlinePaint(Color.lightGray);
        categoryplot.setRangeGridlinePaint(Color.lightGray);
        categoryplot.setBackgroundPaint(Color.WHITE); // fond du la partie plot (gris)
        categoryplot.setDomainGridlinesVisible(true);
        categoryplot.setDrawSharedDomainAxis(true);
        categoryplot.setRangeGridlinesVisible(true);
        //categoryplot.setBackgroundPaint(Color.WHITE);

        // construction du graphique
        JFreeChart jfreechart = new JFreeChart(null, FONT, categoryplot, true);
        jfreechart.setBackgroundPaint(Color.WHITE); // fond blanc

        // update legend size
        LegendTitle legendTitle = jfreechart.getLegend();
        legendTitle.setItemFont(LEGEND_FONT);

        return jfreechart;
    }

    protected JFreeChart getSpiderChart(List<Option> options, List<Criteria> criterias, boolean showClasses) {
        DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();

        for (Criteria criteria : criterias) {

            for (Option option : options) {

                String optionName = option.getName();
                String criteriaName = criteria.getName();

                Collection<OptionValue> optionValuesForCriteria = MascUtil.getOptionValuesForCriteria(option, criteria);

                for (OptionValue optionValue : optionValuesForCriteria) {
                    if (optionValue != null && optionValue.getValue() != null) {
                        Double value = (double) optionValue.getValue();

                        // FIXME sletellier 20110125: its a hack to solve JFreeChart bug, if we add value 0, no criteria name is displayed...
                        if (value == 0) {
                            value = ZERO;
                        }
                        categoryDataset.addValue(value, optionName, criteriaName);
                    } else {
                        categoryDataset.addValue(ZERO, optionName, criteriaName);
                    }
                }
            }
        }

        MascSpiderWebPlot spiderwebplot = new MascSpiderWebPlot(categoryDataset, options, criterias, showClasses);

        JFreeChart jfreechart = new JFreeChart(null, FONT, spiderwebplot, true);

        // update legend size
        LegendTitle legendTitle = jfreechart.getLegend();
        legendTitle.setItemFont(LEGEND_FONT);

        return jfreechart;
    }

    protected CategoryDataset getSimpleDataSet(List<Option> options,
                                               Collection<Criteria> criterias) {
        DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();

        for (Option option : options) {

            for (Criteria criteria : criterias) {

                Collection<OptionValue> optionValuesForCriteria = MascUtil.getOptionValuesForCriteria(option, criteria);

                List<Integer> values = Lists.newArrayList();
                for (OptionValue optionValue : optionValuesForCriteria) {
                    if (optionValue != null) {
                        Integer value = optionValue.getValue();

                        if (value != null) {
                            values.add(value);
                        }

                    }
                }
                String scaleName = MascUtil.OPTION_VALUE_DEFAULT;
                int finalValue = 0;
                if (!values.isEmpty()) {
                    // get average
                    int sum = 0;
                    for (Integer value : values) {
                        sum += value;
                    }
                    finalValue = sum / values.size();
                }
                categoryDataset.setValue(finalValue + 1, scaleName, option.getName());
            }
        }
        return categoryDataset;
    }

    public BufferedImage getChartImage(JFreeChart chart) {
        return chart.createBufferedImage(IMAGE_GRAPH_WIDTH, IMAGE_GRAPH_HEIGHT);
    }

    public SVGDocument convertModelToTreeImage(CriteriaGraphBuilder builder,
                                                 SynopticModel model) {

        mxGraph graph = builder.build(model);

        mxRectangle graphBounds = graph.getGraphBounds();

        int height = (int)graphBounds.getHeight() + 10;
        graphBounds.setHeight(height);
        graphBounds.setY(-10);

        if (log.isDebugEnabled()) {
            log.debug("width[" + graphBounds.getWidth() + "] height[" + graphBounds.getHeight() + "]");
        }

        Document svgDocument = mxCellRenderer.createSvgDocument(graph,
                                                                null,
                                                                1,
                                                                model.getBackgroundColor(),
                                                                graphBounds);

        String xml = mxXmlUtils.getXml(svgDocument);
        String uri = "http://www.w3.org/2000/svg";

        StringReader reader = new StringReader(xml);
        try {
            String parser = XMLResourceDescriptor.getXMLParserClassName();
            SAXSVGDocumentFactory f = new SAXSVGDocumentFactory(parser);
            SVGDocument result = f.createSVGDocument(uri, reader);

            return result;
        } catch (Exception ex) {
            log.error("Failed to convert document to svg one", ex);
            return null;
        } finally {
            reader.close();
        }
    }
}
