package fr.inra.masc.services;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import fr.inra.masc.MascTechnicalException;
import fr.inra.masc.utils.MascUtil;
import fr.inra.masc.charts.ChartModel;
import fr.inra.masc.io.parser.DexiModelParser;
import fr.inra.masc.io.writer.DexiXmlWriter;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionImpl;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.model.OptionValueImpl;
import fr.inra.masc.model.ScaleOrder;
import fr.inra.masc.model.ScaleValue;
import fr.inra.masc.model.ThresholdValue;
import fr.reseaumexico.model.DistributionParameter;
import fr.reseaumexico.model.DistributionParameterImpl;
import fr.reseaumexico.model.Domain;
import fr.reseaumexico.model.DomainImpl;
import fr.reseaumexico.model.ExperimentDesign;
import fr.reseaumexico.model.ExperimentDesignImpl;
import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.FactorImpl;
import fr.reseaumexico.model.Feature;
import fr.reseaumexico.model.FeatureImpl;
import fr.reseaumexico.model.InputDesign;
import fr.reseaumexico.model.InputDesignImpl;
import fr.reseaumexico.model.Level;
import fr.reseaumexico.model.LevelImpl;
import fr.reseaumexico.model.Scenario;
import fr.reseaumexico.model.ScenarioImpl;
import fr.reseaumexico.model.ValueType;
import fr.reseaumexico.model.parser.ExperimentDesignParser;
import fr.reseaumexico.model.parser.InputDesignParser;
import fr.reseaumexico.model.writer.ExperimentDesignXmlWriter;
import fr.reseaumexico.model.writer.InputDesignXmlWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Service to import and export masc model.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.8
 */
public class MascModelService extends MascService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MascModelService.class);

    public static final String DESCRIPTION_SEPARATOR = "\n";

    public MascModel loadModelFromDexiFile(File file) {

        if (log.isDebugEnabled()) {
            log.debug("Will import dexi model: " + file);
        }
        try {
            DexiModelParser mascModelParser = new DexiModelParser();
            MascModel mascModel = mascModelParser.getModel(file);
            return mascModel;
        } catch (XmlPullParserException eee) {
            throw new MascTechnicalException(
                    "Failed to parse masc file '" + file.getName() + "'", eee);
        } catch (FileNotFoundException eee) {
            throw new MascTechnicalException(
                    "Masc file '" + file.getName() + "' not found", eee);
        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Error reading masc file '" + file.getName() + "'", eee);
        } catch (ParseException eee) {
            throw new MascTechnicalException(
                    "Failed to parse masc file '" + file.getName() + "'", eee);
        }
    }


    /**
     * Methode used to export MascModel to Dexi file.
     *
     * @param model      to export
     * @param file       DEXi export file
     * @param forROutput true if DEXi must replace special charts and keep only main tree
     */
    public void exportToDEXiModel(MascModel model, File file, boolean forROutput) {

        if (log.isDebugEnabled()) {
            log.debug("Will export model to file: " + file);
        }
        try {
            DexiXmlWriter writer = new DexiXmlWriter(model, forROutput);
            writer.write(file);
        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Failed to write DEXi xml file", eee);
        }
    }

    public ExperimentDesign convertMascModelToMexicoExperimentDesignModel(MascModel mascModel) {
        ExperimentDesign experimentDesign = new ExperimentDesignImpl();

        // id = mascModel name
        experimentDesign.setId(mascModel.getName());

        // description
        String description = StringUtils.join(mascModel.getDescription(), DESCRIPTION_SEPARATOR);
        experimentDesign.setDescription(description);

        // date is now
        experimentDesign.setDate(new Date());

        // criterias = factors
        Map<String, Factor> factors = Maps.newLinkedHashMap();
        for (Criteria criteria : mascModel.getCriteria()) {
            factors.putAll(getFactors(criteria));
        }

        experimentDesign.setFactor(factors.values());

        return experimentDesign;
    }

    public InputDesign convertMascModelToMexicoInputDesignModel(MascModel mascModel) {
        ExperimentDesign experimentDesign = convertMascModelToMexicoExperimentDesignModel(mascModel);
        return convertMascModelToMexicoInputDesignModel(mascModel, experimentDesign);
    }

    public InputDesign convertMascModelToMexicoInputDesignModel(MascModel mascModel,
                                                                ExperimentDesign experimentDesign) {
        InputDesign inputDesign = new InputDesignImpl();

        // experience design
        inputDesign.setExperimentDesign(experimentDesign);

        // date is now
        inputDesign.setDate(new Date());

        // scenarios = options
        Collection<Option> options = mascModel.getOption();
        List<Scenario> scenarios = Lists.newArrayList();
        int i = 0;
        if (options != null) {
            for (Option option : options) {
                Scenario scenario = new ScenarioImpl();

                // order
                scenario.setOrderNumber(i++);

                // name
                scenario.setName(option.getName());

                // get factor corresponding to criteria
                Map<Factor, Object> factorValues = Maps.newHashMap();

                // optionValue = entry of factorValues
                Collection<OptionValue> optionValues = option.getOptionValue();
                if (optionValues != null) {
                    for (OptionValue optionValue : optionValues) {

                        Criteria criteria = optionValue.getCriteria();

                        if (criteria instanceof EditableCriteria) {
                            String criteriaId = criteria.getUuid();
                            Collection<Factor> factors = experimentDesign.getFactor();
                            for (Factor factor : factors) {
                                if (factor.getId().equals(criteriaId)) {

                                    Number value = optionValue.getValue();
                                    if (MascUtil.THRESHOLD_CRITERIA.equals(factor.getDomain().getName())) {
                                        value = optionValue.getInputValue();
                                    }
                                    if (value != null) {
                                        factorValues.put(factor, Float.valueOf(value.toString()));
                                    }
                                }
                            }
                        }
                    }
                }
                scenario.setFactorValues(factorValues);

                scenarios.add(scenario);
            }
        }
        inputDesign.setScenario(scenarios);

        return inputDesign;
    }

    public ExperimentDesign loadExperimentDesignModel(File file) {

        if (log.isDebugEnabled()) {
            log.debug("Will load experimentDesignModel: " + file);
        }
        try {

            ExperimentDesignParser parser = new ExperimentDesignParser();
            ExperimentDesign result = parser.getModel(file);
            return result;

        } catch (XmlPullParserException eee) {
            throw new MascTechnicalException(
                    "Failed to parse ExperimentDesign file '" +
                    file.getName() + "'", eee);
        } catch (FileNotFoundException eee) {
            throw new MascTechnicalException(
                    "ExperimentDesign file '" +
                    file.getName() + "' not found", eee);
        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Error reading ExperimentDesign file '" +
                    file.getName() + "'", eee);
        } catch (ParseException eee) {
            throw new MascTechnicalException(
                    "Failed to parse ExperimentDesign file '" +
                    file.getName() + "'", eee);
        }
    }

    public InputDesign loadInputDesignDesignModel(File file) {

        if (log.isDebugEnabled()) {
            log.debug("Will load inputDesignModel: " + file);
        }
        try {
            InputDesignParser parser = new InputDesignParser();
            InputDesign result = parser.getModel(file);
            return result;
        } catch (XmlPullParserException eee) {
            throw new MascTechnicalException(
                    "Failed to parse InputDesign file '" + file.getName()
                    + "'", eee);
        } catch (FileNotFoundException eee) {
            throw new MascTechnicalException(
                    "InputDesign file '" + file.getName() + "' not found", eee);
        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Error reading InputDesign file '" + file.getName() +
                    "'", eee);
        } catch (ParseException eee) {
            throw new MascTechnicalException(
                    "Failed to parse InputDesign file '" + file.getName()
                    + "'", eee);
        }
    }

    public void saveExperimentDesign(ExperimentDesign model, File file) {

        if (log.isDebugEnabled()) {
            log.debug("Will save experimentDesignModel to file: " + file);
        }
        try {
            ExperimentDesignXmlWriter writer = new ExperimentDesignXmlWriter(model);
            writer.write(file);
        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Failed to write experiment design xml file", eee);
        }
    }

    public void saveInputDesign(InputDesign model, File file) {

        if (log.isDebugEnabled()) {
            log.debug("Will save inputDesignModel to file: " + file);
        }
        try {
            InputDesignXmlWriter writer = new InputDesignXmlWriter(model);
            writer.write(file);
        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Failed to write input design xml file", eee);
        }
    }

    public MascModel convertInputDesignModelToMascModel(MascModel mascModel, ChartModel chartModel, InputDesign inputDesign) {

        ExperimentDesign experimentDesign = inputDesign.getExperimentDesign();

        // mascModel name
        mascModel.setName(experimentDesign.getId());

        // chart selected options
        List<Option> selectedOptions = new ArrayList<Option>(chartModel.getSelectedOptions());

        // description
        String description = experimentDesign.getDescription();
        if (!StringUtils.isEmpty(description)) {
            String[] splitedDescription = description.split(DESCRIPTION_SEPARATOR);
            mascModel.setDescription(Lists.newArrayList(splitedDescription));
        }

        // factors = criterias
        Collection<Scenario> scenarios = inputDesign.getScenario();
        if (scenarios == null) {
            // FIXME echatellier 20130627 : masc can't handle null scenario
            return mascModel;
        }
        Map<String, Pair<Scenario, Map<String, Pair<Factor, Object>>>> scenarioAndFactorValue =
                MascUtil.extractAllScenariosAndFactors(scenarios);

        // extract scenarios
        Map<String, Option> options = MascUtil.extractAllOptions(mascModel);

        // extract options
        List<String> optionNames = new ArrayList<String>(options.keySet());

        for (String scenarioName : scenarioAndFactorValue.keySet()) {

            // remove treated options
            optionNames.remove(scenarioName);

            // get scenario corresponding
            Pair<Scenario, Map<String, Pair<Factor, Object>>> scenarioMapPair =
                    scenarioAndFactorValue.get(scenarioName);

            // get option
            Option option = options.get(scenarioName);

            // if option dont exist, creating new one
            if (option == null) {
                Scenario scenario = scenarioMapPair.getKey();

                option = new OptionImpl();
                option.setName(scenario.getName());
                mascModel.addOption(option);
                selectedOptions.add(option);
            }

            for (String factorId : scenarioMapPair.getValue().keySet()) {

                Multimap<String, OptionValue> optionValuesMapForCriteria = MascUtil.extractAllOptionValues(option);

                Collection<OptionValue> optionValuesForCriteria = optionValuesMapForCriteria.get(factorId);

                Criteria criteria = MascUtil.findCriteria(mascModel, factorId);
                if (optionValuesForCriteria.isEmpty()) {

                    Collection<OptionValue> optionValues = option.getOptionValue();
                    if (optionValues == null) {
                        optionValues = Lists.newArrayList();
                        option.setOptionValue(optionValues);
                    }

                    OptionValue optionValue = new OptionValueImpl();
                    optionValue.setCriteria(criteria);
                    option.addOptionValue(optionValue);

                    optionValuesForCriteria.add(optionValue);
                }

                for (OptionValue optionValue : optionValuesForCriteria) {

                    Pair<Factor, Object> factorObjectPair = scenarioMapPair.getValue().get(factorId);

                    if (factorObjectPair != null) {
                        Object value = factorObjectPair.getValue();
                        Factor factor = factorObjectPair.getKey();

                        Domain domain = factor.getDomain();
                        Number inputValue = (Number) value;
                        Integer optionValueToSave;

                        // for threshold criteria
                        if (MascUtil.THRESHOLD_CRITERIA.equals(domain.getName())) {

                            // keep input value
                            if (inputValue == null) {
                                optionValue.setInputValue(null);
                                optionValueToSave = null;
                            } else {

                                optionValue.setInputValue(inputValue.doubleValue());

                                // resolve threshold
                                List<ThresholdValue> thresholdValues = Lists.newArrayList();
                                for (DistributionParameter param : domain.getDistributionParameter()) {
                                    thresholdValues.add((ThresholdValue) param.getValue());
                                }

                                // use resolved value
                                optionValueToSave = resolvThresholdValue(thresholdValues, inputValue.doubleValue());
                            }
                        } else {
                            optionValueToSave = inputValue == null ? null : inputValue.intValue();
                        }
                        optionValue.setValue(optionValueToSave);

                        // criteria description
                        criteria.setDescription(factor.getDescription());
                    }
                }
            }
        }

        // left options are deleted
        for (String optionName : optionNames) {

            // get option
            Option option = options.get(optionName);

            mascModel.removeOption(option);
            selectedOptions.remove(option);
        }

        // fire event
        chartModel.setSelectedOptions(selectedOptions);

        return mascModel;
    }

    protected Map<String, Factor> getFactors(Criteria criteria) {

        Map<String, Factor> factors = Maps.newLinkedHashMap();

        // keep only criteria without children
        Collection<Criteria> children = criteria.getChild(false);
        if (children == null || children.isEmpty()) {

            // keep only editable criterias
            if (criteria instanceof EditableCriteria && !criteria.isReference()) {
                Factor factor = new FactorImpl();

                // id = criteria.id
                String id = criteria.getUuid();
                factor.setId(id);

                // name = criteria.name
                String name = criteria.getName();
                factor.setName(name);

                // description
                factor.setDescription(criteria.getDescription());

                // domain
                Domain domain = new DomainImpl();
                domain.setValueType(ValueType.DECIMAL);

                List<Feature> features = Lists.newArrayList();

                EditableCriteria editableCriteria = (EditableCriteria) criteria;
                Collection<ThresholdValue> values = editableCriteria.getValues();

                if (values != null && !values.isEmpty()) {
                    // set specific name
                    domain.setName(MascUtil.THRESHOLD_CRITERIA);

                    List<DistributionParameter> distributionParameters = Lists.newArrayList();

                    for (ThresholdValue thresholdValue : values) {
                        DistributionParameter parameter = new DistributionParameterImpl();

                        // FIXME sletellier : add name ?
                        parameter.setValue(thresholdValue);

                        distributionParameters.add(parameter);
                    }
                    domain.setDistributionParameter(distributionParameters);

                    List<Level> levels = Lists.newArrayList();
                    for (ScaleValue scaleValue : criteria.getScale().getScaleValue()) {
                        Level level = new LevelImpl();
                        level.setValue(scaleValue.getName());
                        levels.add(level);
                    }
                    domain.setLevels(levels);
                }
                factor.setDomain(domain);

                // scales are put in features
                Collection<ScaleValue> scaleValues = criteria.getScale().getScaleValue();
                for (ScaleValue scaleValue : scaleValues) {
                    Feature feature = new FeatureImpl();

                    // scale value
                    String scaleValuePrefix = ScaleValue.class.getSimpleName() + "_";
                    feature.setName(scaleValuePrefix + ScaleValue.PROPERTY_NAME);
                    feature.setValue(scaleValue);
                    feature.setValueType(ValueType.OTHER);

                    features.add(feature);
                }

                factor.setFeature(features);

                factors.put(id, factor);
            }
        } else {
            // get all children factors
            for (Criteria child : children) {
                factors.putAll(getFactors(child));
            }
        }

        return factors;
    }

    protected int resolvThresholdValue(List<ThresholdValue> thresholdValues,
                                       Double inputValue) {

        int i = 0;
        for (ThresholdValue thresholdValue : thresholdValues) {
            if (thresholdValue != null) {
                if (thresholdValue.isCanBeEqual()) {
                    if (inputValue.equals(thresholdValue.getValue())) {
                        return i;
                    }
                }
                if (ScaleOrder.ASC.equals(thresholdValue.getSign())) {
                    if (inputValue < thresholdValue.getValue()) {
                        return i;
                    }
                } else {
                    if (inputValue > thresholdValue.getValue()) {
                        return i;
                    }
                }
            }
            i++;
        }
        return i;
    }
}
