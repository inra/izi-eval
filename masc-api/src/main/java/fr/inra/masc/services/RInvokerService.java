/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.services;

import com.google.common.collect.Multimap;
import fr.inra.masc.MascTechnicalException;
import fr.inra.masc.utils.MascUtil;
import fr.inra.masc.R_SCRIPT;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.model.RScriptModel;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 * Service used to execute R scripts
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.5
 */
public class RInvokerService extends MascService {

    /** Logger. */
    private static Log log = LogFactory.getLog(RInvokerService.class);

    public void executeRSCript(String mascFileName,
                               MascModel mascModel,
                               RScriptModel script) {

        // export dexi file to tmp file
        mascFileName = RScriptModel.getRFileName(mascFileName);
        File exportFile = new File(getConfig().getTmpDirectory(), mascFileName);

        serviceContext.newService(MascModelService.class).exportToDEXiModel(mascModel, exportFile, true);
        executeRScript(exportFile, script);
    }

    /**
     * Execute R script passed in param and add args to call script
     *
     * @param dexiFile    dexi file to use
     * @param scriptModel to execute
     */
    public void executeRScript(File dexiFile, RScriptModel scriptModel) {

        // move resources to tmp dir
        scriptModel.setId(System.currentTimeMillis());
        File workDir = scriptModel.getWorkDir(getConfig().getTmpDirectory());
        if (!workDir.exists()) {
            workDir.mkdir();
        }

        // for OAT script generate csv for option selected
        if (scriptModel.getScript().equals(R_SCRIPT.OAT)) {
            try {
                Option option = scriptModel.getOption();
                if (option == null) {
                    log.warn("No option selected, abording !");
                    return;
                }
                File csvFile = new File(workDir, option.getName().replaceAll(" ", "") + ".csv");
                createCsvOptionFile(csvFile, option);
            } catch (Exception eee) {
                log.error("Failed to create csv tmp file", eee);
            }
        }

        String dexiFileName = dexiFile.getName();

        // copy to tmp
        File copiedDexiFile = new File(workDir, dexiFileName);
        try {
            IOUtils.copy(new FileInputStream(dexiFile), new FileOutputStream(copiedDexiFile));
        } catch (IOException eee) {
            throw new MascTechnicalException(
                    "Failed to copy rScript files to work dir : " + workDir, eee);
        }

        // copy r scripts to work dir
        try {
            for (R_SCRIPT rScript : R_SCRIPT.values()) {
                File file = new File(workDir, rScript.getScriptFileName());
                InputStream stream = rScript.getStream();
                FileOutputStream outputStream = new FileOutputStream(file);
                IOUtils.copy(stream, outputStream);
            }
        } catch (Exception eee) {
            throw new MascTechnicalException(
                    "Failed to copy rScript files to work dir : " + workDir, eee);
        }

        // launch R command
        File rExecutableFile = getConfig().getRExecutableFile();
        CommandLine commandLine;
        if (MascUtil.isExecutableFile(rExecutableFile)) {
            commandLine = new CommandLine(rExecutableFile);
        } else {
            commandLine = new CommandLine("R");
        }
        String cmd = scriptModel.getCmd(dexiFileName);
        commandLine.addArguments(cmd, false);
        CommandLineUtils.invokeCommandeLine(log, commandLine, "Failed to launch R script : " + dexiFileName, workDir);
    }

    public File createCsvOptionFile(File mascFile, Option option) {

        StringBuilder builder = new StringBuilder("\"\",\"" + MascUtil.normalize(option.getName()) + "\"\n");
        Multimap<Criteria, OptionValue> criteriaOptionValue = MascUtil.extractAllOptionValuesByEditableCriteria(option);
        for (Criteria criteria : criteriaOptionValue.keySet()) {
            Collection<OptionValue> optionValues = criteriaOptionValue.get(criteria);
            StringUtil.ToString<OptionValue> optionValueToString = new StringUtil.ToString<OptionValue>() {

                @Override
                public String toString(OptionValue optionValue) {
                    return String.valueOf(optionValue.getValue());
                }
            };
            String valuesAsString = StringUtil.join(optionValues, optionValueToString, ";", false);

            builder.append("\"").append(MascUtil.normalize(criteria.getName())).append("\",\"").append(valuesAsString).append("\"\n");
        }
        try {
            FileUtils.write(mascFile, builder.toString(), "utf8");
        } catch (Exception eee) {
            throw new MascTechnicalException("Failed to generate csv", eee);
        }
        return mascFile;
    }
}
