package fr.inra.masc.services;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.ChartColor;
import org.jfree.chart.plot.DefaultDrawingSupplier;

/**
 * Drawing supplier surcharger pour pouvoir fixer la sequence de couleurs par rapport à la sequence
 * par defaut. Et agrandir la taille des symboles.
 * 
 * L'inra trouve que certaines couleur par defaut ne sont pas lisibles ou trop proche.
 * 
 * @author Eric Chatellier
 */
public class MascDrawingSupplier extends DefaultDrawingSupplier {

    /** The current paint index. */
    private int paintIndex;

    /** The current shape index. */
    private int shapeIndex;

    /** The default fill paint sequence.
     * 
     * vert, rouge, bleu, magenta, noire, orange, marron , bleu ciel, rose, vert claire, jaune...
     */
    private Paint[] paintSequence = new Paint[] {
        new Color(0x55, 0xFF, 0x55), // vert
        new Color(0xFF, 0x55, 0x55), // rouge
        new Color(0x55, 0x55, 0xFF), // bleu
        ChartColor.DARK_MAGENTA,
        Color.BLACK,
        Color.ORANGE,
        new Color(150, 75, 0), // marron
        new Color(119, 181, 254), // bleau ciel
        Color.PINK,
        Color.GREEN,
        Color.YELLOW,

        // some other default
        ChartColor.DARK_CYAN,
        Color.darkGray,
        ChartColor.LIGHT_RED,
        ChartColor.LIGHT_BLUE,
        ChartColor.LIGHT_GREEN,
        ChartColor.LIGHT_YELLOW,
        ChartColor.LIGHT_MAGENTA,
        ChartColor.LIGHT_CYAN,
        Color.lightGray,
        ChartColor.VERY_DARK_RED,
        ChartColor.VERY_DARK_BLUE,
        ChartColor.VERY_DARK_GREEN,
        ChartColor.VERY_DARK_YELLOW,
        ChartColor.VERY_DARK_MAGENTA,
        ChartColor.VERY_DARK_CYAN,
        ChartColor.VERY_LIGHT_RED,
        ChartColor.VERY_LIGHT_BLUE,
        ChartColor.VERY_LIGHT_GREEN,
        ChartColor.VERY_LIGHT_YELLOW,
        ChartColor.VERY_LIGHT_MAGENTA,
        ChartColor.VERY_LIGHT_CYAN
    };
    
    /** The shape sequence. */
    private transient Shape[] shapeSequence;

    /**
     * Constructor.
     * 
     * @param useCustomShape use custom shape size
     */
    public MascDrawingSupplier(boolean useCustomShape) {
        
        if (useCustomShape) {
            // augmente la taille des shapes par defaut
            shapeSequence = createStandardSeriesShapes(10);
        }
    }

    @Override
    public Paint getNextPaint() {
        Paint result = this.paintSequence[this.paintIndex % this.paintSequence.length];
        this.paintIndex++;
        return result;
    }
    
    @Override
    public Shape getNextShape() {
        Shape result = this.shapeSequence[this.shapeIndex % this.shapeSequence.length];
        this.shapeIndex++;
        return result;
    }

    /**
     * Creates an array of standard shapes to display for the items in series
     * on charts.
     * 
     * Modified for masc to support custom size.
     * 
     * @param size size
     * @return The array of shapes.
     */
    public static Shape[] createStandardSeriesShapes(double size) {

        Shape[] result = new Shape[10];

        //double size = 6.0;
        double delta = size / 2.0;
        int[] xpoints = null;
        int[] ypoints = null;

        // square
        result[0] = new Rectangle2D.Double(-delta, -delta, size, size);
        // circle
        result[1] = new Ellipse2D.Double(-delta, -delta, size, size);

        // up-pointing triangle
        xpoints = intArray(0.0, delta, -delta);
        ypoints = intArray(-delta, delta, delta);
        result[2] = new Polygon(xpoints, ypoints, 3);

        // diamond
        xpoints = intArray(0.0, delta, 0.0, -delta);
        ypoints = intArray(-delta, 0.0, delta, 0.0);
        result[3] = new Polygon(xpoints, ypoints, 4);

        // horizontal rectangle
        result[4] = new Rectangle2D.Double(-delta, -delta / 2, size, size / 2);

        // down-pointing triangle
        xpoints = intArray(-delta, +delta, 0.0);
        ypoints = intArray(-delta, -delta, delta);
        result[5] = new Polygon(xpoints, ypoints, 3);

        // horizontal ellipse
        result[6] = new Ellipse2D.Double(-delta, -delta / 2, size, size / 2);

        // right-pointing triangle
        xpoints = intArray(-delta, delta, -delta);
        ypoints = intArray(-delta, 0.0, delta);
        result[7] = new Polygon(xpoints, ypoints, 3);

        // vertical rectangle
        result[8] = new Rectangle2D.Double(-delta / 2, -delta, size / 2, size);

        // left-pointing triangle
        xpoints = intArray(-delta, delta, delta);
        ypoints = intArray(0.0, -delta, +delta);
        result[9] = new Polygon(xpoints, ypoints, 3);

        return result;

    }

    /**
     * Helper method to avoid lots of explicit casts in getShape().  Returns
     * an array containing the provided doubles cast to ints.
     *
     * @param a  x
     * @param b  y
     * @param c  z
     *
     * @return int[3] with converted params.
     */
    private static int[] intArray(double a, double b, double c) {
        return new int[] {(int) a, (int) b, (int) c};
    }
    
    /**
     * Helper method to avoid lots of explicit casts in getShape().  Returns
     * an array containing the provided doubles cast to ints.
     *
     * @param a  x
     * @param b  y
     * @param c  z
     * @param d  t
     *
     * @return int[4] with converted params.
     */
    private static int[] intArray(double a, double b, double c, double d) {
        return new int[] {(int) a, (int) b, (int) c, (int) d};
    }
}
