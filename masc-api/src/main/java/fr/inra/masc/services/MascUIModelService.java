package fr.inra.masc.services;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.masc.MascTechnicalException;
import fr.inra.masc.MascUIModel;
import fr.inra.masc.model.RScriptModel;
import fr.inra.masc.reports.ReportModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.image.BufferedImage;
import java.beans.BeanInfo;
import java.beans.ExceptionListener;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import org.w3c.dom.svg.SVGDocument;

/**
 * Service to persist, load {@link MascUIModel}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.8
 */
public class MascUIModelService extends MascService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MascUIModelService.class);

    public MascUIModel loadModel(File mascFile) {

        try {
            XMLDecoder decoder = new XMLDecoder(new FileInputStream(mascFile));
            decoder.setExceptionListener(new ExceptionListener() {

                @Override
                public void exceptionThrown(Exception eee) {
                    log.warn("Xml encoder throw an exception : ", eee);
                }
            });
            try {
                MascUIModel model = (MascUIModel) decoder.readObject();
                return model;
            } finally {
                decoder.close();
            }
        } catch (Exception eee) {
            throw new MascTechnicalException(
                    "Failed to parse mascFile : " + mascFile.getName(), eee);
        }
    }

    public void saveModel(MascUIModel model, File saveFile) {

        try {
            XMLEncoder encoder = new XMLEncoder(new FileOutputStream(saveFile));
            encoder.setExceptionListener(new ExceptionListener() {

                @Override
                public void exceptionThrown(Exception eee) {
                    log.warn("Xml encoder throw an exception : ", eee);
                }
            });

            // exclude images
            excludeTypeOnModel(ReportModel.class, BufferedImage.class);
            excludeTypeOnModel(ReportModel.class, SVGDocument.class);
            excludeTypeOnModel(ReportModel.class, File.class);
            excludeTypeOnModel(RScriptModel.class, File.class);
            excludeTypeOnModel(MascUIModel.class, File.class);
            try {
                encoder.writeObject(model);
                encoder.flush();
            } finally {
                encoder.close();
            }
        } catch (FileNotFoundException eee) {
            throw new MascTechnicalException(
                    "Failed to save model to file : " + saveFile.getName(), eee);
        }
    }

    protected void excludeTypeOnModel(Class<?> model, Class<?> propertyType) {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(model);
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                if (propertyDescriptor.getPropertyType().equals(propertyType)) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        } catch (IntrospectionException eee) {
            log.error("Failed to exclude '" +
                      propertyType.getName() + "' on : " + model.getName(), eee);
        }
    }
}