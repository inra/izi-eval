package fr.inra.masc.services;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.masc.ExecutorException;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.LogOutputStream;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.logging.Log;

import java.io.File;
import java.io.IOException;

/**
 * Utils method around a command line execution.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.8
 */
public class CommandLineUtils {

    public static CommandLine newCommand(File exec, Object... args) {
        return newCommand(exec.getAbsolutePath(), args);
    }

    public static CommandLine newCommand(String exec, Object... args) {
        CommandLine result = new CommandLine(exec);

        if (args != null) {
            for (Object arg : args) {
                String argument;
                if (arg instanceof File) {
                    argument = ((File) arg).getAbsolutePath();
                } else {
                    argument = arg.toString();
                }
                result.addArgument(argument);
            }
        }
        return result;
    }

    public static void invokeCommandeLine(Log logger,
                                          CommandLine commandLine,
                                          String errorMsg,
                                          File workDir) {
        DefaultExecutor executor = new DefaultExecutor();

        ExecutorLog executorLog = new ExecutorLog();
        executor.setStreamHandler(new PumpStreamHandler(executorLog));

        // set resource directory
        executor.setWorkingDirectory(workDir);

        // invoke
        int exitValue;
        try {
            if (logger.isInfoEnabled()) {
                logger.info("Command line '" + commandLine.toString() + "' will be execute");
            }
            exitValue = executor.execute(commandLine);
        } catch (IOException eee) {
            throw new ExecutorException(executorLog.getLog(), errorMsg, eee);
        }

        if (logger.isInfoEnabled()) {
            logger.info("Command line end with exit with value : " + exitValue);
        }
        if (exitValue != 0) {
            throw new ExecutorException(executorLog.getLog(), errorMsg);
        }
    }

    protected static class ExecutorLog extends LogOutputStream {

        protected StringBuilder executorLog = new StringBuilder();

        @Override
        protected void processLine(String line, int level) {
            executorLog.append(line);
        }

        public String getLog() {
            return executorLog.toString();
        }
    }
}
