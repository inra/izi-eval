package fr.inra.masc.synoptic;

/*
 * #%L
 * Masc :: API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2013 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.inra.masc.model.ComputableCriteria;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.model.Function;
import fr.inra.masc.model.ThresholdValue;
import fr.inra.masc.utils.CriteriaVisitor;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 */
public class DuplicatedLinkedCloneVisitor implements CriteriaVisitor {

    /** Logger */
    private static Log log = LogFactory.getLog(DuplicatedLinkedCloneVisitor.class);

    protected Multimap<String, String> clonedIdRef = ArrayListMultimap.create();
    protected Map<Criteria, Criteria> criteriaCloned = Maps.newHashMap();
    protected Map<Criteria, Criteria> clonedCriteria = Maps.newHashMap();
    protected Criteria root;
    protected boolean resolveReference;

    public DuplicatedLinkedCloneVisitor(Criteria root, boolean resolveReference) {
        this.root = root;
        this.resolveReference = resolveReference;
    }

    @Override
    public boolean visit(Criteria parentCriteria, Criteria criteria) {
        try {
            Criteria cloned = criteria.getClass().newInstance();

            Criteria parent = criteriaCloned.get(parentCriteria);

            cloned.setName(criteria.getName());
            cloned.setDescription(criteria.getDescription());
            cloned.setScale(criteria.getScale());

            if (criteria instanceof EditableCriteria) {
                List<ThresholdValue> values = ((EditableCriteria) criteria).getValues();
                ((EditableCriteria) cloned).setValues(values);
            }
            if (criteria instanceof ComputableCriteria) {
                Function function = ((ComputableCriteria) criteria).getFunction();
                ((ComputableCriteria) cloned).setFunction(function);
            }

            // keep ids ref
            clonedIdRef.put(criteria.getUuid(), cloned.getUuid());
            clonedCriteria.put(cloned, criteria);
            criteriaCloned.put(criteria, cloned);

            if (parent != null) {
                if (parent.getChild() == null) {
                    List<Criteria> children = Lists.newArrayList();
                    parent.setChild(children);
                }
                parent.addChild(cloned);
            } else {
                this.root = cloned;
            }
        } catch (Exception eee) {
            log.error("Failed to clone criteria", eee);
        }
        return true;
    }

    @Override
    public boolean resolvRefs() {
        return this.resolveReference;
    }

    public Multimap<String, String> getClonedIdRef() {
        return clonedIdRef;
    }

    public Criteria getRoot() {
        return root;
    }

    public Map<Criteria, Criteria> getClonedCriteria() {
        return clonedCriteria;
    }
}
