/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.synoptic;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;

import fr.inra.masc.io.MascXmlConstant;
import fr.inra.masc.model.ComputableCriteria;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.Function;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.model.ScaleValue;
import fr.inra.masc.utils.MascUtil;

/**
 * Used to build Masc {@link mxGraph} version 2.
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.8
 */
public class CriteriaGraphBuilder {

    /** Logger */
    private static Log log = LogFactory.getLog(CriteriaGraphBuilder.class);

    protected Criteria root;

    /**
     * Contient la matrice de représentation du graph synaptic.
     * 
     * Par exemple:
     * <pre>
     *  | | |e|
     *  |b|d| |
     *  | | |f|
     *  | | |e|
     * a|c|d| |
     *  | | |f|
     *  | |e| |
     *  |d| | |
     *  | |f| |
     * </pre>
     */
    protected Criteria[][] criteriaMatrix;

    protected Map<Criteria, Criteria> clonedCriteria;

    public CriteriaGraphBuilder(Criteria root, final SynopticModel model) {
        this.root = root;

        List<String> criteriaList = model.getCriteriaList();

        if (model.isDuplicateLinkedCriterias()) {

            DuplicatedLinkedCloneVisitor cloneVisitor = new DuplicatedLinkedCloneVisitor(root, model.isResolveReference());

            // clone all duplicate linked criterias
            root.accept(cloneVisitor, null);

            this.root = cloneVisitor.getRoot();
            Multimap<String, String> clonedIdRef = cloneVisitor.getClonedIdRef();
            this.clonedCriteria = cloneVisitor.getClonedCriteria();

            // convert criteria selected
            if (criteriaList != null) {
                List<String> cloneCriteriaListId = Lists.newArrayList();
                for (String criteriaId : criteriaList) {
                    cloneCriteriaListId.addAll(clonedIdRef.get(criteriaId));
                }
                criteriaList = cloneCriteriaListId;
            }
        }

        List<LegendItem> legend = model.getLegend();

        // TODO sletellier : get nb classes and refactor
        int nbClasses = MascUtil.getMaxClasses(root, model.isResolveReference());

        if (CollectionUtils.isEmpty(legend) || legend.size() < nbClasses) {

            legend = LegendItem.completeLegend(legend, nbClasses);

            model.setLegend(legend);
        }

        SynopticMatrixResolver matrixResolver = new SynopticMatrixResolver(criteriaList, model.isResolveReference());

        // build matrix
        criteriaMatrix = matrixResolver.buildMatrix(this.root, 1);

        // invert matrix if needed
        if (model.isMirror()) {
            for (Criteria[] criterias : criteriaMatrix) {
                ArrayUtils.reverse(criterias);
            }
        }
    }

    protected static class SynopticNode {

        protected double width;
        protected mxCell node;
        protected mxCell inputValueNode;

        public SynopticNode(mxCell node, mxCell inputValueNode, double width) {
            this.width = width;
            this.node = node;
            this.inputValueNode = inputValueNode;
        }

        public double getWidth() {
            //return width;
            
            // more accurate width
            double width = node.getGeometry().getWidth();
            if (inputValueNode != null) {
                width += inputValueNode.getGeometry().getWidth();
            }
            return width;
        }

        public mxCell getNode() {
            return node;
        }
        
        public mxCell getInputValueNode() {
            return inputValueNode;
        }
    }

    /**
     * Build all tree containing node, association and legend.
     *
     * @param model synoptic model
     */
    public mxGraph build(SynopticModel model) {

        mxGraph graph = new mxGraph();
        graph.getModel().beginUpdate();

        try {

            // get parent used to attach nodes
            Object parent = graph.getDefaultParent();

            // association between criteria and graph node
            Map<Criteria, SynopticNode> criteriaNodes = Maps.newHashMap();

            // keep all x for cells
            Map<Object, Double> xCell = Maps.newHashMap();
// TODO poussin modifier la largeur des feuilles (largeur constante)
            mxRectangle cellSize = getTextSize(graph, model, "A");

            double height = cellSize.getHeight();
            double initY = (height * ((model.getBoxYSize() + 10) / 10)) * 2;
            double y = initY;
            height = y;
            double x = 0;

            List<SynopticNode> leafNodes = new ArrayList<SynopticNode>();
            double previousX = x;
            // premier parcours par colonne
            for (int i=0;i<criteriaMatrix[0].length;i++) {

                double maxWidth = 0;

                // dans chaque colonne, on traite l'element de la colonne pour chaque ligne
                for (Criteria[] criterias : criteriaMatrix) {

                    Criteria criteria = criterias[i];

                    if (criteria != null) {
                        SynopticNode node = buildNode(graph, parent, model, criteria, x, y);

                        criteriaNodes.put(criteria, node);

                        maxWidth = Math.max(maxWidth, node.getWidth());

                        // keep node to retrieve x position for associations
                        xCell.put(node.getNode(), x);
                        
                        // keep node if node is leaf
                        if (CollectionUtils.isEmpty(criteria.getChild())) {
                            leafNodes.add(node);
                        }
                    }
                    y += model.getYGap();
                }

                height = Math.max(height, y);
                y = initY;

                previousX = x;
                x += model.getXGap() + maxWidth;
            }

            // alignement des noeuds feuille à droite
            if (model.isAlignLeafToRight()) {
                double maxLeafWidth = 0;
                for (SynopticNode synopticNode : leafNodes) {
                    // on positionne le "x" suivant le max "x" - la taille de la cellule
                    mxCell node = (mxCell)synopticNode.getNode();
                    mxGeometry geometry = node.getGeometry();
                    
                    if (model.isMirror()) {

                        // also move input value node
                        if (synopticNode.getInputValueNode() != null) {
                            synopticNode.getInputValueNode().getGeometry().setX(0);
                            geometry.setX(synopticNode.getInputValueNode().getGeometry().getWidth());
                        } else {
                            geometry.setX(0);
                        }
                    } else {
                        geometry.setX(previousX);
                        
                        // also move input value node
                        if (synopticNode.getInputValueNode() != null) {
                            synopticNode.getInputValueNode().getGeometry().setX(previousX + geometry.getWidth());
                        }
                    }

                    maxLeafWidth = Math.max(maxLeafWidth, synopticNode.getWidth());
                }

                // ajoust new x with max leaf width to draw legend
                if (!model.isMirror()) {
                    x = previousX + model.getXGap() + maxLeafWidth;
                }
            }

            // build all associations
            buildAssociations(graph, model, parent, criteriaNodes, root);

            // Override default style to use own
            if (!model.isDefaultEdgeStyle()) {
                Map<String, Object> style = graph.getStylesheet().getDefaultEdgeStyle();
                style.put(mxConstants.STYLE_EDGE, new MascEdgeStyleFunction(model, xCell));
            }

            // build title
            this.buildTitle(graph, model, x);

            // build legend
            this.buildLegend(graph, model, x, height);

            return graph;
        } finally {
            graph.getModel().endUpdate();
        }
    }

    /**
     * Build all nodes in graph.
     *
     * @param graph     to fill
     * @param parent    used to add nodes
     * @param model     synoptic model
     * @param criteria  criteria
     * @param x         x position
     * @param y         y position
     */
    protected SynopticNode buildNode(mxGraph graph,
                              Object parent,
                              SynopticModel model,
                              Criteria criteria,
                              double x,
                              double y) {

        if (model.isDuplicateLinkedCriterias()) {
            criteria = this.clonedCriteria.get(criteria);
        }

        // get last one
        while (criteria.isReference()) {
            criteria = criteria.getChild(0);
        }

        // get option value
        Collection<OptionValue> optionValueForCriteria =
                MascUtil.getOptionValuesForCriteria(model.getOption(), criteria);

        Integer value = MascUtil.getMinValue(optionValueForCriteria);

        int valueMax = 0;
        if (criteria.getScale() != null && criteria.getScale().getScaleValue() != null) {

            // get max possible value
            // TODO seletellier 20120124 : take care of order
            Collection<ScaleValue> scaleValues =
                    criteria.getScale().getScaleValue();

            valueMax = scaleValues.size();
        }

        // get color to apply
        Color color = LegendItem.getColor(value, valueMax - 1, model);

        // build node
        String nodeName = criteria.getName();

        // add case if value is define
        if (value != null && !model.isDisplayNotEvaluated() && model.isDisplayValues()) {
            nodeName += " (" + (value + 1) + "/" + valueMax + ")";
        }

        // si le texte est trop grand on l'affiche sur deux lignes
        /*if (nodeName.length() >= model.getCutCharacterCount()) {
            // cut after next ' ' if exist
            int cutIndex = nodeName.indexOf(' ', model.getCutCharacterCount());
            if (cutIndex == -1) {
                cutIndex = model.getCutCharacterCount();
                // le trim() corrige un bug de dépassement du texte des cases (' ' est special ?)
                nodeName = nodeName.substring(0, cutIndex).trim() + "\n" + nodeName.substring(cutIndex);
            } else {
                // +1 pour supprimer l'espace inutile
                nodeName = nodeName.substring(0, cutIndex) + "\n" + nodeName.substring(cutIndex + 1);
            }
        }*/

        // si le texte est trop grand on l'affiche sur plusieurs lignes
        int lastIndex = 0;
        while (nodeName.length() - lastIndex >= model.getCutCharacterCount()) {
            
            int cutIndex = nodeName.indexOf(' ', lastIndex + model.getCutCharacterCount());
            if (cutIndex == -1) {
                cutIndex = lastIndex + model.getCutCharacterCount();
                // le trim() corrige un bug de dépassement du texte des cases (' ' est special ?)
                nodeName = nodeName.substring(0, cutIndex).trim() + "\n" + nodeName.substring(cutIndex);
            } else {
                // +1 pour supprimer l'espace inutile
                nodeName = nodeName.substring(0, cutIndex) + "\n" + nodeName.substring(cutIndex + 1);
            }
            lastIndex = cutIndex + 1;
        }

        double xSize = getTextWidth(graph, model, nodeName);

        mxCell node = buildNode(graph, parent, model,
                criteria.getUuid(), nodeName, color,
                xSize, x, y);
        mxCell inputValueNode = null;

        // show input values
        if (model.isShowInputValue()) {
            List<String> inputValues = MascUtil.getInputValues(model.getOption(), criteria, model.isResolveReference());
            double inputValueX = x;
            if (!model.isMirror()) {
                inputValueX += xSize;
            }
            // FIXME echatellier only care about first value even if there is more
            if (!inputValues.isEmpty()) {
                String inputValue = inputValues.get(0);
                
                double inputValueWidth = getTextWidth(graph, model, inputValue);
                if (model.isMirror()) {
                    inputValueX -= inputValueWidth;
                }
                inputValueNode = buildNode(graph, parent, model, criteria.getUuid() + inputValue, inputValue, color, inputValueWidth, inputValueX, y);

                if (!model.isMirror()) {
                    inputValueX += inputValueWidth;
                }
                xSize += inputValueWidth;
            }
        }

        return new SynopticNode(node, inputValueNode, xSize);
    }

    /**
     * Build node with specified option
     *
     * @param graph  to fill
     * @param parent used to add nodes
     * @param model  synoptic model
     * @param id     to use
     * @param name   to display
     * @param color  to use
     * @param xSize  box x size
     * @param x      position
     * @param y      position
     * @return node created
     */
    protected mxCell buildNode(mxGraph graph,
                               Object parent,
                               SynopticModel model,
                               String id,
                               String name,
                               Color color,
                               double xSize,
                               double x,
                               double y) {

        if (log.isDebugEnabled()) {
            log.debug("name[" + name + "] xSize[" + xSize + "] x[" + x
                    + "] y[" + y + "] color[" + color + "]");
        }

        String style = "fontStyle=1;fontColor=black;fontSize=" +
                model.getCriteriaNameFontSize() +
                ";fillColor=" + mxUtils.hexString(color);

        return (mxCell)graph.insertVertex(parent, id, name, x, y, xSize,
                getTextHeight(graph, model, name),
                style);
    }

    /**
     * Recurs method to build all associations for model.
     *
     * @param graph          to fill
     * @param model          synoptique model
     * @param parent         used to add nodes
     * @param criteriaParent current parent criteria
     */
    protected void buildAssociations(mxGraph graph,
                                     SynopticModel model,
                                     Object parent,
                                     Map<Criteria, SynopticNode> criteriaNodes,
                                     Criteria criteriaParent) {

        // get node associated to parent criteria
        Object parentNode = criteriaNodes.get(criteriaParent).getNode();

        // get weights
        String[] weights = null;

        if (model.isDisplayWeight()) {
            if (criteriaParent instanceof ComputableCriteria) {
                Function function =
                        ((ComputableCriteria) criteriaParent).getFunction();
                if (function != null) {
                    String weightsAsString = function.getWeights();
                    if (weightsAsString != null) {
                        weights = weightsAsString.split(
                                MascXmlConstant.WEIGHTS_SEPARATOR);
                    }
                }
            }
        }

        int i = 0;
        for (Criteria child : criteriaParent.getChild(model.isResolveReference())) {

            // get node associated to child
            SynopticNode synopticNode = criteriaNodes.get(child);

            if (synopticNode != null) {

                Object childNode = synopticNode.getNode();

                // build edge (association)
                String edgeName = weights == null ? "" :
                        MascUtil.getWeightAsString(weights[i++]);

                String style = "fontStyle=1;fontColor=black;fontSize=" +
                        model.getWeightFontSize() + ";labelPosition=right;endArrow=none;strokeWidth=" +
                        model.getEdgeWidth();

                graph.insertEdge(parent, null, edgeName, childNode, parentNode,
                        style);

                // recurs
                buildAssociations(graph, model, parent, criteriaNodes, child);
            }
        }
    }

    /**
     * Build all tree containing node, association and legend.
     *
     * @param graph synoptic char
     * @param model synoptic model
     */
    protected mxGraph buildTitle(mxGraph graph, SynopticModel model, double x) {

        // add option name
        Option option = model.getOption();

        String name;
        if (option != null) {
            name = option.getName();
        } else {
            name = "";
        }
        buildNode(graph, graph.getDefaultParent(), model, name, name,
                Color.WHITE,
                getTextWidth(graph, model, name), x / 2, 0);

        return graph;
    }

    protected double getTextWidth(mxGraph graph,
                                  SynopticModel model,
                                  String name) {

        // xSize of box = max char size on criteria for current depth
        mxRectangle sizeForString = getTextSize(graph, model, name);
        return sizeForString.getWidth() * 1.3 * ((model.getBoxXSize() + 10) / 10);
    }

    /**
     * Build legend for graph.
     *
     * @param model synoptic model
     */
    protected mxGraph buildLegend(mxGraph graph, SynopticModel model, double x, double y) {

        Object parent = graph.getDefaultParent();

        List<LegendItem> legend = model.getLegend();

        // start to max y of graph
        int currentY = 0;

        // determine maxX size
        double xSize = 0;
        for (LegendItem legendItem : legend) {
            String name = legendItem.getTitle();
            xSize = Math.max(xSize, getTextWidth(graph, model, name));
        }

        for (LegendItem legendItem : legend) {

            // build value node
            String name = legendItem.getTitle();

            buildNode(graph, parent, model, name, name,
                    legendItem.getColor(), xSize,
                    (x * model.getXLegend()) / 100, ((y * model.getYLegend()) / 100) + currentY);

            // add ySize for y position
            currentY += getTextHeight(graph, model, name);
        }
        return graph;
    }

    protected double getTextHeight(mxGraph graph,
                                  SynopticModel model,
                                  String name) {

        mxRectangle sizeForString = getTextSize(graph, model, name);
        return sizeForString.getHeight() * 1.5 * ((model.getBoxYSize() + 10) / 10);
    }


    protected mxRectangle getTextSize(mxGraph graph,
                                  SynopticModel model,
                                  String name) {

        // xSize of box = max char size on criteria for current depth
        graph.getStylesheet().getDefaultEdgeStyle().put(mxConstants.STYLE_FONTSIZE, model.getCriteriaNameFontSize());
        Font font = mxUtils.getFont(graph.getStylesheet().getDefaultEdgeStyle());
        return mxUtils.getSizeForString(name, font, mxConstants.LABEL_SCALE_BUFFER);
    }
}

