package fr.inra.masc.synoptic;

/*
 * #%L
 * Masc :: API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2013 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.masc.model.Criteria;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SynopticMatrixResolver {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SynopticMatrixResolver.class);

    protected List<String> criteriaSelected;
    protected boolean resolvRef;
    protected Map<Criteria, Integer> criteriaHeight;
    protected Map<Criteria, Integer> criteriaDepth;
    protected Map<Criteria, Integer> criteriaY;
    protected int maxDepth = 0;
    protected Criteria root = null;

    public SynopticMatrixResolver(List<String> criteriaSelected, boolean resolvRef) {
        this.criteriaSelected = criteriaSelected;
        this.resolvRef = resolvRef;
        this.criteriaHeight = Maps.newHashMap();
        this.criteriaDepth = Maps.newHashMap();
        this.criteriaY = Maps.newHashMap();
    }

    public Criteria[][] buildMatrix(Criteria root, int depth) {
        computeMatrix(root, depth, 0);

        // build matrix
        int maxDepth = getMaxDepth();
        int maxHeight = getMaxHeight();
        Criteria[][] criterias = new Criteria[maxHeight][];
        for (int i=0;i<criterias.length;i++) {
            Criteria[] criteria = new Criteria[maxDepth];
            criterias[i] = criteria;
        }

        Pair<Integer, Integer> position = getPosition(root);
        Integer height = position.getLeft();
        Integer rootDepth = position.getRight();
        criterias[height][rootDepth] = root;

        if (log.isDebugEnabled()) {
            log.debug(root.getName() + " height[" + height + "]" + " depth[" + rootDepth + "]");
        }

        getCriteriaMatrix(root, criterias);

        return criterias;
    }

    protected int computeMatrix(Criteria criteria, int depth, int y) {
        if (root == null) {
            root = criteria;
        }

        log.debug("Start " + criteria.getName() + " depth[" + depth + "] y[" + y + "]");

        List<Criteria> children = getChildren(criteria);
        int height = 0;
        if (children != null && !children.isEmpty()) {
            for (Criteria child : children) {
                height += computeMatrix(child, depth + 1, y + height);
            }
        }

        if (height % 2 == 0) {

            adjustAllY(criteria, y + height / 2);
            height += 1;
        }

        setHeight(criteria, height);
        setDepth(criteria, depth);
        setMaxDepth(depth);
        setY(criteria, y);

        log.debug("End " + criteria.getName() + " depth[" + depth + "] y[" + y + "] height[" + height + "]");

        return height;
    }

    protected void adjustAllY(Criteria criteria, int yCeil) {
        for (Criteria child : getChildren(criteria)) {

            Integer height = criteriaHeight.get(child);

            int y = getY(child);
            log.debug("adjustAllY " + child + " yCeil[" + yCeil + "]" + " y[" + y + "]");
            if (y + height / 2 >= yCeil) {
                setY(child, y + 1);
                adjustAllY(child, y);
            }
        }
    }

    protected void getCriteriaMatrix(Criteria root, Criteria[][] criterias) {
        List<Criteria> children = getChildren(root);
        for (Criteria child : children) {
            getCriteriaMatrix(child, criterias);
            Pair<Integer, Integer> position = getPosition(child);

            Integer height = position.getLeft();
            Integer depth = position.getRight();

            if (log.isDebugEnabled()) {
                log.debug(child.getName() + " height[" + height + "]" + " depth[" + depth + "]");
            }

            criterias[height][depth] = child;
        }
    }

    protected Pair<Integer, Integer> getPosition(Criteria criteria) {
        int depth = criteriaDepth.get(criteria);
        int y = getY(criteria);
        int height = criteriaHeight.get(criteria);

        int finalY = y + height / 2;

        log.debug("getPosition " + criteria.getName() + " depth[" + depth + "] y[" + y + "] height[" + height + "] finalY[" + finalY + "]");

        return Pair.of(finalY, depth - 1);
    }

    /**
     * Return only selected children.
     *
     * @param parent concerned
     * @return children of parent in selected
     */
    protected List<Criteria> getChildren(Criteria parent) {
        Collection<Criteria> children =
                parent.getChild(resolvRef);

        List<Criteria> result = Lists.newArrayList(children);
        for (Criteria child : children) {
            if (criteriaSelected == null || !criteriaSelected.contains(child.getUuid())) {
                result.remove(child);
            }
        }
        return result;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = Math.max(this.maxDepth, maxDepth);
    }

    public void setHeight(Criteria criteria, Integer height) {
        criteriaHeight.put(criteria, height);
    }

    public void setDepth(Criteria criteria, Integer depth) {
        criteriaDepth.put(criteria, depth);
    }

    public int getMaxHeight() {
        return criteriaHeight.get(root);
    }

    protected void setY(Criteria criteria, int y) {
        criteriaY.put(criteria, y);
    }

    protected int getY(Criteria criteria) {
        return criteriaY.get(criteria);
    }

}