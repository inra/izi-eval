package fr.inra.masc.synoptic;

/*
 * #%L
 * Masc :: API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2013 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxEdgeStyle;
import com.mxgraph.view.mxGraphView;
import java.util.List;
import java.util.Map;

/**
 * It's cleaned copy of {@link com.mxgraph.view.mxEdgeStyle#SideToSide} to manage own point x
 */
public class MascEdgeStyleFunction implements mxEdgeStyle.mxEdgeStyleFunction {

    protected SynopticModel model;
    protected Map<Object, Double> xCell;

    public MascEdgeStyleFunction(SynopticModel model, Map<Object, Double> xCell) {
        this.model = model;
        this.xCell = xCell;
    }

    public void apply(mxCellState state, mxCellState source,
                      mxCellState target, List<mxPoint> points, List<mxPoint> result) {

        mxGraphView view = state.getView();

        if (source != null && target != null) {

            double y1 = view.getRoutingCenterY(source);
            double y2 = view.getRoutingCenterY(target);

            Double reelX = xCell.get(target.getCell());

            double x = reelX - model.getXEdgeGap();

            if (!model.isMirror()) {
                x = reelX + target.getWidth() + model.getXEdgeGap();
            }

            if (!target.contains(x, y1) && !source.contains(x, y1)) {
                result.add(new mxPoint(x, y1));
            }

            if (!target.contains(x, y2) && !source.contains(x, y2)) {
                result.add(new mxPoint(x, y2));
            }
        }
    }
}