package fr.inra.masc.synoptic;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.inra.masc.model.Criteria;

import java.util.Collection;
import java.util.Map;

/**
 * Model of a criteria graph.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.8
 */
public class CriteriaGraphModel {

    /**
     * The root criteria to display.
     *
     * @since 1.8
     */
    protected final Criteria rootCriteria;

    /**
     * Criteria indexed by their depth.
     *
     * @since 1.8
     */
    protected final Multimap<Integer, Criteria> criteriaByDepth;

    /**
     * Resolved max criteria name by depth.
     *
     * @since 1.8
     */
    protected final Map<Integer, Integer> maxCharByDepth;

    /**
     * Max depth in model.
     *
     * @since 1.8
     */
    protected final int maxDepth;

    public CriteriaGraphModel(Criteria rootCriteria) {
        this.rootCriteria = rootCriteria;
        this.criteriaByDepth = LinkedListMultimap.create();
        this.maxCharByDepth = Maps.newTreeMap();

        // build the criteriaByDepth and maxCharByDepth
        Multimap<Integer, Criteria> tmp = LinkedListMultimap.create();
        fillModel(tmp, rootCriteria, 0);
        this.maxDepth = tmp.keySet().size();

        for (int depth = 0; depth < maxDepth; depth++) {
            int newDepth = maxDepth - depth - 1;
            Collection<Criteria> criterias = tmp.get(depth);
            int maxChars = getMaxCharByDepth(criterias);
            maxCharByDepth.put(newDepth, maxChars);
            criteriaByDepth.putAll(newDepth, criterias);
        }
    }

    protected void fillModel(Multimap<Integer, Criteria> tmp,
                             Criteria criteria,
                             int depth) {

        // store that criteria at this level
        tmp.put(depth, criteria);

        if (!criteria.isChildEmpty()) {

            // walk through childs of this criteria
            int nextDepth = depth + 1;
            for (Criteria child : criteria.getChild()) {
                fillModel(tmp, child, nextDepth);
            }
        }
    }

    protected int getMaxCharByDepth(Collection<Criteria> criterias) {
        int result = 0;
        for (Criteria criteria : criterias) {
            result = Math.max(result, criteria.getName().length());
        }
        return result;
    }
}
