/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.synoptic;

import fr.inra.masc.model.Option;
import java.awt.Color;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.beans.AbstractSerializableBean;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Regroup all properties to generate a synaptic graph.
 *
 * @author sletellier  &lt;letellier@codelutin.com&gt;
 * @since 0.2
 */
public class SynopticModel extends AbstractSerializableBean {

    public static final String PROPERTY_OPTION = "option";

    public static final String PROPERTY_DISPLAY_NOT_EVALUATED = "displayNotEvaluated";

    public static final String PROPERTY_DISPLAY_WEIGHT = "displayWeight";

    public static final String PROPERTY_X_GAP = "XGap";

    public static final String PROPERTY_X_EDGE_GAP = "xEdgeGap";

    public static final String PROPERTY_Y_GAP = "YGap";

    public static final String PROPERTY_BOX_X_SIZE = "boxXSize";

    public static final String PROPERTY_BOX_Y_SIZE = "boxYSize";

    public static final String PROPERTY_LEGEND = "legend";

    public static final String PROPERTY_X_MAX = "XMax";

    public static final String PROPERTY_Y_MAX = "YMax";

    public static final String PROPERTY_WEIGHT_FONT_SIZE = "weightFontSize";

    public static final String PROPERTY_CRITERIA_NAME_FONT_SIZE = "criteriaNameFontSize";

    public static final String PROPERTY_X_LEGEND = "XLegend";

    public static final String PROPERTY_Y_LEGEND = "YLegend";

    public static final String PROPERTY_CRITERIA_LIST = "criteriaList";

    public static final String PROPERTY_RESOLVE_REFERENCE = "resolveReference";

    public static final String PROPERTY_DISPLAY_VALUES = "displayValues";

    public static final String PROPERTY_MIRROR = "mirror";

    public static final String PROPERTY_DEFAULT_EDGE_STYLE = "defaultEdgeStyle";

    public static final String PROPERTY_EDGE_WIDTH = "edgeWidth";

    public static final String PROPERTY_BACKGROUND_COLOR = "backgroundColor";

    public static final String PROPERTY_DUPLICATE_LINKED_CRITERIAS = "duplicateLinkedCriterias";

    public static final String PROPERTY_SHOW_INPUT_VALUE = "showInputValue";
    
    public static final String PROPERTY_CUT_CHARACTER_COUNT = "cutCharacterCount";

    public static final String PROPERTY_ALIGN_LEAF_TO_RIGHT = "alignLeafToRight";

    private static final long serialVersionUID = 1L;

    private final PropertyChangeListener legendChangeListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            firePropertyChange(PROPERTY_LEGEND, null, SynopticModel.this.legend);
        }
    };

    protected Option option;

    // gap between too levels
    protected int xGap = 100;

    // gap between source and edge disjunction
    protected int xEdgeGap = 11;

    // first level y gap
    protected int yGap = 100;

    // size used to calculate char weight
    private int boxXSize = 0;

    // height box size
    protected int boxYSize = 0;

    // represent legend
    protected List<LegendItem> legend;

    // legend x position
    protected int xLegend = 100;

    // legend y position
    protected int yLegend = 0;

    // nombre de caractères avant de faire un retour à la ligne
    protected int cutCharacterCount = 100;

    // font sizes
    protected int weightFontSize = 19;

    protected int criteriaNameFontSize = 22;

    protected boolean displayNotEvaluated = false;

    protected boolean displayWeight = false;

    protected boolean showInputValue = true;

    protected boolean resolveReference = false;

    protected boolean displayValues = true;

    protected boolean mirror = false;

    protected boolean duplicateLinkedCriterias = true;

    protected List<String> criteriaList;

    protected boolean defaultEdgeStyle = false;

    protected int edgeWidth = 1;

    protected Color backgroundColor = null;
    
    protected boolean alignLeafToRight = false;

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        Option oldOption = getOption();
        this.option = option;
        firePropertyChange(PROPERTY_OPTION, oldOption, option);
    }

    public boolean isDisplayNotEvaluated() {
        return displayNotEvaluated;
    }

    public void setDisplayNotEvaluated(boolean displayNotEvaluated) {
        boolean oldValue = isDisplayNotEvaluated();
        this.displayNotEvaluated = displayNotEvaluated;
        firePropertyChange(PROPERTY_DISPLAY_NOT_EVALUATED, oldValue, displayNotEvaluated);
    }

    public boolean isDisplayWeight() {
        return displayWeight;
    }

    public void setDisplayWeight(boolean displayWeight) {
        boolean oldDisplayWeight = isDisplayWeight();
        this.displayWeight = displayWeight;
        firePropertyChange(PROPERTY_DISPLAY_WEIGHT, oldDisplayWeight, displayWeight);
    }

    public int getXGap() {
        return xGap;
    }

    public void setXGap(int xGap) {
        int oldXGap = getXGap();
        this.xGap = xGap;
        firePropertyChange(PROPERTY_X_GAP, oldXGap, xGap);
    }

    public int getXEdgeGap() {
        return xEdgeGap;
    }

    public void setXEdgeGap(int xEdgeGap) {
        int oldXEdgeGap = getXEdgeGap();
        this.xEdgeGap = xEdgeGap;
        firePropertyChange(PROPERTY_X_EDGE_GAP, oldXEdgeGap, xEdgeGap);
    }

    public int getYGap() {
        return yGap;
    }

    public void setYGap(int yGap) {
        int oldYGap = getYGap();
        this.yGap = yGap;
        firePropertyChange(PROPERTY_Y_GAP, oldYGap, yGap);
    }

    public int getBoxXSize() {
        return boxXSize;
    }

    public void setBoxXSize(int boxXSize) {
        int oldBoxXSize = getBoxXSize();
        this.boxXSize = boxXSize;
        firePropertyChange(PROPERTY_BOX_X_SIZE, oldBoxXSize, boxXSize);
    }

    public int getBoxYSize() {
        return boxYSize;
    }

    public void setBoxYSize(int boxYSize) {
        int oldBoxYSize = getBoxYSize();
        this.boxYSize = boxYSize;
        firePropertyChange(PROPERTY_BOX_Y_SIZE, oldBoxYSize, boxYSize);
    }

    public List<LegendItem> getLegend() {
        return legend;
    }

    public void setLegend(List<LegendItem> legend) {
        if (CollectionUtils.isNotEmpty(this.legend)) {
            for (LegendItem legendItem : legend) {
                legendItem.removePropertyChangeListener(legendChangeListener);
            }
        }
        this.legend = legend;
        for (LegendItem legendItem : legend) {

            legendItem.addPropertyChangeListener(legendChangeListener);
            if ("Middle".equals(legendItem.getTitle())) {
                legendItem.setTitle("=");
            }
        }
    }

    public Integer getXLegend() {
        return xLegend;
    }

    public void setXLegend(Integer xLegend) {
        int oldXLegend = getXLegend();
        this.xLegend = xLegend;
        firePropertyChange(PROPERTY_X_LEGEND, oldXLegend, xLegend);
    }

    public Integer getYLegend() {
        return yLegend;
    }

    public void setYLegend(Integer yLegend) {
        int oldYLegend = getYLegend();
        this.yLegend = yLegend;
        firePropertyChange(PROPERTY_Y_LEGEND, oldYLegend, yLegend);
    }

    public List<String> getCriteriaList() {
        return criteriaList;
    }

    public void setCriteriaList(List<String> criteriaList) {
        List<String> oldCriteriaList = getCriteriaList();
        this.criteriaList = criteriaList;
        firePropertyChange(PROPERTY_CRITERIA_LIST, oldCriteriaList, criteriaList);
    }

    public boolean isResolveReference() {
        return resolveReference;
    }

    public void setResolveReference(boolean resolveReference) {
        boolean oldResolveReference = isResolveReference();
        this.resolveReference = resolveReference;
        firePropertyChange(PROPERTY_RESOLVE_REFERENCE, oldResolveReference, resolveReference);
    }

    public int getWeightFontSize() {
        return weightFontSize;
    }

    public void setWeightFontSize(int weightFontSize) {
        int oldValue = getWeightFontSize();
        this.weightFontSize = weightFontSize;
        firePropertyChange(PROPERTY_WEIGHT_FONT_SIZE, oldValue, weightFontSize);
    }

    public int getCriteriaNameFontSize() {
        return criteriaNameFontSize;
    }

    public void setCriteriaNameFontSize(int criteriaNameFontSize) {
        int oldValue = getCriteriaNameFontSize();
        this.criteriaNameFontSize = criteriaNameFontSize;
        firePropertyChange(PROPERTY_CRITERIA_NAME_FONT_SIZE, oldValue, criteriaNameFontSize);
    }

    public boolean isDisplayValues() {
        return displayValues;
    }

    public void setDisplayValues(boolean displayValues) {
        boolean oldValue = isDisplayValues();
        this.displayValues = displayValues;
        firePropertyChange(PROPERTY_DISPLAY_VALUES, oldValue, displayValues);
    }

    public boolean isMirror() {
        return mirror;
    }

    public void setMirror(boolean mirror) {
        boolean oldValue = isMirror();
        this.mirror = mirror;
        firePropertyChange(PROPERTY_MIRROR, oldValue, mirror);
    }

    public boolean isDefaultEdgeStyle() {
        return defaultEdgeStyle;
    }

    public void setDefaultEdgeStyle(boolean defaultEdgeStyle) {
        boolean oldValue = isDefaultEdgeStyle();
        this.defaultEdgeStyle = defaultEdgeStyle;
        firePropertyChange(PROPERTY_DEFAULT_EDGE_STYLE, oldValue, defaultEdgeStyle);
    }

    public int getEdgeWidth() {
        return edgeWidth;
    }

    public void setEdgeWidth(int edgeWidth) {
        int oldValue = getEdgeWidth();
        this.edgeWidth = edgeWidth;
        firePropertyChange(PROPERTY_EDGE_WIDTH,  oldValue, edgeWidth);
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        Color oldValue = getBackgroundColor();
        this.backgroundColor = backgroundColor;
        firePropertyChange(PROPERTY_BACKGROUND_COLOR,  oldValue, backgroundColor);
    }

    public boolean isDuplicateLinkedCriterias() {
        return duplicateLinkedCriterias;
    }

    public void setDuplicateLinkedCriterias(boolean duplicateLinkedCriterias) {
        boolean oldValue = isDuplicateLinkedCriterias();
        this.duplicateLinkedCriterias = duplicateLinkedCriterias;
        firePropertyChange(PROPERTY_DUPLICATE_LINKED_CRITERIAS, oldValue, duplicateLinkedCriterias);
    }

    public boolean isShowInputValue() {
        return showInputValue;
    }

    public void setShowInputValue(boolean showInputValue) {
        boolean oldValue = isShowInputValue();
        this.showInputValue = showInputValue;
        firePropertyChange(PROPERTY_SHOW_INPUT_VALUE, oldValue, showInputValue);
    }
    
    public void setCutCharacterCount(int cutCharacterCount) {
        int oldValue = getCutCharacterCount();
        this.cutCharacterCount = cutCharacterCount;
        firePropertyChange(PROPERTY_CUT_CHARACTER_COUNT, oldValue, cutCharacterCount);
    }
    
    public int getCutCharacterCount() {
        return cutCharacterCount;
    }
    
    public boolean isAlignLeafToRight() {
        return alignLeafToRight;
    }
    
    public void setAlignLeafToRight(boolean alignLeafToRight) {
        boolean oldValue = isAlignLeafToRight();
        this.alignLeafToRight = alignLeafToRight;
        firePropertyChange(PROPERTY_ALIGN_LEAF_TO_RIGHT, oldValue, alignLeafToRight);
    }
}
