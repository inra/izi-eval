/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.synoptic;

import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.beans.AbstractSerializableBean;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

/**
 * Represent a legend line
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.2
 */
public class LegendItem extends AbstractSerializableBean {

    public static final String PROPERTY_LEGEND_TITLE = "title";

    public static final String PROPERTY_LEGEND_COLOR = "color";

    private static final long serialVersionUID = 1L;

    private static final Color[] DEFAULT_LEGEND_COLORS = new Color[]{
            new Color(255, 0, 0),
            new Color(255, 0, 0),
            new Color(255, 102, 0),
            new Color(255, 204, 102),
            new Color(255, 255, 0),
            new Color(204, 255, 102),
            new Color(102, 204, 0),
            new Color(0, 153, 0),
            new Color(0, 153, 0)
    };

    // default color (for * alias null value)
    private static final Color DEFAULT_COLOR = Color.LIGHT_GRAY;

    private static final List<int[]> RESOLV_VALUES = Lists.newArrayList(
            new int[]{1, 9},
            new int[]{1, 5, 9},
            new int[]{1, 3, 7, 9},
            new int[]{1, 3, 5, 7, 9},
            new int[]{1, 2, 4, 6, 7, 9},
            new int[]{1, 2, 4, 5, 6, 7, 9},
            new int[]{1, 2, 3, 4, 6, 7, 8, 9},
            new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}
    );

    /**
     * Build default legend for this graph
     *
     * @return the max scale value count
     */
    public static List<LegendItem> buildDefaultLegend() {

        LinkedList<LegendItem> legend = Lists.newLinkedList();
        legend.add(new LegendItem("+ + + +", DEFAULT_LEGEND_COLORS[8]));
        legend.add(new LegendItem("+ + +", DEFAULT_LEGEND_COLORS[7]));
        legend.add(new LegendItem("+ +", DEFAULT_LEGEND_COLORS[6]));
        legend.add(new LegendItem("+", DEFAULT_LEGEND_COLORS[5]));
        legend.add(new LegendItem("=", DEFAULT_LEGEND_COLORS[4]));
        legend.add(new LegendItem("-", DEFAULT_LEGEND_COLORS[3]));
        legend.add(new LegendItem("- -", DEFAULT_LEGEND_COLORS[2]));
        legend.add(new LegendItem("- - -", DEFAULT_LEGEND_COLORS[1]));
        legend.add(new LegendItem("- - - -", DEFAULT_LEGEND_COLORS[0]));

        return legend;
    }

    public static List<LegendItem> completeLegend(List<LegendItem> legend, int maxClasses) {

        if (CollectionUtils.isEmpty(legend)) {

            // use a default one
            legend = LegendItem.buildDefaultLegend();
        }

        LegendItem legendItem = legend.get(0);

        // must add more legend on it
        while (legend.size() < maxClasses) {
            LegendItem newLegendItem = new LegendItem(
                    legendItem.getTitle() + "+",
                    legendItem.getColor());
            legend.add(0, newLegendItem);
        }

        return legend;
    }

    public static Color getColor(Integer value, int valueMax, SynopticModel model) {

        Color result;

        // default color is return for null value (alias * in masc)
        if (model.isDisplayNotEvaluated() || value == null) {
            result = DEFAULT_COLOR;
        } else {

            List<LegendItem> legend = model.getLegend();

            int index;

            if (valueMax >= RESOLV_VALUES.size()) {
                index = value;
                if (index == 0) {
                    index++; // fix array index out of bound exception
                }
            } else {
                if (valueMax==0) {
                    valueMax++;
                }
                index = RESOLV_VALUES.get(valueMax - 1)[value];
            }

            LegendItem legendItem = legend.get(legend.size() - index);
            result = legendItem.getColor();
        }
        return result;
    }

    protected String title;

    protected Color color;

    public LegendItem() {
        // for serialisation
    }

    public LegendItem(String title, Color color) {
        this();
        this.title = title;
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        String oldTitle = getTitle();
        this.title = title;
        firePropertyChange(PROPERTY_LEGEND_TITLE, oldTitle, title);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        Color oldColor = getColor();
        this.color = color;
        firePropertyChange(PROPERTY_LEGEND_COLOR, oldColor, color);
    }
}
