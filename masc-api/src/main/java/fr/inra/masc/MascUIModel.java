/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc;

import static org.nuiton.i18n.I18n._;

import java.beans.PropertyChangeListener;
import java.io.File;

import org.jdesktop.beans.AbstractSerializableBean;

import fr.inra.masc.charts.ChartModel;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.RScriptModel;
import fr.inra.masc.reports.ReportModel;
import fr.inra.masc.synoptic.SynopticModel;

/**
 * Model for masc application
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class MascUIModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_MASC_MODEL = "mascModel";

    public static final String PROPERTY_SYNOPTIQUE_MODEL = "synopticModel";

    public static final String PROPERTY_CHART_MODEL = "chartModel";

    public static final String PROPERTY_REPORT_MODEL = "reportModel";

    public static final String PROPERTY_MASC_FILE = "mascFile";

    public static final String PROPERTY_DEXI_FILE = "dexiFile";

    public static final String PROPERTY_MODEL_CHANGED = "modelChanged";
    
    public static final String PROPERTY_EVALUATION_DIRTY = "evaluationDirty";

    public static final String PROPERTY_FILE_OPEN = "fileOpen";

    public static final String PROPERTY_MASC_UI_TITLE = "mascUITitle";

    public static final String PROPERTY_TRANSFORM_CRITERIA_TO_BASIC = "transformCriteriaToBasic";

    public static final String PROPERTY_COMMENT = "comment";

    protected MascModel mascModel;

    protected SynopticModel synopticModel;

    protected ChartModel chartModel;

    protected ReportModel reportModel;

    protected String comment;

    protected boolean fileOpen;

    protected String mascUITitle;

    protected boolean showThresholdHelp = true;

    protected boolean showSynopticHelp = true;

    protected transient File mascFile;

    protected transient File dexiFile;

    protected transient String mascFileName;

    protected transient boolean modelChanged;
    
    // always dirty before first evaluation
    protected transient boolean evaluationDirty = true;

    protected transient boolean transformCriteriaToBasic;

    protected transient PropertyChangeListener modelChangedListener;

    public boolean isShowThresholdHelp() {
        return showThresholdHelp;
    }

    public void setShowThresholdHelp(boolean showThresholdHelp) {
        this.showThresholdHelp = showThresholdHelp;
    }

    public boolean isShowSynopticHelp() {
        return showSynopticHelp;
    }

    public void setShowSynopticHelp(boolean showSynopticHelp) {
        this.showSynopticHelp = showSynopticHelp;
    }

    public boolean isTransformCriteriaToBasic() {
        return transformCriteriaToBasic;
    }

    public void setTransformCriteriaToBasic(boolean transformCriteriaToBasic) {
        boolean oldValue = isTransformCriteriaToBasic();
        this.transformCriteriaToBasic = transformCriteriaToBasic;
        firePropertyChange(PROPERTY_TRANSFORM_CRITERIA_TO_BASIC, oldValue, transformCriteriaToBasic);
    }

    public String getMascFileName() {
        return mascFileName;
    }

    public void setMascFileName(String mascFileName) {
        this.mascFileName = mascFileName;
    }

    public MascModel getMascModel() {
        return mascModel;
    }

    public String getMascUITitle() {
        if (mascUITitle == null) {
            mascUITitle = _("masc.ui.title");
        }
        return mascUITitle;
    }

    public void setMascUITitle(String mascUITitle) {
        String oldMascUITitle = this.mascUITitle;
        this.mascUITitle = mascUITitle;
        firePropertyChange(PROPERTY_MASC_UI_TITLE, oldMascUITitle, mascUITitle);
    }

    public void setMascModel(MascModel mascModel) {
        MascModel oldMascModel = this.mascModel;
        this.mascModel = mascModel;
        setFileOpen(true);
        mascModel.addPropertyChangeListener(modelChangedListener);
        firePropertyChange(PROPERTY_MASC_MODEL, oldMascModel, mascModel);
    }

    public boolean isFileOpen() {
        return fileOpen;
    }

    public void setFileOpen(boolean fileOpen) {
        boolean oldValue = isFileOpen();
        this.fileOpen = fileOpen;
        firePropertyChange(PROPERTY_FILE_OPEN, oldValue, fileOpen);
    }

    public SynopticModel getSynopticModel() {
        if (synopticModel == null) {
            setSynopticModel(new SynopticModel());
        }
        return synopticModel;
    }

    public void setSynopticModel(SynopticModel synopticModel) {
        SynopticModel oldSynopticModel = this.synopticModel;
        this.synopticModel = synopticModel;
        synopticModel.addPropertyChangeListener(modelChangedListener);
        firePropertyChange(PROPERTY_SYNOPTIQUE_MODEL, oldSynopticModel, synopticModel);
    }

    public ChartModel getChartModel() {
        if (chartModel == null) {
            setChartModel(new ChartModel());
        }
        return chartModel;
    }

    public void setChartModel(ChartModel chartModel) {
        ChartModel oldChartModel = this.chartModel;
        this.chartModel = chartModel;
        chartModel.addPropertyChangeListener(modelChangedListener);
        firePropertyChange(PROPERTY_CHART_MODEL, oldChartModel, chartModel);
    }

    public ReportModel getReportModel() {
        if (reportModel == null) {
            setReportModel(new ReportModel());
        }
        return reportModel;
    }

    public void setReportModel(ReportModel reportModel) {
        ReportModel oldReportModel = this.reportModel;
        this.reportModel = reportModel;
        reportModel.addPropertyChangeListener(modelChangedListener);
        firePropertyChange(PROPERTY_REPORT_MODEL, oldReportModel, reportModel);
    }

    public File getMascFile() {
        return mascFile;
    }

    public void setMascFile(File mascFile) {
        File oldMascFile = getMascFile();
        this.mascFile = mascFile;
        if (mascFile != null) {
            setMascFileName(mascFile.getName().replace(".masc", ""));
            setMascUITitle(_("masc.ui.title.fileOpen", mascFile.getPath()));
        }
        firePropertyChange(PROPERTY_MASC_FILE, oldMascFile, mascFile);
    }

    public File getDexiFile() {
        return dexiFile;
    }

    public void setDexiFile(File dexiFile) {
        File oldDexiFile = getDexiFile();
        this.dexiFile = dexiFile;
        if (dexiFile != null) {
            setMascFileName(dexiFile.getName().replace(".dxi", ""));
            setMascUITitle(_("masc.ui.title.fileOpen", dexiFile.getPath()));
        }
        firePropertyChange(PROPERTY_DEXI_FILE, oldDexiFile, dexiFile);
    }

    public boolean isModelChanged() {
        return modelChanged;
    }

    public void setModelChanged(boolean modelChanged) {
        boolean oldValue = isModelChanged();
        this.modelChanged = modelChanged;
        firePropertyChange(PROPERTY_MODEL_CHANGED, oldValue, modelChanged);
    }
    
    public boolean isEvaluationDirty() {
        return evaluationDirty;
    }

    public void setEvaluationDirty(boolean evaluationDirty) {
        boolean oldValue = isEvaluationDirty();
        this.evaluationDirty = evaluationDirty;
        firePropertyChange(PROPERTY_EVALUATION_DIRTY, oldValue, evaluationDirty);
    }

    public RScriptModel getScriptModel() {
        if (getReportModel().getScriptModel() == null) {
            setScriptModel(new RScriptModel());
        }
        return getReportModel().getScriptModel();
    }

    public void setScriptModel(RScriptModel scriptModel) {
        scriptModel.addPropertyChangeListener(modelChangedListener);
        getReportModel().setScriptModel(scriptModel);
    }
    
    public void setComment(String comment) {
        String oldValue = getComment();
        this.comment = comment;
        setModelChanged(true);
        firePropertyChange(PROPERTY_MODEL_CHANGED, oldValue, comment);
    }
    
    public String getComment() {
        return comment;
    }
}
