/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.reports;

import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.RScriptModel;
import java.awt.image.BufferedImage;
import java.io.File;
import org.jdesktop.beans.AbstractSerializableBean;
import org.w3c.dom.svg.SVGDocument;

public class ReportModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_MASC_MODEL = "mascModel";

    public static final String PROPERTY_REPORT_FILE = "reportFile";

    public static final String PROPERTY_SHOW_DESCRIPTION = "showDescription";

    public static final String PROPERTY_SHOW_TREE = "showTree";

    public static final String PROPERTY_SHOW_THRESHOLDS = "showThresholds";

    public static final String PROPERTY_SHOW_OPTIONS = "showOptions";

    public static final String PROPERTY_SHOW_GRAPHIC = "showGraphic";

    public static final String PROPERTY_SHOW_SYNOPTIC = "showSynoptic";

    public static final String PROPERTY_SHOW_AS = "showAs";

    public static final String PROPERTY_GRAPHIC = "graphic";

    public static final String PROPERTY_SYNOPTIC = "synoptic";

    public static final String PROPERTY_SCRIPT_MODEL = "scriptModel";

    protected MascModel mascModel;

    protected RScriptModel scriptModel;

    protected File reportFile;

    protected boolean showDescription = true;

    protected boolean showTree = true;

    protected boolean showThresholds = true;

    protected boolean showOptions = true;

    protected boolean showGraphic = true;

    protected boolean showSynoptic = true;

    protected boolean showAs = false;

    protected transient BufferedImage graphic;

    protected transient SVGDocument synoptic;

    public MascModel getMascModel() {
        return mascModel;
    }

    public void setMascModel(MascModel mascModel) {
        MascModel oldValue = getMascModel();
        this.mascModel = mascModel;
        firePropertyChange(PROPERTY_MASC_MODEL, oldValue, mascModel);
    }

    public File getReportFile() {
        return reportFile;
    }

    public void setReportFile(File reportFile) {
        File oldValue = getReportFile();
        this.reportFile = reportFile;
        firePropertyChange(PROPERTY_REPORT_FILE, oldValue, reportFile);
    }

    public boolean isShowDescription() {
        return showDescription;
    }

    public boolean getShowDescription() {
        return showDescription;
    }

    public void setShowDescription(boolean showDescription) {
        boolean oldValue = getShowDescription();
        this.showDescription = showDescription;
        firePropertyChange(PROPERTY_SHOW_DESCRIPTION, oldValue, showDescription);
    }

    public boolean isShowTree() {
        return showTree;
    }

    public boolean getShowTree() {
        return showTree;
    }

    public void setShowTree(boolean showTree) {
        boolean oldValue = getShowTree();
        this.showTree = showTree;
        firePropertyChange(PROPERTY_SHOW_TREE, oldValue, showTree);
    }

    public boolean isShowThresholds() {
        return showThresholds;
    }

    public boolean getShowThresholds() {
        return showThresholds;
    }

    public void setShowThresholds(boolean showThresholds) {
        boolean oldValue = getShowThresholds();
        this.showThresholds = showThresholds;
        firePropertyChange(PROPERTY_SHOW_THRESHOLDS, oldValue, showThresholds);
    }

    public boolean isShowOptions() {
        return showOptions;
    }

    public boolean getShowOptions() {
        return showOptions;
    }

    public void setShowOptions(boolean showOptions) {
        boolean oldValue = getShowOptions();
        this.showOptions = showOptions;
        firePropertyChange(PROPERTY_SHOW_OPTIONS, oldValue, showOptions);
    }

    public boolean isShowGraphic() {
        return showGraphic;
    }

    public boolean getShowGraphic() {
        return showGraphic;
    }

    public void setShowGraphic(boolean showGraphic) {
        boolean oldValue = getShowGraphic();
        this.showGraphic = showGraphic;
        firePropertyChange(PROPERTY_SHOW_GRAPHIC, oldValue, showGraphic);
    }

    public boolean isShowSynoptic() {
        return showSynoptic;
    }

    public boolean getShowSynoptic() {
        return showSynoptic;
    }

    public void setShowSynoptic(boolean showSynoptic) {
        boolean oldValue = getShowSynoptic();
        this.showSynoptic = showSynoptic;
        firePropertyChange(PROPERTY_SHOW_SYNOPTIC, oldValue, showSynoptic);
    }

    public boolean isShowAs() {
        return showAs;
    }

    public boolean getShowAs() {
        return showAs;
    }

    public void setShowAs(boolean showAs) {
        boolean oldValue = getShowAs();
        this.showAs = showAs;
        firePropertyChange(PROPERTY_SHOW_AS, oldValue, showAs);
    }

    public BufferedImage getGraphic() {
        return graphic;
    }

    public void setGraphic(BufferedImage graphic) {
        BufferedImage oldValue = getGraphic();
        this.graphic = graphic;
        firePropertyChange(PROPERTY_GRAPHIC, oldValue, graphic);
    }

    public SVGDocument getSynoptic() {
        return synoptic;
    }

    public void setSynoptic(SVGDocument synoptic) {
        SVGDocument oldValue = getSynoptic();
        this.synoptic = synoptic;
        firePropertyChange(PROPERTY_SYNOPTIC, oldValue, synoptic);
    }

    public RScriptModel getScriptModel() {
        return scriptModel;
    }

    public void setScriptModel(RScriptModel scriptModel) {
        RScriptModel oldValue = getScriptModel();
        this.scriptModel = scriptModel;
        firePropertyChange(PROPERTY_SCRIPT_MODEL, oldValue, scriptModel);
    }

} //ReportModel
