/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.io.parser;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.inra.masc.MascTechnicalException;
import fr.inra.masc.utils.MascUtil;
import fr.inra.masc.io.MascXmlConstant;
import fr.inra.masc.model.ComputableCriteria;
import fr.inra.masc.model.ComputableCriteriaImpl;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.EditableCriteriaImpl;
import fr.inra.masc.model.Function;
import fr.inra.masc.model.FunctionImpl;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.MascModelImpl;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionImpl;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.model.OptionValueImpl;
import fr.inra.masc.model.Scale;
import fr.inra.masc.model.ScaleGroup;
import fr.inra.masc.model.ScaleImpl;
import fr.inra.masc.model.ScaleOrder;
import fr.inra.masc.model.ScaleValue;
import fr.inra.masc.model.ScaleValueImpl;
import fr.reseaumexico.model.parser.XmlParser;
import org.apache.commons.lang3.StringUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Parser for DEXi file, build a {@link MascModel}
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class DexiModelParser extends XmlParser<MascModel> implements MascXmlConstant {

    // key is criteria name + scale values
    protected Map<String, Criteria> targetCriterias;

    protected Multimap<String, Criteria> criteriaCached;

    protected Map<String, Criteria> rootCriterias;

    public DexiModelParser() {
        targetCriterias = Maps.newHashMap();
        criteriaCached = ArrayListMultimap.create();
        rootCriterias = Maps.newHashMap();
    }

    @Override
    protected MascModel parseModel(XmlPullParser parser) throws IOException, XmlPullParserException, ParseException {
        MascModel model = new MascModelImpl();

        parseMeta(parser, model);
        parseAttributes(parser, model);

        return model;
    }

    protected void parseMeta(XmlPullParser parser, MascModel model) throws IOException, XmlPullParserException {

        // file must start with DEXi tag
        if (parser.next() == XmlPullParser.START_TAG &&
            !parserEqual(parser, DEXI)) {
            throw new MascTechnicalException("Masc file must start with " + DEXI + " tag");
        }

        // while all child attributes is not parsed
        while (!(testCurrentStartTag(parser, ATTRIBUTE))) {

            // start parsing model attributes
            // parse name
            if (testNextStartTag(parser, MODEL_NAME)) {
                model.setName(nextText(parser));

                // read next tag
                parser.nextTag();
            }
            
            if (testCurrentStartTag(parser, MODEL_VERSION)) {
                // added in dexi 4.0
                model.setVersion(nextText(parser));
                
                // read next tag
                parser.nextTag();
            }
            
            if (testCurrentStartTag(parser, MODEL_CREATED)) {
                // added in dexi 4.0
                model.setCreated(nextText(parser));
                
                // read next tag
                parser.nextTag();
            }

            // parse description
            if (testCurrentStartTag(parser, MODEL_DESCRIPTION)) {

                List<String> descriptions = Lists.newArrayList();
                while (!(parser.nextTag() == XmlPullParser.END_TAG &&
                         parserEqual(parser, MODEL_DESCRIPTION))) {

                    if (parserEqual(parser, LINE)) {
                        descriptions.add(nextText(parser));
                    }
                }

                model.setDescription(descriptions);

                // read next tag
                parser.nextTag();
            }

            // parse options
            List<Option> options = Lists.newArrayList();
            while (testCurrentStartTag(parser, OPTION)) {

                Option option = new OptionImpl();
                option.setName(nextText(parser));
                options.add(option);

                // read end option tag
                parser.nextTag();
            }
            model.setOption(options);

            // parse settings
            if (testCurrentStartTag(parser, SETTINGS)) {

                Map<String, String> settings = new LinkedHashMap<String, String>();
                while (!(testNextEndTag(parser, SETTINGS))) {
                    settings.put(parser.getName(), nextText(parser));
                }
                model.setSettings(settings);

                // read next tag
                parser.nextTag();
            }
        }
    }

    protected void parseAttributes(XmlPullParser parser, MascModel model) throws IOException, XmlPullParserException {
        List<Criteria> criterias = Lists.newArrayList();

        boolean firstRoot = true;
        while (testCurrentStartTag(parser, ATTRIBUTE)) {

            Criteria criteria = parseAttribute(parser, model, firstRoot);
            criterias.add(criteria);

            firstRoot = false;

            // Read closeTag
            parser.nextTag();
        }
        model.setCriteria(criterias);
    }

    protected Criteria parseAttribute(XmlPullParser parser,
                                      MascModel model,
                                      boolean firstRoot) throws IOException, XmlPullParserException {

        String name = null;
        String description = null;
        Scale scale = new ScaleImpl();
        Function function = null;
        List<OptionValue> optionValues = Lists.newArrayList();
        List<Criteria> criterias = Lists.newArrayList();

        // while all child attributes is not parsed
        while (!(testCurrentEndTag(parser, ATTRIBUTE))) {

            // parse name
            if (testNextStartTag(parser, CRITERIA_NAME)) {

                name = nextText(parser);

                // read next tag
                parser.nextTag();
            }

            // parse description
            if (testCurrentStartTag(parser, CRITERIA_DESCRIPTION)) {

                description = nextText(parser);

                // read next tag
                parser.nextTag();
            }

            // parse scales
            if (testCurrentStartTag(parser, SCALE)) {

                scale = parseScale(parser);
            }

            // parse function
            if (testCurrentStartTag(parser, FUNCTION)) {

                function = parseFunction(parser);
            }

            // parse option value
            int optionValueCnt = 0;
            while (testCurrentStartTag(parser, OPTION)) {

                String text = nextText(parser);

                // verify that value is like "123" not "null" by example
                boolean canBeParsed = true;
                if (!text.equals(MascUtil.OPTION_VALUE_DEFAULT)) {
                    try {
                        Integer.parseInt(text);
                    } catch (NumberFormatException eee) {
                        canBeParsed = false;
                    }
                }

                // can contain multiple values
                if (StringUtils.isNotBlank(text) && canBeParsed) {
                    for (char c : text.toCharArray()) {

                        Integer value = null;
                        if (c != '*') {
                            value = c - '0';
                        }

                        OptionValue optionValue = new OptionValueImpl();
                        optionValue.setValue(value);
                        Option option = model.getOption(optionValueCnt);

                        if (option != null) {

                            // initialize option value list
                            if (option.getOptionValue() == null) {
                                List<OptionValue> optionValuesList = Lists.newArrayList();
                                option.setOptionValue(optionValuesList);
                            }
                            option.addOptionValue(optionValue);
                            optionValues.add(optionValue);
                        }

                    }
                }
                
                optionValueCnt++;

                // read next tag
                parser.nextTag();
            }

            // parse attributes
            if (testCurrentStartTag(parser, ATTRIBUTE)) {

                while (!(testCurrentEndTag(parser, ATTRIBUTE))) {

                    criterias.add(parseAttribute(parser, model, firstRoot));
                    parser.nextTag();
                }
            }
        }

        Criteria current;
        // root node override other created
//        if (!criterias.isEmpty()) {
        if (function != null) {

            // non editable criteria
            ComputableCriteria computableCriteria = new ComputableCriteriaImpl();
            computableCriteria.setFunction(function);
            current = computableCriteria;
        } else {

            // editable criteria
            current = new EditableCriteriaImpl();
        }

        // see if is already created
        Collection<Criteria> existingCriteria = criteriaCached.get(name);

        // if same node existing, root node will have name, reference will have name : name + _reference count
        current.setReference(false);

        // fill children
        if (!criterias.isEmpty()) {
            current.setChild(criterias);
        }

        // all node in first root are references
        if (!existingCriteria.isEmpty()) {

            // get target criteria
            Criteria targetCriteria = getTagetCriteria(name, scale);

            // duplicate node in root branch are all reference,
            // in other branchs target have children have no childs
            if (targetCriteria == null) {

                // prefer one with children
                Criteria target = current;
                for (Criteria existing : Lists.newArrayList(existingCriteria)) {
                    Collection<Criteria> existingChildren = existing.getChild();
                    if (existingChildren != null && !existingChildren.isEmpty()) {
                        existingCriteria.remove(existing);
                        existingCriteria.add(target);
                        target = existing;
                    }
                }

                // register current as target
                registerAsTarget(target, name, scale, existingCriteria);
            } else {

                if (!criterias.isEmpty()) {

                    // register current as new target
                    registerAsTarget(current, name, scale, existingCriteria);
                } else if (!firstRoot) {

                    // if old target have no child, prefer last
                    Collection<Criteria> targetChildren = targetCriteria.getChild();
                    if (targetChildren == null || targetChildren.isEmpty()) {

                        // register current as new target
                        registerAsTarget(current, name, scale, existingCriteria);
                    } else {
                        registerAsReference(current, targetCriteria);
                    }
                } else {
                    registerAsReference(current, targetCriteria);
                }
            }

        }

        // fill criteria attributes
        current.setName(name);
        if (StringUtils.isNotEmpty(description)) {
            current.setDescription(description);
        }
        current.setScale(scale);

        // fill options values
        for (OptionValue optionValue : optionValues) {
            optionValue.setCriteria(current);
        }

        criteriaCached.put(name, current);

        return current;
    }

    protected void registerAsTarget(Criteria criteria, String name, Scale scale, Collection<Criteria> referenceCriterias) {

        Criteria targetCriteria = getTagetCriteria(name, scale);

        // this node is the real target
        keepTargetCriteria(name, scale, criteria);

        if (targetCriteria != null) {
            // so old target is now reference
            registerAsReference(targetCriteria, criteria);
        }

        // apply for all references
        for (Criteria existing : referenceCriterias) {
            registerAsReference(existing, criteria);
        }
    }

    protected void registerAsReference(Criteria criteria, Criteria targetCriteria) {
        if (criteria.getChild() == null || criteria.getChild().isEmpty()) {
            criteria.setReference(true);
            criteria.setChild(Lists.newArrayList(targetCriteria));
        }
    }

    protected Scale parseScale(XmlPullParser parser) throws IOException, XmlPullParserException {

        Scale scale = new ScaleImpl();

        // order is important
        List<ScaleValue> scaleValues = Lists.newArrayList();
        while (!(testCurrentEndTag(parser, SCALE))) {

            // read next tag
            parser.nextTag();

            // scale order
            if (testCurrentStartTag(parser, SCALE_ORDER)) {

                String order = nextText(parser);
                ScaleOrder scaleOrder = ScaleOrder.valueOf(order);
                scale.setOrder(scaleOrder);

                // read next tag
                parser.nextTag();
            }


            if (testCurrentStartTag(parser, SCALE_VALUE)) {

                ScaleValue scaleValue = new ScaleValueImpl();

                // read next tag
                parser.nextTag();

                // while all child attributes is not parsed
                while (!(testCurrentEndTag(parser, SCALE_VALUE))) {

                    // parse name
                    if (testCurrentStartTag(parser, SCALE_VALUE_NAME)) {

                        scaleValue.setName(nextText(parser));

                        // read next tag
                        parser.nextTag();
                    }

                    // parse description
                    if (testCurrentStartTag(parser, SCALE_VALUE_DESCRIPTION)) {

                        scaleValue.setDescription(nextText(parser));

                        // read next tag
                        parser.nextTag();
                    }

                    // parse group
                    if (testCurrentStartTag(parser, SCALE_VALUE_GROUP)) {

                        String groupValue = nextText(parser);
                        scaleValue.setGroup(ScaleGroup.valueOf(groupValue));

                        // read next tag
                        parser.nextTag();
                    }
                }
                scaleValues.add(scaleValue);
            }
        }

        if (scale.getOrder() == null) {
            // ascending is by default
            scale.setOrder(ScaleOrder.ASC);
        }

        // read scale close tag
        parser.nextTag();

        // for decresing order
        scale.setScaleValue(scaleValues);

        return scale;
    }

    protected Function parseFunction(XmlPullParser parser) throws IOException, XmlPullParserException {
        Function function = new FunctionImpl();

        // while all child attributes is not parsed
        while (!(testCurrentEndTag(parser, FUNCTION))) {

            // parse low
            if (testNextStartTag(parser, FUNCTION_LOW)) {

                function.setLow(nextText(parser));

                // read next tag
                parser.nextTag();
            }

            // parse high
            if (testCurrentStartTag(parser, FUNCTION_HIGH)) {

                function.setHigh(nextText(parser));

                // read next tag
                parser.nextTag();
            }

            // parse entered
            if (testCurrentStartTag(parser, FUNCTION_ENTERED)) {

                function.setEntered(nextText(parser));

                // read next tag
                parser.nextTag();
            }

            // parse weights
            if (testCurrentStartTag(parser, FUNCTION_WEIGHTS)) {

                function.setWeights(nextText(parser));

                // read next tag
                parser.nextTag();
            }
            
            // added in dexi 4.0
            if (testCurrentStartTag(parser, FUNCTION_LOCWEIGHTS)) {

                function.setLocweights(nextText(parser));

                // read next tag
                parser.nextTag();
            }
            
            // added in dexi 4.0
            if (testCurrentStartTag(parser, FUNCTION_NORMLOCWEIGHTS)) {

                function.setNormlocweights(nextText(parser));

                // read next tag
                parser.nextTag();
            }

            // parse consist
            if (testCurrentStartTag(parser, FUNCTION_CONSIST)) {

                function.setConsist(Boolean.parseBoolean(nextText(parser)));

                // read next tag
                parser.nextTag();
            }

            // parse rounding
            if (testCurrentStartTag(parser, FUNCTION_ROUNDING)) {

                function.setRounding(Integer.parseInt(nextText(parser)));

                // read next tag
                parser.nextTag();
            }
        }

        // read function close tag
        parser.nextTag();

        return function;
    }

    protected void keepTargetCriteria(String name, Scale scale, Criteria criteria) {
        targetCriterias.put(name + scale.toString(), criteria);
    }

    protected Criteria getTagetCriteria(String name, Scale scale) {
        return targetCriterias.get(name + scale.toString());
    }

    private String nextText(XmlPullParser parser) throws IOException, XmlPullParserException {
//        return MascUtil.convertToUTF8(parser.nextText());
        return parser.nextText();
    }

    @Override
    public boolean parserEqual(XmlPullParser parser, String name) {
        String tagName = parser.getName();
        return tagName.equalsIgnoreCase(name);
    }
}
