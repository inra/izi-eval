/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.io;

import fr.inra.masc.model.ComputableCriteria;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.Function;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Scale;
import fr.inra.masc.model.ScaleValue;

/**
 * Regroup all masc xml constants tags
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public interface MascXmlConstant {

    public static final String WEIGHTS_SEPARATOR = ";";
    public static final String DEXI = "DEXi";
    public static final String LINE = "line";
    public static final String ATTRIBUTE = "attribute";
    public static final String MODEL_NAME = MascModel.PROPERTY_NAME;
    public static final String MODEL_VERSION = MascModel.PROPERTY_VERSION;
    public static final String MODEL_CREATED = MascModel.PROPERTY_CREATED;
    public static final String MODEL_DESCRIPTION = MascModel.PROPERTY_DESCRIPTION;
    public static final String OPTION = MascModel.PROPERTY_OPTION;
    public static final String SETTINGS = MascModel.PROPERTY_SETTINGS;
    public static final String CRITERIA_NAME = Criteria.PROPERTY_NAME;
    public static final String CRITERIA_DESCRIPTION = Criteria.PROPERTY_DESCRIPTION;
    public static final String SCALE = Criteria.PROPERTY_SCALE;
    public static final String SCALE_ORDER = Scale.PROPERTY_ORDER;
    public static final String SCALE_VALUE = ScaleValue.class.getSimpleName();
    public static final String SCALE_VALUE_NAME = ScaleValue.PROPERTY_NAME;
    public static final String SCALE_VALUE_DESCRIPTION = ScaleValue.PROPERTY_DESCRIPTION;
    public static final String SCALE_VALUE_GROUP = ScaleValue.PROPERTY_GROUP;
    public static final String FUNCTION = ComputableCriteria.PROPERTY_FUNCTION;
    public static final String FUNCTION_LOW = Function.PROPERTY_LOW;
    public static final String FUNCTION_ENTERED = Function.PROPERTY_ENTERED;
    public static final String FUNCTION_WEIGHTS = Function.PROPERTY_WEIGHTS;
    public static final String FUNCTION_HIGH = Function.PROPERTY_HIGH;
    public static final String FUNCTION_CONSIST = Function.PROPERTY_CONSIST;
    public static final String FUNCTION_ROUNDING = Function.PROPERTY_ROUNDING;
    public static final String FUNCTION_LOCWEIGHTS = Function.PROPERTY_LOCWEIGHTS;
    public static final String FUNCTION_NORMLOCWEIGHTS = Function.PROPERTY_NORMLOCWEIGHTS;
    
}
