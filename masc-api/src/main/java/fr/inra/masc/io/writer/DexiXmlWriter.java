/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.io.writer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import fr.inra.masc.io.MascXmlConstant;
import fr.inra.masc.model.ComputableCriteria;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.Function;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.model.Scale;
import fr.inra.masc.model.ScaleOrder;
import fr.inra.masc.model.ScaleValue;
import fr.inra.masc.utils.MascUtil;
import fr.reseaumexico.model.writer.XmlNode;
import fr.reseaumexico.model.writer.XmlWriter;

/**
 * Writer to create DEXi file with {@link MascModel} model
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class DexiXmlWriter extends XmlWriter<MascModel> implements MascXmlConstant {

    protected boolean forROutput;

    protected Map<String, Integer> cachedCriteriaNames;

    private static final Log log = LogFactory.getLog(DexiXmlWriter.class);

    public DexiXmlWriter(MascModel model, boolean forROutput) throws IOException {
        super(model);
        this.forROutput = forROutput;
        this.cachedCriteriaNames = Maps.newHashMap();
    }

    @Override
    public XmlNode getRootElement() {
        XmlNode rootXmlNode = new XmlNode(DEXI);
        composeMeta(rootXmlNode);
        composeAttributes(rootXmlNode);

        return rootXmlNode;
    }

    protected void composeMeta(XmlNode rootXmlNode) {

        // model name
        String modelName = model.getName();
        if (modelName != null) {
            add(rootXmlNode, MODEL_NAME, modelName);
        }
        
        // version
        String modelVersion = model.getVersion();
        if (modelVersion != null) {
            add(rootXmlNode, MODEL_VERSION, modelVersion);
        }

        // created
        String modelCreated = model.getCreated();
        if (modelCreated != null) {
            add(rootXmlNode, MODEL_CREATED, modelCreated);
        }

        // model description
        Collection<String> descriptions = model.getDescription();
        if (descriptions != null) {
            XmlNode descriptionXmlNode = createElement(rootXmlNode, MODEL_DESCRIPTION);
            for (String line : descriptions) {
                add(descriptionXmlNode, LINE, normalize(line));
            }
        }

        // options
        Collection<Option> options = model.getOption();
        if (options != null) {
            for (Option option : options) {
                add(rootXmlNode, OPTION, normalize(option.getName()));
            }
        }

        // settings
        Map<String, String> settings = model.getSettings();
        if (MapUtils.isNotEmpty(settings)) {
            XmlNode settingXmlNode = createElement(rootXmlNode, SETTINGS);
            for (String settingName : settings.keySet()) {
                
                // il arrive que l'option <OPTDATAALL>False</OPTDATAALL>
                // soit présente et demandant de ne pas evaluer les criteres agrégés
                // il a été demandé de supprimer cette option
                if (settingName.equalsIgnoreCase("OPTDATAALL")) {
                    if (log.isWarnEnabled()) {
                        log.warn("Removing option OPTDATAALL from dexi settings");
                    }
                    continue;
                }
                if (settingName.equalsIgnoreCase("OPTDATATYPE")) {
                    if (log.isWarnEnabled()) {
                        log.warn("Removing option OPTDATATYPE from dexi settings");
                    }
                    continue;
                }

                String settingsValue = settings.get(settingName);
                add(settingXmlNode, settingName, settingsValue);
            }
        }
    }

    protected void composeAttributes(XmlNode rootXmlNode) {
        Collection<Criteria> criterias = model.getCriteria();
        if (CollectionUtils.isNotEmpty(criterias)) {

            // for R scripts, we must export only root tree
            // was for test, with AOT script, we must specify
            // criteria to output, so this case is useless
//            if (forROutput) {
//                composeAttribute(model.getCriteria(0), rootXmlNode);
//            } else {

            for (Criteria criteria : criterias) {
                composeAttribute(criteria, rootXmlNode);
            }
//            }
        }
    }

    protected void composeAttribute(Criteria criteria, XmlNode rootXmlNode) {

        XmlNode attributeXmlNode = createElement(rootXmlNode, ATTRIBUTE);

        // for satellite node using target name (is the only child)
        String name = criteria.getName();

        if (forROutput) {
            if (cachedCriteriaNames.containsKey(name)) {
                Integer integer = cachedCriteriaNames.get(name);
                name += (" " + ++integer);
                cachedCriteriaNames.put(name, integer);
            } else {
                cachedCriteriaNames.put(name, 0);
            }
        }

        // attribute name
        add(attributeXmlNode, CRITERIA_NAME, normalize(name));

        // attribute description
        add(attributeXmlNode, CRITERIA_DESCRIPTION, normalize(criteria.getDescription()));

        // scale
        XmlNode scaleXmlNode = createElement(attributeXmlNode, SCALE);

        // scale order if not ASC (default)
        Scale scale = criteria.getScale();
        ScaleOrder scaleOrder = scale.getOrder();
        switch (scaleOrder) {
            case DESC:
                add(scaleXmlNode, SCALE_ORDER, ScaleOrder.DESC.toString());
                break;
            case NONE:
                add(scaleXmlNode, SCALE_ORDER, ScaleOrder.NONE.toString());
                break;
        }

        // scale values
        Collection<ScaleValue> scales = scale.getScaleValue();
        if (scales != null) {
            for (ScaleValue scaleValue : scales) {
                XmlNode scaleValueXmlNode = createElement(scaleXmlNode, SCALE_VALUE);
                add(scaleValueXmlNode, SCALE_VALUE_NAME, normalize(scaleValue.getName()));
                add(scaleValueXmlNode, SCALE_VALUE_DESCRIPTION, normalize(scaleValue.getDescription()));
                add(scaleValueXmlNode, SCALE_VALUE_GROUP, scaleValue.getGroup());
            }
        }

        // function
        if (criteria instanceof ComputableCriteria) {
            ComputableCriteria computableCriteria = (ComputableCriteria) criteria;
            Function function = computableCriteria.getFunction();
            if (function != null) {
                XmlNode functionXmlNode = createElement(attributeXmlNode, FUNCTION);
                add(functionXmlNode, FUNCTION_LOW, function.getLow());
                add(functionXmlNode, FUNCTION_HIGH, function.getHigh());
                add(functionXmlNode, FUNCTION_ENTERED, function.getEntered());
                add(functionXmlNode, FUNCTION_WEIGHTS, function.getWeights());
                add(functionXmlNode, FUNCTION_LOCWEIGHTS, function.getLocweights());
                add(functionXmlNode, FUNCTION_NORMLOCWEIGHTS, function.getNormlocweights());
                add(functionXmlNode, FUNCTION_CONSIST, function.getConsist());
                add(functionXmlNode, FUNCTION_ROUNDING, function.getRounding());
            }
        }

        // options
        Collection<Option> options = model.getOption();
        List<String> optionsAsText = new ArrayList<>();
        boolean atLeastOneNotEmpty = false;
        for (Option option : options) {

            Collection<OptionValue> optionValuesForCriteria = MascUtil.getOptionValuesForCriteria(option, criteria, false);

            String valueAsTest = "";
            // FIXME echatellier 20130717 take only first value du to bug
            if (!optionValuesForCriteria.isEmpty()) {
                OptionValue optionValue = Lists.newArrayList(optionValuesForCriteria).get(0);
                Integer value = optionValue.getValue();
                if (value == null) {
                    if (optionValuesForCriteria.size() == 1) {
                        valueAsTest = MascUtil.OPTION_VALUE_DEFAULT;
                    }
                } else {
                    valueAsTest += value;
                }
            }
            optionsAsText.add(valueAsTest);
            if (!valueAsTest.isEmpty()) {
                atLeastOneNotEmpty = true;
            }
        }
        // if all option are empty, dexi does not put options
        if (atLeastOneNotEmpty) {
            for (String optionAsText : optionsAsText) {
                add(attributeXmlNode, OPTION, optionAsText);
            }
        }

        // compose children only if is not an satellite tree
        if (!criteria.isReference()) {

            // children
            Collection<Criteria> children = criteria.getChild(false);
            if (children != null) {
                for (Criteria child : children) {

                    // if child is on model (root node) is a satellite one
                    composeAttribute(child, attributeXmlNode);
                }
            }
        }
    }

    protected void add(XmlNode parentXmlNode, String tagName, Object value) {
        if (value != null) {
            String toString = value.toString();

            // boolean are capitalized in masc xml file
            if (value instanceof Boolean) {
                toString = StringUtils.capitalize(toString);
            }
            createElement(parentXmlNode, tagName, toString);
        }
    }

    // Override to upper case tagNames
    public XmlNode createElement(XmlNode parentXmlNode, String tagName) {
        return createElement(parentXmlNode, tagName, null);
    }

    public XmlNode createElement(XmlNode parentXmlNode, String tagName, String text) {
        XmlNode xmlNode = XmlNode.createElement(tagName.toUpperCase(), text);
        parentXmlNode.add(xmlNode);
        return xmlNode;
    }

    public String normalize(String toNormalize) {

        return forROutput ? MascUtil.normalize(toNormalize) : toNormalize;
    }
}
