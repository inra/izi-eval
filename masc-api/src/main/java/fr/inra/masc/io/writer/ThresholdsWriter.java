package fr.inra.masc.io.writer;
/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.model.ThresholdOperator;
import fr.inra.masc.model.ThresholdValue;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * To export in a file thresholds.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public class ThresholdsWriter {

    public void write(List<EditableCriteria> criterias, File file) throws IOException {

        BufferedWriter writer = Files.newWriter(file, Charsets.UTF_8);
        try {

            for (EditableCriteria criteria : criterias) {

                // transform criteria to string
                String criteraString = criteriaToString(criteria);
                if (criteraString != null) {

                    // criteria is threshold
                    // write to output
                    writer.write(criteraString);
                    writer.newLine();
                }
            }

            // close writer
            writer.close();
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    protected String criteriaToString(EditableCriteria criteria) {
        List<ThresholdValue> values = criteria.getValues();
        String result = null;

        if (!CollectionUtils.isEmpty(values)) {

            StringBuilder buffer = new StringBuilder(criteria.getName());

            String unit = criteria.getUnit();
            buffer.append('|').append(StringUtils.isBlank(unit) ? "" : unit);

            for (ThresholdValue value : values) {

                ThresholdOperator operator =
                        ThresholdOperator.fromThresholdValue(value);
                buffer.append('|').append(operator.getText());
                buffer.append('|').append(value.getValue());
            }
            result = buffer.toString();
        }
        return result;
    }
}
