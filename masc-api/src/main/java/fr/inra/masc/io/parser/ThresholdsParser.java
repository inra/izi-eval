package fr.inra.masc.io.parser;
/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.model.EditableCriteriaImpl;
import fr.inra.masc.model.ThresholdOperator;
import fr.inra.masc.model.ThresholdValue;
import fr.inra.masc.model.ThresholdValueImpl;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.IOUtils;

/**
 * To load thresholds from a file.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public class ThresholdsParser {

    public List<EditableCriteria> getModel(File file) throws IOException {

        List<EditableCriteria> criterias = Lists.newArrayList();

        BufferedReader reader = Files.newReader(file, Charsets.UTF_8);

        try {

            String line;

            while ((line = reader.readLine()) != null) {

                EditableCriteria criteria = loadCriteria(line);
                criterias.add(criteria);
            }

            // close reader
            reader.close();

            return criterias;
        } finally {

            // make sure model is closed
            IOUtils.closeQuietly(reader);
        }
    }

    protected EditableCriteria loadCriteria(String line) {

        List<String> parts = Lists.newArrayList(line.split("\\|"));

        Iterator<String> itr = parts.iterator();

        String criteriaName = itr.next();
        String unit = itr.next();

        List<ThresholdValue> values = Lists.newArrayList();

        EditableCriteria result = new EditableCriteriaImpl();
        result.setUnit(unit);
        result.setName(criteriaName);
        while (itr.hasNext()) {

            String operatorString = itr.next();
            ThresholdOperator operator =
                    ThresholdOperator.fromString(operatorString);

            String valueString = itr.next();
            Double value = Double.valueOf(valueString);

            values.add(new ThresholdValueImpl(value,
                                              operator.getSign(),
                                              operator.isCanBeEqual()));
        }
        result.setValues(values);

        return result;
    }
}
