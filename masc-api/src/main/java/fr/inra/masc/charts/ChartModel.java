/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.charts;

import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.Option;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.List;

/**
 * Regroup all informations to generate chart
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.3
 */
public class ChartModel extends AbstractSerializableBean {

    public static final String PROPERTY_SELECTED_CRITERIAS = "selectedCriterias";

    public static final String PROPERTY_SELECTED_OPTIONS = "selectedOptions";

    public static final String PROPERTY_SHOW_CLASSES = "showClasses";

    private static final long serialVersionUID = 1L;

    protected List<Criteria> selectedCriterias;

    protected List<Option> selectedOptions;

    protected boolean showClasses = true;

    public boolean isShowClasses() {
        return showClasses;
    }

    public void setShowClasses(boolean showClasses) {
        boolean oldValue = isShowClasses();
        this.showClasses = showClasses;
        firePropertyChange(PROPERTY_SHOW_CLASSES, oldValue, showClasses);
    }

    public List<Criteria> getSelectedCriterias() {
        return selectedCriterias;
    }

    public void setSelectedCriterias(List<Criteria> selectedCriterias) {
        List<Criteria> oldSelectedCriterias = getSelectedCriterias();
        this.selectedCriterias = selectedCriterias;
        firePropertyChange(PROPERTY_SELECTED_CRITERIAS, oldSelectedCriterias, selectedCriterias);
    }

    public List<Option> getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(List<Option> selectedOptions) {
        List<Option> oldSelectedOptions = getSelectedOptions();
        this.selectedOptions = selectedOptions;
        firePropertyChange(PROPERTY_SELECTED_OPTIONS, oldSelectedOptions, selectedOptions);
    }
}
