package fr.inra.masc;

/*
 * #%L
 * Masc :: API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.net.URL;
import java.util.Locale;
import org.nuiton.util.Version;
import org.nuiton.util.config.ConfigOptionDef;

import static org.nuiton.i18n.I18n.n_;

/**
 * All Masc configuration options.
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public enum MascConfigOption implements ConfigOptionDef {

    /** Main directory where to put masc data (logs, and others...). */
    DATA_DIRECTORY(
            "masc.data.directory",
            n_("masc.config.data.directory.description"),
            "${user.home}/.izieval",
            File.class),
    TMP_DIRECTORY(
            "masc.tmp.directory",
            n_("masc.config.tmp.directory.description"),
            "${masc.data.directory}/tmp",
            File.class),
    UI_CONFIG_FILE(
            "masc.ui.config",
            n_("masc.config.ui.config.description"),
            "${masc.data.directory}/izievalUI.properties",
            File.class),
    LOCALE(
            "ui.locale",
            n_("masc.config.ui.locale"),
            Locale.FRANCE.toString(),
            Locale.class),
    LOG_LEVEL(
            "masc.config.ui.logLevel",
            n_("masc.config.ui.logLevel"),
            "INFO",
            String.class),
    LOG_PATTERN_LAYOUT(
            "masc.ui.logPatternLayout",
            n_("masc.config.ui.logPatternLayout"),
            "%5p [%t] (%F:%L) %M - %m%n",
            String.class),
    TEMPLATE_DIRECTORY(
            "masc.templates.directory",
            n_("masc.config.templates.directory.description"),
            "/templates/",
            File.class),
    VERSION(
            "masc.application.version",
            n_("masc.config.application.version"),
            "",
            Version.class),
    SITE_URL(
            "masc.application.site.url",
            n_("masc.config.site.url"),
            "http://wiki.inra.fr/wiki/deximasc/Main/",
            URL.class),
    HELP_URL("masc.application.help.url",
            n_("masc.config.help.url"),
            "http://wiki.inra.fr/wiki/deximasc/Interface+IZI-EVAL/Accueil",
            URL.class),
    R_APP_PATH(
            "masc.r.path",
            n_("masc.config.r.path"),
            "",
            String.class),
    DEXI_APP_PATH(
            "masc.dexi.path",
            n_("masc.config.dexi.path"),
            "",
            String.class),
    DEXI_EVAL_APP_PATH(
            "masc.dexiEval.path",
            n_("masc.config.dexiEval.path"),
            "",
            String.class);

    /** Configuration key. */
    protected final String key;

    /** I18n key of option description */
    protected final String description;

    /** Type of option */
    protected final Class<?> type;

    /** Default value of option. */
    protected String defaultValue;

    MascConfigOption(String key,
                     String description,
                     String defaultValue,
                     Class<?> type) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean isTransient() {
        return false;
    }

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean newValue) {
        // not used
    }

    @Override
    public void setFinal(boolean newValue) {
        // not used
    }
}
