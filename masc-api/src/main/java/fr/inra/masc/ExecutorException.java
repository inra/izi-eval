/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc;

/**
 * Throw if executor fail, return also executor logs
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 */
public class ExecutorException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    protected String logs;

    public ExecutorException(String logs, String msg) {
        super(msg);
        this.logs = logs;
    }

    public ExecutorException(String logs, String msg, Throwable cause) {
        super(msg, cause);
        this.logs = logs;
    }

    public String getLogs() {
        return logs;
    }
}