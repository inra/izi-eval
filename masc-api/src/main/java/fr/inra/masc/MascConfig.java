/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2013 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc;

import com.google.common.base.Preconditions;
import fr.inra.masc.utils.MascUtil;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;
import org.nuiton.util.Version;
import org.nuiton.util.config.ApplicationConfig;

import static org.nuiton.i18n.I18n._;

/**
 * Masc configuration.
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class MascConfig {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MascConfig.class);

    public static final String CSV_SEPARATOR = ",";

    public static final String UNIX_DEXI_EVAL_BIN = "DEXiEval";

    public static final String WIN_DEXI_EVAL_BIN = "DEXiEval.exe";

    public static final String DEXI_400_EN_SETU = "DEXi400en_setup.exe";

    /** Delegate application config object containing configuration. */
    protected ApplicationConfig applicationConfig;

    public enum OS {
        WIN,
        LINUX,
        OTHER
    }

    public MascConfig() {
        this("masc.properties", true);
    }

    public MascConfig(String file, boolean createDirectories) {

        applicationConfig = new ApplicationConfig();
        applicationConfig.setConfigFileName(file);

        if (log.isDebugEnabled()) {
            log.debug(this + " configuration is initializing...");
        }
        try {
            applicationConfig.loadDefaultOptions(MascConfigOption.values());
            applicationConfig.parse();
        } catch (org.nuiton.util.config.ArgumentsParserException e) {
            throw new MascTechnicalException(
                    "Could not parse configuration", e);
        }

        if (createDirectories) {

            // create directory if needed
            createDirectory(MascConfigOption.DATA_DIRECTORY, false);
            createDirectory(MascConfigOption.TMP_DIRECTORY, true);

        }

        if (log.isDebugEnabled()) {
            log.debug("parsed options in config file" +
                      applicationConfig.getOptions());
        }
    }

    public ApplicationConfig getApplicationConfig() {
        return applicationConfig;
    }

    public void save() {
        applicationConfig.saveForUser();
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public File getTmpDirectory() {
        File file = applicationConfig.getOptionAsFile(MascConfigOption.TMP_DIRECTORY.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getUIConfigFile() {
        File file = applicationConfig.getOptionAsFile(MascConfigOption.UI_CONFIG_FILE.key);
        return file;
    }

    public File getDexiExecutableFile() {
        File dexiExecutableFile = applicationConfig.getOptionAsFile(MascConfigOption.DEXI_APP_PATH.key);
        return dexiExecutableFile;
    }

    public void setDexiExecutableFile(File dexiApp) {
        applicationConfig.setOption(MascConfigOption.DEXI_APP_PATH.key, dexiApp.getAbsolutePath());
    }

    public File getDexiEvalExecutableFile() {
        File dexiEvalBinPath = applicationConfig.getOptionAsFile(MascConfigOption.DEXI_EVAL_APP_PATH.key);
        if (!MascUtil.isExecutableFile(dexiEvalBinPath)) {
            switch (getOs()) {
                case LINUX:
                    dexiEvalBinPath = new File("dexi/", UNIX_DEXI_EVAL_BIN);
                    break;
                case WIN:
                    dexiEvalBinPath = new File("dexi/", WIN_DEXI_EVAL_BIN);
                    break;
            }
        }
        return dexiEvalBinPath;
    }

    public File getDexiInstallationFile() {
        return new File("dexi/", DEXI_400_EN_SETU);
    }

    public void setDexiEvalExecutableFile(File dexiEvalExecutableFile) {
        applicationConfig.setOption(MascConfigOption.DEXI_EVAL_APP_PATH.key, dexiEvalExecutableFile.getAbsolutePath());
    }

    public File getRExecutableFile() {
        File dexiExecutableFile = applicationConfig.getOptionAsFile(MascConfigOption.R_APP_PATH.key);
        return dexiExecutableFile;
    }

    public void setRExecutableFile(File rExecutableFile) {
        applicationConfig.setOption(MascConfigOption.R_APP_PATH.key, rExecutableFile.getAbsolutePath());
    }

    public void setLocale(Locale locale) {
        applicationConfig.setOption(MascConfigOption.LOCALE.key, locale.toString());
    }

    public Locale getLocale() {
        return applicationConfig.getOption(Locale.class, MascConfigOption.LOCALE.key);
    }

    public String getTemplateDir() {
        String templateDir = applicationConfig.getOption(MascConfigOption.TEMPLATE_DIRECTORY.key);
        return templateDir;
    }

    public URL getSiteUrl() {
        return applicationConfig.getOptionAsURL(MascConfigOption.SITE_URL.key);
    }
    
    public URL getHelpUrl() {
        return applicationConfig.getOptionAsURL(MascConfigOption.HELP_URL.key);
    }

    public String getLogLevel() {
        String level = applicationConfig.getOption(MascConfigOption.LOG_LEVEL.key);
        return level;
    }

    public String getLogPatternLayout() {
        String result = applicationConfig.getOption(MascConfigOption.LOG_PATTERN_LAYOUT.key);
        return result;
    }

    public String getCopyrightText() {
        return _("masc.copyright.text", getVersion());
    }

    /** @return la version de l'application. */
    public Version getVersion() {
        Version option = applicationConfig.getOption(Version.class, MascConfigOption.VERSION.key);
        return option;
    }

    public OS getOs() {
        String osName = applicationConfig.getOption("os.name").toLowerCase();
        OS result = OS.OTHER;
        if (osName.contains("win")) {
            result = OS.WIN;
        } else if (osName.contains("linux")) {
            result = OS.LINUX;
        }
        if (log.isDebugEnabled()) {
            log.debug("Os name '" + osName + "' return : " + result);
        }
        return result;
    }

    /**
     * Creates a directory given the configuration given key.
     *
     * @param key the configuration option key which contains the location of
     *            the directory to create
     */
    protected void createDirectory(MascConfigOption key, boolean cleanIt) {

        File directory = applicationConfig.getOptionAsFile(key.getKey());

        Preconditions.checkNotNull(
                directory,
                "Could not find directory " + directory + " (key " +
                key +
                "in your configuration file named echobase.properties)"
        );
        if (log.isInfoEnabled()) {
            log.info(key + " = " + directory);
        }
        
        if (cleanIt) {
            try {
                FileUtils.deleteDirectory(directory);
            } catch (IOException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't clean directory", ex);
                }
            }
        }

        try {
            FileUtil.createDirectoryIfNecessary(directory);
        } catch (IOException e) {
            throw new MascTechnicalException(
                    "Could not create directory " + directory, e);
        }
    }

}
