/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc;

import java.io.InputStream;

import static org.nuiton.i18n.I18n._;
import static org.nuiton.i18n.I18n.n_;

/**
 * Regroup all MASC R scripts
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.5
 */
public enum R_SCRIPT {

    SI("SI.r",
       n_("masc.r.si"),
       n_("masc.r.si.tip"),
       new String[0],
       new String[]{"SI_${dexiModelName}.jpeg"}),

    OAT("OAT.r",
         n_("masc.r.oat"),
         n_("masc.r.oat.tip"),
         new String[0],
         new String[]{"OAT_${dexiModelName}.jpeg"}),

//    "option", "{optionName}.csv"

    // MC_nom du noeud.jpeg: fichier représentant l'histogramme MC. C'est le fichier dessin à représenter
    // MC bar lengths.csv: fichier texte donnant les % de l'histogramme
    // MC.txt: fichier donnant pour chacun des noeuds les % des tirages et des aggrégations
    // MC options.csv: l'ensemble des tirages
    MC("MC.r",
        n_("masc.r.mc"),
        n_("masc.r.mc.tip"),
        new String[]{n_("MC bar lengths.csv"), n_("MC.txt"), n_("MC options.csv")},
        new String[]{n_("MC_${criteriaName}.jpeg")});

//            "nbRuns", "5000", "node", "1"

    private String scriptFileName;
    private String i18n;
    private String i18nTip;
    private String[] resultTextFileNames;
    private String[] resultImageFileNames;

    R_SCRIPT(String scriptFileName, String i18n, String i18nTip, String[] resultTextFileNames, String[] resultImageFileNames) {
        this.i18n = i18n;
        this.i18nTip = i18nTip;
        this.scriptFileName = scriptFileName;
        this.resultTextFileNames = resultTextFileNames;
        this.resultImageFileNames = resultImageFileNames;
    }

    public String getName() {
        return _(i18n);
    }

    public String getToolTip() {
        return _(i18nTip);
    }

    public String[] getResultTextFileNames() {
        return resultTextFileNames;
    }

    public String[] getResultImageFileNames() {
        return resultImageFileNames;
    }

    public String getScriptFileName() {
        return scriptFileName;
    }

    public InputStream getStream() {
        String path = "/r/" + getScriptFileName();
        return MascConfig.class.getResourceAsStream(path);
    }
}
