/*
 * #%L
 * Masc :: API
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.utils;

import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.format.StringFormatI18nMessageFormatter;
import org.nuiton.util.StringUtil;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.model.Scale;
import fr.inra.masc.model.ScaleGroup;
import fr.inra.masc.model.ScaleOrder;
import fr.inra.masc.model.ScaleValue;
import fr.inra.masc.model.ThresholdValue;
import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.Scenario;

/**
 * Regroup all masc utility methods
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class MascUtil {

    /** Logger */
    private static Log log = LogFactory.getLog(MascUtil.class);

    public static final String OPTION_VALUE_DEFAULT = "*";

    public static final String THRESHOLD_CRITERIA = "thresholdCriteria";

    public static final Color GREEN_COLOR = new Color(0, 170, 0);

    public static final String HTML_BLACK = "#000000";

    public static final String HTML_GREEN = "#32CD32";

    public static final String HTML_RED = "#FF0000";

    public static String normalize(String toNormalize) {

        if (toNormalize != null) {

            toNormalize = toNormalize.replaceAll("[^\\p{ASCII}]", "");
            toNormalize = toNormalize.replaceAll("[^\\p{Alpha}\\p{Digit}\\s]", "");
        }
        return toNormalize;
    }

    public static Color getColorForScaleValue(ScaleValue scaleValue) {

        ScaleGroup group = scaleValue.getGroup();
        if (group != null) {
            switch (group) {
                case BAD:
                    return Color.RED;
                case GOOD:
                    return GREEN_COLOR;
            }
        }
        return Color.BLACK;
    }

    public static BufferedImage rotateImage90(BufferedImage img) {
        int width = img.getWidth();
        int height = img.getHeight();

        BufferedImage newImg2 = new BufferedImage(height, width, img.getType());

        AffineTransform tx = new AffineTransform();
        AffineTransform tx2 = new AffineTransform();
        tx.rotate((Math.toRadians(90)), (width / 2), ((height) / 2));
        int t = (width - height) / 2;
        tx.translate(t, t);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
        AffineTransformOp op2 = new AffineTransformOp(tx2, AffineTransformOp.TYPE_BILINEAR);

        op.filter(img, newImg2);
        op2.filter(newImg2, null);
        return newImg2;
    }

    public static Multimap<Criteria, OptionValue> extractAllOptionValuesByEditableCriteria(Option option) {
        Multimap<Criteria, OptionValue> result = HashMultimap.create();
        if (option == null) {
            return result;
        }
        Collection<OptionValue> optionValues = option.getOptionValue();

        if (optionValues != null) {
            for (OptionValue optionValue : optionValues) {
                Criteria criteria = optionValue.getCriteria();
                if (criteria instanceof EditableCriteria) {
                    result.put(criteria, optionValue);
                }
            }
        }
        return result;
    }

    // FIXME echatellier 20130531 should return Map instead of Multimap
    public static Multimap<String, OptionValue> extractAllOptionValues(Option option) {
        Multimap<String, OptionValue> result = HashMultimap.create();
        if (option == null) {
            return result;
        }
        Collection<OptionValue> optionValues = option.getOptionValue();

        if (optionValues != null) {
            Iterator<OptionValue> itOptionValues = optionValues.iterator();
            while (itOptionValues.hasNext()) {
                OptionValue optionValue = itOptionValues.next();
                Criteria criteria = optionValue.getCriteria();
                result.put(criteria.getUuid(), optionValue);
            }
        }
        return result;
    }

    public static Map<Option, Multimap<String, OptionValue>> extractAllOptionValues(MascModel mascModel) {
        Map<Option, Multimap<String, OptionValue>> result = Maps.newHashMap();

        Collection<Option> options = mascModel.getOption();
        for (Option option : options) {

            result.put(option, extractAllOptionValues(option));
        }

        return result;
    }

    public static String getColorHEXOfWorstResolvedOptionValuesGroupForCriteria(Option option, final Criteria criteria) {
        ScaleGroup worstResolvedOptionValuesGroupForCriteria = getWorstResolvedOptionValuesGroupForCriteria(option, criteria);
        switch (worstResolvedOptionValuesGroupForCriteria) {
            case BAD:
                return HTML_RED;
            case GOOD:
                return HTML_GREEN;
            default:
                return HTML_BLACK;
        }
    }

    public static ScaleGroup getWorstResolvedOptionValuesGroupForCriteria(Option option, final Criteria criteria) {

        Collection<OptionValue> optionValuesForCriteria = getOptionValuesForCriteria(option, criteria);

        ScaleGroup worstGroup = ScaleGroup.GOOD;
        for (OptionValue optionValue : optionValuesForCriteria) {
            ScaleGroup group = ScaleGroup.NEUTRAL;
            if (optionValue != null && optionValue.getValue() != null) {
                ScaleValue scaleValue = criteria.getScale().getScaleValue(optionValue.getValue());
                group = scaleValue.getGroup();
                if (group == null) {
                    group = ScaleGroup.NEUTRAL;
                }
            }
            switch (group) {
                case BAD:
                    return ScaleGroup.BAD;
                case NEUTRAL:
                    worstGroup = ScaleGroup.NEUTRAL;
                    break;
            }
        }

        return worstGroup;
    }

    public static String getResolvedOptionValuesForCriteriaAsString(Option option, final Criteria criteria) {

        Collection<OptionValue> optionValuesForCriteria = getOptionValuesForCriteria(option, criteria);

        StringUtil.ToString<OptionValue> optionValueToString = new StringUtil.ToString<OptionValue>() {

            @Override
            public String toString(OptionValue optionValue) {
                String value = MascUtil.OPTION_VALUE_DEFAULT;
                if (optionValue != null && optionValue.getValue() != null) {
                    Double inputValue = optionValue.getInputValue();
                    if (inputValue != null) {
                        value = String.valueOf(inputValue);
                    } else {
                        ScaleValue scaleValue = criteria.getScale().getScaleValue(optionValue.getValue());
                        value = scaleValue.getName();
                    }
                }
                return value;
            }
        };

        return StringUtil.join(optionValuesForCriteria, optionValueToString, ", ", true);
    }

    public static String getGroupForScaleValueDecoration(String scaleGroup) {
        return getGroupForScaleValueDecoration(ScaleGroup.valueOf(scaleGroup));
    }

    public static String getGroupForScaleValueDecoration(ScaleGroup scaleGroup) {
        if (scaleGroup == null) {
            return "";
        }
        switch (scaleGroup) {
            case BAD:
                return "(-)";
            case GOOD:
                return "(+)";
            default:
                return "";
        }
    }

    public static int resolvThresholdValue(List<ThresholdValue> thresholdValues, Double inputValue) {

        int i = 0;
        for (ThresholdValue thresholdValue : thresholdValues) {
            if (thresholdValue != null) {
                if (thresholdValue.isCanBeEqual()) {
                    if (inputValue.equals(thresholdValue.getValue())) {
                        return i;
                    }
                }

                if (ScaleOrder.ASC.equals(thresholdValue.getSign())) {
                    if (inputValue < thresholdValue.getValue()) {
                        return i;
                    }
                } else {
                    if (inputValue > thresholdValue.getValue()) {
                        return i;
                    }
                }
            }
            i++;
        }
        return i;
    }

    public static String getThresholdAsString(EditableCriteria criteria) {
        List<ThresholdValue> thresholds = Lists.newArrayList(criteria.getValues());
        Scale scale = criteria.getScale();
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (ScaleValue scaleValue : scale.getScaleValue()) {
            builder.append("<strong style=\"color:");
            builder.append(getHEXColorForScaleValue(scaleValue));
            builder.append("\">");
            builder.append(scaleValue.getName());
            builder.append("</strong>");

            if (thresholds.size() > i) {
                ThresholdValue thresholdValue = thresholds.get(i++);

                if (thresholdValue != null) {
                    String op;
                    if (ScaleOrder.ASC.equals(thresholdValue.getSign())) {
                        op = " &lt;";
                    } else {
                        op = " &gt;";
                    }
                    builder.append(op);
                    if (thresholdValue.isCanBeEqual()) {
                        builder.append("=");
                    }

                    builder.append(" ");
                    builder.append(thresholdValue.getValue());
                    builder.append(" ");
                    builder.append(op);
                    if (!thresholdValue.isCanBeEqual()) {
                        builder.append("=");
                    }
                    builder.append(" ");
                }
            }
        }
        return builder.toString();
    }

    public static String getHEXColorForScaleValue(ScaleValue scaleValue) {

        ScaleGroup group = scaleValue.getGroup();
        if (group != null) {
            switch (group) {
                case BAD:
                    return HTML_RED;
                case GOOD:
                    return HTML_GREEN;
            }
        }
        return HTML_BLACK;
    }

    public static Collection<OptionValue> getOptionValuesForCriteria(Option option, Criteria criteria) {
        return getOptionValuesForCriteria(option, criteria, true);
    }

    public static Collection<OptionValue> getOptionValuesForCriteria(Option option, Criteria criteria, boolean resolvRef) {

        // for reference, use target one
        while (criteria.isReference() && resolvRef) {
            criteria = criteria.getChild(0);
        }

        Multimap<String, OptionValue> stringOptionValueMultimap = extractAllOptionValues(option);

        return stringOptionValueMultimap.get(criteria.getUuid());
    }

    public static Integer getMinValue(Collection<OptionValue> optionValues) {
        if (optionValues == null || optionValues.isEmpty()) {
            return null;
        }

        Integer result = Integer.MAX_VALUE;
        for (OptionValue optionValue : optionValues) {
            Integer value = optionValue.getValue();
            if (value == null) {
                return null;
            }
            if (value < result) {
                result = value;
            }
        }
        return result;
    }

    public static boolean isExecutableFile(File file) {
        boolean isExecutableFile = (file != null && file.exists() && file.canExecute() && !file.isDirectory());
        return isExecutableFile;
    }

    public static String getClasses(double value, Option option, Criteria criteria) {

        // get max possible value
        // TODO seletellier 20120124 : take care of order
        Scale scale = criteria.getScale();
        Collection<ScaleValue> scaleValues = scale.getScaleValue();
        int valueMax = 0;
        if (scaleValues != null) {
            valueMax = scaleValues.size() - 1;
        }

        return " " + ((int) value) + "/" + valueMax;
    }

    public static String getWeightAsString(String weight) {

        double weightL = Double.parseDouble(weight);

        // round
        int round = (int) Math.round(weightL);
        weight = String.valueOf(round) + " %";
        return weight;
    }

    public static String convertToUTF8(String string) {

        try {
            string = new String(string.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException eee) {
            log.error("Failed to convert '" + string + "' to utf8", eee);
        }
        return string;
    }

    public static String readLines(File file) throws IOException {
        List<String> readed = IOUtils.readLines(new FileReader(file));
        return StringUtil.join(readed, "\n", false);
    }

    public static Map<String, Pair<Scenario, Map<String, Pair<Factor, Object>>>> extractAllScenariosAndFactors(Collection<Scenario> scenarios) {

        Map<String, Pair<Scenario, Map<String, Pair<Factor, Object>>>> result = Maps.newHashMap();

        for (Scenario scenario : scenarios) {
            Map<String, Pair<Factor, Object>> factorMap = extractAllFactorsValuesInMap(scenario.getFactorValues());
            Pair<Scenario, Map<String, Pair<Factor, Object>>> pair =
                    new ImmutablePair<Scenario, Map<String, Pair<Factor, Object>>>(scenario, factorMap);
            result.put(scenario.getName(), pair);
        }

        return result;
    }

    protected static Map<String, Pair<Factor, Object>> extractAllFactorsValuesInMap(Map<Factor, Object> factors) {

        Map<String, Pair<Factor, Object>> result = Maps.newHashMap();

        for (Factor factor : factors.keySet()) {
            result.put(factor.getId(), new ImmutablePair<Factor, Object>(factor, factors.get(factor)));
        }

        return result;
    }

    public static Map<String, Scale> extractAllScales(MascModel model) {
        Extractor<Map<String, Scale>> scaleCollector = new Extractor<Map<String, Scale>>() {

            protected Map<String, Scale> result = Maps.newHashMap();

            @Override
            public boolean visit(Criteria criteriaParent, Criteria value) {
                Scale scale = value.getScale();
                result.put(scale.toString(), scale);
                return true;
            }

            @Override
            public boolean resolvRefs() {
                return true;
            }

            @Override
            public Map<String, Scale> getResult() {
                return result;
            }
        };

        extractOnAllCriterias(scaleCollector, model);

        Map<String, Scale> result = scaleCollector.getResult();

        return result;
    }

    public static Criteria findCriteria(MascModel mascModel, final String id) {
        Extractor<Criteria> criteriaFinder = new Extractor<Criteria>() {

            protected Criteria criteriaFound;

            @Override
            public boolean visit(Criteria criteriaParent, Criteria value) {
                if (criteriaFound == null && id.equals(value.getUuid())) {
                    this.criteriaFound = value;
                }
                return true;
            }

            @Override
            public boolean resolvRefs() {
                return false;
            }

            @Override
            public Criteria getResult() {
                return criteriaFound;
            }
        };
        extractOnAllCriterias(criteriaFinder, mascModel);
        return criteriaFinder.getResult();
    }

    public static Map<String, Criteria> extractAllCriteriasInMap(MascModel model) {
        return extractAllCriteriasInMap(model.getCriteria());
    }

    public static Map<String, Criteria> extractAllCriteriasInMap(Collection<Criteria> criterias) {
        SimpleMapExtractor<String> criteriaCollector = new SimpleMapExtractor<String>() {

            @Override
            protected String getKey(Criteria value) {
                return value.getName();
            }

            @Override
            public boolean resolvRefs() {
                return false;
            }
        };
        extractOnAllCriterias(criteriaCollector, criterias);

        Map<String, Criteria> result = criteriaCollector.getResult();

        return result;
    }

    public static Map<String, Criteria> extractAllCriteriasInMapUUIDKey(MascModel model) {
        SimpleMapExtractor<String> criteriaCollector = new SimpleMapExtractor<String>() {

            @Override
            protected String getKey(Criteria value) {
                return value.getUuid();
            }

            @Override
            public boolean resolvRefs() {
                return false;
            }
        };
        extractOnAllCriterias(criteriaCollector, model);

        Map<String, Criteria> result = criteriaCollector.getResult();

        return result;
    }

    public static Map<String, Criteria> extractCriteriaInMapUUIDKey(Criteria criteria) {
        SimpleMapExtractor<String> criteriaCollector = new SimpleMapExtractor<String>() {

            @Override
            protected String getKey(Criteria value) {
                return value.getUuid();
            }

            @Override
            public boolean resolvRefs() {
                return false;
            }
        };
        extractOnAllCriterias(criteriaCollector, criteria);

        Map<String, Criteria> result = criteriaCollector.getResult();

        return result;
    }

    public static List<EditableCriteria> extractAllThresholdCriterias(MascModel mascModel) {

        Extractor<List<EditableCriteria>> editableCriteriaExtractor = new Extractor<List<EditableCriteria>>() {

            protected List<EditableCriteria> result = Lists.newArrayList();

            @Override
            public boolean visit(Criteria criteriaParent, Criteria value) {
                if (!value.isReference() && value instanceof EditableCriteria && CollectionUtils.isNotEmpty(((EditableCriteria) value).getValues())) {
                    result.add((EditableCriteria) value);
                }
                return true;
            }

            @Override
            public List<EditableCriteria> getResult() {
                return result;
            }

            @Override
            public boolean resolvRefs() {
                return true;
            }
        };

        extractOnAllCriterias(editableCriteriaExtractor, mascModel);
        return editableCriteriaExtractor.getResult();
    }

    public static Map<EditableCriteria, Scale> extractAllEditableCriteriasAndScale(MascModel mascModel) {
        Extractor<Map<EditableCriteria, Scale>> editableCriteriaExtractor = new Extractor<Map<EditableCriteria, Scale>>() {

            protected Map<EditableCriteria, Scale> result = Maps.newLinkedHashMap();

            @Override
            public boolean visit(Criteria criteriaParent, Criteria value) {
                if (!value.isReference() && value instanceof EditableCriteria) {
                    result.put((EditableCriteria) value, value.getScale());
                }
                return true;
            }

            @Override
            public boolean resolvRefs() {
                return true;
            }

            @Override
            public Map<EditableCriteria, Scale> getResult() {
                return result;
            }
        };

        extractOnAllCriterias(editableCriteriaExtractor, mascModel);
        return editableCriteriaExtractor.getResult();
    }

    public static int getMaxClasses(Criteria root, final boolean resolvRef) {

        Extractor<Integer> maxClassCriteriaExtractor = new Extractor<Integer>() {

            protected int maxClasses = 0;

            @Override
            public boolean visit(Criteria criteriaParent, Criteria criteria) {
                Scale scale = criteria.getScale();
                if (scale != null) {
                    Collection<ScaleValue> scaleValues = scale.getScaleValue();
                    maxClasses = Math.max(maxClasses, scaleValues.size());
                }
                return true;
            }

            @Override
            public Integer getResult() {
                return maxClasses;
            }

            @Override
            public boolean resolvRefs() {
                return resolvRef;
            }
        };

        extractOnAllCriterias(maxClassCriteriaExtractor, root);

        return maxClassCriteriaExtractor.getResult();
    }

    public static List<Criteria> extractAllCriterias(MascModel model) {
        ListExtractor criteriaCollector = new SimpleListExtractor() {

            @Override
            public boolean resolvRefs() {
                return false;
            }
        };
        extractOnAllCriterias(criteriaCollector, model);

        List<Criteria> result = criteriaCollector.getResult();

        return result;
    }

    public static List<Criteria> findAllReferenceCriteria(MascModel mascModel, Criteria refCriteria) {
        List<Criteria> result = Lists.newArrayList();

        for (Criteria criteria : mascModel.getCriteria()) {
            result.addAll(extractAllReferenceCriteria(refCriteria, criteria));
        }

        return result;
    }

    public static List<Criteria> extractAllReferenceCriteria(Criteria refCriteria, Criteria curentCriteria) {
        List<Criteria> result = Lists.newArrayList();

        String name = refCriteria.getName();

        Collection<Criteria> children = curentCriteria.getChild(true);
        if (curentCriteria.getName().equals(name) &&
            !curentCriteria.equals(refCriteria)) {
            result.add(curentCriteria);
        }
        for (Criteria child : children) {
            result.addAll(extractAllReferenceCriteria(refCriteria, child));
        }
        return result;
    }

    public static List<EditableCriteria> extractAllEditableCriterias(MascModel mascModel) {

        Map<String, EditableCriteria> criteriaMap = Maps.newLinkedHashMap();
        for (Criteria criteria : mascModel.getCriteria()) {
            criteriaMap.putAll(extractEditableCriterias(criteria));
        }

        return Lists.newArrayList(criteriaMap.values());
    }

    public static Map<String, EditableCriteria> extractEditableCriterias(Criteria criteria) {

        Map<String, EditableCriteria> criteriaMap = Maps.newLinkedHashMap();

        Collection<Criteria> children = criteria.getChild(false);
        if (children == null || children.isEmpty()) {

            // keep only editable criterias
            if (criteria instanceof EditableCriteria && !criteria.isReference()) {
                criteriaMap.put(criteria.getUuid(), (EditableCriteria) criteria);
            }
        } else {
            for (Criteria child : children) {
                criteriaMap.putAll(extractEditableCriterias(child));
            }
        }
        return criteriaMap;
    }

    public static Integer[] countCriterias(MascModel model) {
        CriteriaCountExtractor collector = new CriteriaCountExtractor(true);
        extractOnAllCriterias(collector, model);
        return collector.getResult();
    }

    protected static void extractOnAllCriterias(Extractor<?> collector, MascModel model) {
        extractOnAllCriterias(collector, model.getCriteria());
    }

    protected static void extractOnAllCriterias(Extractor<?> collector, Collection<Criteria> criterias) {
        for (Criteria child : criterias) {
            extractOnAllCriterias(collector, child);
        }
    }

    protected static void extractOnAllCriterias(Extractor<?> collector, Criteria parent) {
        if (parent != null) {

            parent.accept(collector, null);
        }
    }

    public static Map<String, Option> extractAllOptions(MascModel mascModel) {
        Collection<Option> options = mascModel.getOption();

        Map<String, Option> result = Maps.newHashMap();
        if (options != null) {
            for (Option option : options) {
                result.put(option.getName(), option);
            }
        }

        return result;
    }

    public static List<String> getInputValues(Option option, Criteria criteria, boolean resolveReference) {

        List<String> result = Lists.newArrayList();

        // show input values
        if (criteria instanceof EditableCriteria) {
            Collection<OptionValue> optionValuesForCriteria = MascUtil.getOptionValuesForCriteria(option, criteria, resolveReference);
            for (OptionValue optionValue : optionValuesForCriteria) {
                Double inputValue = optionValue.getInputValue();
                if (inputValue != null) {
                    String unit = ((EditableCriteria) criteria).getUnit();
                    
                    // fixer le nb de décimales dans les histogrammes (un chiffre après la virgule)
                    String strValue = String.format("%.1f %s", inputValue, unit);
                    result.add(strValue);
                }
            }
        }

        return result;
    }
    /** Retourne pour chaque critere, la liste des critere pointant vers lui */
    public static Multimap<Criteria, Criteria> extractAllTargetCriterias(MascModel currentMascModel) {
        final Multimap<Criteria, Criteria> result = ArrayListMultimap.create();

        for (Criteria root : currentMascModel.getCriteria()) {
            root.accept(new CriteriaVisitor() {
                @Override
                public boolean visit(Criteria parentCriteria, Criteria criteria) {

                    if (criteria.isReference()) {
                        Criteria target = criteria;
                        while (target.isReference()) {
                            target = target.getChild(0);
                        }
                        result.put(target, criteria);
                    }
                    return true;
                }

                @Override
                public boolean resolvRefs() {
                    return true;
                }
            }, null);
        }

        return result;
    }

    protected static abstract class Extractor<E> implements CriteriaVisitor {

        public abstract E getResult();
    }

    protected static abstract class SimpleListExtractor extends ListExtractor {

        @Override
        protected Criteria getElement(Criteria value) {
            return value;
        }
    }

    protected static abstract class ListExtractor extends Extractor<List<Criteria>> {

        protected List<Criteria> values;

        protected ListExtractor() {
            values = Lists.newLinkedList();
        }

        @Override
        public boolean visit(Criteria criteriaParent, Criteria criteria) {
            values.add(getElement(criteria));
            return true;
        }

        public List<Criteria> getResult() {
            return values;
        }

        protected abstract Criteria getElement(Criteria value);
    }

    protected static abstract class SimpleMapExtractor<K> extends MapExtractor<K> {

        protected abstract K getKey(Criteria value);

        @Override
        protected Criteria getElement(Criteria value) {
            return value;
        }
    }

    protected static abstract class SimpleMultimapExtractor<K> extends MultimapExtractor<K> {

        protected abstract K getKey(Criteria value);

        @Override
        protected Criteria getElement(Criteria value) {
            return value;
        }
    }

    protected static abstract class MultimapExtractor<K> extends Extractor<Multimap<K, Criteria>> {

        protected Multimap<K, Criteria> values;

        protected MultimapExtractor() {
            values = HashMultimap.create();
        }

        @Override
        public boolean visit(Criteria criteriaParent, Criteria criteria) {
            values.put(getKey(criteria), getElement(criteria));
            return true;
        }

        @Override
        public Multimap<K, Criteria> getResult() {
            return values;
        }

        protected abstract K getKey(Criteria value);

        protected abstract Criteria getElement(Criteria value);
    }

    protected static abstract class MapExtractor<K> extends Extractor<Map<K, Criteria>> {

        protected Map<K, Criteria> values;

        protected MapExtractor() {
            values = Maps.newLinkedHashMap();
        }


        @Override
        public boolean visit(Criteria criteriaParent, Criteria criteria) {
            values.put(getKey(criteria), getElement(criteria));
            return true;
        }

        @Override
        public Map<K, Criteria> getResult() {
            return values;
        }

        protected abstract K getKey(Criteria value);

        protected abstract Criteria getElement(Criteria value);
    }

    protected static class CriteriaCountExtractor extends Extractor<Integer[]> {

        protected List<String> alreadyCount = Lists.newArrayList();

        protected int criteriaCount = 0;

        protected int valuedCriteriaCount = 0;

        protected int referenceCriteriaCount = 0;

        protected int parentCriteriaCount = 0;

        private boolean resolvRefs;

        public CriteriaCountExtractor(boolean resolvRefs) {
            this.resolvRefs = resolvRefs;
        }

        @Override
        public boolean visit(Criteria criteriaParent, Criteria value) {

            if (!alreadyCount.contains(value.getUuid())) {

                criteriaCount++;
                if (value.isReference()) {
                    referenceCriteriaCount++;
                } else {
                    if (value instanceof EditableCriteria) {
                        valuedCriteriaCount++;
                    }
                    Collection<Criteria> children = value.getChild(true);
                    if (children != null && !children.isEmpty()) {
                        parentCriteriaCount++;
                    }
                }
                alreadyCount.add(value.getUuid());
            }
            return true;
        }

        @Override
        public Integer[] getResult() {
            return new Integer[]{criteriaCount, valuedCriteriaCount, referenceCriteriaCount, parentCriteriaCount};
        }

        @Override
        public boolean resolvRefs() {
            return this.resolvRefs;
        }
    }
}
