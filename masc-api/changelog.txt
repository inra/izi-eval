0.1
===

Création du model MASC
Création d'un parser pour les fichiers DEXi
Création du service pour lire les fichiers DEXi
Création d'un writer pour les fichiers DEXi
Création du service pour l'écriture des fichiers DEXi
Création d'un service pour ouvrir un fichier dans DEXi
Création d'un service pour les appels à DEXiEval
Création d'un service pour la génération des graphiques
Création d'un service pour la génération des arbre synoptique

0.2
===

[Model] Prise en compte du ROUNDING
[Model] Résolution des réferences

0.3
===

Bugs
----
[#1576] [Option] NPE à l'ouverture d'un fichier DEXi
[#1578] pb avec la fenêtre de gauche dans l'onglet modèle

Features
--------
[#1544] [Valeurs-seuils] Permettre la saisie de valeurs seuils
[#1543] [Modèle] Création d'un nouveau fichier "pivot" en .masc

0.4
===

Features
--------
[#1543] [Modèle] Création d'un nouveau fichier "pivot" en .masc
[#1544] [Valeurs-seuils] Permettre la saisie de valeurs seuils
[#1527] [Exports] Création d'un service de génération de rapport

0.5
====

Features
--------
[#1529] [AS] Création d'un service pour les analyses de sensibilités

1.0
===

Bugs
----
[#1620] Beug
[#1626] pb à l'ouverture d'un fichier DEXi
[#1627] génération de rapport
[#1628] bug avec la version masc2.0

Features
--------
[#1546] [Graphiques] 2 entrées
[#1565] [Graphique synoptique] Ajouter la case de chaque critère la classe prise sur le nombre de classes possibles
[#1575] [AS] Installation de R
[#1597] [rapport d'export]

1.1
===

Bugs
----
[#1629] [Option] Les valeurs disparaissent
[#1591] [graphique synoptique] : erreur d'affichage
[#1625] Généricité des analyses de sensibilité dans l'interface
[#1631] bug dans les couleurs du graphique synoptique
[#1632] mise en forme des graphiques synoptiques
[#1624] Erreur d'affichage lorsque j'importe le fichier DEXi qui est en PJ

Features
--------
[#1577] onglet graphique
[#1563] [Option] Agrandir la police de chaque ligne pour améliorer la lisibilité.

1.2
===

Bugs
----
[#1638] [graphique synoptique]
[#1639] [sauvegarder sous]

Features
--------
[#1643] Ajouter le script OAT.r