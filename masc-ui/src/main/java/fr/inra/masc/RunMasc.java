/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc;

import fr.inra.masc.ui.MascMainUIHandler;
import fr.inra.masc.ui.MascUIHelper;
import javax.swing.SwingUtilities;
import jaxx.runtime.context.JAXXInitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n.n_;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class RunMasc {

    /** Logger */
    private static Log log = LogFactory.getLog(RunMasc.class);

    public static void main(String... args) {

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                String errorMsg = "Error";
                log.error(errorMsg, e);
                MascUIHelper.showError(null, "Error", e);
            }
        });

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {

                    // init ui
                    JAXXInitialContext jaxxContext = new JAXXInitialContext();

                    MascMainUIHandler mainHandler = new MascMainUIHandler();

                    mainHandler.initUI(jaxxContext);
                } catch (Exception eee) {
                    String errorMsg = "Error";
                    log.error(errorMsg, eee);
                    MascUIHelper.showError(null, errorMsg, eee);
                    System.exit(1);
                }
            }
        });
    }

    static {

        // BEGIN - DO NOT REMOVE THIS!

        // TO auto-detect i18n override from jmexico and still permit us
        // to use the i18n.strictMode ;)

        n_("jmexico.action.add.scenario");
        n_("jmexico.action.clone.scenario");
        n_("jmexico.action.rename.scenario");
        n_("jmexico.action.remove.scenario");
        n_("jmexico.action.import.scenario");
        n_("jmexico.action.export.scenario");
        n_("jmexico.error.scenario.name.required");
        n_("jmexico.error.scenario.name.used");
        n_("jmexico.error.scenario.not.selected");
        n_("jmexico.factor.name");
        n_("jmexico.label.scenario.add.name");
        n_("jmexico.label.scenario.clone.name");
        n_("jmexico.label.scenario.clone.selected");
        n_("jmexico.label.scenario.rename.name");
        n_("jmexico.label.scenario.rename.selected");
        n_("jmexico.label.scenario.remove.selected");
        n_("jmexico.label.scenario.import.name");
        n_("jmexico.label.scenario.export.selected");
        n_("jmexico.label.scenario.import.file");
        n_("jmexico.title.scenario.add");
        n_("jmexico.title.scenario.clone");
        n_("jmexico.title.scenario.rename");
        n_("jmexico.title.scenario.remove");
        n_("jmexico.title.scenario.import");
        n_("jmexico.title.scenario.export");
        n_("jmexico.title.error");
        n_("jmexico.config.scenario.extension");
        n_("jmexico.config.scenario.extension.description");
        n_("jmexico.config.scenario.extension.csv.description");
        n_("jmexico.info.scenario.export.filename");

        n_("jmexico.title.importScenario");
        n_("jmexico.warning.factor.not.imported");

        n_("masc.report.as");
        n_("masc.report.description");
        n_("masc.report.graphic");
        n_("masc.report.options");
        n_("masc.report.criteria");
        n_("masc.report.synoptic");
        n_("masc.report.threshold");
        n_("masc.report.unit");
        n_("masc.report.treeDescription");
        n_("masc.report.tree");
        // END - DO NOT REMOVE THIS!
    }
}
