/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Inra, Codelutin, Tony chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc;

import fr.inra.masc.model.MascModel;
import fr.inra.masc.services.MascServiceFactory;
import fr.inra.masc.ui.MascDecoratorProvider;
import fr.inra.masc.ui.MascHelpBroker;
import fr.inra.masc.ui.content.MascTabs;
import fr.inra.masc.ui.tree.criteria.CriteriaDataProvider;
import org.nuiton.widget.SwingSession;

import java.io.File;
import java.util.Properties;

/**
 * Context of masc application
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class MascApplicationContext {

    private static final MascApplicationContext context =
            new MascApplicationContext();

    protected SwingSession swingSession;

    protected MascConfig config;

    protected MascServiceFactory serviceFactory;

    protected CriteriaDataProvider dataProvider;

    protected MascDecoratorProvider decoratorProvider;

    protected MascUIModel mascUIModel;

    protected MascTabs mascTabsUI;

    @Deprecated
    protected MascHelpBroker mascHelpBroker;

    //protected Properties helpMapping;

    public static MascApplicationContext getContext() {
        return context;
    }

    // use singleton
    private MascApplicationContext() {
        dataProvider = CriteriaDataProvider.newInstance();
    }

    public MascConfig getConfig() {
        if (config == null) {
            config = new MascConfig();
        }
        return config;
    }

    public MascServiceFactory getServiceFactory() {
        if (serviceFactory == null) {
            serviceFactory = new MascServiceFactory();
        }
        return serviceFactory;
    }

    public CriteriaDataProvider getDataProvider() {
        return dataProvider;
    }

    public MascDecoratorProvider getDecoratorProvider() {
        return decoratorProvider;
    }

    public void setDecoratorProvider(MascDecoratorProvider decoratorProvider) {
        this.decoratorProvider = decoratorProvider;
    }

    public MascUIModel getMascUIModel() {
        if (mascUIModel == null) {
            mascUIModel = new MascUIModel();
        }
        return mascUIModel;
    }

    public void setMascUIModel(MascUIModel mascUIModel) {
        this.mascUIModel = mascUIModel;
    }

    public MascModel getMascModel() {
        return getMascUIModel().getMascModel();
    }

    public void setMascModel(MascModel mascModel) {
        getMascUIModel().setMascModel(mascModel);
    }

    public void setMascFile(File mascFile) {
        getMascUIModel().setMascFile(mascFile);
    }

    public SwingSession getSwingSession() {
        return swingSession;
    }

    public void setSwingSession(SwingSession swingSession) {
        this.swingSession = swingSession;
    }

    public void setMascTabsUI(MascTabs mascTabsUI) {
        this.mascTabsUI = mascTabsUI;
    }

    public MascTabs getMascTabsUI() {
        return mascTabsUI;
    }

    /*public MascHelpBroker getMascHelpBroker() {
        return mascHelpBroker;
    }

    public void setMascHelpBroker(MascHelpBroker mascHelpBroker) {
        this.mascHelpBroker = mascHelpBroker;
    }*/

    /*public Properties getHelpMapping() {
        return helpMapping;
    }

    public void setHelpMapping(Properties helpMapping) {
        this.helpMapping = helpMapping;
    }*/
}
