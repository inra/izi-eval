/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.tree.criteria;

import com.google.common.collect.Lists;

import fr.inra.masc.MascApplicationContext;
import fr.inra.masc.MascUIModel;
import fr.inra.masc.model.Criteria;
import jaxx.runtime.swing.nav.tree.NavTreeHelper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class CriteriaTreeHelper extends NavTreeHelper<CriteriaNode> {

    /** Logger */
    static private final Log log = LogFactory.getLog(CriteriaTreeHelper.class);

    public CriteriaTreeHelper() {

        setDataProvider(MascApplicationContext.getContext().getDataProvider());

        //TODO Remove this from here this is not the good place to do such thing

        // add listener on model to reload on change
        MascApplicationContext.getContext().getMascUIModel().addPropertyChangeListener(MascUIModel.PROPERTY_MASC_MODEL, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                // reload model
                createModel();
            }
        });
    }

    @Override
    public CriteriaDataProvider getDataProvider() {
        return (CriteriaDataProvider) super.getDataProvider();
    }

    @Override
    public void setUI(JTree tree,
                      boolean addExpandTreeListener,
                      boolean addOneClickSelectionListener,
                      TreeSelectionListener listener,
                      TreeWillExpandListener willExpandListener) {
        super.setUI(tree,
                    addExpandTreeListener,
                    addOneClickSelectionListener,
                    listener,
                    willExpandListener);

        ToolTipManager.sharedInstance().registerComponent(tree);
    }

    public TreeModel createModel() {

        // Create root static node
        CriteriaNode root = new CriteriaNode.ValuedCriteriaNode(null);

        // Create model
        DefaultTreeModel model = createModel(root);

        // load all nodes of model
        loadAllNodes(root, getDataProvider());

        return model;
    }

    public CriteriaNode addNode(CriteriaNode parent, Criteria criteriaToAdd, int position) {
        CriteriaNodeLoador childLoador = getChildLoador(CriteriaNodeLoador.class);
        CriteriaNode node = childLoador.createNode(criteriaToAdd);

        insertNode(parent, node, position);

        return node;
    }

    public CriteriaNode getNode(Criteria criteria) {
        return findNode(getRootNode(), criteria.getUuid());
    }

    public String selectFirstNode() {
        Collection<Criteria> rootCriterias = getDataProvider().getRootCriterias();
        String uuid = rootCriterias.iterator().next().getUuid();
        selectNode(uuid);
        return uuid;
    }

    /**
     * Select all nodes.
     */
    public void selectAllNodes() {
        Collection<Criteria> rootCriterias = getDataProvider().getRootCriterias();

        List<CriteriaNode> nodes = new ArrayList<CriteriaNode>();
        recursiveGetNodes(getBridge().getRoot(), rootCriterias, nodes);
        selectNodes(nodes); // uniq call (otherwise it's slow)
    }

    protected void recursiveGetNodes(CriteriaNode root, Collection<Criteria> criterias, List<CriteriaNode> nodes) {
        if (CollectionUtils.isNotEmpty(criterias)) {
            for (Criteria criteria : criterias) {
                CriteriaNode node = findNode(root, criteria.getUuid());
                nodes.add(node);

                recursiveGetNodes(root, criteria.getChild(), nodes);
            }
        }
    }

    /**
     * Unselect all nodes.
     */
    public void unSelectAllNodes() {
        JTree tree = getUI();
        tree.getSelectionModel().clearSelection();
    }

    public List<Criteria> getSelectedCriteria() {

        List<Criteria> result = Lists.newArrayList();
        List<CriteriaNode> selectedNodes = getSelectedNodes();
        for (CriteriaNode selectedNode : selectedNodes) {
            result.add(getDataProvider().getCriteria(selectedNode.getId()));
        }

        return result;
    }

    public List<String> getSelectedCriteriaId() {
        List<String> result = Lists.newArrayList();
        List<CriteriaNode> selectedNodes = getSelectedNodes();
        for (CriteriaNode selectedNode : selectedNodes) {
            result.add(selectedNode.getId());
        }

        return result;
    }

    public TreePath getPath(CriteriaNode criteriaNode) {
        return new TreePath(getBridge().getPathToRoot(criteriaNode));
    }
}
