/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.editor.criteria;

import fr.inra.masc.model.Criteria;
import fr.inra.masc.ui.MascHandler;
import fr.inra.masc.ui.content.editor.criteria.editor.AbstractCriteriaEditor;
import fr.inra.masc.ui.content.editor.criteria.editor.CriteriaUIModel;
import fr.inra.masc.ui.tree.criteria.CriteriaNode;
import fr.inra.masc.ui.tree.criteria.CriteriaTreeHelper;
import jaxx.runtime.JAXXBinding;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.CardLayout2;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class MascModelEditorHandler extends MascHandler {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(MascModelEditorHandler.class);

    private final MascModelEditor ui;

    public MascModelEditorHandler(MascModelEditor ui) {
        this.ui = ui;

        // share handler with ui
        ui.setContextValue(this);
    }

    public CriteriaTreeHelper getTreeHelper() {
        return ui.getTreeHelper();
    }

    public void initUI() {

        // Creation of selection listener to open ui when tree selection change
        TreeSelectionListener listener = new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent event) {
                TreePath path = event.getPath();
                CriteriaNode node = (CriteriaNode) path.getLastPathComponent();

                if (log.isDebugEnabled()) {
                    log.debug("Select node " + node);
                }

                showUI(node);
            }
        };

        initCriteriaEditor(ui.get$ComputableCriteriaEditor0());
        initCriteriaEditor(ui.get$ValuedCriteriaEditor0());
        initCriteriaEditor(ui.get$ReferenceCriteriaEditor0());

        JTree tree = ui.getNavigation();

        ui.getTreeHelper().setUI(tree, true, listener);

        // expend tree by default
        SwingUtil.expandTree(tree);

        // select first
        ui.getTreeHelper().selectFirstNode();

        // single selection mode
        tree.getSelectionModel().setSelectionMode(
                TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    public void showUI(CriteriaNode node) {

        JPanel content = ui.getContent();

        Class<?> type = node.getEditorClass();

        String constraints = type.getSimpleName();

        if (log.isDebugEnabled()) {
            log.debug("Show for " + constraints);
        }

        CardLayout2 contentLayout = ui.getContentLayout();

        AbstractCriteriaEditor<?> criteriaEditor = (AbstractCriteriaEditor<?>)
                contentLayout.getComponent(content, constraints);

        // push criteria
        Criteria criteria = ui.getTreeHelper().getDataProvider().getCriteria(node.getId());
        CriteriaUIModel model = criteriaEditor.getModel();
        model.setCriteria(criteria);

        // show ui
        contentLayout.show(content, constraints);
    }

    protected void initCriteriaEditor(final AbstractCriteriaEditor<?> ui) {
        ui.getModel().addPropertyChangeListener(CriteriaUIModel.PROPERTY_CRITERIA, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Criteria criteria = ui.getModel().getCriteria();
                ui.setCriteria(criteria);
                JAXXBinding[] dataBindings = ui.getDataBindings();
                for (JAXXBinding dataBinding : dataBindings) {
                    ui.processDataBinding(dataBinding.getId());
                }
                ui.getCriteriaScale().setCriteria(criteria);
            }
        });
    }

}
