/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.editor.evalutedCriteria;

import fr.inra.masc.ExecutorException;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.services.DexiEvalInvokerService;
import fr.inra.masc.services.MascModelService;
import fr.inra.masc.ui.MascHandler;
import fr.inra.masc.ui.MascUIHelper;
import fr.inra.masc.utils.MascUtil;

import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

import jaxx.runtime.SwingUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTreeTable;

import static org.nuiton.i18n.I18n._;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class EvaluatedCriteriaEditorHandler extends MascHandler {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(EvaluatedCriteriaEditorHandler.class);

    /** UI. */
    private final EvaluatedCriteriaEditor ui;

    public EvaluatedCriteriaEditorHandler(EvaluatedCriteriaEditor ui) {
        this.ui = ui;
    }

    public void initUI() {

        JXTreeTable treeTable = ui.getEvaluatedCriteriaTreeTable();

        ui.getTreeHelper().setUI(treeTable, true);

        // select first
        ui.getTreeHelper().selectFirstNode();

        // single selection mode
        treeTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        OptionValueCellRenderer renderer = new OptionValueCellRenderer();
        treeTable.setDefaultRenderer(Object.class, renderer);
        treeTable.setHighlighters(renderer);
    }

    public void loadEvaluatedModel() {

        File dexiEvalApp = getConfig().getDexiEvalExecutableFile();

        // if dexi is not configured
        if (!MascUtil.isExecutableFile(dexiEvalApp)) {
            int returnVal = JOptionPane.showConfirmDialog(
                    ui,
                    _("masc.dexiEval.notConfigured"),
                    _("masc.dexiEval.notConfigured.title"),
                    JOptionPane.YES_NO_OPTION);
            if (returnVal == JOptionPane.YES_OPTION) {
                dexiEvalApp = MascUIHelper.openFile(ui, MascUIHelper.MascFileType.DEXI_EVAL_APP);
                if (dexiEvalApp == null) {
                    return;
                }
                getConfig().setDexiEvalExecutableFile(dexiEvalApp);
            } else {
                return;
            }
        }
        try {

            // creating tmp file
            File dexiTmpFile = File.createTempFile("dexi", ".dxi");
            dexiTmpFile.deleteOnExit();

            // export in tmp file
            MascModel mascModel = getCurrentMascModel();
            getService(MascModelService.class).exportToDEXiModel(
                    mascModel, dexiTmpFile, false);

            // eval model
            DexiEvalInvokerService service = getService(DexiEvalInvokerService.class);

            try {
                service.evalMascModel(mascModel, dexiTmpFile);
                getMascUIModel().setMascModel(mascModel);
            } catch (ExecutorException eee) {

                // show dexiEvalLogs on errors
                JOptionPane.showMessageDialog(
                        ui,
                        _("masc.error.dexiEval", eee.getLogs()),
                        _("masc.error.dexiEval.title"),
                        JOptionPane.ERROR_MESSAGE);
            }

            // update ui
            ui.getTreeHelper().setMascModel(mascModel);

            // expend tree by default
            SwingUtil.expandTreeTable(ui.getEvaluatedCriteriaTreeTable());
            
            // clear dirty
            getMascUIModel().setEvaluationDirty(false);
        } catch (Exception eee) {
            String errorMsg = "Failed to launch DEXiEval";
            log.error(errorMsg, eee);
            MascUIHelper.showError(ui, errorMsg, eee);
        }
    }
}
