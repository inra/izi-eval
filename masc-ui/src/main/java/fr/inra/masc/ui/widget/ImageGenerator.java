package fr.inra.masc.ui.widget;

/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Dimension;
import java.awt.image.BufferedImage;

/**
 * To generate an image.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public interface ImageGenerator {

    /**
     * Get the image using the given scale factor.
     *
     * @param scaleFactor the scale factor to use.
     * @return the generated image
     */
    BufferedImage getImage(double scaleFactor);

    /**
     * Get the actual dimension of the image if no scale factor is given (says scale factor is {@code 1.0}).
     *
     * @return the actual dimension of the image for a {@code 1.0} scale factor.
     */
    Dimension getActualDimension();
}
