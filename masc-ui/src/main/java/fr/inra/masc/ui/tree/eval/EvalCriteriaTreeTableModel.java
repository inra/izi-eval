package fr.inra.masc.ui.tree.eval;

/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.ui.tree.criteria.CriteriaDataProvider;
import java.util.Collection;
import java.util.List;
import jaxx.runtime.swing.nav.treetable.NavTreeTableModel;

import static org.nuiton.i18n.I18n._;

/**
 * @author sletellier
 * @since 0.1
 */
public class EvalCriteriaTreeTableModel extends NavTreeTableModel.MyDefaultTreeTableModel {

    protected CriteriaDataProvider dataProvider;

    public EvalCriteriaTreeTableModel(CriteriaDataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    @Override
    public Object getValueAt(Object node, int column) {
        String id = ((EvalCriteriaNode) node).getId();
        if (column == 0) {
            Criteria criteria = dataProvider.getCriteria(id);
            return criteria;
        }

        Collection<OptionValue> values = dataProvider.getValues(id, column);
        if (values == null || values.isEmpty()) {
            return null;
        }
        return values;
    }

    @Override
    public void setValueAt(Object value, Object node, int column) {
//        String id = ((EvalCriteriaNode) node).getId();
//
//        OptionValue[] optionValues = dataProvider.getValues(id);
//        optionValues[column - 1].setValue((Integer)value);
    }

    @Override
    public String[] getColumnsNames() {

        List<String> optionNames = dataProvider.getOptionNames();

        optionNames.add(0, _("masc.eval.criteria"));

        return optionNames.toArray(new String[optionNames.size()]);
    }

    @Override
    public boolean isCellEditable(Object node, int column) {

        return false;

//        not editable for moment
        // not for first column
//        if (column == 0) {
//            return false;
//        }
//
//        // only if node is editable
//        EvalCriteriaNode evalNode = (EvalCriteriaNode)node;
//        Criteria criteria = dataProvider.getCriteria(evalNode.getId());
//
//        return criteria instanceof EditableCriteria;
    }
}
