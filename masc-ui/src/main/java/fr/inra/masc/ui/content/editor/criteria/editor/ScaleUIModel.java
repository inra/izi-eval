/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.editor.criteria.editor;

import fr.inra.masc.MascApplicationContext;
import fr.inra.masc.utils.MascUtil;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Scale;

import javax.swing.AbstractListModel;
import javax.swing.MutableComboBoxModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class ScaleUIModel extends AbstractListModel implements MutableComboBoxModel {

    private static final long serialVersionUID = 1L;

    protected Criteria criteria;

    protected Map<String, Scale> scaleValues;

    protected List<Scale> flatValues;

    protected Scale selectedScaleValues;

    public ScaleUIModel() {
        MascModel mascModel = MascApplicationContext.getContext().getMascModel();

        scaleValues = MascUtil.extractAllScales(mascModel);

        Collection<Scale> values = scaleValues.values();
        flatValues = new ArrayList<Scale>(values);
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
        Scale scale = criteria.getScale();
        if (scale != null) {
            this.selectedScaleValues = scaleValues.get(scale.toString());
        }
    }

    @Override
    public void addElement(Object obj) {
        // never append
    }

    @Override
    public void removeElement(Object obj) {
        // never append
    }

    @Override
    public void insertElementAt(Object obj, int index) {
        // never append
    }

    @Override
    public void removeElementAt(int index) {
        // never append
    }

    @Override
    public void setSelectedItem(Object anItem) {
        this.selectedScaleValues = (Scale) anItem;

        // apply selected scale to criteria
        criteria.getScale().setScaleValue(selectedScaleValues.getScaleValue());
    }

    @Override
    public Object getSelectedItem() {
        return selectedScaleValues;
    }

    @Override
    public int getSize() {
        return scaleValues.size();
    }

    @Override
    public Object getElementAt(int index) {
        return flatValues.get(index);
    }

}
