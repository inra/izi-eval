/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.tree.eval;

import fr.inra.masc.model.Criteria;
import jaxx.runtime.swing.nav.treetable.NavTreeTableNode;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 */
public class EvalCriteriaNode extends NavTreeTableNode<EvalCriteriaNode> {

    private static final long serialVersionUID = 1L;

    protected EvalCriteriaNode(String id) {
        super(Criteria.class,
                id,
                null,
                EvalCriteriaTreeTableHelper.getChildLoador(EvalCriteriaNodeLoador.class)
        );
    }

    /**
     * @author sletellier &lt;letellier@codelutin.com&gt;
     */
    public static class ReferenceEvalCriteriaNode extends EvalCriteriaNode {

        private static final long serialVersionUID = 1L;

        public ReferenceEvalCriteriaNode(String id) {
            super(id);
        }
    }
}
