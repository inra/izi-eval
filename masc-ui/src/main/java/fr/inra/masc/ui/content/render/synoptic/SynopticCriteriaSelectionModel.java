package fr.inra.masc.ui.content.render.synoptic;

/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Lists;

import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.synoptic.SynopticModel;
import fr.inra.masc.ui.tree.SelectableCriteriaSelectionModel;
import fr.inra.masc.ui.tree.criteria.CriteriaNode;
import fr.inra.masc.ui.tree.criteria.CriteriaTreeHelper;
import fr.inra.masc.utils.MascUtil;

/**
 * Selection model for criteria to display in synoptic graphic.
 * 
 * <strong>Note:</strong> this is revisited version of the
 * {@link SelectableCriteriaSelectionModel} which improve unselection.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.8
 */
public class SynopticCriteriaSelectionModel extends DefaultTreeSelectionModel {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SynopticCriteriaSelectionModel.class);

    /**
     * Tree helper used to find node from criteria id.
     *
     * @since 1.8
     */
    protected final CriteriaTreeHelper treeHelper;

    /**
     * Masc modelwhich contains all criteria.
     *
     * @since 1.8
     */
    protected final MascModel mascModel;

    /**
     * To select all childs (and their refs).
     *
     * @since 1.8
     */
    protected final DepthSelector selector;

    /**
     * To unselect all childs (and their refs).
     *
     * @since 1.8
     */
    protected final DepthSelector unselector;

    protected SynopticModel synopticModel;

    /**
     * Current selected root criteria.
     *
     * @since 1.8
     */
    protected Criteria selectedCriteria;

    public SynopticCriteriaSelectionModel(CriteriaTreeHelper treeHelper,
                                          MascModel mascModel,
                                          SynopticModel synopticModel) {
        this.treeHelper = treeHelper;
        this.mascModel = mascModel;
        this.synopticModel = synopticModel;
        setSelectionMode(DISCONTIGUOUS_TREE_SELECTION);
        selector = new DepthSelector() {

            private static final long serialVersionUID = 1L;

            @Override
            public void apply(List<TreePath> treePaths, TreePath path) {

                // add path to selected paths
                treePaths.add(path);
            }
        };
        unselector = new DepthSelector() {

            private static final long serialVersionUID = 1L;

            @Override
            public void apply(List<TreePath> treePaths, TreePath path) {

                // remove current from selected paths
                treePaths.remove(path);
            }
        };
    }

    // Hum should not have this here but in SynopticModel ?
    public Criteria getSelectedCriteria() {
        if (selectedCriteria == null) {
            selectedCriteria = mascModel.getCriteria(0);
        }
        return selectedCriteria;
    }

    @Override
    public void setSelectionPaths(TreePath[] pPaths) {
        //tchemit-2012-11-05 Does not authorize to use this method
        //super.setSelectionPaths(pPaths);
    }

    @Override
    public void addSelectionPaths(TreePath[] paths) {
        //tchemit-2012-11-05 Does not authorize to use this method
        //super.addSelectionPaths(paths);
    }

    @Override
    public void removeSelectionPaths(TreePath[] paths) {
        //tchemit-2012-11-05 Does not authorize to use this method
        //tchemit-2012-11-05 This only happens will collapse a node (wants to deselect all his child but we do not want this behaviour...)
        //super.removeSelectionPaths(paths);
    }

    @Override
    public void addSelectionPath(TreePath path) {
        if (log.isDebugEnabled()) {
            log.debug("try to add path: " + path);
        }
        if (!isPathSelected(path)) {

            // select it with his shell

            if (log.isInfoEnabled()) {
                log.info("add path: " + path);
            }

            TreePath[] selectedPaths = getSelectionPaths();
            List<TreePath> treePaths = Lists.newArrayList(selectedPaths);

            Criteria criteria = getCriteria(path);

            select(treePaths, path, criteria);
        }
    }

    @Override
    public void removeSelectionPath(TreePath path) {
        if (log.isDebugEnabled()) {
            log.debug("try to del path: " + path);
        }

        if (isPathSelected(path)) {

            // unselect it with his shell

            if (log.isInfoEnabled()) {
                log.info("del path: " + path);
            }

            unselect(path);
        }
    }

    @Override
    public void setSelectionPath(TreePath path) {

        if (log.isInfoEnabled()) {
            log.info("set path: " + path);
        }

        // keep a track of the top root selected criteria
        selectedCriteria = getCriteria(path);

        select(Lists.<TreePath>newArrayList(), path, selectedCriteria);
    }

    protected Criteria getCriteria(TreePath path) {
        // criteria node to treat
        CriteriaNode node = (CriteriaNode) path.getLastPathComponent();

        // keep a track of the top root selected criteria
        Criteria result = MascUtil.findCriteria(mascModel, node.getId());
        return result;
    }

    protected void unselect(TreePath path) {

        TreePath[] selectedPaths = getSelectionPaths();
        List<TreePath> treePaths = Lists.newArrayList(selectedPaths);

        unselector.applyOnChild(treePaths, path);

        super.setSelectionPaths(treePaths.toArray(new TreePath[treePaths.size()]));
    }

    protected void select(List<TreePath> treePaths,
                          TreePath path,
                          Criteria criteria) {

        // if is ref, select target
        while (criteria.isReference()) {

            // select both
            selector.applyOnChild(treePaths, path);

            criteria = criteria.getChild(0);
            CriteriaNode node = treeHelper.findNode(
                    treeHelper.getRootNode(), criteria.getUuid());
            path = treeHelper.getPath(node);
        }

        selector.applyOnChild(treePaths, path);

        super.setSelectionPaths(treePaths.toArray(new TreePath[treePaths.size()]));
    }

    protected abstract class DepthSelector implements Serializable {

        private static final long serialVersionUID = 1L;

        public void applyOnChild(List<TreePath> treePaths, TreePath path) {

            // select in depth
            CriteriaNode selectedNode = (CriteriaNode) path.getLastPathComponent();
            String nodeId = selectedNode.getId();
            Criteria criteria = MascUtil.findCriteria(mascModel, nodeId);

            // if is ref, select target
            if (criteria.isReference()) {
                // select ref too
                apply(treePaths, path);

                // lors d'une désélection, on désélectionne la référence, et on
                // arrete, pour ne pas déselectionner une autre branche de l'arbre.
                if (!synopticModel.isResolveReference()) {
                    return;
                }

                criteria = criteria.getChild(0);
                CriteriaNode node = treeHelper.findNode(treeHelper.getRootNode(), criteria.getUuid());
                path = treeHelper.getPath(node);
            }
            Collection<Criteria> children = criteria.getChild(true);
            for (Criteria child : children) {
                CriteriaNode node = treeHelper.findNode(treeHelper.getRootNode(), child.getUuid());
                if (node != null) {
                    TreePath childPath = treeHelper.getPath(node);
                    applyOnChild(treePaths, childPath);
                }
            }
            apply(treePaths, path);
        }

        protected abstract void apply(List<TreePath> treePaths, TreePath path);
    }
}
