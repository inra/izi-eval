package fr.inra.masc.ui.widget;

/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Handler of ui {@link ImagePanel}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.5
 */
public abstract class ImagePanelHandler<U extends ImagePanel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImagePanelHandler.class);

    protected final U ui;

    protected ImagePanelMenu menuUI;

    public ImagePanelMenu getMenuUI() {
        if (menuUI == null) {

            // share handler for others child ui
            ui.setContextValue(this);

            // share ui model for others child ui
            ui.setContextValue(ui.getModel());

            menuUI = new ImagePanelMenu(ui);
        }
        return menuUI;
    }

    public ImagePanelHandler(U ui) {
        this.ui = ui;
    }

    public void initUI() {

        ui.getContent().addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(final MouseEvent mouseEvent) {

                if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
                    getMenuUI().show(ui, mouseEvent.getX(), mouseEvent.getY());
                }
            }
        });

        ui.getModel().addPropertyChangeListener(ImagePanelModel.PROPERTY_ZOOM, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                Double newValue = (Double) evt.getNewValue();
                generateImage(newValue);
            }
        });
    }

    public void zoomIn() {
        ui.getModel().incrementsZoom();
    }

    public void zoomOut() {
        ui.getModel().decrementsZoom();
    }

    public void zoomActual() {
        ui.getModel().setZoomFactor(1d);
    }

    public void zoomFit() {

        ImagePanelModel model = ui.getModel();

        Dimension parentSize = getParentDimension();
        Dimension imageSize = getActualDimension();
        double xRatio = parentSize.getWidth() / imageSize.getWidth();
        double yRatio = parentSize.getHeight() / imageSize.getHeight();
        double bestRatio = Math.min(xRatio, yRatio);
        if (log.isDebugEnabled()) {
            log.debug("Parent size:  " + parentSize);
            log.debug("Image size:   " + imageSize);
            log.debug("xRatio:       " + xRatio);
            log.debug("yRatio:       " + yRatio);
            log.debug("bestRatio:    " + bestRatio);
        }
        model.setZoomFactor(bestRatio);
    }

    public Dimension getParentDimension() {
        Container parent = ui.getContent().getParent();
        Dimension parentSize = parent.getSize();
        return parentSize;
    }

    public abstract JComponent getViewer();

    public abstract void generateImage(Double newValue);

    public abstract void exportAsFile();

    public abstract Dimension getActualDimension();
}
