package fr.inra.masc.ui.widget;

/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Component to paint the image.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public class ImageComponent extends JPanel {

    private static final long serialVersionUID = 1L;

    private final JComponent parent;

    private final ImagePanelModel model;

    public ImageComponent(JComponent parent, ImagePanelModel model) {
        this.parent = parent;
        this.model = model;

        // when model image changed, let's resize the component
        // and repaint his parent
        model.addPropertyChangeListener(ImagePanelModel.PROPERTY_IMAGE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Dimension size = new Dimension(getWidth(), getHeight());
                setPreferredSize(size);
                setSize(size);
                ImageComponent.this.parent.repaint();
            }
        });
    }

    @Override
    public int getWidth() {

        BufferedImage image = model.getImage();
        int result;
        if (image == null) {
            result = super.getWidth();
        } else {
            result = Math.max(parent.getWidth(), image.getWidth());
        }
        return result;
    }

    @Override
    public int getHeight() {
        BufferedImage image = model.getImage();
        int result;
        if (image == null) {
            result = super.getHeight();
        } else {
            result = Math.max(parent.getHeight(), image.getHeight());
        }
        return result;
    }

    @Override
    public void paintComponent(Graphics g) {
        BufferedImage image = model.getImage();
        if (image != null) {

            int width = parent.getWidth();
            int height = parent.getHeight();
            int imageWidth = image.getWidth();
            int imageHeight = image.getHeight();
            int x = 0;
            if (width > imageWidth) {
                x = (width - imageWidth) / 2;
            }

            int y = 0;
            if (height > imageHeight) {
                y = (height - imageHeight) / 2;
            }

            g.drawImage(image, x, y, parent);
        }
    }

}
