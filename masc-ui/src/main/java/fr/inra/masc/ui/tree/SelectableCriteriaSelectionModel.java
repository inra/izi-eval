/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.tree;

import com.google.common.collect.Lists;
import fr.inra.masc.MascApplicationContext;
import fr.inra.masc.utils.MascUtil;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.ui.tree.criteria.CriteriaNode;
import fr.inra.masc.ui.tree.criteria.CriteriaTreeHelper;

import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;
import java.util.Collection;
import java.util.List;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 */
public class SelectableCriteriaSelectionModel extends DefaultTreeSelectionModel {

    private static final long serialVersionUID = -2189052292360274560L;

    protected boolean selectDepth;

    /**
     * Flag to select the target of a ref criteria.
     *
     * @since 1.5
     */
    protected boolean selectRefTarget = true;
    protected CriteriaTreeHelper treeHelper;
    protected Criteria selectedCriteria;
    protected MascModel mascModel;

    public SelectableCriteriaSelectionModel(CriteriaTreeHelper treeHelper) {
        this(treeHelper, false, true);
    }

    public SelectableCriteriaSelectionModel(CriteriaTreeHelper treeHelper,
                                            boolean selectDepth) {
        this(treeHelper, selectDepth, true);
    }

    public SelectableCriteriaSelectionModel(CriteriaTreeHelper treeHelper,
                                            boolean selectDepth,
                                            boolean selectRefTarget ) {
        this.treeHelper = treeHelper;
        this.selectDepth = selectDepth;
        this.selectRefTarget = selectRefTarget;
        mascModel = MascApplicationContext.getContext().getMascModel();
    }

    @Override
    public void setSelectionPath(TreePath path) {

        if (!selectDepth) {
            TreePath[] selectionPaths = getSelectionPaths();
            if (selectionPaths != null) {
                for (TreePath selectedPath : selectionPaths) {
                    if (selectedPath.equals(path)) {
                        unselect(selectedPath);
                        return;
                    }
                }
            }
        } else {
            clearSelection();
        }
        select(path);
    }

    @Override
    public int getSelectionMode() {
        return DISCONTIGUOUS_TREE_SELECTION;
    }

    protected void select(TreePath path) {
        TreePath[] selectedPaths = getSelectionPaths();

        List<TreePath> treePaths;
        if (selectedPaths != null) {
            treePaths = Lists.newArrayList(selectedPaths);
        } else {
            treePaths = Lists.newArrayList();
        }
        CriteriaNode selectedNode = (CriteriaNode) path.getLastPathComponent();
        selectedCriteria = MascUtil.findCriteria(mascModel, selectedNode.getId());

        if (selectRefTarget) {

            // if is ref, select target
            while (selectedCriteria.isReference()) {

                // select both
                selectAllChild(treePaths, path);

                selectedCriteria = selectedCriteria.getChild(0);
                CriteriaNode node = treeHelper.findNode(treeHelper.getRootNode(), selectedCriteria.getUuid());
                path = treeHelper.getPath(node);
            }
        }

        selectAllChild(treePaths, path);
        setSelectionPaths(treePaths.toArray(new TreePath[treePaths.size()]));
    }

    protected void selectAllParent(List<TreePath> treePaths, TreePath path) {
        TreePath parentPath = path.getParentPath();
        if (parentPath != null) {

            // select only if not already selected
            if (!isPathSelected(parentPath)) {
                treePaths.add(parentPath);
                selectAllParent(treePaths, parentPath);
            }
        }
    }

    protected void selectAllChild(List<TreePath> treePaths, TreePath path) {
        DepthSelector depthSelector = new DepthSelector(mascModel, treePaths, treeHelper, selectDepth) {

            @Override
            public void apply(TreePath path) {
                treePaths.add(path);
            }
        };

        depthSelector.applyOnChild(path);
    }

    protected void unselect(TreePath path) {

        TreePath[] selectedPaths = getSelectionPaths();
        List<TreePath> treePaths = Lists.newArrayList(selectedPaths);

        DepthSelector depthSelector = new DepthSelector(mascModel, treePaths, treeHelper, selectDepth) {

            @Override
            public void apply(TreePath path) {
                treePaths.remove(path);
            }
        };

        depthSelector.applyOnChild(path);

        setSelectionPaths(treePaths.toArray(new TreePath[treePaths.size()]));
    }

    protected static abstract class DepthSelector {

        protected boolean selectDepth;
        protected CriteriaTreeHelper treeHelper;
        protected List<TreePath> treePaths;
        protected MascModel mascModel;

        protected DepthSelector(MascModel mascModel, List<TreePath> treePaths, CriteriaTreeHelper treeHelper, boolean selectDepth) {
            this.selectDepth = selectDepth;
            this.treeHelper = treeHelper;
            this.treePaths = treePaths;
            this.mascModel = mascModel;
        }

        public void applyOnChild(TreePath path) {

            if (selectDepth) {
                // select in depth
                CriteriaNode selectedNode = (CriteriaNode) path.getLastPathComponent();
                String nodeId = selectedNode.getId();
                Criteria criteria = MascUtil.findCriteria(mascModel, nodeId);

                // if is ref, select target
                if (criteria.isReference()) {

                    // select ref too
                    apply(path);
                    criteria = criteria.getChild(0);
                    CriteriaNode node = treeHelper.findNode(treeHelper.getRootNode(), criteria.getUuid());
                    path = treeHelper.getPath(node);
                }
                Collection<Criteria> children = criteria.getChild(true);
                for (Criteria child : children) {
                    CriteriaNode node = treeHelper.findNode(treeHelper.getRootNode(), child.getUuid());
                    if (node != null) {
                        TreePath childPath = treeHelper.getPath(node);
                        applyOnChild(childPath);
                    }

                }
            }
            apply(path);
        }

        protected abstract void apply(TreePath path);
    }
}
