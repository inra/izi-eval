package fr.inra.masc.ui.report;

/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n._;
import fr.inra.masc.ExecutorException;
import fr.inra.masc.MascUIModel;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.RScriptModel;
import fr.inra.masc.reports.ReportModel;
import fr.inra.masc.services.RInvokerService;
import fr.inra.masc.services.ReportGeneratorService;
import fr.inra.masc.ui.MascHandler;
import fr.inra.masc.ui.MascUIHelper;

import java.io.File;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Handler of {@link ReportUI}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public class ReportUIHandler extends MascHandler {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReportUIHandler.class);

    /** UI managed byt his handler. */
    private final ReportUI ui;

    public ReportUIHandler(ReportUI ui) {
        this.ui = ui;
    }

    public void cancel() {
        ui.setModel(null);
        ui.dispose();
    }

    public void generateReport() {

        // prepare model
        ReportModel reportModel = ui.getModel();

        // ask where to save reports
        MascUIModel mascUIModel = getMascUIModel();
        File reportFile = MascUIHelper.saveAsFile(
                ui,
                MascUIHelper.MascFileType.REPORT,
                mascUIModel.getMascFileName());

        if (reportFile != null) {

            reportModel.setReportFile(reportFile);

            // add masc model to report model
            reportModel.setMascModel(getCurrentMascModel());

            // for r script, verify that files are already generated
            if (reportModel.isShowAs()) {
                File tmpDirectory = getConfig().getTmpDirectory();

                RScriptModel scriptModel = mascUIModel.getScriptModel();
                if (!scriptModel.isAlreadyLaunched(tmpDirectory)) {

                    MascModel currentMascModel = getCurrentMascModel();

                    RInvokerService rInvokerService =
                            getService(RInvokerService.class);

                    try {
                        rInvokerService.executeRSCript(mascUIModel.getMascFileName(),
                                                       currentMascModel,
                                                       scriptModel);

                    } catch (ExecutorException eee) {

                        if (scriptModel.logFileExist(tmpDirectory)) {
                            showLogs(ui, scriptModel, JOptionPane.ERROR_MESSAGE);
                        } else {
                            log.error("Failed to launch R script", eee);
                        }
                    }
                }
            }

            // generate reports
            try {
                getService(ReportGeneratorService.class).generateReport(mascUIModel.getMascFileName(), reportModel);
            } catch (Exception eee) {
                if (eee.getCause() instanceof OutOfMemoryError) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't generate report", eee);
                    }
                    JOptionPane.showMessageDialog(ui, _("masc.menu.generate.report.memoryError"),
                        _("masc.menu.generate.report"), JOptionPane.ERROR_MESSAGE);
                } else {
                    String errorMsg = "Failed to generate report";
                    if (log.isErrorEnabled()) {
                        log.error(errorMsg, eee);
                    }
                    MascUIHelper.showError(ui, errorMsg, eee);
                }
            }
        }

        ui.dispose();
    }

}
