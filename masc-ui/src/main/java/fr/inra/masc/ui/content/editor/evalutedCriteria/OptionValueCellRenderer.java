/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.editor.evalutedCriteria;

import com.google.common.collect.Lists;
import fr.inra.masc.utils.MascUtil;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.model.ScaleValue;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.decorator.AbstractHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 */
public class OptionValueCellRenderer extends AbstractHighlighter implements TableCellRenderer {

    public static String VALUE_SEPARATOR = " - ";

    protected DefaultTableCellRenderer delegate;

    public OptionValueCellRenderer() {
        this.delegate = new DefaultTableCellRenderer();
    }

    @Override
    protected Component doHighlight(Component component, ComponentAdapter adapter) {
        Criteria criteria = (Criteria) adapter.getValueAt(adapter.row, 0);
        List<ScaleValue> scale = new ArrayList<ScaleValue>(criteria.getScale().getScaleValue());

        Object value = adapter.getValue();
        if (!(value instanceof Criteria)) {
            Collection<OptionValue> optionValues = (Collection<OptionValue>) value;

            Color color = Color.BLACK;
            if (value != null) {
                List<String> valuedScales = Lists.newArrayList();

                for (OptionValue optionValue : optionValues) {
                    Integer valueInt = optionValue.getValue();
                    if (valueInt == null) {
                        valuedScales.add(MascUtil.OPTION_VALUE_DEFAULT);
                    } else {
                        ScaleValue scaleValue = scale.get(valueInt);
                        valuedScales.add(scaleValue.getName());

                        // on laisse noir s'il y a plusieurs valeurs
                        if (optionValues.size() == 1) {
                            color = MascUtil.getColorForScaleValue(scaleValue);
                        }
                    }
                }
            }
            component.setForeground(color);
        }

        return component;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Criteria criteria = (Criteria) table.getModel().getValueAt(row, 0);
        List<ScaleValue> scale = new ArrayList<ScaleValue>(criteria.getScale().getScaleValue());

        String text;
        Collection<OptionValue> optionValues = (Collection<OptionValue>) value;

        if (value == null) {
            text = MascUtil.OPTION_VALUE_DEFAULT;
        } else {
            List<String> valuedScales = Lists.newArrayList();
            
            for (OptionValue optionValue : optionValues) {
                Integer valueInt = optionValue.getValue();
                if (valueInt == null) {
                    valuedScales.add(MascUtil.OPTION_VALUE_DEFAULT);
                } else {
                    ScaleValue scaleValue = scale.get(valueInt);
                    valuedScales.add(scaleValue.getName());
                }
            }
            text = StringUtils.join(valuedScales, VALUE_SEPARATOR);
        }

        Component rendered = delegate.getTableCellRendererComponent(table, text, isSelected, hasFocus, row, column);
        return rendered;
    }
}
