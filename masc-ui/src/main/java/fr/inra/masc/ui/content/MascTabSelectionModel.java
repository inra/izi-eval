/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content;

import javax.swing.DefaultSingleSelectionModel;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class MascTabSelectionModel extends DefaultSingleSelectionModel {

    private static final long serialVersionUID = 1L;

    protected MascTabs ui;

    public MascTabSelectionModel(MascTabs ui) {

        this.ui = ui;

        // select first
        setSelectedIndex(0);
    }

    @Override
    public void setSelectedIndex(int index) {
        int oldIndex = getSelectedIndex();
        if (ui.getHandler().changeTab(ui, index, oldIndex)) {
            super.setSelectedIndex(index);
        }
    }
}
