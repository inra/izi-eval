/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.editor.criteria.editor;

import fr.inra.masc.utils.MascUtil;
import fr.inra.masc.io.MascXmlConstant;
import fr.inra.masc.model.ComputableCriteria;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.Function;

import javax.swing.table.AbstractTableModel;
import java.util.Collection;

import static org.nuiton.i18n.I18n._;

/**
 * Model used to display child criteria weight
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 1.3
 */
public class WeightTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    protected ComputableCriteria criteria;

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = (ComputableCriteria) criteria;
        fireTableStructureChanged();
    }

    @Override
    public int getRowCount() {
        Collection<Criteria> children = criteria.getChild();
        int result;
        if (children == null) {
            result = 0;
        } else {
            result = children.size();
        }
        return result;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int rowIndex) {
        String result;
        switch (rowIndex) {
            case 0:
                result = _("masc.weight.colomnName");
                break;
            default:
                result = _("masc.weight.colomnWeight");
        }
        return result;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Criteria child = criteria.getChild(rowIndex);
        Object result;
        switch (columnIndex) {
            case 0:
                result = child.getName();
                break;

            default:
                Function function = criteria.getFunction();
                String weights = function.getWeights();
                if (weights == null) {
                    result = "";
                } else {
                    String weight = weights.split(MascXmlConstant.WEIGHTS_SEPARATOR)[rowIndex];
                    result = MascUtil.getWeightAsString(weight);
                }
        }
        return result;
    }
}
