package fr.inra.masc.ui.content.editor.threshold;
/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

import java.io.File;

/**
 * Exort threshold model.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public class ExportThresholdModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    protected String exportFilename;

    protected File exportDirectory;

    public String getExportFilename() {
        return exportFilename;
    }

    public void setExportFilename(String exportFilename) {
        Object oldValue = this.exportFilename;
        this.exportFilename = exportFilename;
        firePropertyChange("exportFilename", oldValue, exportFilename);
    }

    public File getExportDirectory() {
        return exportDirectory;
    }

    public void setExportDirectory(File exportDirectory) {
        Object oldValue = this.exportDirectory;
        this.exportDirectory = exportDirectory;
        firePropertyChange("exportDirectory", oldValue, exportDirectory);

    }

    public File getExportFile(String extension) {
        return new File(getExportDirectory(),
                        getExportFilename() + "." + extension);
    }

    public void copyTo(ExportThresholdModel model) {
        model.setExportDirectory(getExportDirectory());
        model.setExportFilename(getExportFilename());
    }
}
