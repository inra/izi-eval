/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui;

import static org.nuiton.i18n.I18n._;
import static org.nuiton.i18n.I18n.n_;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.Locale;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.ClassPathI18nInitializer;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.util.config.ApplicationConfig;
import org.nuiton.widget.SwingSession;

import fr.inra.masc.MascApplicationContext;
import fr.inra.masc.MascConfig;
import fr.inra.masc.MascConfigOption;
import fr.inra.masc.MascUIModel;
import fr.inra.masc.charts.ChartModel;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Option;
import fr.inra.masc.reports.ReportModel;
import fr.inra.masc.services.DexiEvalInvokerService;
import fr.inra.masc.services.DexiInvokerService;
import fr.inra.masc.services.MascModelService;
import fr.inra.masc.services.MascUIModelService;
import fr.inra.masc.services.ReportGeneratorService;
import fr.inra.masc.ui.content.MascTabs;
import fr.inra.masc.ui.content.editor.evalutedCriteria.EvaluatedCriteriaEditorHandler;
import fr.inra.masc.ui.content.editor.factorValue.MascFactorValueEditorFactory;
import fr.inra.masc.ui.content.render.chart.ChartUI;
import fr.inra.masc.ui.content.render.synoptic.SynopticUI;
import fr.inra.masc.ui.report.ReportUI;
import fr.inra.masc.utils.MascUtil;
import fr.reseaumexico.editor.ImportScenarioListener;
import fr.reseaumexico.editor.InputDesignEditor;
import fr.reseaumexico.editor.InputDesignEditorHandler;
import fr.reseaumexico.model.InputDesign;
import fr.reseaumexico.model.Scenario;
import fr.reseaumexico.model.event.InputDesignFactorEvent;
import fr.reseaumexico.model.event.InputDesignFactorListener;
import fr.reseaumexico.model.event.InputDesignScenarioEvent;
import fr.reseaumexico.model.event.InputDesignScenarioListener;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.context.JAXXInitialContext;
import jaxx.runtime.swing.AboutPanel;
import jaxx.runtime.swing.config.ConfigUIHelper;
import jaxx.runtime.swing.log.JAXXLog4jUI;

/**
 * Handler of main UI
 *
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class MascMainUIHandler extends MascHandler /*implements JAXXHelpUIHandler*/ {

    /** Logger. */
    private static Log log = LogFactory.getLog(MascMainUIHandler.class);

    private final PropertyChangeListener footerListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            refreshFooter();
        }
    };

    private final InputDesignFactorListener inputDesignFactorListener = new InputDesignFactorListener() {

        @Override
        public void factorValueChanged(InputDesignFactorEvent event) {
            getMascUIModel().setModelChanged(true);
            getMascUIModel().setEvaluationDirty(true);
        }
    };

    private final InputDesignScenarioListener inputDesignScenarioListener = new InputDesignScenarioListener() {

        @Override
        public void scenarioAdded(InputDesignScenarioEvent event) {
            getMascUIModel().setModelChanged(true);
            getMascUIModel().setEvaluationDirty(true);
            refreshFooter();
        }

        @Override
        public void scenarioRemoved(InputDesignScenarioEvent event) {
            getMascUIModel().setModelChanged(true);
            getMascUIModel().setEvaluationDirty(true);
            refreshFooter();
        }
        
        @Override
        public void scenarioRenamed(InputDesignScenarioEvent event) {
            getMascUIModel().setModelChanged(true);
            getMascUIModel().setEvaluationDirty(true);
            refreshFooter();
        }
    };

    private MascMainUI ui;

    public MascMainUI initUI(JAXXInitialContext context) {

        MascApplicationContext applicationContext = getApplicationContext();

        MascConfig config = getConfig();

        // init i18n
        Locale locale = config.getLocale();

        try {
            I18n.init(new DefaultI18nInitializer("masc-i18n"), locale);
        } catch (RuntimeException ex) {
            I18n.init(new ClassPathI18nInitializer(), locale);
        }

        // load help mapping
        /*String mappingProperties = "/helpMapping_" + locale.toString() +
                                   ".properties";
        try {

            InputStream resourceAsStream =
                    getClass().getResourceAsStream(mappingProperties);
            Properties properties = new Properties();
            properties.load(resourceAsStream);
            applicationContext.setHelpMapping(properties);

        } catch (Exception eee) {
            log.error("Failed to load help mapping file at '" +
                      mappingProperties + "'", eee);
        }*/

        // init jaxx logger
        JAXXLog4jUI.init(config.getLogLevel(), config.getLogPatternLayout());

        // prepare ui look&feel and load ui properties
        try {
            SwingUtil.initNimbusLoookAndFeel();
        } catch (Exception e) {
            // could not find nimbus look-and-feel
            if (log.isWarnEnabled()) {
                log.warn("Failed to init nimbus look and feel");
            }
        }

        // share handler
        context.add(this);

        // construct main UI
        ui = new MascMainUI(context);

        // display
        ui.setVisible(true);

        // Init swingSession
        SwingSession swingSession =
                new SwingSession(config.getUIConfigFile(), false);

        // keep it in session
        applicationContext.setSwingSession(swingSession);

        // Add this frame
        swingSession.add(ui);
        swingSession.save();

        // share help broker
        //applicationContext.setMascHelpBroker(ui.getBroker());

        return ui;
    }

    public void initTabUI(MascTabs mascTabs) {
        InputDesignEditor inputDesignEditor = mascTabs.getInputDesignEditor();
        InputDesignEditorHandler inputDesignEditorHandler = inputDesignEditor.getHandler();
        
        // dispaly message before import
        inputDesignEditorHandler.addScenarioImportListener(new ImportScenarioListener() {
            @Override
            public void beforeImportScenario() {
                JOptionPane.showMessageDialog(ui, _("masc.importThreshold.warning"),
                        _("masc.importThreshold.warning.title"), JOptionPane.WARNING_MESSAGE);
            }
            
            @Override
            public void afterImportScenario() {
                
            }
        });

        // disable column ordering
        mascTabs.getInputDesignEditor().getInputDesignTable().getTableHeader().setReorderingAllowed(false);
        mascTabs.getInputDesignEditor().getInputDesignTable().setSortable(false);
    }

    public boolean closeApplication(boolean exit) {
        boolean result = askToSave(
                _("masc.ask.save.beforeQuit"),
                _("masc.ask.save.beforeQuit.title"),
                true);

        if (result) {

            try {
                // save swing disposition
                getApplicationContext().getSwingSession().save();

                // dispose ui
                ui.dispose();

                // save config
                getConfig().save();

            } finally {

                if (exit) {
                    System.exit(0);
                }
            }
        }
        return result;
    }

    public void importFile() {

        importFile(false);
    }

    public void importThresholdFile() {

        JOptionPane.showMessageDialog(ui, _("masc.importThreshold.warning"), _("masc.importThreshold.warning.title"), JOptionPane.WARNING_MESSAGE);

        importFile(true);
    }

    protected void importFile(boolean threshold) {

        // ask masc file
        File selectedFile = MascUIHelper.openFile(
                ui,
                MascUIHelper.MascFileType.CSV.getOpenTitle(),
                MascUIHelper.MascFileType.CSV);

        if (selectedFile != null) {
            MascModel model = getService(DexiEvalInvokerService.class).importOptionsValues(getCurrentMascModel(), selectedFile, threshold);
            getApplicationContext().setMascModel(model);
            loadModel(getMascUIModel());
            getMascUIModel().setModelChanged(true);
        }
    }

    public void openFile() {

        if (askToSave(
                _("masc.ask.save.beforeOpen"),
                _("masc.ask.save.beforeOpen.title"),
                true)) {

            // ask masc file
            File selectedFile = MascUIHelper.openFile(
                    ui,
                    MascUIHelper.MascFileType.MASC.getOpenTitle(),
                    MascUIHelper.MascFileType.MASC,
                    MascUIHelper.MascFileType.DEXI);

            if (selectedFile != null) {

                if (selectedFile.getName().matches("^.*\\.masc$")) {

                    // masc file
                    reOpenFiles(selectedFile);
                } else {

                    // dexi file
                    doImportFile(selectedFile);
                }
            }
        }
    }

    public boolean changeTab(final MascTabs tabUI, int newIndex, int oldIndex) {

        if (oldIndex == 1 || oldIndex == 2) {
            convertInputDesignModelToMascModel(tabUI);
        }

        // second tab is input design editor
        if (newIndex == 1 || newIndex == 2) {

            // get input design model
            try {
                InputDesign oldInputDesign = tabUI.getInputDesignEditor().getInputDesign();

                if (oldInputDesign != null) {
                    // remove listeners
                    oldInputDesign.removeInputDesignFactorListener(inputDesignFactorListener);
                    oldInputDesign.removeInputDesignScenarioListener(inputDesignScenarioListener);
                }

                InputDesign inputDesign = getService(MascModelService.class).convertMascModelToMexicoInputDesignModel(getCurrentMascModel());

                // add listener to listen factor values changes
                inputDesign.addInputDesignFactorListener(inputDesignFactorListener);

                // add listener to listen option values changes
                inputDesign.addInputDesignScenarioListener(inputDesignScenarioListener);

                tabUI.getInputDesignEditor().setInputDesign(inputDesign);
            } catch (Exception eee) {
                String errorMsg = "Failed to convert model to input design";
                log.error(errorMsg, eee);
                MascUIHelper.showError(ui, errorMsg, eee);
            }

        // call evaluation for both tab 3 to 5 only if evaluation is dirty
        } else if ((newIndex == 3 || newIndex == 4 || newIndex == 5) && getMascUIModel().isEvaluationDirty()) {
            
            // display wiat cursor (long operation)
            tabUI.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    // get evaluated masc model
                    EvaluatedCriteriaEditorHandler handler =
                            tabUI.getEvaluatedCriteriaEditor().getHandler();
                    handler.loadEvaluatedModel();
                    
                    // restore default cursor
                    tabUI.setCursor(null);
                }
            });
        }

        // for threshold, verify that all lines are filled before quit
        if (oldIndex == 1) {
            boolean isEditing = tabUI.getThresholdEditor().getModel().isEditing();
            if (isEditing) {
                JOptionPane.showMessageDialog(
                        this.ui,
                        _("masc.threshold.valueLeft.msg"),
                        _("masc.threshold.valueLeft.title"),
                        JOptionPane.ERROR_MESSAGE);
            }
            return !isEditing;
        }

        // refresh graph chart on change
        if (newIndex == 4) {
            ChartUI chartUI = tabUI.getChartUI();
            chartUI.getHandler().showUI();
        }

        // refresh synoptic chart on change
        if (newIndex == 5) {
            SynopticUI synopticUI = tabUI.getSynopticUI();
            synopticUI.getHandler().showUI();
        }

        return true;
    }

    public File exportFile() {
        MascModel mascModel = getCurrentMascModel();

        File fileToSave = MascUIHelper.saveAsFile(
                ui,
                MascUIHelper.MascFileType.DEXI,
                getMascUIModel().getMascFileName());

        if (fileToSave != null) {
            try {
                // save file
                getService(MascModelService.class).exportToDEXiModel(mascModel, fileToSave, false);
            } catch (Exception eee) {
                String errorMsg = "Failed to export masc model to dexi file '" + fileToSave.getName() + "'";
                log.error(errorMsg, eee);
                MascUIHelper.showError(ui, errorMsg, eee);
            }
        }
        return fileToSave;
    }

    public void saveFile() {
        File mascFile = getMascFile();
        if (mascFile == null) {
            mascFile = MascUIHelper.saveAsFile(
                    ui,
                    MascUIHelper.MascFileType.MASC,
                    getMascUIModel().getMascFileName());
        }
        if (mascFile != null) {

            MascTabs tabsUI = getApplicationContext().getMascTabsUI();
            if (tabsUI.getSelectionModel().getSelectedIndex() == 2) {
                convertInputDesignModelToMascModel(tabsUI);
            }

            try {

                // save file
                getService(MascUIModelService.class).saveModel(getMascUIModel(), mascFile);

                // its now current file
                getApplicationContext().setMascFile(mascFile);

                // mark as no modified
                getMascUIModel().setModelChanged(false);

                // change frame title
                ui.setTitle(
                        _("masc.ui.title.fileOpen", mascFile.getPath()));

            } catch (Exception eee) {
                String errorMsg = "Failed to save masc model to file '" + mascFile.getName() + "'";
                log.error(errorMsg, eee);
                MascUIHelper.showError(ui, errorMsg, eee);
            }
        }
    }

    public void print() {

        // get active tab
        MascTabs mascTabsUI = getApplicationContext().getMascTabsUI();
        int activeTab = mascTabsUI.getSelectionModel().getSelectedIndex();

        // fill model
        ReportModel reportModel = getMascUIModel().getReportModel();
        reportModel.setShowAs(false);
        reportModel.setShowDescription(false);
        reportModel.setShowGraphic(false);
        reportModel.setShowOptions(false);
        reportModel.setShowSynoptic(false);
        reportModel.setShowThresholds(false);
        reportModel.setShowTree(false);
        ReportModel existingReportModel = getMascUIModel().getReportModel();
        reportModel.setSynoptic(existingReportModel.getSynoptic());
        reportModel.setGraphic(existingReportModel.getGraphic());

        boolean removePageBreak = true;
        boolean landscape = false;
        switch (activeTab) {
            case 0:
                reportModel.setShowDescription(true);
                reportModel.setShowTree(true);
                removePageBreak = false;
                break;
            case 1:
                reportModel.setShowThresholds(true);
                break;
            case 2:
            case 3:
                reportModel.setShowOptions(true);
                landscape = true;
                break;
            case 4:
                reportModel.setShowGraphic(true);
                break;
            case 5:
                reportModel.setShowSynoptic(true);
                landscape = true;
                break;
            case 6:
                reportModel.setShowAs(true);
                break;
            default:
                return;
        }

        try {
            reportModel.setMascModel(getCurrentMascModel());

            // write pdf file
            File reportFile = File.createTempFile("report", ".pdf");
            reportModel.setReportFile(reportFile);

            getService(ReportGeneratorService.class).generateReport(
                    getMascUIModel().getMascFileName(),
                    reportModel, removePageBreak, landscape);

            Desktop.getDesktop().open(reportFile);

        } catch (Exception eee) {
            log.error("Failed to print pdf", eee);
        }
    }

    public void saveAsFile() {
        File mascFile = getMascFile();
        getApplicationContext().setMascFile(null);
        saveFile();

        // if not saved, retrieve old selected file
        if (getMascFile() == null) {
            getApplicationContext().setMascFile(mascFile);
        }
    }

    public void openDexi() {
        File dexiApp = getConfig().getDexiExecutableFile();
        File dexiFile = getMascUIModel().getDexiFile();
        if (dexiFile == null) {
            dexiFile = exportFile();
        } else {

            // if is mexico editor
            MascTabs tabsUI = getApplicationContext().getMascTabsUI();
            if (tabsUI.getSelectionModel().getSelectedIndex() == 2) {
                convertInputDesignModelToMascModel(tabsUI);
            }

            // save file
            try {
                getService(MascModelService.class).exportToDEXiModel(
                        getCurrentMascModel(), dexiFile, false);
            } catch (Exception eee) {
                log.error("Failed to export model", eee);
            }
        }

        // if dexi is not configured
        if (!MascUtil.isExecutableFile(dexiApp)) {
            int returnVal1 = JOptionPane.showConfirmDialog(
                    ui,
                    _("masc.dexi.install"),
                    _("masc.dexi.install.title"),
                    JOptionPane.YES_NO_OPTION);
            if (returnVal1 == JOptionPane.YES_OPTION) {

                try {
                    getService(DexiInvokerService.class).invokeDexiInstall();
                } catch (Exception eee) {
                    String errorMsg = "Failed to run dexi installation";
                    log.error(errorMsg, eee);
                    MascUIHelper.showError(ui, errorMsg, eee);
                }
            }
            int returnVal2 = JOptionPane.showConfirmDialog(
                    ui,
                    _("masc.dexi.notConfigured"),
                    _("masc.dexi.notConfigured.title"),
                    JOptionPane.YES_NO_OPTION);
            if (returnVal2 == JOptionPane.YES_OPTION) {
                dexiApp = MascUIHelper.openFile(
                        ui,
                        MascUIHelper.MascFileType.DEXI_APP);
                if (dexiApp == null) {
                    return;
                }
                getConfig().setDexiExecutableFile(dexiApp);
            } else {
                return;
            }
        }
        try {
            // open dexi without file
            getService(DexiInvokerService.class).invokeDexi(dexiFile);
        } catch (Exception eee) {
            String errorMsg = "Failed to run dexi with no file";
            log.error(errorMsg, eee);
            MascUIHelper.showError(ui, errorMsg, eee);
        }
    }

    public void generateReport() {

        // ask what to report
        ReportUI reportUI = new ReportUI();
        reportUI.setModel(getMascUIModel().getReportModel());
        reportUI.pack();
        reportUI.setVisible(true);
    }

    public void changeLanguage(Locale locale) {

        if (closeApplication(false)) {

            // sauvegarde de la nouvelle locale
            getConfig().setLocale(locale);

            // reload masc
            reloadMasc();
        }
    }

    public void showConfig() {

        ConfigUIHelper helper =
                new ConfigUIHelper(getConfig().getApplicationConfig());

        helper.registerCallBack("ui",
                                n_("masc.action.reload.ui"),
                                SwingUtil.createActionIcon("reload-ui"),
                                new Runnable() {

                                    @Override
                                    public void run() {
                                        reloadMasc();
                                    }
                                }).
                registerCallBack("log",
                                 n_("masc.action.reload.logAppender"),
                                 SwingUtil.createActionIcon("reload-log"),
                                 new Runnable() {

                                     @Override
                                     public void run() {
                                         if (log.isInfoEnabled()) {
                                             log.info("will reload log appender");
                                         }

                                         // reinit jaxx logger
                                         JAXXLog4jUI.init(getConfig().getLogLevel(), getConfig().getLogPatternLayout());
                                     }
                                 });

        // categorie applications
        helper.addCategory(n_("masc.config.category.applications"),
                           n_("masc.config.category.applications.description")).
                addOption(MascConfigOption.DEXI_APP_PATH).
                addOption(MascConfigOption.DEXI_EVAL_APP_PATH).
                addOption(MascConfigOption.R_APP_PATH);

        // others
        helper.addCategory(n_("masc.config.category.other"),
                           n_("masc.config.category.other.description")).
                addOption(MascConfigOption.LOCALE).
                setOptionCallBack("ui").
                addOption(MascConfigOption.SITE_URL).
                addOption(MascConfigOption.DATA_DIRECTORY).
                addOption(MascConfigOption.TEMPLATE_DIRECTORY).
                addOption(MascConfigOption.UI_CONFIG_FILE).
                setOptionCallBack("ui").
                addOption(MascConfigOption.LOG_LEVEL).
                setOptionCallBack("log").
                addOption(MascConfigOption.LOG_PATTERN_LAYOUT).
                setOptionCallBack("log");

        helper.buildUI(ui, "masc.config.category.other");

        helper.displayUI(ui, false);
    }

    public void gotoSite() {

        MascConfig config = getConfig();

        URL siteURL = config.getSiteUrl();

        if (log.isInfoEnabled()) {
            log.info(_("masc.message.goto.site", siteURL));
        }

        Desktop desktop = Desktop.getDesktop();

        if (Desktop.isDesktopSupported() &&
            desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(siteURL.toURI());
            } catch (Exception ex) {
                String errorMsg = "Failed to open '" + siteURL + "' in browser";
                log.error(errorMsg, ex);
                MascUIHelper.showError(ui, errorMsg, ex);
            }
        }
    }

    public void showAbout() {

        ApplicationConfig config = getConfig().getApplicationConfig();

        String iconPath = config.getOption("masc.application.icon.path");
        String licensePath = config.getOption("masc.application.license.path");
        String thirdPartyPath = config.getOption("masc.application.third-party.path");

        AboutPanel about = new AboutPanel();
        about.setTitle(_("masc.title.about"));
        about.setAboutText(_("masc.about.message"));
        about.setBottomText(getConfig().getCopyrightText());
        about.setIconPath(iconPath);
        about.setLicenseFile(licensePath);
        about.setThirdpartyFile(thirdPartyPath);
        about.buildTopPanel();
        about.init();
        about.showInDialog(ui, true);
    }

    public void showLogs() {

        JAXXLog4jUI log4jUI = new JAXXLog4jUI();
        String title = _("masc.title.showLog");
        log4jUI.setTitle(title);
        log4jUI.showInDialog(ui, false);
    }

    /*@Override
    public void showHelp(JAXXContext context,
                         JAXXHelpBroker broker,
                         String helpId) {

        if (helpId == null) {
            helpId = broker.getDefaultID();
        }

        if (log.isDebugEnabled()) {
            log.debug("show help " + helpId);
        }

        // display help
        Properties properties = getApplicationContext().getHelpMapping();

        URI resolvedUri = null;
        try {
            String value = (String) properties.get(helpId);

            if (StringUtils.isEmpty(value)) {

                // default we use anchor with helpId
                value = "#" + helpId;
            }

            resolvedUri = getConfig().getSiteUrl().toURI().resolve(value);

            if (log.isDebugEnabled()) {
                log.debug("Opening uri " + resolvedUri);
            }

            Desktop.getDesktop().browse(resolvedUri);
        } catch (Exception eee) {
            log.error("Failed to open uri '" + resolvedUri + "'", eee);
        }
    }*/

    public void showHelpSite() {

        URI resolvedUri = null;
        try {
            resolvedUri = getConfig().getHelpUrl().toURI();

            if (log.isDebugEnabled()) {
                log.debug("Opening uri " + resolvedUri);
            }

            Desktop.getDesktop().browse(resolvedUri);
        } catch (Exception eee) {
            log.error("Failed to open uri '" + resolvedUri + "'", eee);
        }
    }

    protected boolean askToSave(String title,
                                String msg,
                                boolean showNoButton) {
        // other, must ask to save if options have changed
        if (!getMascUIModel().isModelChanged()) {
            return true;
        } else {

            int options = JOptionPane.YES_NO_OPTION;
            if (showNoButton) {
                options = JOptionPane.YES_NO_CANCEL_OPTION;
            }
            int answer = JOptionPane.showConfirmDialog(
                    ui,
                    msg,
                    title,
                    options,
                    JOptionPane.QUESTION_MESSAGE);

            if (answer == JOptionPane.YES_OPTION) {
                try {
                    // realy save
                    saveFile();

                    return true;
                } catch (Exception eee) {
                    String errorMsg = "Failed to save options modifications";
                    log.error(errorMsg, eee);
                    MascUIHelper.showError(ui, errorMsg, eee);
                }
            } else if (answer == JOptionPane.NO_OPTION) {

                return true;
            }
            return false;
        }
    }

    protected void reloadMasc() {

        // init ui
        JAXXInitialContext jaxxContext = new JAXXInitialContext();

        initUI(jaxxContext);

        File mascFile = getMascFile();

        if (mascFile != null) {
            reOpenFiles(mascFile);
        }
    }

    protected void refreshFooter() {

        MascModel model = getMascUIModel().getMascModel();

        Integer[] criteriaCount = MascUtil.countCriterias(model);

        int optionCount = 0;

        MascTabs tabsUI = getApplicationContext().getMascTabsUI();
        if (tabsUI != null &&
            tabsUI.getSelectionModel().getSelectedIndex() == 2) {

            // use input design to count options
            InputDesign inputDesign =
                    tabsUI.getInputDesignEditor().getInputDesign();

            Collection<Scenario> options = inputDesign.getScenario();
            if (CollectionUtils.isNotEmpty(options)) {
                optionCount = options.size();
            }
        } else {

            // use masc model to count options
            Collection<Option> options = model.getOption();
            if (CollectionUtils.isNotEmpty(options)) {
                optionCount = options.size();
            }
        }


        String footer = _("masc.footer",
                          criteriaCount[0],
                          criteriaCount[1],
                          criteriaCount[2],
                          criteriaCount[3],
                          optionCount);

        ui.getMessagePanel().setText(footer);
    }

    protected void reOpenFiles(File mascFile) {

        MascApplicationContext mascApplicationContext = getApplicationContext();
        MascUIModel oldUIModel = mascApplicationContext.getMascUIModel();

        unregisterModel(oldUIModel);

        // parse file
        try {
            MascUIModel mascUIModel =
                    getService(MascUIModelService.class).loadModel(mascFile);

            mascApplicationContext.setMascUIModel(mascUIModel);

            // keep selected file
            mascApplicationContext.setMascFile(mascFile);

            loadModel(mascUIModel);

            // set modified false
            mascUIModel.setModelChanged(false);

        } catch (Exception eee) {
            String errorMsg = "Failed to parse file '" + mascFile.getName() + "'";
            log.error(errorMsg, eee);
            MascUIHelper.showError(ui, errorMsg, eee);
        }

    }

    protected void doImportFile(File selectedFile) {

        if (selectedFile != null) {
            try {

                MascApplicationContext applicationContext = getApplicationContext();

                MascUIModel oldUIModel = applicationContext.getMascUIModel();

                unregisterModel(oldUIModel);

                MascUIModel mascUIModel = new MascUIModel();

                applicationContext.setMascUIModel(mascUIModel);

                // keep selected file
                applicationContext.getMascUIModel().setDexiFile(selectedFile);

                MascModel mascModel = getService(MascModelService.class).loadModelFromDexiFile(selectedFile);

                applicationContext.setMascModel(mascModel);

                loadModel(mascUIModel);
                getMascUIModel().setModelChanged(true);


            } catch (Exception eee) {
                String errorMsg = "Failed to import dexi file";
                log.error(errorMsg, eee);
                MascUIHelper.showError(ui, errorMsg, eee);
            }
        }
    }

    protected void loadModel(MascUIModel mascUIModel) {

        MascModel mascModel = mascUIModel.getMascModel();

        // create a new data provider and register it to application context
        getApplicationContext().getDataProvider().setMascModel(mascModel);

        // bind model to footer
        mascModel.addPropertyChangeListener(footerListener);
        mascUIModel.addPropertyChangeListener(MascUIModel.PROPERTY_MODEL_CHANGED, footerListener);

        mascUIModel.setTransformCriteriaToBasic(
                ui.getMenuCanTransformCriteria().isSelected());

        ui.setModel(mascUIModel);

        refreshFooter();

        // open all tabs
        JPanel contentPane = ui.getContent();
        contentPane.removeAll();

        ui.setContextValue(new MascFactorValueEditorFactory());
        MascTabs mainTabbedPane = new MascTabs(ui);

        // keep it in context
        getApplicationContext().setMascTabsUI(mainTabbedPane);

        contentPane.add(mainTabbedPane, BorderLayout.CENTER);

        contentPane.revalidate();

        // TODO sletellier : move this to JMexico
        MascUIHelper.makeTableTabFocusable(
                mainTabbedPane.getInputDesignEditor().getInputDesignTable());
    }

    protected void unregisterModel(MascUIModel mascUIModel) {

        if (mascUIModel != null) {
            MascModel mascModel = mascUIModel.getMascModel();

            if (mascModel != null) {
                mascModel.removePropertyChangeListener(footerListener);
            }
        }
    }

    protected void convertInputDesignModelToMascModel(MascTabs ui) {
        try {
            InputDesign inputDesign =
                    ui.getInputDesignEditor().getInputDesign();
            MascModelService service =
                    getService(MascModelService.class);
            ChartModel chartModel = getMascUIModel().getChartModel();
            MascModel mascModel = service.convertInputDesignModelToMascModel(
                    getCurrentMascModel(), chartModel, inputDesign);
            getMascUIModel().setMascModel(mascModel);
        } catch (Exception eee) {
            String errorMsg = "Failed to convert input design to model";
            log.error(errorMsg, eee);
            MascUIHelper.showError(ui, errorMsg, eee);
        }
    }
}
