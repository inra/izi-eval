/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.tree.criteria;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.inra.masc.utils.MascUtil;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionValue;
import jaxx.runtime.swing.nav.NavDataProvider;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class CriteriaDataProvider extends AbstractSerializableBean implements NavDataProvider {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_STRUCTURE_CHANGED = "structureChanged";

    protected MascModel mascModel;

    protected transient Map<String, Criteria> criterias;

    protected transient Map<Option, Multimap<String, OptionValue>> evaluatedCriterias;

    public static CriteriaDataProvider newInstance() {
        return new CriteriaDataProvider();
    }

    public Criteria getCriteria(String uuid) {
        return criterias.get(uuid);
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Collection<Criteria> getRootCriterias() {
        return mascModel.getCriteria();
    }

    public List<Criteria> getChildrenForTree(String parentId) {


        // root
        Collection<Criteria> children;
        if (parentId == null) {
            children = getRootCriterias();
        } else {
            Criteria parentCriteria = getCriteria(parentId);

            // dont display children for reference nodes
            children = parentCriteria.getChild(false);
        }
        List<Criteria> result;
        if (children == null) {
            result = null;
        } else {
            result = Lists.newArrayList(children);
        }
        return result;
    }

    public Collection<OptionValue> getValues(String uuid, int column) {
        Option option = mascModel.getOption(column - 1);
        Collection<OptionValue> result;
        if (option == null) {
            result = null;
        } else {
            result = getValues(uuid, option);
        }
        return result;
    }

    public Collection<OptionValue> getValues(String uuid, Option option) {
        Preconditions.checkNotNull(evaluatedCriterias);
        Multimap<String, OptionValue> stringOptionValueMultimap = evaluatedCriterias.get(option);

        Collection<OptionValue> result;
        if (stringOptionValueMultimap == null) {
            result = null;
        } else {
            Criteria criteria = getCriteria(uuid);
            while (criteria.isReference()) {
                criteria = criteria.getChild(0);
            }
            result = stringOptionValueMultimap.get(criteria.getUuid());
        }
        return result;
    }

    public void setMascModel(MascModel mascModel) {
        this.mascModel = mascModel;

        reloadCaches();
    }

    public void reloadCaches() {

        // extract all criteria
        criterias = MascUtil.extractAllCriteriasInMapUUIDKey(mascModel);

        // extract all criteria value
        evaluatedCriterias = MascUtil.extractAllOptionValues(mascModel);

        firePropertyChange(PROPERTY_STRUCTURE_CHANGED, false, true);
    }

    public List<String> getOptionNames() {
        Collection<Option> options = mascModel.getOption();
        List<String> optionNames = Lists.newArrayList();

        for (Option option : options) {
            optionNames.add(option.getName());
        }
        return optionNames;
    }
}
