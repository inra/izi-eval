/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.render.chart;

import static org.nuiton.i18n.I18n._;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeModel;

import jaxx.runtime.SwingUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.JFreeChart;

import com.google.common.collect.Lists;

import fr.inra.masc.charts.ChartModel;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.services.ImageGeneratorService;
import fr.inra.masc.ui.MascHandler;
import fr.inra.masc.ui.content.render.CheckBoxListSelectionModel;
import fr.inra.masc.ui.tree.criteria.CriteriaTreeHelper;
import fr.inra.masc.ui.widget.ImageGenerator;
import fr.inra.masc.utils.MascUtil;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @author tchemit &lt;chemit@codelutin.com&gt;
 */
public class ChartUIHandler extends MascHandler {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ChartUIHandler.class);

    /**
     * UI managed by this handler.
     *
     * @since 1.5
     */
    private final ChartUI ui;

    /**
     * To generate image from model.
     *
     * @since 1.6
     */
    private final ChartImageGenerator imageGenerator;

    /** Flag to prevent event infinite loop. */
    protected boolean adjustingEvent;

    public ChartUIHandler(ChartUI ui) {
        this.ui = ui;
        this.imageGenerator = new ChartImageGenerator();
    }

    public void initUI() {

        JTree tree = ui.getNavigation();
        CriteriaTreeHelper treeHelper = ui.getTreeHelper();

        treeHelper.setUI(tree, true);

        // listen when data provider structure changed (will recreate tree model)
        listenCriteriaModelChanged(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                loadCriteriaModel();
            }
        });

        // add listener one option selection
        ui.getOptionsSelection().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                // prevent infinite loop
                if (!adjustingEvent) {
                    ChartModel chartModel = getMascUIModel().getChartModel();
                    Object[] selectedValues = ui.getOptionsSelection().getSelectedValues();
                    List selectedOptions = Lists.newArrayList(selectedValues);
                    chartModel.setSelectedOptions(selectedOptions);
                }
            }
        });

        // show chart
        loadCriteriaModel();
        loadOptionModel();
        showUI();

        // Creation of selection listener to open ui when tree selection change
        tree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                showUI();
            }
        });

        // reload model when defaut options change
        ChartModel chartModel = getMascUIModel().getChartModel();
        chartModel.addPropertyChangeListener(ChartModel.PROPERTY_SELECTED_OPTIONS, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                // select all options
                adjustingEvent = true;
                loadOptionModel();
                showUI();
                adjustingEvent = false;
            }
        });
    }

    public void showUI() {

        ChartModel chartModel = getMascUIModel().getChartModel();

        List<Criteria> selectedCriterias = ui.getTreeHelper().getSelectedCriteria();
        chartModel.setSelectedCriterias(selectedCriterias);
        chartModel.setShowClasses(true);

        if (log.isDebugEnabled()) {
            log.debug("Will regenerate image");
        }

        // make sure to reset image generator internal states
        imageGenerator.reset();
        ui.getImagePanel().getModel().setZoomFactor(1);
        ui.getImagePanel().setImageGenerator(imageGenerator);
        BufferedImage image = imageGenerator.getImage(1);
        ui.setChartWithNullValues(imageGenerator.isContainsStars());

        // keep generated image for report
        getMascUIModel().getReportModel().setGraphic(image);
    }

    protected void loadCriteriaModel() {

        JTree tree = ui.getNavigation();
        CriteriaTreeHelper treeHelper = ui.getTreeHelper();

        TreeModel model = treeHelper.createModel();
        tree.setModel(model);

        // expend tree by default
        SwingUtil.expandTree(tree);

        // select all criterias
        ChartModel chartModel = getMascUIModel().getChartModel();
        List<Criteria> selectedCriterias = chartModel.getSelectedCriterias();
        if (selectedCriterias == null) {
            treeHelper.selectFirstNode();
        } else {
            for (Criteria criteria : selectedCriterias) {
                treeHelper.selectNode(criteria.getUuid());
            }
        }
    }
    
    protected void loadOptionModel() {
        // select all options
        ChartModel chartModel = getMascUIModel().getChartModel();
        List<Option> selectedOptions = chartModel.getSelectedOptions();
        List<Option> options = getCurrentMascModel().getOption();
        CheckBoxListSelectionModel optionListModel = ui.getOptionListModel();
        if (selectedOptions == null) {
            selectedOptions = options;
            // FIXME NPE if there is not fire event by next addSelectionInterval (empty option case)
            chartModel.setSelectedOptions(new ArrayList<Option>());
            // / NPE
        }
        optionListModel.clearSelection();
        for (Option option : selectedOptions) {
            int i = options.indexOf(option);
            optionListModel.addSelectionInterval(i, i);
        }
    }

    private class ChartImageGenerator implements ImageGenerator {

        private Dimension actualDimension;

        private BufferedImage image;

        /** Si le graphique contient des etoiles, il n'est pas valide. */
        private boolean containsStars;

        private final ImageGeneratorService service;

        private ChartImageGenerator() {
            this.service = getService(ImageGeneratorService.class);
        }

        @Override
        public BufferedImage getImage(double scaleFactor) {

            ChartModel model = getMascUIModel().getChartModel();

            if (image == null) {

                // generate it

                Dimension parentDimension =
                        ui.getImagePanel().getHandler().getParentDimension();

                int width = Math.max((int) parentDimension.getWidth(), ImageGeneratorService.IMAGE_GRAPH_WIDTH);
                int height = Math.max((int) parentDimension.getHeight(), ImageGeneratorService.IMAGE_GRAPH_HEIGHT);

                JFreeChart chart = service.getChart(model);

                if (chart != null) {
                    image = chart.createBufferedImage(width, height);

                    actualDimension = new Dimension(image.getWidth(),
                                                    image.getHeight());
                }
            }

            BufferedImage result = image;

            if (image != null && scaleFactor != 1) {

                // rescale image to result

                AffineTransformOp op = new AffineTransformOp(
                        AffineTransform.getScaleInstance(scaleFactor, scaleFactor),
                        AffineTransformOp.TYPE_BILINEAR
                );
                result = op.filter(image, null);
            }
            
            // check if chart model contains stars (to display message to user)
            containsStars = false;
            List<Option> selectedOptions = model.getSelectedOptions();
            if (selectedOptions != null) {
                for (Option option : selectedOptions) {
                    List<Criteria> selectedCriterias = model.getSelectedCriterias();
                    for (Criteria selectedCriteria : selectedCriterias) {
                        Collection<OptionValue> optionValuesForCriteria = MascUtil.getOptionValuesForCriteria(option, selectedCriteria);
                        for (OptionValue optionValue : optionValuesForCriteria) {
                            if (optionValue != null && optionValue.getValue() == null) {
                                containsStars = true;
                                break;
                            }
                        }
                    }
                }
            }

            return result;
        }

        @Override
        public Dimension getActualDimension() {
            return actualDimension;
        }

        public void reset() {
            image = null;
            actualDimension = null;
        }

        public boolean isContainsStars() {
            return containsStars;
        } 
    }

    /**
     * Gestion du click souris sur l'arbre, affichage d'un menu contextuel.
     * 
     * @param event mouse event
     */
    public void navigationTreeMouseClicked(MouseEvent event) {

        // clic droit
        if (event.getButton() == MouseEvent.BUTTON3) {

            JPopupMenu menu = new JPopupMenu();

            // select all button
            JMenuItem selectAllButton = new JMenuItem(_("masc.common.selectAll"));
            selectAllButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ui.getTreeHelper().selectAllNodes();
                }
            });
            menu.add(selectAllButton);

            // unselect all button
            JMenuItem unSelectAllButton = new JMenuItem(_("masc.common.unSelectAll"));
            unSelectAllButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ui.getTreeHelper().unSelectAllNodes();
                }
            });
            menu.add(unSelectAllButton);

            menu.show((Component)event.getSource(), event.getX(), event.getY());
        }
    }
}
