package fr.inra.masc.ui.content.editor.threshold;
/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

import java.io.File;

/**
 * Exort threshold model.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public class ImportThresholdModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    protected File importFile;

    public File getImportFile() {
        return importFile;
    }

    public void setImportFile(File importFile) {
        Object oldValue = this.importFile;
        this.importFile = importFile;
        firePropertyChange("importFile", oldValue, importFile);
    }

    public void copyTo(ImportThresholdModel model) {
        model.setImportFile(getImportFile());
    }
}
