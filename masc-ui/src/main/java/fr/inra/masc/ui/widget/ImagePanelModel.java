package fr.inra.masc.ui.widget;

/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.image.BufferedImage;

import org.jdesktop.beans.AbstractSerializableBean;

/**
 * Model of a {@link ImagePanel}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public class ImagePanelModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_IMAGE = "image";

    public static final String PROPERTY_ZOOM = "zoomFactor";

    public static final String PROPERTY_SVG = "svg";

    public static final String PROPERTY_BACKGROUND_COLOR = "backgroundColor";

    protected transient BufferedImage image;

    protected double zoomFactor = 1d;

    protected boolean svg = false;

    protected Color backgroundColor = Color.WHITE;

    public BufferedImage getImage() {
        return image;
    }

    public double getZoomFactor() {
        return zoomFactor;
    }

    public void setZoomFactor(double zoomFactor) {
        Object oldValue = this.zoomFactor;
        this.zoomFactor = zoomFactor;
        firePropertyChange(PROPERTY_ZOOM, oldValue, zoomFactor);
    }

    public boolean isSvg() {
        return svg;
    }

    public void setSvg(boolean svg) {
        boolean oldValue = isSvg();
        this.svg = svg;
        firePropertyChange(PROPERTY_SVG, oldValue, svg);
    }

    public void incrementsZoom() {
        setZoomFactor(zoomFactor + 0.1);
    }

    public void decrementsZoom() {
        setZoomFactor(zoomFactor - 0.1);
    }

    public void setImage(BufferedImage image) {
        this.image = image;
        firePropertyChange(PROPERTY_IMAGE, null, image);
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        // fixme do not handle null value
        if (backgroundColor != null) {
            Color oldValue = getBackgroundColor();
            this.backgroundColor = backgroundColor;
            firePropertyChange(PROPERTY_BACKGROUND_COLOR, oldValue, backgroundColor);
        }
    }
}
