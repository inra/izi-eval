/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.render.r;

import com.google.common.collect.Lists;
import fr.inra.masc.R_SCRIPT;

import javax.swing.AbstractListModel;
import java.util.List;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.5
 */
public class OutputScriptFilesModel extends AbstractListModel {

    private static final long serialVersionUID = 1L;

    protected R_SCRIPT script;

    protected List<String> outputFiles = Lists.newArrayList();

    public void setScript(R_SCRIPT script) {
        this.script = script;

        if (script != null) {
            // get current model
            String[] resultTextFiles = script.getResultTextFileNames();
            String[] resultImageFiles = script.getResultImageFileNames();
            outputFiles = Lists.newArrayList();
            if (resultTextFiles != null) {
                outputFiles.addAll(Lists.newArrayList(resultTextFiles));
            }
            if (resultImageFiles != null) {
                outputFiles.addAll(Lists.newArrayList(resultImageFiles));
            }
            fireContentsChanged(this, 0, getSize());
        }
    }

    public int indexOf(String outputSelected) {
        return outputFiles.indexOf(outputSelected);
    }

    @Override
    public int getSize() {
        return outputFiles.size();
    }

    @Override
    public Object getElementAt(int index) {
        String result;
        if (outputFiles.size() < index) {
            result = null;
        } else {
            result = outputFiles.get(index);
        }
        return result;
    }
}
