package fr.inra.masc.ui.content.editor.criteria.editor;

/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n._;

import java.awt.BorderLayout;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.tree.TreePath;

import jaxx.runtime.swing.Table;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.model.EditableCriteriaImpl;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Option;
import fr.inra.masc.model.OptionValue;
import fr.inra.masc.model.OptionValueImpl;
import fr.inra.masc.ui.MascHandler;
import fr.inra.masc.ui.content.editor.criteria.MascModelEditorHandler;
import fr.inra.masc.ui.tree.criteria.CriteriaDataProvider;
import fr.inra.masc.ui.tree.criteria.CriteriaNode;
import fr.inra.masc.ui.tree.criteria.CriteriaTreeHelper;
import fr.inra.masc.utils.CriteriaVisitor;
import fr.inra.masc.utils.MascUtil;

/**
 * Handler of the {@link ComputableCriteriaEditor} ui.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.7
 */
public class ComputableCriteriaEditorHandler extends MascHandler {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ComputableCriteriaEditorHandler.class);

    private final ComputableCriteriaEditor ui;

    private MascModelEditorHandler editorHandler;

    public ComputableCriteriaEditorHandler(ComputableCriteriaEditor ui) {
        this.ui = ui;
        this.editorHandler = ui.getContextValue(MascModelEditorHandler.class);
    }

    public void initUI() {
        ui.getTransformToBasicCriteriaPanel().remove(ui.getTransformComputedToBasicCriteriaPanel());
        ui.getTransformToBasicCriteriaPanel().remove(ui.getTransformLinkedToBasicCriteriaPanel());
    }
// TODO poussin laisse visible le panneau transformer
    public boolean updateTransformToBasciCriteriaPanel(Criteria criteria,
                                                       boolean canModifyCriteria) {

        JPanel container = ui.getTransformToBasicCriteriaPanel();
        CriteriaTreeHelper treeHelper = editorHandler.getTreeHelper();

        boolean showComputed = canModifyCriteria;
        {
            if (showComputed) {

                // can not transform any root
                CriteriaNode node = treeHelper.getNode(criteria);
                if (node == null || node.getParent().isRoot()) {
                    showComputed = false;
                }
            }

            Table content = ui.getTransformComputedToBasicCriteriaPanel();
            if (showComputed) {
                container.add(content, BorderLayout.NORTH);
            } else {
                container.remove(content);
            }
        }

        boolean showLinked = true;
        {

            // can transform only root
            CriteriaNode node = treeHelper.getNode(criteria);
            if (node == null || !node.getParent().isRoot()) {
                showLinked = false;
            } else {

                // but not first root (not a linked tree root)
                CriteriaNode rootNode = treeHelper.getRootNode();
                if (node.equals(rootNode.getChildAt(0))) {
                    showLinked = false;
                }
            }
            Table content = ui.getTransformLinkedToBasicCriteriaPanel();
            if (showLinked) {
                container.add(content, BorderLayout.NORTH);
            } else {
                container.remove(content);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("show TransformComputed/LinkedToBasicCriteria? " +
                      showComputed + "/" + showLinked);
        }
        return showComputed || showLinked;
    }


    /* FIXME echatellier 20130517 unused
     * Transforme un noeud de l'arbre en un arbre satellite, et remplace le
     * noeud devenu satellite par un noeud reference vers se satellite
     *
     * Cette method est fonctionnel, mais aucun bouton n'est encore en place
     * sur l'interface pour l'utiliser. Elle a ete cree car toutes les methodes
     * necessaire a sa mise en place existe et que pour les tests, elle a ete
     * utilisee pour verifier le bon fonctionnement.
     * @param criteria
     *
    public void transformNodeToRefenceOfSatellite(Criteria criteria) {

        // re-ask user to confirm
        String criteriaName = criteria.getName();
        String msg =
                _ ("mask.info.confirm.transformNodeToRefenceOfSatellite",
                        criteriaName);

        int answer = JOptionPane.showConfirmDialog(
                ui,
                msg,
                _ ("masc.ask.confirm.transformNodeToRefenceOfSatellite.title"),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        if (answer != JOptionPane.YES_OPTION) {
            if (log.isInfoEnabled()) {
                log.info("ABORT by user to transform node " + criteriaName
                        + " to reference");
            }
        } else {

            // do the action
            if (log.isInfoEnabled()) {
                log.info("Will transform node " + criteriaName
                        + " to reference");
            }

            CriteriaDataProvider dataProvider =
                    getApplicationContext().getDataProvider();

            CriteriaTreeHelper treeHelper = editorHandler.getTreeHelper();

            // selected node
            CriteriaNode selectedNode = treeHelper.getSelectedNode();

            // parent of selected node
            CriteriaNode selectedNodeParent = selectedNode.getParent();

            // parent of selected criteria
            Criteria parentCriteria =
                    dataProvider.getCriteria(selectedNodeParent.getId());

            if (parentCriteria != null) {
                // seuls les noeuds fils peuvent etre convertis pas les roots

                // get a fresh copy of childs
                List<Criteria> childs = Lists.newArrayList(parentCriteria.getChild());

                // get index of criteria to replace
                int criteriaIndex = childs.indexOf(criteria);

                if (log.isDebugEnabled()) {
                    log.debug("Selected node:            " + selectedNode.getId());
                    log.debug("Selected criteria:        " + criteria.getName());
                }

                MascModel mascModel = getCurrentMascModel();

                Criteria ref = createSatAndUseAsReference(
                        mascModel, treeHelper, dataProvider, parentCriteria, criteria);

                // reset model to clean caches (some other models will recreate
                // their model while listening the data provider
                // structureChanged property)
                dataProvider.reloadCaches();

                CriteriaNode node = treeHelper.getNode(ref);
                treeHelper.selectNode(node);

                getMascUIModel().setModelChanged(true);
            }
        }
    }*/

    /**
     * Converti un noeud (arbre satellite, feuille, ... en un critere simple).
     * Si le noeud contient des elements utilises comme reference alors ils sont
     * eux meme converti en arbre satellite.
     * @param criteria le critere a modifier
     */
    public void transformNodeToBasic(Criteria criteria) {

        // re-ask user to confirm
        String criteriaName = criteria.getName();
        String msg =
                _("mask.info.confirm.transformNodeToBasic",
                        criteriaName);

        int answer = JOptionPane.showConfirmDialog(
                ui,
                msg,
                _("masc.ask.confirm.transformNodeToBasic.title"),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        if (answer != JOptionPane.YES_OPTION) {
            if (log.isInfoEnabled()) {
                log.info("ABORT by user to transform node " + criteriaName
                        + " to basic");
            }
        } else {

            // do the action
            if (log.isInfoEnabled()) {
                log.info("Will transform node " + criteriaName
                        + " to basic");
            }

            CriteriaDataProvider dataProvider =
                    getApplicationContext().getDataProvider();

            CriteriaTreeHelper treeHelper = editorHandler.getTreeHelper();

            // selected node
            CriteriaNode selectedNode = treeHelper.getSelectedNode();

            // parent of selected node
            CriteriaNode selectedNodeParent = selectedNode.getParent();

            // parent of selected criteria
            Criteria parentCriteria =
                    dataProvider.getCriteria(selectedNodeParent.getId());

            if (log.isDebugEnabled()) {
                log.debug("Selected node:            " + selectedNode.getId());
                log.debug("Selected criteria:        " + criteria.getName());
            }

            MascModel mascModel = getCurrentMascModel();




            // pour tous les criteres du modele
            // key: critere cible, value reference vers la cible
            Multimap<Criteria, Criteria> targetCriterias =
                    MascUtil.extractAllTargetCriterias(mascModel);
            
            // create new criteria
            EditableCriteria newCriteria = convertToEditable(criteria);

            int criteriaIndex;

            if (parentCriteria == null) {
                // recuperation de la position actuel de l'arbre a modifier en basic
                List<Criteria> childs = Lists.newArrayList(mascModel.getCriteria());
                criteriaIndex = childs.indexOf(criteria);

                // on commence par creer les nouveaux arbres satellite avec les
                // noeuds qui servent de reference a d'autre, ils sont ajoute a
                // la fin
                criteria.accept(new DeepConvertTargetNodeToSatCriteria(criteria, mascModel,
                        treeHelper, targetCriterias, dataProvider), null);

                criteria.accept(new DeepDeleteCriteria(mascModel,
                        treeHelper, targetCriterias, dataProvider), null);

                // on recupere a nouveau la liste car elle a change (ajout/suppression de noeud)
                childs = Lists.newArrayList(mascModel.getCriteria());
                // add at same place the new criteria
                childs.add(criteriaIndex, newCriteria);
 
                mascModel.setCriteria(childs);

            } else {
                // conversion d'un noeud de l'arbre

                // get a fresh copy of childs
                List<Criteria> childs = Lists.newArrayList(parentCriteria.getChild());

                // get index of criteria to replace
                criteriaIndex = childs.indexOf(criteria);

                // remove old criteria from his parent
                childs.remove(criteria);

                // add at same place the new criteria
                childs.add(criteriaIndex, newCriteria);

                // reset childs
                parentCriteria.setChild(childs);

                criteria.accept(new DeepConvertTargetNodeToSatCriteria(criteria, mascModel,
                        treeHelper, targetCriterias, dataProvider), parentCriteria);

                criteria.accept(new DeepDeleteCriteria(mascModel,
                        treeHelper, targetCriterias, dataProvider), parentCriteria);
            }


            Collection<Option> options = mascModel.getOption();
            if (CollectionUtils.isNotEmpty(options)) {

                // switch all criteria option to this new criteria
                for (Option option : options) {
                    Collection<OptionValue> optionValues = option.getOptionValue();
                    if (optionValues == null) {
                        optionValues = Lists.newArrayList();
                    }
                    for (OptionValue optionValue : optionValues) {
                        if (optionValue.getCriteria() == criteria) {
                            optionValue.setCriteria(newCriteria);
                            optionValue.setValue(null);
                        }
                    }
                }
            }

            // reset model to clean caches (some other models will recreate
            // their model while listening the data provider
            // structureChanged property)
            dataProvider.reloadCaches();

            //add new criteria as node as same position as before and select it
            CriteriaNode newNode = treeHelper.addNode(
                    selectedNodeParent, newCriteria, criteriaIndex);

            treeHelper.selectNode(newNode);
            getMascUIModel().setModelChanged(true);
        }
    }

    /**
     * Converti un critere en un critere editable
     * @param toConvert
     * @return
     */
    protected static EditableCriteria convertToEditable(Criteria toConvert) {

        EditableCriteria newCriteria = new EditableCriteriaImpl();
        newCriteria.setUuid(toConvert.getUuid());
        newCriteria.setName(toConvert.getName());
        newCriteria.setScale(toConvert.getScale());
        newCriteria.setDescription(toConvert.getDescription());

        return newCriteria;
    }

    /**
     * Converti un noeud en arbre satellite, et le remplace a son ancienne
     * position en reference vers ce nouvel arbre
     *
     * @param parentCriteria le noeud parent ou null si c'est un arbre satellite
     * @param toConvert le noeud a convertir
     * @return
     */
    protected static Criteria createSatAndUseAsReference(
            MascModel mascModel,
            CriteriaTreeHelper treeHelper, CriteriaDataProvider dataProvider,
            Criteria parentCriteria, Criteria toConvert) {
        Criteria result = null;
        if (parentCriteria != null) {
            List<Criteria> children = Lists.newArrayList(parentCriteria.getChild());
            int index = children.indexOf(toConvert);
            children.remove(toConvert);

            CriteriaNode oldNode = treeHelper.getNode(toConvert);
            if (oldNode != null) {
                treeHelper.removeNode(oldNode);
            }

            // on le transforme en arbre satellite
            mascModel.addCriteria(toConvert);

            result = new EditableCriteriaImpl();
            result.setName(toConvert.getName());
            result.setScale(toConvert.getScale());
            result.setReference(true);
            result.setChild(Lists.newArrayList(toConvert));
            result.setDescription(toConvert.getDescription());

            children.add(index, result);
            parentCriteria.setChild(children);

            dataProvider.reloadCaches();
            treeHelper.addNode(treeHelper.getRootNode(), toConvert, mascModel.sizeCriteria() - 1);
            
            CriteriaNode selectedNodeParent = treeHelper.getNode(parentCriteria);
            treeHelper.addNode(selectedNodeParent, result, index);
        }

        return result;
    }

    /**
     * Supprime un noeud et tous ses fils. Si un noeud fils etait la derniere
     * reference vers un arbre satellite, l'arbre satelitte est lui aussi supprime
     */
    protected static class DeepDeleteCriteria implements CriteriaVisitor {
        protected Multimap<Criteria, Criteria> targetCriterias;
        protected MascModel mascModel;
        protected CriteriaTreeHelper treeHelper;
        protected CriteriaDataProvider dataProvider;

        public DeepDeleteCriteria(MascModel mascModel,
                CriteriaTreeHelper treeHelper,
                Multimap<Criteria, Criteria> targetCriterias,
                CriteriaDataProvider dataProvider) {

            this.targetCriterias = targetCriterias;
            this.mascModel = mascModel;
            this.treeHelper = treeHelper;
            this.dataProvider = dataProvider;
        }

        /**
         * Retourne l'arbre satellite auquel appartient le critere.
         * 
         * @param mascModel
         * @param c
         * @return l'arbre satellite ou null si le noeud n'appartient pas a
         * un arbre satellite
         */
        protected Criteria getRootSat(MascModel mascModel, Criteria c) {
            Criteria result = null;
            
            int nbSat = mascModel.sizeCriteria();
            if (nbSat > 1) {
                Set<Criteria> satellite = new HashSet<Criteria>(mascModel.getCriteria().subList(1, nbSat));

                CriteriaNode node = treeHelper.getNode(c);
                TreePath tp = treeHelper.getPath(node);
                CriteriaNode rootNode = (CriteriaNode)tp.getPathComponent(1);
                if (rootNode != null) {
                    c = dataProvider.getCriteria(rootNode.getId());
                    if (satellite.contains(c)) {
                        result = c;
                    }
                }
            }

            return result;
        }

        @Override
        public boolean visit(Criteria parentCriteria, Criteria criteria) {

            if (criteria.isReference()) {

                Criteria target = criteria;
                while (target.isReference()) {
                    target = target.getChild(0);
                }

                // si target est un arbre satellite qui ne sert plus de reference, alors
                // on le supprime

                // on recherche l'arbre satellite auquel il appartient.
                target = getRootSat(mascModel, target);
                if (target != null) {
                    targetCriterias.get(target).remove(criteria);

                    if (CollectionUtils.isEmpty(targetCriterias.get(target))) {

                        // check if children doesn't have reference too
                        DeepDeleteCriteriaVerifier deepDeleteCriteriaVerifier = new DeepDeleteCriteriaVerifier(targetCriterias);
                        target.accept(deepDeleteCriteriaVerifier, null);

                        if (deepDeleteCriteriaVerifier.isCanDelete()) {
                            removeCriteriaFormModel(target);
                            CriteriaNode targetNode = treeHelper.getNode(target);
                            if (targetNode != null) {
                                treeHelper.removeNode(targetNode);
                            }
                        }
                    }
                }
            }

            CriteriaNode criteriaNode = treeHelper.getNode(criteria);
            if (criteriaNode != null) {
                treeHelper.removeNode(criteriaNode);
            }

            if (parentCriteria != null) {
                parentCriteria.removeChild(criteria);
            } else {
                removeCriteriaFormModel(criteria);
                
            }

            return true;
        }

        @Override
        public boolean resolvRefs() {
            return false;
        }

        /**
         * Remove criteria and associated object from model.
         */
        protected void removeCriteriaFormModel(Criteria criteria) {
            mascModel.removeCriteria(criteria);
            
            // remove option value used by deleted criteria
            List<Option> options = mascModel.getOption();
            for (Option option : options) {
                Collection<OptionValue> optionValues = option.getOptionValue();
                Iterator<OptionValue> itOptionValue = optionValues.iterator();
                while (itOptionValue.hasNext()) {
                    OptionValue optionValue = itOptionValue.next();
                    if (optionValue.getCriteria().equals(criteria)) {
                        itOptionValue.remove();
                    }
                }
            }
        }
    }

    protected static class DeepDeleteCriteriaVerifier implements CriteriaVisitor {

        protected Multimap<Criteria, Criteria> targetCriterias;
        protected boolean canDelete = true;

        public DeepDeleteCriteriaVerifier(Multimap<Criteria, Criteria> targetCriterias) {
            this.targetCriterias = targetCriterias;
        }

        @Override
        public boolean visit(Criteria parentCriteria, Criteria criteria) {
            if (!canDelete) {
                return false;
            }
            Collection<Criteria> targetCriterias = this.targetCriterias.get(criteria);
            if (targetCriterias != null) {

                canDelete = targetCriterias.isEmpty();
            }
            return true;
        }

        @Override
        public boolean resolvRefs() {
            return true;
        }

        public boolean isCanDelete() {
            return canDelete;
        }
    }

    /**
     * Permet de convertir les noeuds fils servant de reference en
     * un nouvel arbre satellite
     */
    protected static class DeepConvertTargetNodeToSatCriteria implements CriteriaVisitor {
        protected Criteria firstNode;
        protected Multimap<Criteria, Criteria> targetCriterias;
        protected MascModel mascModel;
        protected CriteriaTreeHelper treeHelper;
        protected CriteriaDataProvider dataProvider;

        public DeepConvertTargetNodeToSatCriteria(
                Criteria firstNode, MascModel mascModel,
                CriteriaTreeHelper treeHelper,
                Multimap<Criteria, Criteria> targetCriterias,
                CriteriaDataProvider dataProvider) {

            this.firstNode = firstNode;
            this.targetCriterias = targetCriterias;
            this.mascModel = mascModel;
            this.treeHelper = treeHelper;
            this.dataProvider = dataProvider;

        }

        @Override
        public boolean visit(Criteria parentCriteria, Criteria criteria) {
            // indique si l'on doit continuer la visite des noeud fils
            boolean result = true;

            // on ne travail que sur les noeud fils (du firstNode) qui sont la cible d'une reference
            if (firstNode != criteria && !targetCriterias.get(criteria).isEmpty()) {
                // on cree un nouveau noeud qui reference cet arbre satellite
                Criteria ref = createSatAndUseAsReference(
                        mascModel, treeHelper, dataProvider, parentCriteria, criteria);

                // on ne parcours plus les fils, car ils servent dans le nouvel arbre satellite
                result = false;
            }

            return result;
        }

        @Override
        public boolean resolvRefs() {
            return false;
        }
    }

}
