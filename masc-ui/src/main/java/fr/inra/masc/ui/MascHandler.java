/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui;

import fr.inra.masc.MascApplicationContext;
import fr.inra.masc.MascConfig;
import fr.inra.masc.MascUIModel;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.RScriptModel;
import fr.inra.masc.services.MascService;
import fr.inra.masc.services.MascServiceContext;
import fr.inra.masc.services.MascServiceFactory;
import fr.inra.masc.ui.tree.criteria.CriteriaDataProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

import static org.nuiton.i18n.I18n._;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public abstract class MascHandler {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MascHandler.class);

    public MascApplicationContext getApplicationContext() {
        return MascApplicationContext.getContext();
    }

    public MascConfig getConfig() {
        return getApplicationContext().getConfig();
    }

    public MascUIModel getMascUIModel() {
        return getApplicationContext().getMascUIModel();
    }

    public MascModel getCurrentMascModel() {
        return getApplicationContext().getMascModel();
    }

    public File getMascFile() {
        return getApplicationContext().getMascUIModel().getMascFile();
    }

    public MascServiceFactory getServiceFactory() {
        return getApplicationContext().getServiceFactory();
    }

    protected <E extends MascService> E getService(Class<E> clazz) {
        MascServiceContext context = new MascServiceContext(getConfig(), getServiceFactory());
        return getServiceFactory().newService(clazz, context);
    }

    protected void listenCriteriaModelChanged(PropertyChangeListener listener) {

        CriteriaDataProvider dataProvider =
                getApplicationContext().getDataProvider();
        dataProvider.removePropertyChangeListener(
                CriteriaDataProvider.PROPERTY_STRUCTURE_CHANGED, listener);
        dataProvider.addPropertyChangeListener(
                CriteriaDataProvider.PROPERTY_STRUCTURE_CHANGED, listener);
    }

    protected void showLogs(Component ui, RScriptModel script, int messageType) {

        File tmpDirectory = getConfig().getTmpDirectory();
        try {
            String logs = script.getLogs(tmpDirectory);

            JOptionPane.showMessageDialog(ui,
                                          logs,
                                          _("masc.r.showLogs.title"),
                                          messageType);
        } catch (IOException eee) {
            log.error("Failed to read logs", eee);
        }
    }

}
