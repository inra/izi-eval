package fr.inra.masc.ui.widget;

/*
 * #%L
 * Masc :: Swing UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.masc.ui.MascUIHelper;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 */
public class SimpleImagePanelHandler extends ImagePanelHandler<SimpleImagePanel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SimpleImagePanelHandler.class);

    protected ImageGenerator imageGenerator;

    public SimpleImagePanelHandler(SimpleImagePanel ui) {
        super(ui);
    }

    @Override
    public JComponent getViewer() {

        // fix image scrolling
        JScrollPane contentPane = new JScrollPane();
        contentPane.setViewportView(new ImageComponent(contentPane, ui.getModel()));

        contentPane.setBorder(null);
        contentPane.getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);

        return contentPane;
    }

    public void setImageGenerator(ImageGenerator imageGenerator) {
        this.imageGenerator = imageGenerator;
        generateImage(ui.getModel().getZoomFactor());
    }

    @Override
    public void exportAsFile() {
        File file = MascUIHelper.saveAsFile(
                ui, MascUIHelper.MascFileType.PNG, "izi-eval");

        if (file != null) {
            try {
                ImageIO.write(ui.getModel().getImage(), "png", file);
            } catch (IOException eee) {
                String errorMsg = "Failed to save image: " + file.getPath();
                log.error(errorMsg, eee);
                MascUIHelper.showError(ui, errorMsg, eee);
            }
        }
    }

    @Override
    public Dimension getActualDimension() {
        return imageGenerator.getActualDimension();
    }

    @Override
    public void generateImage(Double factor) {
        if (log.isDebugEnabled()) {
            log.debug("Regenerate image with scale factor: " + factor);
        }
        BufferedImage image = imageGenerator.getImage(factor);
        ui.getModel().setImage(image);
    }
}
