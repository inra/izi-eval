package fr.inra.masc.ui.content.editor.threshold;
/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.jdesktop.swingx.util.PaintUtils.blend;
import static org.nuiton.i18n.I18n._;

import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

import jaxx.runtime.swing.editor.FileEditor;
import jaxx.runtime.swing.renderer.DecoratorProviderTableCellRenderer;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import fr.inra.masc.MascUIModel;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.ScaleValue;
import fr.inra.masc.services.ThresholdService;
import fr.inra.masc.ui.MascDecoratorProvider;
import fr.inra.masc.ui.MascHandler;
import fr.inra.masc.ui.MascUIHelper;
import fr.inra.masc.utils.MascUtil;

/**
 * Handler of {@link ThresholdEditor}.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.5
 */
public class ThresholdEditorHandler extends MascHandler {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ThresholdEditorHandler.class);


    public static final Color BACKGROUND = new Color(212, 212, 212);

    /** UI. */
    private final ThresholdEditor ui;

    public ThresholdEditorHandler(ThresholdEditor ui) {
        this.ui = ui;
    }

    public void initUI() {

        // table renderer
        MascDecoratorProvider mascDecoratorProvider =
                getApplicationContext().getDecoratorProvider();

        JXTable table = ui.getThresholdTable();

        table.setDefaultRenderer(
                Criteria.class,
                new DecoratorProviderTableCellRenderer(mascDecoratorProvider));

        table.setDefaultRenderer(
                Object.class,
                new ThresholdTableCellRenderer(ui.getModel()));

        table.addHighlighter(new ColorHighlighter(HighlightPredicate.ALWAYS,
                                                  Color.WHITE,
                                                  Color.BLACK,
                                                  UIManager.getColor("nimbusSelectionBackground"),
                                                  Color.BLACK) {
            @Override
            protected void applyBackground(Component renderer, ComponentAdapter adapter) {

                Color color;
                if (adapter.isSelected()) {

                    // default selected color
                    color = getSelectedBackground();
                } else {
                    if (adapter.isEditable()) {

                        // default editable color
                        color = getBackground();
                    } else {

                        // non editable color (gray)
                        color = BACKGROUND;
                    }
                }

                renderer.setBackground(blend(renderer.getBackground(), color));
            }

            @Override
            protected void applyForeground(Component renderer, ComponentAdapter adapter) {
                Color color;
                if (adapter.isEditable()) {

                    // default editable color
                    color = getForeground();
                } else {
                    Object value = adapter.getValue();
                    if (value instanceof ScaleValue) {

                        // use scale value color
                        ScaleValue scaleValue = (ScaleValue) value;
                        color = MascUtil.getColorForScaleValue(scaleValue);
                    } else {

                        // default non editable color (black)
                        color = getForeground();
                    }
                }

                renderer.setForeground(blend(renderer.getForeground(), color));
            }
        });

        MascUIHelper.makeTableTabFocusable(table);

        PropertyChangeListener listener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                reloadModel();
            }
        };

        // listen that masc model changed
        getMascUIModel().addPropertyChangeListener(MascUIModel.PROPERTY_MASC_MODEL, listener);

        // listen on dataProvider structure changed
        listenCriteriaModelChanged(listener);
    }

    public void importValues() {

        //ImportThresholdModel model = new ImportThresholdModel();

        //boolean accept = showImportUI(model);
        
        File importFile = MascUIHelper.openFile(
                ui, MascUIHelper.MascFileType.THRESHOLD.getOpenTitle(),
                MascUIHelper.MascFileType.THRESHOLD, MascUIHelper.MascFileType.THRESHOLDCSV);

        if (importFile != null) {

            // can import thresholds

            //File importFile = model.getImportFile();

            if (log.isInfoEnabled()) {
                log.info("Import threshold from " + importFile);
            }

            try {

                MascModel mascModel = getMascUIModel().getMascModel();

                // load thresholds in current model
                Set<String> unknownCriteriaNames =
                        getService(ThresholdService.class).loadThresholds(
                                importFile, mascModel);

                if (CollectionUtils.isNotEmpty(unknownCriteriaNames)) {

                    // some criteria are not found in model
                    StringBuilder sb = new StringBuilder();
                    for (String unknownFactor : unknownCriteriaNames) {
                        sb.append("\n'").append(unknownFactor).append('\'');
                    }
                    JOptionPane.showMessageDialog(
                            ui,
                            _("masc.threshold.warning.criteria.not.imported",
                              unknownCriteriaNames.size(), sb.toString()),
                            _("masc.title.threshold.import"),
                            JOptionPane.WARNING_MESSAGE);
                }
                // reload table model
                ui.getModel().updateModel(mascModel);

            } catch (Exception eee) {
                String errorMsg = "Failed to import threshold values";
                log.error(errorMsg);
                MascUIHelper.showError(ui, errorMsg, eee);
            }
        }
    }

    public void exportValues() {

        File exportFile = MascUIHelper.saveAsFile(
                ui, MascUIHelper.MascFileType.THRESHOLD.getSaveTitle(), "izi-eval",
                MascUIHelper.MascFileType.THRESHOLD, MascUIHelper.MascFileType.THRESHOLDCSV);

        //ExportThresholdModel model = new ExportThresholdModel();

        //boolean accept = showExportUI(model);

        if (exportFile != null) {

            // export thresholds

            //File exportFile = model.getExportFile(getThresholdExtension());

            if (log.isInfoEnabled()) {
                log.info("Export thresholds to file " + exportFile);
            }
            try {

                getService(ThresholdService.class).saveThresholds(
                        ui.getModel().getEditableCriterias(), exportFile
                );
            } catch (Exception eee) {
                String errorMsg = "Failed to export thresholds";
                log.error(errorMsg, eee);
                MascUIHelper.showError(ui, errorMsg, eee);
            }
        }
    }

    public static String getExportFilenameLabelInfo() {
        return _("masc.info.threshold.export.filename",
                 getThresholdExtension());
    }

    public static String getThresholdExtension() {
        return _("masc.config.threshold.extension");
    }

    /*protected boolean showExportUI(ExportThresholdModel model) {

        // show ui

        ExportThresholdPanel panel = new ExportThresholdPanel();
        panel.init(model);

        FileEditor fileEditor = panel.getExportDirectoryEditor();
        fileEditor.setDirectoryEnabled(true);
        fileEditor.setFileEnabled(false);

        int response = JOptionPane.showConfirmDialog(
                ui,
                panel,
                _("masc.title.threshold.export"),
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        boolean doIt = response == JOptionPane.OK_OPTION;

        if (doIt) {

            // user ask to perform operation

            //copy back model
            panel.getModel().copyTo(model);

            // validate model
            boolean valid = true;

            //check export directory is filled
            File exportDirectory = model.getExportDirectory();
            if (exportDirectory == null) {
                valid = false;
                JOptionPane.showMessageDialog(
                        ui,
                        _("masc.error.threshold.exportDirectory.required"),
                        _("masc.title.error"),
                        JOptionPane.ERROR_MESSAGE);

            }

            if (valid) {

                // check export filename  is filled
                String exportFilename = model.getExportFilename();

                if (StringUtils.isBlank(exportFilename)) {
                    valid = false;
                    JOptionPane.showMessageDialog(
                            ui,
                            _("masc.error.threshold.exportFilename.required"),
                            _("masc.title.error"),
                            JOptionPane.ERROR_MESSAGE);

                }
            }

            doIt = valid || showExportUI(model);
        }
        return doIt;
    }*/

    /*protected boolean showImportUI(ImportThresholdModel model) {

        // show ui

        ImportThresholdPanel panel = new ImportThresholdPanel();
        panel.init(model);

        FileEditor fileEditor = panel.getImportFileEditor();

        fileEditor.setAcceptAllFileFilterUsed(false);
        fileEditor.setExts(getThresholdExtension());
        fileEditor.setExtsDescription(
                _("masc.config.threshold.extension.description"));


        int response = JOptionPane.showConfirmDialog(
                ui,
                panel,
                _("masc.title.threshold.import"),
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        boolean doIt = response == JOptionPane.OK_OPTION;

        if (doIt) {

            // user ask to perform operation

            //copy back model
            panel.getModel().copyTo(model);

            // validate model
            boolean valid = true;

            // check import file filled
            File importFile = model.getImportFile();

            if (importFile == null) {
                valid = false;
                JOptionPane.showMessageDialog(
                        ui,
                        _("masc.error.threshold.importFile.required"),
                        _("masc.title.error"),
                        JOptionPane.ERROR_MESSAGE);
            }

            doIt = valid || showImportUI(model);
        }
        return doIt;
    }*/

    protected void reloadModel() {
        // reload model
        ui.getModel().updateModel(getCurrentMascModel());

    }

    /**
     * @author sletellier &lt;letellier@codelutin.com&gt;
     * @since 0.3
     */
    public static class ThresholdTableCellRenderer extends JTextField implements TableCellRenderer {


        private static final long serialVersionUID = 1L;

        public final static Border EMPTY_BORDER = BorderFactory.createEmptyBorder();

        protected final ThresholdTableModel model;

        public ThresholdTableCellRenderer(ThresholdTableModel model) {
            this.model = model;
            setOpaque(true);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            String text = StringUtils.EMPTY;

            if (value != null) {
                if (value instanceof ScaleValue) {
                    ScaleValue scaleValue = (ScaleValue) value;
                    text = scaleValue.getName();
                } else {
                    text = String.valueOf(value);
                }
            }

            setText(text);
            setBorder(EMPTY_BORDER);
            return this;
        }

    }
}
