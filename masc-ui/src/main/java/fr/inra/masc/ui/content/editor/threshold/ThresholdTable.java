package fr.inra.masc.ui.content.editor.threshold;

/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Inra, Codelutin, Eric Chatellier
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.JXTable;

import fr.inra.masc.model.Criteria;

/**
 * Surcharge de JXTable pour pouvoir surcharger getStringAt et faire fontionner la recherche.
 * 
 * @author Eric Chatellier
 */
public class ThresholdTable extends JXTable {

    /** serialVersionUID. */
    private static final long serialVersionUID = -4465612553398877675L;

    @Override
    public String getStringAt(int row, int column) {
        String result = null;
        
        // gestion seulement de la première colonne
        if (column == 0) {
            result = ((Criteria)getValueAt(row, column)).getName();
        } else {
            result = super.getStringAt(row, column);
        }
        return result;
    }

}
