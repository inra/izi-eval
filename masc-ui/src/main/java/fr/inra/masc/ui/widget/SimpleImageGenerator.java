package fr.inra.masc.ui.widget;

/*
 * #%L
 * Masc :: Swing UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Dimension;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

/**
 * SImple image generator which used the original image given in constructor.
 * 
 * Only scale will be compute here but not the image itself.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public class SimpleImageGenerator implements ImageGenerator {

    protected final BufferedImage image;

    Dimension actualDimension;

    public SimpleImageGenerator(BufferedImage image) {
        this.image = image;
        actualDimension = new Dimension(image.getWidth(), image.getHeight());
    }

    @Override
    public BufferedImage getImage(double scaleFactor) {

        BufferedImage result;

        if (1.0 == scaleFactor) {
            result = image;
        } else {
            AffineTransformOp op = new AffineTransformOp(
                    AffineTransform.getScaleInstance(scaleFactor, scaleFactor),
                    AffineTransformOp.TYPE_BILINEAR
            );
            result = op.filter(image, null);
        }
        return result;
    }

    @Override
    public Dimension getActualDimension() {
        return actualDimension;
    }
}
