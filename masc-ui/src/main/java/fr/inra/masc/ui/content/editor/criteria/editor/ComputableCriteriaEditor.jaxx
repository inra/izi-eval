<!--
  #%L
  Masc :: Swing UI
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<AbstractCriteriaEditor superGenericType='ComputableCriteria'>

  <import>
    fr.inra.masc.MascUIModel
    fr.inra.masc.MascApplicationContext
    fr.inra.masc.model.ComputableCriteria
    fr.inra.masc.model.ComputableCriteriaImpl

    static org.nuiton.i18n.I18n._
  </import>

  <ComputableCriteriaEditorHandler id='handler' constructorParams='this'/>

  <MascUIModel id='uiModel'
               initializer='MascApplicationContext.getContext().getMascUIModel()'/>

  <!-- model -->
  <CriteriaUIModel id='model' genericType='ComputableCriteria'
                   constructorParams='ComputableCriteriaImpl.class'/>

  <ComputableCriteria id='criteria'/>

  <WeightTableModel id='heightTableModel'/>

<script>
    <![CDATA[
private void $afterCompleteSetup() { getHandler().initUI(); }
    ]]>
  </script>

  <JPanel id='specificCriteriaEditor' layout='{new BorderLayout()}'>

    <JPanel id='weightTablePanel' constraints='BorderLayout.NORTH'
            layout='{new BorderLayout()}'>
      <JTable id='weightTable' constraints='BorderLayout.CENTER'/>
    </JPanel>

    <JPanel id='transformToBasicCriteriaPanel' layout='{new BorderLayout()}'
            constraints='BorderLayout.CENTER'>

      <Table id='transformComputedToBasicCriteriaPanel' weightx='1' fill='horizontal'>
          <row>
            <cell fill='horizontal' weightx='1'>
              <JLabel id='transformComputedToBasicCriteriaInfo'/>
            </cell>
          </row>
          <row>
            <cell fill='horizontal' weightx='1'>
              <JButton id='transformComputedToBasicCriteriaAction'
                       onActionPerformed='handler.transformNodeToBasic(getCriteria())'/>
            </cell>
          </row>
        </Table>

        <Table id='transformLinkedToBasicCriteriaPanel' weightx='1' fill='horizontal'>
          <row>
            <cell fill='horizontal' weightx='1'>
              <JLabel id='transformLinkedToBasicCriteriaInfo'/>
            </cell>
          </row>
          <row>
            <cell fill='horizontal' weightx='1'>
              <JButton id='transformLinkedToBasicCriteriaAction'
                       onActionPerformed='handler.transformNodeToBasic(getCriteria())'/>
            </cell>
          </row>
        </Table>

    </JPanel>
    <JPanel constraints='BorderLayout.SOUTH'/>
  </JPanel>

</AbstractCriteriaEditor>
