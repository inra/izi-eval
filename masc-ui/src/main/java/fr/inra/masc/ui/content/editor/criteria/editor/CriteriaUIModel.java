/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.editor.criteria.editor;

import fr.inra.masc.model.Criteria;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractSerializableBean;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class CriteriaUIModel<E extends Criteria> extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(CriteriaUIModel.class);

    public static final String PROPERTY_CRITERIA = "criteria";

    protected E criteria;

    public CriteriaUIModel(Class<? extends E> criteriaImplType) {

        try {
            // creating empty instance
            setCriteria(criteriaImplType.newInstance());
        } catch (Exception eee) {
            // never append
            log.error("Failed to create new instance of '" + criteriaImplType + "'");
        }
    }

    public E getCriteria() {
        return criteria;
    }

    public void setCriteria(E criteria) {
        Object oldValue = this.criteria;
        this.criteria = criteria;
        firePropertyChange(PROPERTY_CRITERIA, oldValue, criteria);
    }

}
