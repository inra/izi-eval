/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui;

import com.google.common.collect.Lists;
import fr.inra.masc.MascApplicationContext;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.model.ThresholdValue;
import java.awt.Component;
import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.filechooser.FileFilter;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.swing.JAXXWidgetUtil;
import jaxx.runtime.swing.renderer.DecoratorListCellRenderer;
import jaxx.runtime.swing.renderer.DecoratorProviderListCellRenderer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.SystemUtils;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.error.ErrorInfo;
import org.nuiton.util.decorator.Decorator;

import static org.nuiton.i18n.I18n._;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class MascUIHelper extends JAXXWidgetUtil {

    // TODO sletellier 20120208 : put selected directory in config
    static protected File currentOpenDirectory = null;
    static protected File currentSaveDirectory = null;

    public enum MascFileType {
        DEXI(_("masc.file.import.dialog"), _("masc.file.export.dialog"), _("masc.fileType.dexi"), ".dxi"),
        // for compatibility purpose, izi extension is used by default, but user can still open .masc files
        MASC(_("masc.file.open.dialog"), _("masc.file.saveAs.dialog"), _("masc.fileType.masc"), ".izi", ".masc"),
        DEXI_EVAL_APP(_("masc.file.app.dexiEval.dialog"), null, _("masc.fileType.app.dexiEval"), ".exe"),
        DEXI_APP(_("masc.file.app.dexi.dialog"), null, _("masc.fileType.app.dexi"), ".exe"),
        R_APP(_("masc.file.app.r.dialog"), null, _("masc.fileType.app.r"), ".exe"),
        PNG(_("masc.file.app.image.dialog"), null, _("masc.fileType.app.png"), ".png"),
        JPG(_("masc.file.app.image.dialog"), null, _("masc.fileType.app.jpg"), ".jpg", ".jpeg"),
        SVG(_("masc.file.app.image.svg"), null, _("masc.fileType.app.svg"), ".svg"),
        CSV(_("masc.file.app.import.csv"), null, _("masc.fileType.app.csv"), ".csv"),
        REPORT(null, _("masc.file.report.dialog"), _("masc.fileType.pdf"), ".pdf"),
        THRESHOLD(_("masc.file.app.threshold.open"), _("masc.file.app.threshold.save"), _("masc.fileType.threshold"), ".mthreshold"),
        THRESHOLDCSV(_("masc.file.app.threshold.open"), _("masc.file.app.threshold.save"), _("masc.fileType.threshold.csv"), ".csv");

        private String openTitle;

        private String saveTitle;

        private String desc;

        private String[] ext;

        MascFileType(String openTitle,
                     String saveTitle,
                     String desc,
                     String... ext) {
            this.openTitle = openTitle;
            this.saveTitle = saveTitle;
            this.desc = desc;
            this.ext = ext;
        }

        public String getOpenTitle() {
            return openTitle;
        }

        public String getSaveTitle() {
            return saveTitle;
        }

        public FileFilter getFileFilter() {
            return new MascFileFilter(ext, desc);
        }
    }

    protected static class MascFileFilter extends FileFilter {

        protected String[] ext;
        protected String desc;

        public MascFileFilter(String[] ext, String desc) {
            this.ext = ext;
            this.desc = desc;
        }

        @Override
        public boolean accept(File file) {
            boolean result = file.isDirectory();
            if (!result) {
                for (String singleExt : ext) {
                    if (file.getName().endsWith(singleExt)) {
                        result = true;
                    }
                }
            }
            return result;
        }

        @Override
        public String getDescription() {
            return desc;
        }

        public String[] getExt() {
            return ext;
        }
    }

    public static DecoratorProviderListCellRenderer newDecoratorProviderListCellRenderer() {
        return new DecoratorProviderListCellRenderer(MascApplicationContext.getContext().getDecoratorProvider());
    }

    public static DecoratorListCellRenderer newDecoratorListCellRenderer(Class<?> type) {
        MascDecoratorProvider decoratorProvider = MascApplicationContext.getContext().getDecoratorProvider();
        Decorator<?> decorator = decoratorProvider.getDecoratorByType(type);
        return new DecoratorListCellRenderer(decorator);
    }

    /*public static JAXXHelpBroker getBroker() {
        return MascApplicationContext.getContext().getMascHelpBroker();
    }*/

    public static boolean isCriteriaWithThreshold(EditableCriteria criteria) {
        return CollectionUtils.isNotEmpty(criteria.getValues());
    }

    public static String decorateValues(EditableCriteria criteria) {
        Collection<ThresholdValue> values = criteria.getValues();
        String result = "";
        if (CollectionUtils.isNotEmpty(values)) {
            StringBuilder bf = new StringBuilder();
            for (ThresholdValue value : values) {
                bf.append(",").append(getStringValue(value));
            }
            result = bf.substring(1);
        }
        return result;
    }

    /**
     * Convinient method to process more than one binding on a JAXX ui.
     * 
     * TODO Move this to JAXXUtil
     *
     * @param src      the ui to treate
     * @param bindings the list of binding to process.
     */
    public static void processDataBinding(JAXXObject src, Iterable<String> bindings) {
        for (String binding : bindings) {
            src.processDataBinding(binding);
        }
    }


    public static String getStringValue(ThresholdValue value) {
        StringBuilder builder = new StringBuilder();
        if (value != null) {

            switch (value.getSign()) {
                case DESC:
                    builder.append('>');
                    break;
                case ASC:
                    builder.append('<');
                    break;
            }
            if (value.isCanBeEqual()) {
                builder.append('=');
            }

            builder.append(value.getValue());
        }
        return builder.toString();
    }

    public static JLabel getVerticalLabel(String lbl) {
        return getRotatedLabel(lbl, 270);
    }

    public static JLabel getRotatedLabel(String lbl, double degres) {
        JXLabel result = new JXLabel(_(lbl));
        result.setTextRotation(Math.toRadians(degres));
        return result;
    }

    /**
     * Open file chooser filtered by ext and return selected file to open if one choosed
     *
     * @param ui           parent
     * @param mascFileType representation of masc file type
     * @return file to open if selected
     */
    public static File openFile(Component ui,
                                MascUIHelper.MascFileType mascFileType) {
        return openFile(ui, mascFileType.getOpenTitle(), mascFileType);
    }

    /**
     * Open file chooser filtered by ext and return selected file to open if one choosed
     *
     * @param ui           parent
     * @param title        title of popup
     * @param mascFileType representation of masc file type
     * @return file to open if selected
     */
    public static File openFile(Component ui,
                                String title,
                                MascUIHelper.MascFileType... mascFileType) {
        JFileChooser fileChooser = new JFileChooser(currentOpenDirectory);
        fileChooser.setDialogTitle(title);
        fileChooser.setAcceptAllFileFilterUsed(false);
        List<MascFileType> types = Lists.newArrayList(mascFileType);
        if (!SystemUtils.IS_OS_LINUX) {

            // on windows os, the default filter is the last one, so as
            // asked by client reverse order to have correct default filter
            Collections.reverse(types);
        }

        for (MascFileType fileType : types) {
            fileChooser.addChoosableFileFilter(fileType.getFileFilter());
        }
        int returnVal = fileChooser.showOpenDialog(ui);
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            // ask masc file
            File selectedFile = fileChooser.getSelectedFile();

            if (selectedFile != null) {
                currentOpenDirectory = selectedFile;
            }
            return selectedFile;
        }
        return null;
    }

    /**
     * Open file chooser filtered by ext and return selected file to save if one choosed
     *
     * @param ui           parent
     * @param mascFileType representation of masc file type
     * @return file to save if selected
     */
    public static File saveAsFile(Component ui,
                                  MascUIHelper.MascFileType mascFileType,
                                  String defaultFileName) {

        return saveAsFile(ui, mascFileType.getSaveTitle(), defaultFileName, mascFileType);
    }

    /**
     * Open file chooser filtered by ext and return selected file to save if one choosed
     *
     * @param ui           parent
     * @param mascFileType representation of masc file type
     * @return file to save if selected
     */
    public static File saveAsFile(Component ui,
                                  String title,
                                  String defaultFileName,
                                  MascUIHelper.MascFileType... mascFileType) {

        JFileChooser fileChooser = new JFileChooser(currentSaveDirectory);
        List<MascFileType> types = Lists.newArrayList(mascFileType);

        // [#2508] [graphique synoptique] : export d'images
        // dont keep all file option
        fileChooser.setAcceptAllFileFilterUsed(false);
        for (MascFileType fileType : types) {
            fileChooser.addChoosableFileFilter(fileType.getFileFilter());
        }

        fileChooser.setDialogTitle(title);
        File f = new File(defaultFileName);
        fileChooser.setSelectedFile(f);
        int returnVal = fileChooser.showSaveDialog(ui);
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            // ask masc file
            File selectedFile = fileChooser.getSelectedFile();

            if (selectedFile != null) {
                
                // get all allowed extension for selected filters
                String[] ext = ((MascFileFilter)fileChooser.getFileFilter()).getExt();
                boolean addExt = true;
                for (String aExt : ext) {
                    if (selectedFile.getName().endsWith(aExt)) {
                        addExt = false;
                    }
                }

                // add default extension if none is present
                if (addExt) {
                    selectedFile = new File(selectedFile.getPath() + ext[0]);
                }

                currentSaveDirectory = selectedFile;
            }
            return selectedFile;
        }
        return null;
    }

    /**
     * Display a user friendly error frame.
     *
     * @param message message for user
     * @param cause   exception cause
     */
    public static void showError(Component ui, String message, Throwable cause) {
        JXErrorPane pane = new JXErrorPane();
        ErrorInfo info = new ErrorInfo(_("masc.common.error"),
                _("masc.error.errorpane.htmlmessage", message), null, null,
                cause, null, null);
        pane.setErrorInfo(info);
        JXErrorPane.showDialog(ui, pane);
    }

}
