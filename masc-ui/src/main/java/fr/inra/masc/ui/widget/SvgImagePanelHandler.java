package fr.inra.masc.ui.widget;

/*
 * #%L
 * Masc :: Swing UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n._;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.AffineTransform;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.Date;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.JSVGScrollPane;
import org.apache.batik.swing.svg.GVTTreeBuilderAdapter;
import org.apache.batik.swing.svg.GVTTreeBuilderEvent;
import org.apache.batik.swing.svg.JSVGComponent;
import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.batik.transcoder.svg2svg.SVGTranscoder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.svg.SVGDocument;

import fr.inra.masc.ui.MascUIHelper;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 */
public class SvgImagePanelHandler extends ImagePanelHandler<SvgImagePanel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SvgImagePanelHandler.class);

    protected JSVGCanvas jsvgCanvas;

    protected JSVGScrollPane scrollPane;

    public SvgImagePanelHandler(SvgImagePanel ui) {
        super(ui);
    }

    @Override
    public JComponent getViewer() {
        ui.getModel().setSvg(true);
        jsvgCanvas = new JSVGCanvas();
        jsvgCanvas.setEnableZoomInteractor(false);
        jsvgCanvas.setEnableImageZoomInteractor(false);
        jsvgCanvas.setRecenterOnResize(true);
        jsvgCanvas.setVerifyInputWhenFocusTarget(false);
        jsvgCanvas.setDisableInteractions(true);
        jsvgCanvas.setEnableResetTransformInteractor(false);
        jsvgCanvas.setEnableRotateInteractor(false);
        jsvgCanvas.setEnablePanInteractor(false);
        jsvgCanvas.setDocumentState(JSVGComponent.ALWAYS_STATIC);

        ui.getModel().addPropertyChangeListener(ImagePanelModel.PROPERTY_BACKGROUND_COLOR, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                jsvgCanvas.setBackground((Color)evt.getNewValue());
            }
        });

        scrollPane = new JSVGScrollPane(jsvgCanvas);

        jsvgCanvas.addGVTTreeBuilderListener(new GVTTreeBuilderAdapter() {

            @Override
            public void gvtBuildCompleted(GVTTreeBuilderEvent e) {
                generateImage(ui.getModel().getZoomFactor());
            }
        });

        return scrollPane;
    }

    @Override
    public void zoomFit() {
        super.zoomActual();
    }

    public void setSVGDocument(SVGDocument doc) {

        Date start = new Date();

        if (log.isDebugEnabled()) {
            log.debug("[setSVGDocument start] width[" + jsvgCanvas.getWidth() + "] height[" + jsvgCanvas.getHeight() + "]");
        }

        jsvgCanvas.setSVGDocument(doc);

        if (log.isDebugEnabled()) {
            log.debug("[setSVGDocument end] width[" + jsvgCanvas.getWidth() + "] height[" + jsvgCanvas.getHeight() + "] in " + (new Date().getTime() - start.getTime() + "ms"));
        }
    }

    @Override
    public void exportAsFile() {

        File file = MascUIHelper.saveAsFile(
                ui, MascUIHelper.MascFileType.SVG.getSaveTitle(), "izi-eval",
                MascUIHelper.MascFileType.SVG, MascUIHelper.MascFileType.JPG, MascUIHelper.MascFileType.PNG);

        if (file != null) {
            try {
                Transcoder transcoder;
                TranscoderOutput transcoderOutput;
                String filenameLC = file.getName().toLowerCase();
                if (filenameLC.endsWith(".png")) {
                    transcoderOutput = new TranscoderOutput(new FileOutputStream(file));
                    transcoder = new PNGTranscoder();

                    // to handle memory leak
                    // 7680*4320 fails (but ok with 2048 xmx)
                    // 3840*2160 fails (but ok with 2048 xmx)
                    // 1920*1080 ok
                    transcoder.addTranscodingHint(PNGTranscoder.KEY_MAX_HEIGHT, new Float(4320));
                    transcoder.addTranscodingHint(PNGTranscoder.KEY_MAX_WIDTH, new Float(7680));

                } else if (filenameLC.endsWith(".jpg") || filenameLC.endsWith(".jpeg")) {
                    transcoderOutput = new TranscoderOutput(new FileOutputStream(file));
                    transcoder = new JPEGTranscoder();

                    // to handle memory leak
                    transcoder.addTranscodingHint(PNGTranscoder.KEY_MAX_HEIGHT, new Float(4320));
                    transcoder.addTranscodingHint(PNGTranscoder.KEY_MAX_WIDTH, new Float(7680));
                } else {
                    transcoderOutput = new TranscoderOutput(new FileWriter(file));
                    transcoder = new SVGTranscoder();
                }

                transcoder.transcode(new TranscoderInput(jsvgCanvas.getSVGDocument()), transcoderOutput);
            } catch (OutOfMemoryError eee) {
                JOptionPane.showMessageDialog(ui, _("masc.menu.image.action.exportMemoryError"),
                        _("masc.menu.image.action.exportToPng"), JOptionPane.ERROR_MESSAGE);
            } catch (Throwable eee) {
                String errorMsg = "Failed to save image: " + file.getPath();
                log.error(errorMsg, eee);
                MascUIHelper.showError(ui, errorMsg, eee);
            }
        }
    }

    @Override
    public void generateImage(Double scaleFactor) {

        AffineTransform at = new AffineTransform();
        at.setToScale(scaleFactor, scaleFactor);

        Date start = new Date();

        if (log.isDebugEnabled()) {
            log.debug("[generateImage before] width[" + jsvgCanvas.getWidth() + "] height[" + jsvgCanvas.getHeight() + "] factor[" + scaleFactor + "]");
        }

        jsvgCanvas.setRenderingTransform(at, true);

        if (log.isDebugEnabled()) {
            log.debug("[generateImage end] width[" + jsvgCanvas.getWidth() + "] height[" + jsvgCanvas.getHeight() + "] in " + (new Date().getTime() - start.getTime() + "ms"));
        }
    }

    @Override
    public Dimension getActualDimension() {
        return jsvgCanvas.getSize();
    }
}
