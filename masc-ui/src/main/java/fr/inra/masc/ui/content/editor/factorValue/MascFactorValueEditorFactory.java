/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.editor.factorValue;

import com.google.common.collect.Maps;
import fr.inra.masc.model.ScaleValue;
import fr.inra.masc.utils.MascUtil;
import fr.reseaumexico.editor.factorValue.FactorValueEditorFactory;
import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.Feature;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.renderer.DefaultListRenderer;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class MascFactorValueEditorFactory extends FactorValueEditorFactory {

    protected Map<Factor, FactorValueCellRenderer> rendererCache = Maps.newHashMap();

    @Override
    public FactorValueCellEditor getCellEditor(Factor factor, Object oldValue) {

        FactorValueCellEditor<?> factorValueCellEditor = getFactorValueCellEditor(factor);

        if (factorValueCellEditor == null) {

            List<Feature> features = (List<Feature>) factor.getFeature();
            if (CollectionUtils.isEmpty(features)) {

                // use default editor
                factorValueCellEditor = super.getCellEditor(factor, oldValue);
            } else {

                if (MascUtil.THRESHOLD_CRITERIA.equals(factor.getDomain().getName())) {
                    factorValueCellEditor = new ThreasholdValueInlineEditor();
                } else {
                    // for others
                    factorValueCellEditor = new OptionValueInlineEditor(features);
                }
            }
        }
        setFactorValueCellEditor(factor, factorValueCellEditor);
        factorValueCellEditor.setValue(oldValue);
        return factorValueCellEditor;
    }

    @Override
    public FactorValueCellRenderer getRenderedComponent(Factor factor, Object value) {
        FactorValueCellRenderer factorValueCellRenderer = rendererCache.get(factor);

        if (factorValueCellRenderer == null) {
            List<Feature> features = (List<Feature>) factor.getFeature();
            if (features != null && !features.isEmpty()) {

                // TODO sletellier 20120105 : use constant for MascModelService too
                if (MascUtil.THRESHOLD_CRITERIA.equals(factor.getDomain().getName())) {
                    factorValueCellRenderer = new ThreasholdValueInlineEditor();
                } else {
                    // for all
                    factorValueCellRenderer = new OptionValueInlineRenderer(factor);
                }
            } else {
                factorValueCellRenderer = super.getRenderedComponent(factor, value);
            }
        }

        rendererCache.put(factor, factorValueCellRenderer);
        factorValueCellRenderer.setValue(value);
        return factorValueCellRenderer;
    }

    @Override
    public void clearCache() {
        super.clearCache();
        rendererCache = Maps.newHashMap();
    }

    public static class OptionValueInlineEditor implements FactorValueCellEditor, ListCellRenderer {

        protected JComboBox component;

        protected ListCellRenderer delegate;

        public OptionValueInlineEditor(List<Feature> values) {

            values = new ArrayList<Feature>(values);
            values.add(0, null);
            component = new JComboBox(values.toArray());
            delegate = new DefaultListRenderer();
            component.setRenderer(this);

            Font font = component.getFont();

            // reduce font to enter in cell
//            font = font.deriveFont(Font.PLAIN);
//            font = font.deriveFont(10f);

            component.setFont(font);

        }

        @Override
        public Object getValue() {
            Object result = null;
            if (component.getSelectedIndex() > 0) {
                result = component.getSelectedIndex() - 1;
            }
            return result;
        }

        @Override
        public void setValue(Object value) {
            // select feature
            if (value == null) {
                component.setSelectedIndex(0);
            } else {
                Number aDouble = (Number) value;
                component.setSelectedIndex(aDouble.intValue() + 1);
            }
        }

        @Override
        public Component getComponent() {
            return component;
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            String text;
            Color color = Color.BLACK;
            if (value == null) {
                text = MascUtil.OPTION_VALUE_DEFAULT;
            } else {
                ScaleValue scaleValue = (ScaleValue) ((Feature) value).getValue();
                color = MascUtil.getColorForScaleValue(scaleValue);
                text = scaleValue.getName();
            }

            Component component = delegate.getListCellRendererComponent(list, text, index, isSelected, cellHasFocus);
            component.setForeground(color);
            return component;
        }
    }

    public static class OptionValueInlineRenderer implements FactorValueCellRenderer {

        protected JTextField component;

        protected List<Feature> values;

        protected Factor factor;

        public OptionValueInlineRenderer(Factor factor) {
            this.factor = factor;
            this.values = (List<Feature>) factor.getFeature();
            this.component = new JTextField();
            component.setBorder(null);
        }

        @Override
        public void setValue(Object value) {
            // select feature
            Number valueInt = (Number) value;

            component.setText(MascUtil.OPTION_VALUE_DEFAULT);
            if (valueInt != null && values.size() > valueInt.intValue()) {

                Feature feature = values.get(valueInt.intValue());
                ScaleValue scaleValue = (ScaleValue) feature.getValue();
                component.setText(scaleValue.getName());
                component.setForeground(MascUtil.getColorForScaleValue(scaleValue));
            }
        }

        @Override
        public Component getComponent() {
            return component;
        }
    }

    public static class ThreasholdValueInlineEditor implements FactorValueCellEditor, FactorValueCellRenderer {

        protected JFormattedTextField component;

        public ThreasholdValueInlineEditor() {
            // TODO poussin c'est le renderer, il doit aussi avoir l'editor a modifier
            NumberFormat numberInstance = NumberFormat.getNumberInstance(Locale.ENGLISH);
            this.component = new JFormattedTextField(numberInstance);
            component.setBorder(null);
        }

        @Override
        public Object getValue() {

            try {
                component.commitEdit();
            } catch (ParseException e1) {
                component.setValue(null);
            }

            // get inputText
            return component.getValue();
        }

        @Override
        public void setValue(Object value) {
            component.setValue(value);
        }

        @Override
        public Component getComponent() {

            return component;
        }
    }

}
