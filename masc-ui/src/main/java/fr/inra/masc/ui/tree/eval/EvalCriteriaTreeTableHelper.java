/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.tree.eval;

import fr.inra.masc.MascApplicationContext;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.ui.tree.criteria.CriteriaDataProvider;
import jaxx.runtime.swing.nav.treetable.NavTreeTableHelper;
import jaxx.runtime.swing.nav.treetable.NavTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableModel;

import java.util.Collection;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.1
 */
public class EvalCriteriaTreeTableHelper extends NavTreeTableHelper<EvalCriteriaNode> {

    public EvalCriteriaTreeTableHelper() {

        // use shared DataProvider
        setDataProvider(MascApplicationContext.getContext().getDataProvider());
    }

    @Override
    public CriteriaDataProvider getDataProvider() {
        return (CriteriaDataProvider) super.getDataProvider();
    }

    public void setMascModel(MascModel mascModel) {
        getDataProvider().setMascModel(mascModel);
        getUI().setTreeTableModel(createModel());
    }

    public TreeTableModel createModel() {

        // Create root static node
        EvalCriteriaNode root = new EvalCriteriaNode(null);

        // Create model
        CriteriaDataProvider provider = getDataProvider();

        NavTreeTableModel model =
                createModel(root, new EvalCriteriaTreeTableModel(provider));

        // load all nodes of model
        loadAllNodes(root, provider);

        return model;
    }

    public void selectFirstNode() {
        Collection<Criteria> rootCriterias = getDataProvider().getRootCriterias();
        String uuid = rootCriterias.iterator().next().getUuid();
        selectNode(uuid);
    }
}
