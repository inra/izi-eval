/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.render;

import fr.inra.masc.MascApplicationContext;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.Option;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 */
public class OptionsListModel extends AbstractListModel implements ComboBoxModel {

    private static final long serialVersionUID = 1L;

    protected List<Option> options;
    protected Option option;
    protected MascModel mascModel;

    public OptionsListModel() {
        mascModel = MascApplicationContext.getContext().getMascUIModel().getMascModel();

        updateValues();
        mascModel.addPropertyChangeListener(MascModel.PROPERTY_OPTION, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                updateValues();
            }
        });
    }

    protected void updateValues() {
        options = new ArrayList<Option>(mascModel.getOption());

        if (!options.isEmpty() && (!options.contains(option) || option == null)) {
            setSelectedItem(options.get(0));
        }
        fireContentsChanged(this, 0, getSize());
    }

    @Override
    public int getSize() {
        return options.size();
    }

    @Override
    public Object getElementAt(int index) {
        return options.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        this.option = (Option) anItem;
    }

    @Override
    public Option getSelectedItem() {
        return option;
    }
}
