/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.render.synoptic;

import com.google.common.collect.Sets;

import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.Option;
import fr.inra.masc.services.ImageGeneratorService;
import fr.inra.masc.synoptic.CriteriaGraphBuilder;
import fr.inra.masc.synoptic.LegendItem;
import fr.inra.masc.synoptic.SynopticModel;
import fr.inra.masc.ui.MascHandler;
import fr.inra.masc.ui.tree.criteria.CriteriaTreeHelper;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;

import javax.swing.JColorChooser;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeModel;

import jaxx.runtime.SwingUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.svg.SVGDocument;

import static org.nuiton.i18n.I18n._;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @author tchemit &lt;chemit@codelutin.com&gt;
 */
public class SynopticUIHandler extends MascHandler {

    /** Logger. */
    static private Log log = LogFactory.getLog(SynopticUIHandler.class);

    /**
     * UI managed by this handler.
     *
     * @since 1.0
     */
    private final SynopticUI ui;

    /**
     * To build the graph used to produce image.
     *
     * @since 1.0
     */
    private CriteriaGraphBuilder builder;

    public SynopticUIHandler(SynopticUI ui) {
        this.ui = ui;
    }

    public static void openColorChooser(LegendColorChooser panel) {

        LegendItem model = panel.getModel();

        Color color = JColorChooser.showDialog(
                panel,
                _("masc.synoptic.colorChooser.title",
                  model.getTitle()), model.getColor());
        if (color != null) {
            model.setColor(color);
        }
    }

    public void initUI() {

        ui.getTreeHelper().setUI(ui.getNavigation(), false, false, null);

        SynopticModel synopticModel = getMascUIModel().getSynopticModel();
        ui.setModel(synopticModel);

        loadModel();

        // select option
        Option optionSelected = synopticModel.getOption();
        if (optionSelected == null) {

            // if no one, select first
            optionSelected = getCurrentMascModel().getOption(0);
        }
        synopticModel.setOption(optionSelected);

        // update synoptic image
        showUI();

        // add listener to rebuild the graph builder
        synopticModel.addPropertyChangeListener(new PropertyChangeListener() {

            private final Set<String> skipProperties = Sets.newHashSet(
                    SynopticModel.PROPERTY_X_MAX,
                    SynopticModel.PROPERTY_Y_MAX
            );

            private final Set<String> rebuildProperties = Sets.newHashSet(
                    SynopticModel.PROPERTY_RESOLVE_REFERENCE,
                    SynopticModel.PROPERTY_CRITERIA_LIST,
                    SynopticModel.PROPERTY_MIRROR,
                    SynopticModel.PROPERTY_DUPLICATE_LINKED_CRITERIAS,
                    SynopticModel.PROPERTY_X_LEGEND,
                    SynopticModel.PROPERTY_Y_LEGEND
            );

            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                String propertyName = evt.getPropertyName();

                if (!skipProperties.contains(propertyName)) {

                    // can treat this update
                    if (rebuildProperties.contains(propertyName)) {

                        // need to update the builder
                        updateBuilder();
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("Will update image by change of property " +
                                  propertyName);
                    }
                    updateImage();
                }


            }
        });

        // listen when data provider structure changed (will recreate tree model)
        listenCriteriaModelChanged(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                loadModel();
            }
        });

        // listne selection change to update selected ids to use for graph build
        ui.getNavigation().addTreeSelectionListener(new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent event) {

                // get selected criterias
                List<String> selectedCriteria =
                        ui.getTreeHelper().getSelectedCriteriaId();
                ui.getModel().setCriteriaList(selectedCriteria);
            }
        });
    }

    public void showUI() {
        updateBuilder();
        updateImage();
    }

    protected void loadModel() {

        JTree tree = ui.getNavigation();
        CriteriaTreeHelper treeHelper = ui.getTreeHelper();

        TreeModel model = treeHelper.createModel();
        tree.setModel(model);

        // expend tree by default
        SwingUtil.expandTree(tree);
    }

    protected void updateBuilder() {

        // get ui model
        SynopticModel model = ui.getModel();

        // get root criteria
        SynopticCriteriaSelectionModel selectionModel = ui.getSelectionModel();
        Criteria criteria = selectionModel.getSelectedCriteria();

        // new builder
        builder = new CriteriaGraphBuilder(criteria, model);

        // fill legend
        List<LegendItem> legend = model.getLegend();

        // get legend panel
        JPanel legendPanel = ui.getLegendPanel();
        legendPanel.removeAll();

        for (LegendItem legendItem : legend) {

            LegendColorChooser legendColorChooser = new LegendColorChooser();
            legendColorChooser.setModel(legendItem);
            legendPanel.add(legendColorChooser);
        }
    }

    protected void updateImage() {

        if (log.isDebugEnabled()) {
            log.debug("Will regenerate image");
        }

        // make sure to reset image generator internal states
        // generate graph
        SVGDocument document = getService(ImageGeneratorService.class).convertModelToTreeImage(builder, ui.getModel());

        // keep generated image for report
        getMascUIModel().getReportModel().setSynoptic(document);
        ui.getImagePanel().setDocument(document);
    }

    public void openLink(HyperlinkEvent event) {
        HyperlinkEvent.EventType type = event.getEventType();
        if (Desktop.isDesktopSupported() && type == HyperlinkEvent.EventType.ACTIVATED) {
          try {
              Desktop.getDesktop().browse(event.getURL().toURI());
            } catch (Exception ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't open link", ex);
                }
            }
        }
    }
    
    /**
     * Gestion du click souris sur l'arbre, affichage d'un menu contextuel.
     * 
     * @param event mouse event
     */
    public void navigationTreeMouseClicked(MouseEvent event) {

        // clic droit
        if (event.getButton() == MouseEvent.BUTTON3) {

            JPopupMenu menu = new JPopupMenu();

            // select all button
            JMenuItem selectAllButton = new JMenuItem(_("masc.common.selectAll"));
            selectAllButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ui.getTreeHelper().selectFirstNode();
                }
            });
            menu.add(selectAllButton);

            // unselect all button
            JMenuItem unSelectAllButton = new JMenuItem(_("masc.common.unSelectAll"));
            unSelectAllButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ui.getTreeHelper().unSelectAllNodes();
                }
            });
            menu.add(unSelectAllButton);

            menu.show((Component)event.getSource(), event.getX(), event.getY());
        }
    }
}
