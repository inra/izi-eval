/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.editor.threshold;

import com.google.common.collect.Lists;
import fr.inra.masc.MascUIModel;
import fr.inra.masc.utils.MascUtil;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.ScaleOrder;
import fr.inra.masc.model.ScaleValue;
import fr.inra.masc.model.ThresholdValue;
import fr.inra.masc.model.ThresholdValueImpl;
import fr.inra.masc.ui.MascUIHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.nuiton.i18n.I18n._;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @since 0.3
 */
public class ThresholdTableModel extends AbstractTableModel {

    /** Logger */
    private static Log log = LogFactory.getLog(ThresholdTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final String THRESHOLD_VALUE_PATTERN =
            "^\\s*([><]?)\\s*(=?)\\s*(\\-?[\\d.]+)\\s*$";

    protected final ThresholdEditor ui;

    protected List<EditableCriteria> editableCriterias;

    protected int maxColumnCount = Integer.MIN_VALUE;

    protected boolean isEditing = false;

    public ThresholdTableModel(ThresholdEditor ui) {
        this.ui = ui;

        updateModel(getMascUIModel().getMascModel());
    }

    protected MascUIModel getMascUIModel() {
        return ui.getHandler().getMascUIModel();
    }

    public List<EditableCriteria> getEditableCriterias() {
        return editableCriterias;
    }

    public void updateModel(MascModel mascModel) {
        editableCriterias = MascUtil.extractAllEditableCriterias(mascModel);

        for (EditableCriteria criteria : editableCriterias) {
            int scaleValuesSize = criteria.getScale().getScaleValue().size() * 2;
            if (maxColumnCount < scaleValuesSize) {
                maxColumnCount = scaleValuesSize;
            }
        }

        fireTableStructureChanged();
    }

    public boolean isEditing() {
        return isEditing;
    }

    @Override
    public int getRowCount() {
        return editableCriterias.size();
    }

    @Override
    public int getColumnCount() {
        return maxColumnCount;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        EditableCriteria criteria = editableCriterias.get(rowIndex);
        Object result = null;
        if (columnIndex == 0) {
            result = criteria;
        } else if (columnIndex == 1) {
            result = criteria.getUnit();
        } else {

            int column = columnIndex - 2;
            int scaleConcerned = column / 2;
            boolean isScaleName = (column % 2 == 0);

            if (editableCriterias.get(rowIndex).getScale().getScaleValue().size() <= scaleConcerned) {
                result = StringUtils.EMPTY;
            } else if (isScaleName) {
                ScaleValue scaleValue = criteria.getScale().getScaleValue(scaleConcerned);
                result = scaleValue;
            } else {
                Collection<ThresholdValue> values = criteria.getValues();
                if (values != null && !values.isEmpty()) {
                    ThresholdValue value = criteria.getValues(scaleConcerned);
                    if (value != null) {
                        result = MascUIHelper.getStringValue(value);
                    }
                }
            }
        }
        if (result == null) {
            result = "";
        }
        return result;
    }

    @Override
    public String getColumnName(int column) {
        String result;

        if (column == 0) {
            result = _("masc.criteria.name");
        } else if (column == 1) {
            result = _("masc.criteria.unit");
        } else if (column % 2 == 0) {
            result = _("masc.threshold.class");
        } else {
            result = _("masc.threshold.threshold");
        }
        return result;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> result;
        if (columnIndex == 0) {
            result = EditableCriteria.class;
        } else {
            result = String.class;
        }
        return result;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean result;
        if (columnIndex == 0) {
            result = false;
        } else if (columnIndex == 1) {
            result = true;
        } else {

            int scaleConcerned = columnIndex / 2;
            if (editableCriterias.get(rowIndex).getScale().getScaleValue().size() <= scaleConcerned) {
                result = false;
            } else {
                columnIndex = columnIndex - 2;
                result = !(columnIndex % 2 == 0);
            }
        }
        return result;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {

        String value = String.valueOf(o);

        // get criteria for current row
        EditableCriteria editableCriteria = editableCriterias.get(rowIndex);

        if (columnIndex == 1) {
            editableCriteria.setUnit(value);

            getMascUIModel().setModelChanged(true);
        } else {

            // get threshold concerned
            int thresholdConcerned = (columnIndex - 2) / 2;

            try {

                // try to set value
                setValueAt(value, editableCriteria, thresholdConcerned);

            } catch (NumberFormatException eee) {
                isEditing = true;
                JOptionPane.showMessageDialog(ui, _("masc.threshold.invalidValues.msg"), _("masc.threshold.invalidValues.title"), JOptionPane.ERROR_MESSAGE);
            } finally {

                Collection<ThresholdValue> values = editableCriteria.getValues();
                if (values == null) {
                    isEditing = false;
                } else {

                    // check if all values are null now
                    boolean noneNull = true;
                    boolean allNull = true;
                    for (ThresholdValue thresholdValue : values) {
                        if (thresholdValue == null) {
                            noneNull = false;
                        } else {
                            allNull = false;
                        }
                    }


                    // if all values are filled, stop editing
                    if (allNull) {
                        isEditing = false;
                        editableCriteria.setValues(null);
                    }

                    if (noneNull && !isEditing) {

                        // means the line is valid (can save the model)
                        getMascUIModel().setModelChanged(true);
                    }
                }
            }
        }
    }

    protected void setValueAt(String value, EditableCriteria editableCriteria, int thresholdConcerned) {

        // value must be like : > int or <= int
        Pattern thresholdPattern = Pattern.compile(THRESHOLD_VALUE_PATTERN);
        Matcher matcher = thresholdPattern.matcher(value);
        if (!StringUtils.isEmpty(value) && !matcher.matches()) {
            throw new NumberFormatException();
        }

        // get all threshold values for criteria
        List<ThresholdValue> thresholdValues = editableCriteria.getValues();

        if (CollectionUtils.isEmpty(thresholdValues)) {

            // if no values found, create new list with scale value length
            int maxThresholdValues = editableCriteria.getScale().getScaleValue().size() - 1;
            thresholdValues = Lists.newArrayList(new ThresholdValue[maxThresholdValues]);
        }

        // if value is empty, we are in edition
        if (StringUtils.isEmpty(value)) {
            thresholdValues.set(thresholdConcerned, null);
            isEditing = true;

        } else {

            // else, get existing threshold value
            ThresholdValue thresholdValue = thresholdValues.get(thresholdConcerned);

            if (thresholdValue == null) {
                // creating new one
                thresholdValue = new ThresholdValueImpl();
            }

            // parse equal
            String canBeEqual = matcher.group(2);
            thresholdValue.setCanBeEqual("=".equals(canBeEqual));

            // parse sign
            String sign = matcher.group(1);
            if (">".equals(sign)) {
                thresholdValue.setSign(ScaleOrder.DESC);
            } else if ("<".equals(sign)) {
                thresholdValue.setSign(ScaleOrder.ASC);
            } else {
                thresholdValue.setSign(ScaleOrder.NONE);
                thresholdValue.setCanBeEqual(true);
            }

            // parse value
            String doubleValue = matcher.group(3);
            thresholdValue.setValue(Double.parseDouble(doubleValue));

            // validate that all threshold is following each other and that equals sign are not conflicting with other one
            boolean validated = validateThresholdValue(thresholdValues, thresholdValue, thresholdConcerned);
            if (validated) {

                // really set value in model
                thresholdValues.set(thresholdConcerned, thresholdValue);
                editableCriteria.setValues(thresholdValues);
            }

            // see if all thresholds values are validated and filled to stop editing
            isEditing = !validated || thresholdValues.contains(null);
        }
    }

    protected boolean validateThresholdValue(List<ThresholdValue> thresholdValues, ThresholdValue thresholdValue, int thresholdConcerned) {

        ArrayList<ThresholdValue> copiedThresholdValues = Lists.newArrayList(thresholdValues);
        copiedThresholdValues.set(thresholdConcerned, thresholdValue);

        // determine order
        ScaleOrder order = null;
        for (ThresholdValue existingThresholdValue : copiedThresholdValues) {
            if (existingThresholdValue != null) {
                ScaleOrder sign = existingThresholdValue.getSign();
                if (!ScaleOrder.NONE.equals(sign)) {
                    order = sign;
                }
            }
        }

        // order not determined
        if (order == null) {
            return true;
        }
        if (log.isDebugEnabled()) {
            log.debug("Order to use " + order);
        }

        // all must have same order
        for (ThresholdValue existingThresholdValue : copiedThresholdValues) {
            if (existingThresholdValue != null) {
                ScaleOrder existingSign = existingThresholdValue.getSign();
                if (!existingSign.equals(ScaleOrder.NONE) && !existingSign.equals(order)) {
                    JOptionPane.showMessageDialog(ui, _("masc.threshold.invalidSign.msg"), _("masc.threshold.invalidSign.title"), JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
        }

        // value must follow each others
        double initialValue;
        if (order.equals(ScaleOrder.ASC)) {
            initialValue = -Float.MAX_VALUE;
        } else {
            initialValue = Float.MAX_VALUE;
        }
        for (ThresholdValue existingThresholdValue : copiedThresholdValues) {
            if (existingThresholdValue != null) {
                Double existingValue = existingThresholdValue.getValue();
                if (order.equals(ScaleOrder.ASC)) {
                    if (initialValue < existingValue) {
                        initialValue = existingValue;
                    } else {
                        JOptionPane.showMessageDialog(ui, _("masc.threshold.invalidValuesOrder.msg"), _("masc.threshold.invalidValuesOrder.title"), JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                } else {
                    if (initialValue > existingValue) {
                        initialValue = existingValue;
                    } else {
                        JOptionPane.showMessageDialog(ui, _("masc.threshold.invalidValuesOrder.msg"), _("masc.threshold.invalidValuesOrder.title"), JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
