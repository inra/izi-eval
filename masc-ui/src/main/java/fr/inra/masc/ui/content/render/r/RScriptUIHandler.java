/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.render.r;

import com.google.common.collect.Lists;
import fr.inra.masc.ExecutorException;
import fr.inra.masc.R_SCRIPT;
import fr.inra.masc.model.MascModel;
import fr.inra.masc.model.RScriptModel;
import fr.inra.masc.services.RInvokerService;
import fr.inra.masc.ui.MascHandler;
import fr.inra.masc.ui.MascUIHelper;
import fr.inra.masc.ui.content.render.CheckBoxListSelectionModel;
import fr.inra.masc.ui.widget.ImageGenerator;
import fr.inra.masc.ui.widget.SimpleImageGenerator;
import fr.inra.masc.ui.widget.SimpleImagePanel;
import java.awt.Component;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import jaxx.runtime.swing.editor.NumberEditor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n._;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 0.5
 */
public class RScriptUIHandler extends MascHandler {

    /** Logger */
    private static Log log = LogFactory.getLog(RScriptUIHandler.class);

    private final RScriptUI ui;

    public RScriptUIHandler(RScriptUI ui) {
        this.ui = ui;
    }

    public void initUI() {

        final RScriptModel scriptModel = getMascUIModel().getScriptModel();
        R_SCRIPT script = scriptModel.getScript();
        CheckBoxListSelectionModel outputScriptFilesSelectionModel = ui.getOutputScriptFilesSelectionModel();
        OutputScriptFilesModel outputScriptFilesModel = ui.getOutputScriptFilesModel();
        if (script != null && script.equals(R_SCRIPT.MC)) {
            outputScriptFilesSelectionModel.clearSelection();
            for (String outputSelected : scriptModel.getSelectedResult()) {
                int i = outputScriptFilesModel.indexOf(outputSelected);

                outputScriptFilesSelectionModel.addSelectionInterval(i, i);
            }
        } else {
            if (script == null) {
                scriptModel.setScript(R_SCRIPT.SI);
            }
        }
        outputScriptFilesSelectionModel.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                // get selected output
                Object[] selectedValues = ui.getOutputScriptFiles().getSelectedValues();
                List<String> selectedValuesList = Lists.newArrayList();

                for (Object selectedValue : selectedValues) {
                    selectedValuesList.add((String) selectedValue);
                }

                scriptModel.setSelectedResult(selectedValuesList);
            }
        });

        scriptModel.addPropertyChangeListener(RScriptModel.PROPERTY_SCRIPT, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                R_SCRIPT newValue = (R_SCRIPT) evt.getNewValue();
                ui.getArgsLayout().show(ui.getArgsPanel(), newValue.name());
                changeScript(newValue);
            }
        });

        final NumberEditor nbRuns = ui.getNbRuns();
        nbRuns.addPropertyChangeListener(NumberEditor.PROPERTY_MODEL, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                scriptModel.setNbRuns((Integer) nbRuns.getModel());
            }
        });
        ui.getScriptChooser().setSelectedItem(scriptModel.getScript());
        ui.getOptionsSelection().setSelectedItem(scriptModel.getOption());
        ui.getCriteriaSelection().setSelectedItem(ui.getMascModel().getCriteria(scriptModel.getNode()));
        nbRuns.setModel(scriptModel.getNbRuns());
        ui.getArgsLayout().show(ui.getArgsPanel(), scriptModel.getScript().name());
        changeScript(scriptModel.getScript());
    }


    protected void changeScript(R_SCRIPT newValue) {
        JLabel outputLbl = ui.getOutputScriptFilesLbl();
        JList outputScriptFiles = ui.getOutputScriptFiles();
        if (newValue.equals(R_SCRIPT.MC)) {
            outputLbl.setVisible(true);
            outputScriptFiles.setVisible(true);
        } else {
            outputLbl.setVisible(false);
            outputScriptFiles.setVisible(false);
        }
    }

    public void showLogs(RScriptModel scriptModel) {
        showLogs(ui, scriptModel, JOptionPane.INFORMATION_MESSAGE);
    }

    public void submit(RScriptModel scriptModel) {

        getMascUIModel().setScriptModel(scriptModel);

        if (scriptModel.getOption() == null) {
            scriptModel.setOption(getMascUIModel().getMascModel().getOption(0));
        }

        MascModel currentMascModel = getCurrentMascModel();

        RInvokerService rInvokerService = getService(RInvokerService.class);

        File tmpDirectory = getConfig().getTmpDirectory();
        try {
            rInvokerService.executeRSCript(getMascUIModel().getMascFileName(),
                                           currentMascModel,
                                           scriptModel);

        } catch (ExecutorException eee) {

            if (scriptModel.logFileExist(tmpDirectory)) {
                showLogs(
                        ui, scriptModel, JOptionPane.ERROR_MESSAGE);
            } else {
                log.error("Failed to launch R script", eee);

                int returnVal2 = JOptionPane.showConfirmDialog(
                        ui,
                        _("masc.error.r.notConfigured"),
                        _("masc.error.r.notConfigured.title"),
                        JOptionPane.YES_NO_OPTION);
                if (returnVal2 == JOptionPane.YES_OPTION) {
                    File rApp = MascUIHelper.openFile(
                            ui, MascUIHelper.MascFileType.R_APP);
                    if (rApp != null) {
                        getConfig().setRExecutableFile(rApp);
                    }
                }
            }
        }

        ui.getShowLogButton().setEnabled(scriptModel.logFileExist(tmpDirectory));

        showResults(scriptModel);
    }

    protected void showResults(RScriptModel scriptModel) {

        File tmpDirectory = getConfig().getTmpDirectory();

        if (!scriptModel.isAlreadyLaunched(tmpDirectory)) {
            return;
        }

        MascModel currentMascModel = getCurrentMascModel();

        // get content panel
        JPanel resultContent = ui.getResultContent();

        // remove all old element
        for (Component c : resultContent.getComponents()) {
            resultContent.remove(c);
        }

        // add all texts in result panel
        File[] resultTextFiles = scriptModel.getResultTextFiles(
                tmpDirectory,
                getMascUIModel().getMascFileName(),
                currentMascModel);
        if (resultTextFiles != null) {
            for (File textFile : resultTextFiles) {
                if (textFile.exists()) {
                    try {
                        JTextArea textArea = new JTextArea(FileUtils.readFileToString(textFile));
                        textArea.setAlignmentX(Component.LEFT_ALIGNMENT);
                        resultContent.add(textArea);
                    } catch (IOException eee) {
                        String errorMsg = "Failed to read file " + textFile.getName();
                        log.error(errorMsg, eee);
                        MascUIHelper.showError(ui, errorMsg, eee);
                    }
                }
            }
        }


        // add all images in result panel
        File[] resultImageFiles = scriptModel.getResultImageFiles(
                tmpDirectory,
                getMascUIModel().getMascFileName(),
                currentMascModel);
        if (resultImageFiles != null) {
            for (File imageFile : resultImageFiles) {
                if (imageFile.exists()) {
                    try {
                        SimpleImagePanel imagePanel = new SimpleImagePanel(ui);
                        BufferedImage image = ImageIO.read(imageFile);

                        ImageGenerator imageGenerator = new SimpleImageGenerator(image);
                        imagePanel.setImageGenerator(imageGenerator);
                        imagePanel.setAlignmentX(Component.LEFT_ALIGNMENT);

                        resultContent.add(imagePanel);
                    } catch (IOException eee) {
                        String errorMsg = "Failed to read file " + imageFile.getName();
                        log.error(errorMsg, eee);
                        MascUIHelper.showError(ui, errorMsg, eee);
                    }
                }
            }
        }

        ui.revalidate();
        ui.repaint();
    }
}
