/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin,Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.render.r;

import fr.inra.masc.R_SCRIPT;
import org.jdesktop.swingx.renderer.DefaultListRenderer;

import javax.swing.JLabel;
import javax.swing.JList;
import java.awt.Component;

/**
 * @author sletellier
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.3
 */
public class RScriptListCellRenderer extends DefaultListRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {
        R_SCRIPT script = (R_SCRIPT) value;

        Component component = super.getListCellRendererComponent(
                list,
                script.getName(),
                index,
                isSelected,
                cellHasFocus);

        ((JLabel) component).setToolTipText(script.getToolTip());

        return component;
    }
}
