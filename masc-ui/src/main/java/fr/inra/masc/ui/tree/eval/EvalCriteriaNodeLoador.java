/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.tree.eval;

import fr.inra.masc.model.Criteria;
import fr.inra.masc.ui.tree.criteria.CriteriaDataProvider;
import jaxx.runtime.swing.nav.NavDataProvider;
import jaxx.runtime.swing.nav.treetable.NavTreeTableNodeChildLoador;

import java.util.List;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class EvalCriteriaNodeLoador extends NavTreeTableNodeChildLoador<Criteria, Criteria, EvalCriteriaNode> {

    private static final long serialVersionUID = 1L;

    public EvalCriteriaNodeLoador() {
        super(Criteria.class);
    }

    @Override
    public List<Criteria> getData(Class<?> parentClass,
                                  String parentId,
                                  NavDataProvider dataProvider) throws Exception {

        CriteriaDataProvider criteriaDataProvider = (CriteriaDataProvider) dataProvider;

        return criteriaDataProvider.getChildrenForTree(parentId);
    }

    @Override
    public EvalCriteriaNode createNode(Criteria data, NavDataProvider dataProvider) {
        String id = data.getUuid();

        EvalCriteriaNode result;
        if (data.isReference()) {
            result = new EvalCriteriaNode.ReferenceEvalCriteriaNode(id);
        } else {
            result = new EvalCriteriaNode(id);
        }
        return result;
    }

    public EvalCriteriaNode createNode(Criteria data) {
        return createNode(data, null);
    }
}
