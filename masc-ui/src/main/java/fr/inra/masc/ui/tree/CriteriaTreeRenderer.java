/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.tree;

import fr.inra.masc.MascApplicationContext;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.ui.MascDecoratorProvider;
import fr.inra.masc.ui.MascUIHelper;
import fr.inra.masc.ui.tree.criteria.CriteriaDataProvider;
import fr.inra.masc.ui.tree.criteria.CriteriaNode;
import fr.inra.masc.ui.tree.eval.EvalCriteriaNode;
import jaxx.runtime.swing.nav.NavNode;
import jaxx.runtime.swing.nav.tree.AbstractNavTreeCellRenderer;
import org.nuiton.util.decorator.Decorator;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import java.awt.Component;

import static org.nuiton.i18n.I18n._;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class CriteriaTreeRenderer<N extends NavNode<DefaultTreeModel, N>> extends AbstractNavTreeCellRenderer<DefaultTreeModel, N> {

    private static final long serialVersionUID = 1L;

    public enum CriteriaIcon {

        ICONS_CRITERIA("criteria.png"),
        ICONS_CRITERIA_EDIT("criteria-edit.png"),
        ICONS_CRITERIA_REFERENCE("criteria-reference.png");

        protected String iconName;

        CriteriaIcon(String iconName) {
            this.iconName = iconName;
        }

        public ImageIcon getIcon() {
            ImageIcon result = MascUIHelper.createIcon("/icons/navigation/" + iconName);
            return result;
        }
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree,
                                                  Object value,
                                                  boolean sel,
                                                  boolean expanded,
                                                  boolean leaf,
                                                  int row,
                                                  boolean hasFocus) {
        super.getTreeCellRendererComponent(
                tree,
                value,
                sel,
                expanded,
                leaf,
                row,
                hasFocus
        );

        if (value instanceof CriteriaNode || value instanceof EvalCriteriaNode) {
            N node = (N) value;
            String toolTip;
            if (value instanceof CriteriaNode.ReferenceCriteriaNode ||
                value instanceof EvalCriteriaNode.ReferenceEvalCriteriaNode) {
                setIcon(CriteriaIcon.ICONS_CRITERIA_REFERENCE.getIcon());
                toolTip = _("masc.model.ref.tip");
            } else if (node.getChildCount() == 0) {
                setIcon(CriteriaIcon.ICONS_CRITERIA_EDIT.getIcon());
                toolTip = _("masc.model.edit.tip");
            } else {
                setIcon(CriteriaIcon.ICONS_CRITERIA.getIcon());
                toolTip = _("masc.model.criteria.tip");
            }
            setToolTipText(toolTip);
            setText(getNodeText(node));
        }

        return this;
    }

    @Override
    public CriteriaDataProvider getDataProvider() {
        CriteriaDataProvider provider = (CriteriaDataProvider)
                super.getDataProvider();

        if (provider == null) {
            provider = MascApplicationContext.getContext().getDataProvider();
            setDataProvider(provider);
        }
        return provider;
    }

    protected MascDecoratorProvider getDecoratorProvider() {
        MascApplicationContext context = MascApplicationContext.getContext();
        MascDecoratorProvider decoratorProvider = context.getDecoratorProvider();
        if (decoratorProvider == null) {
            decoratorProvider = new MascDecoratorProvider();
            context.setDecoratorProvider(decoratorProvider);
        }
        return decoratorProvider;
    }

    @Override
    protected String computeNodeText(N node) {

        // get data
        String id = node.getId();
        Criteria criteria = getDataProvider().getCriteria(id);

        Decorator<Criteria> decorator = getDecoratorProvider().getDecoratorByType(Criteria.class);
        String text = decorator.toString(criteria);

        return text;
    }
}
