/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.tree.criteria;

import fr.inra.masc.model.ComputableCriteria;
import fr.inra.masc.model.Criteria;
import fr.inra.masc.model.EditableCriteria;
import fr.inra.masc.ui.content.editor.criteria.editor.ComputableCriteriaEditor;
import fr.inra.masc.ui.content.editor.criteria.editor.ReferenceCriteriaEditor;
import fr.inra.masc.ui.content.editor.criteria.editor.ValuedCriteriaEditor;
import jaxx.runtime.swing.nav.tree.NavTreeNode;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public abstract class CriteriaNode extends NavTreeNode<CriteriaNode> {

    private static final long serialVersionUID = 1L;

    protected CriteriaNode(Class<? extends Criteria> clazz, String id) {
        super(clazz,
              id,
              null,
              CriteriaTreeHelper.getChildLoador(CriteriaNodeLoador.class)
        );
    }

    public abstract Class<?> getEditorClass();

    /** @author sletellier &lt;letellier@codelutin.com&gt; */
    public static class ComputableCriteriaNode extends CriteriaNode {

        private static final long serialVersionUID = 1L;

        public ComputableCriteriaNode(String id) {
            super(ComputableCriteria.class, id);
        }

        @Override
        public Class<?> getEditorClass() {
            return ComputableCriteriaEditor.class;
        }
    }

    /** @author sletellier &lt;letellier@codelutin.com&gt; */
    public static class ValuedCriteriaNode extends CriteriaNode {

        private static final long serialVersionUID = 1L;

        public ValuedCriteriaNode(String id) {
            super(EditableCriteria.class, id);
        }

        @Override
        public Class<?> getEditorClass() {
            return ValuedCriteriaEditor.class;
        }
    }

    /** @author sletellier &lt;letellier@codelutin.com&gt; */
    public static class ReferenceCriteriaNode extends ValuedCriteriaNode {

        private static final long serialVersionUID = 1L;

        public ReferenceCriteriaNode(String id) {
            super(id);
        }

        @Override
        public Class<?> getEditorClass() {
            return ReferenceCriteriaEditor.class;
        }
    }
}
