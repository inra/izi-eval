/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.tree.criteria;

import fr.inra.masc.model.ComputableCriteria;
import fr.inra.masc.model.Criteria;
import jaxx.runtime.swing.nav.NavDataProvider;
import jaxx.runtime.swing.nav.tree.NavTreeNodeChildLoador;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/** @author sletellier &lt;letellier@codelutin.com&gt; */
public class CriteriaNodeLoador extends NavTreeNodeChildLoador<Criteria, Criteria, CriteriaNode> {

    /** Logger. */
    static private final Log log = LogFactory.getLog(CriteriaNodeLoador.class);

    private static final long serialVersionUID = 1L;

    public CriteriaNodeLoador() {
        super(Criteria.class);
    }

    @Override
    public List<Criteria> getData(Class<?> parentClass,
                                  String parentId,
                                  NavDataProvider dataProvider) throws Exception {


        CriteriaDataProvider criteriaDataProvider = (CriteriaDataProvider) dataProvider;

        List<Criteria> childrenForTree = criteriaDataProvider.getChildrenForTree(parentId);

        return childrenForTree;
    }

    @Override
    public CriteriaNode createNode(Criteria data, NavDataProvider dataProvider) {
        String id = data.getUuid();

        if (log.isDebugEnabled()) {
            log.debug("Creating node '" + data.getName() + " with id : " + id);
        }

        CriteriaNode node;
        if (data.isReference()) {
            node = new CriteriaNode.ReferenceCriteriaNode(id);
        } else if (data instanceof ComputableCriteria) {
            node = new CriteriaNode.ComputableCriteriaNode(id);
        } else {
            node = new CriteriaNode.ValuedCriteriaNode(id);
        }

        return node;
    }

    public CriteriaNode createNode(Criteria data) {
        return createNode(data, null);
    }
}
