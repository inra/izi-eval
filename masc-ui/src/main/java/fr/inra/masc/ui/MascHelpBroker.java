/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui;

import java.awt.event.ActionListener;
import java.util.Locale;
import javax.swing.AbstractButton;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.swing.help.JAXXHelpBroker;
import jaxx.runtime.swing.help.JAXXHelpUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author sletellier &lt;letellier@codelutin.com&gt;
 */
public class MascHelpBroker extends JAXXHelpBroker {

    /** Logger */
    static private Log log = LogFactory.getLog(MascHelpBroker.class);

    public MascHelpBroker(Locale locale, String helpsetName, String helpKey, String defaultID, JAXXHelpUIHandler handler) {
        super(locale, helpsetName, helpKey, defaultID, handler);
    }

    public void prepareUI(JAXXObject c) {
        if (c == null) {
            throw new NullPointerException("parameter c can not be null!");
        }

        // l'ui doit avoir un boutton showHelp
        AbstractButton help = getShowHelpButton(c);

        if (help == null) {
            // no showHelp button
            return;
        }

        // attach context to button
        if (log.isDebugEnabled()) {
            log.debug("attach context to showhelp button " + c);
        }
        help.putClientProperty(JAXX_CONTEXT_ENTRY, c);

        // add tracking action
        ActionListener listener = getShowHelpAction();
        if (log.isDebugEnabled()) {
            log.debug("adding tracking action " + listener);
        }
        help.addActionListener(listener);

        if (log.isDebugEnabled()) {
            log.debug("done for " + c);
        }
    }
}
