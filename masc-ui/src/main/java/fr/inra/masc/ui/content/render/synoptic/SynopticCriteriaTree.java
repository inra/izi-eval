/*
 * #%L
 * Masc :: Swing UI
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Inra, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.masc.ui.content.render.synoptic;

import javax.swing.tree.TreePath;

import fr.inra.masc.ui.tree.CriteriaTree;

/**
 * Surcharge de JXTree pour pouvoir surcharger getStringAt() et faire fontionner la recherche.
 * 
 * @author Eric Chatellier
 */
public class SynopticCriteriaTree extends CriteriaTree {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1087089536077308370L;

    @Override
    public void setSelectionPaths(TreePath[] paths) {
        // fix for custom selection here
        super.setSelectionPath(paths[0]);
    }
}
