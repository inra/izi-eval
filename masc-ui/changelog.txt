0.1
===

Création d'une interface de visualisation des criterias
Création d'une interface d'édition des options
Création d'une interface pour l'affichage des résultats de l'évaluation
Création d'une interface pour la génération des graphiques
Création d'une interface pour la génération des arbres synoptiques

0.2
===

Correction de quelques traductions
[Graphique synoptique] Ajout de la sélection des arbres satellites
[Graphique synoptique] Ajout de la résolution des critères reférents
[Graphique synoptique] La légende est affichée par niveau
[Graphique synoptique] Ajout de la sélection des couleurs pour la légende
[Graphique synoptique] Les flèches sont enlevés
[Graphique synoptique] Ajout du poids sur les associations si désiré
[Graphique synoptique] Possibilité d'afficher l'arbre non évalué
[Graphique synoptique] Ajout de différents réglages (espacement, position de la légende...)

0.3
===

Bugs
----
[#1578] pb avec la fenêtre de gauche dans l'onglet modèle
[#1561] [Option] Plus la possibilité d'éditer des options

Features
--------
[#1540] [Modèle] Trier par ordre d'apparition.
[#1568] [Générale] Filtre sur les types de fichiers lors de l'ouverture d'un fichier
[#1558] [Modèle] Ajout d?information sur le nombre de feuilles, et de n?uds agrégés
[#1544] [Valeurs-seuils] Permettre la saisie de valeurs seuils
[#1548] [Graphiques] Spider (Radar) : élargir la ligne du périmètre.
[#1543] [Modèle] Création d'un nouveau fichier "pivot" en .masc
[#1566] [Générale] Indiqué le nom du fichier en titre
[#1541] [UI] Permettre la configuration de l'application DEXi et de DEXiEval lors de l'appel.
[#1567] [Générale] Internationalisation

0.4
===

Bugs
----
[#1569] [Evaluation / Graphiques] Manque des critères

Features
--------
[#1544] [Valeurs-seuils] Permettre la saisie de valeurs seuils
[#1528] [Exports] Création d'une interface pour la génération des rapports

0.5
====

Bugs
----
[#1590] Fermeture de l'interface
[#1596] Plantage lors de l'export après avoir renseigné des valeurs-seuils

Features
--------
[#1530] [AS] Création d'une interface pour les analyses de sensibilités
[#1616] Ajout d'une interface pour l'édition de la configuration
[#1615] Ajout d'un menu qui point vers le site de l'application
[#1614] Ajout d'une interface pour visualiser les third-party, licence et le "à propos"

1.0
===

Bugs
----
[#1594] plantage : Voulez renseigner DEXi Eval ? (2)
[#1596] plantage lors de l'export après avoir renseigné des valeurs-seuils

Features
--------
[#1542] [UI] Ajout d'un bouton imprimer.
[#1546] [Graphiques] 2 entrées
[#1565] [Graphique synoptique] Ajouter la case de chaque critère la classe prise sur le nombre de classes possibles
[#1580] import des fichiers d'entrée
[#1592] [option] couleur des classes (vert, noir, rouge)
[#1621] interface masc

1.1
===

Bugs
----
[#1629] [Option] Les valeurs disparaissent
[#1591] [graphique synoptique] : erreur d'affichage
[#1625] Généricité des analyses de sensibilité dans l'interface
[#1631] bug dans les couleurs du graphique synoptique
[#1632] mise en forme des graphiques synoptiques
[#1624] Erreur d'affichage lorsque j'importe le fichier DEXi qui est en PJ

Features
--------
[#1577] onglet graphique
[#1563] [Option] Agrandir la police de chaque ligne pour améliorer la lisibilité.

1.2
===

Bugs
----
[#1638] [graphique synoptique]
[#1639] [sauvegarder sous]

Features
--------
[#1643] Ajouter le script OAT.r
[#1646] Pouvoir définir le chemin de l’exécutable R via une boite de dialogue
