Bonjour à tous,

L'équipe Codelutin est fière de vous annoncer la version 0.2 !

Voici l'état d'avencement :

- Correction de quelques traductions
- [Model] Prise en compte du ROUNDING
- [Model] Résolution des réferences
- [Graphique synoptique] Ajout de la sélection des arbres satellites
- [Graphique synoptique] Ajout de la résolution des critères reférents
- [Graphique synoptique] La légende est affichée par niveau
- [Graphique synoptique] Ajout de la sélection des couleurs pour la légende
- [Graphique synoptique] Les flèches sont enlevés
- [Graphique synoptique] Ajout du poids sur les associations si désiré
- [Graphique synoptique] Possibilité d'afficher l'arbre non évalué
- [Graphique synoptique] Ajout de différents réglages (espacement, position de la légende...)

Fichiers téléchargeables :
https://mulcyber.toulouse.inra.fr/frs/?group_id=160

Cordialement,

L'équipe Codelutin