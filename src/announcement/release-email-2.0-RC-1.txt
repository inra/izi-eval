Bonjour à tous,

L'équipe Codelutin est heureuse de vous annoncer la version 2.0-RC-1 de InterfaceMasc!

A propos de la version
----------------------

Cette version corrige quelques bugs.

Fichiers téléchargeables :
https://mulcyber.toulouse.inra.fr/frs/?group_id=160

Anomalies
---------

 o #2508	[graphique synoptique] : export d'images
 o #2509	[Graphique synoptique] : erreur d'affichage des associations
 o #2510	[valeurs-seuils] : affichage d'un rapport d'erreur
 o #2511	[bugs à répétition]
 o #2512	[graphiques synoptiques] : taille de la police
 o #2513	[graphique synoptique] : réorganiser les items dans le sous-onglet Configuration
 o #2514	[model], [Options] et [évaluation]
 o #2515	[graphique synoptique] : fonction "ajuster à la taille de l'écran"
 o #2516	[graphique synoptique] : les poids des arbres satellites n'aparaissent pas

--

Cordialement,

L'équipe Codelutin