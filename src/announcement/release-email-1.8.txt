Bonjour à tous,

L'équipe Codelutin est heureuse de vous annoncer la version 1.8 de InterfaceMasc!

A propos de la version
----------------------

Cette version intègre de nouvelles fonctionnalités, à savoir :

Réécriture de l’algorithme de disposition des Critères et ajouter les options suivantes :
 o Option miroir
 o Épaisseur des associations
 o Style d'association
 o Critères liés autant de fois que de parents
 o Couleur de fond

Quelques corrections ont aussi été apportés sur la version précédente concernant principalement l'arbre synoptique.

Fichiers téléchargeables :
https://mulcyber.toulouse.inra.fr/frs/?group_id=160

Fonctionnalités
---------------

 o #1926	Ajustement de l'affichage des graphiques synoptiques
 o #1928	Faire apparaître les critères linked autant de fois que de parents
 o #1927	Option miroir dans l'onglet synoptique
 o #2459	Mettre à jours les versions des dépendances

Anomalies
---------

 o #2012	Mauvais rafraichissement du graphique synoptique

--

Cordialement,

L'équipe Codelutin