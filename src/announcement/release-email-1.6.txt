Bonjour à tous,

L'équipe Codelutin est heureuse de vous annoncer la version 1.6 de interfacemasc!

A propos de la version
----------------------

Cette version intègre de nouvelles fonctionnalités, à savoir :

 o Nouvelles actions sur les options et valeurs-seuils (import/export/clone)
 o La possibilité de zommer/dézommer sur les graphiques

Quelques corrections ont aussi été apportés sur la version précédente.

Fichiers téléchargeables :
https://mulcyber.toulouse.inra.fr/frs/?group_id=160

Note de version
---------------

Fonctionnalités
---------------

 o #1920	Possibilité de zoomer/dézoomer sur les graphiques
 o #1921	Importer/Exporter des options et des valeurs-seuil séparément
 o #1922	Dupliquer une option déjà renseignée dans l'interface

Anomalies
---------

 o #1930	La sauvegarde ne fonctionne pas sur certains modèles
 o #1931	Mauvaises traductions

Cordialement,

L'équipe Codelutin