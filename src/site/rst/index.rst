.. -
.. * #%L
.. * Masc
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2011 - 2013 Inra, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=====================
Documentation de MASC
=====================

MASC est une interface d'évaluation multicritère de la durabilité de systèmes de culture innovant.

Ce projet a été initiée en 2011 par l'INRA.

Il a été réalisé par la société Codelutin en 2011 - 2013.

MASC permet d'importer des fichiers DEXi et utilise DEXiEval pour l'évaluation des critères.
Pour plus d'informations, veuillez consulter le site de DEXi : http://www-ai.ijs.si/MarkoBohanec/dexi.html.
