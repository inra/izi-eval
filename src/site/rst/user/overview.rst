.. -
.. * #%L
.. * Masc
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2011 - 2013 Inra, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Utilisation de MASC
===================

L'interface MASC à l'ouverture, permet que peu de choses, les actions disponibles sont accessible via les menus et pour les plus usuels par une barre d'outils. L'ensemble des fonctionnalité sont disponibles lorsque un fichier (MASC ou DEXi) est ouvert.
Le menu Fichier regroupe les fonctionnalités suivantes :

- Ouvrir un fichier : permet d'ouvrir un fichier MASC (*.masc) ou un fichier DEXi (*.dxi).
- Sauvegarder
- Sauvegarder le model dans un fichier MASC
- Imprimer (disponible uniquement sous Windows)
- Exporter vers un fichier DEXi
- Générer des exports (création d'un fichier PDF)
- Changer de langues (seul l'anglais et le français sont actuellement disponible)
- Accéder à la configuration de MASC (utilisateur avancé)
- Quitter l'application

Lorsqu'un fichier est ouvert, MASC ouvre 7 onglets :

- Modèle : représente le modèle DEXi
- Valeurs-seuils : permet de définir des valeurs-seuils
- Options : permet de renseigner les valeurs de critères par options
- Évaluation : utilise DEXiEval pour évaluer le modèle selon les valeurs définit pour les options
- Graphiques : permet de générer des graphiques
- Graphique synoptique : permet de générer des graphiques synoptiques
- Analyse de sensibilité : permet de lancer des analyse de sensibilité grâce à des scripts R (nécessite d'avoir R correctement installé sur la machine)