.. -
.. * #%L
.. * Masc
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2011 - 2013 Inra, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Installation de MASC
====================

Java
----

MASC est un programme Java, de ce fait, Java doit être correctement installé sur la machine.

Il est possible de le vérifier en utilisant la commande `java -version` :

Exemple 32bits::

 java version "1.6.0_10"
 Java(TM) SE Runtime Environment (build 1.6.0_10-b33)
 Java HotSpot(TM) Server VM (build 11.0-b15, mixed mode)

Exemple 64 bits::

 java version "1.6.0_29"
 Java(TM) SE Runtime Environment (build 1.6.0_29-b11)
 Java HotSpot(TM) 64-Bit Server VM (build 20.4-b02, mixed mode)

MASC
----

Pour installer MASC, il suffit de `télécharger la dernière version`_ et de le décompresser dans le répertoire d'installation désiré.

Il faut exécuter le fichier 'go.bat' pour Windows et 'go.sh' pour linux. Des fichiers DEXi sont disponibles dans le répertoire 'exemples'.

Installation de R
-----------------

MASC permet d'exécuter des scripts R pour les analyses de sensibilités. Pour cela, il faut que celui-ci soit correctement installé sur le système.

- Pour windows, il vous faut télécharger et installer R sur la machine sur le site : http://cran.r-project.org/bin/windows/base
- Pour Debian et Ubuntu, il vous faut installer les paquets suivants : apt-get install r-base libxml2-dev

Ouvrir le terminal de R et installer les librairies nécessaires aux scripts utilisé dans MASC::

  install.packages("XML", repos = "http://www.omegahat.org/R")
ou
  install.packages("XML")
  install.packages("AlgDesign")

.. _télécharger la dernière version:: https://mulcyber.toulouse.inra.fr/frs/?group_id=160